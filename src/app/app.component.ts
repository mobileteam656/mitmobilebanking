import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { AlertController, App, Events, LoadingController, MenuController, Nav, Platform, ToastController, ViewController, NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AccountTransation } from '../pages/account-transation/account-transation';
import { AtmLocation } from '../pages/atm-location/atm-location';
import { BeneficiaryListPage } from '../pages/beneficiary-list/beneficiary-list';
import { Changefont } from '../pages/changefont/changeFont';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { ChequeStatusPage } from '../pages/cheque-status/cheque-status';
import { ChequeVarificationPage } from '../pages/cheque-varification/cheque-varification';
import { ChequelistPage } from '../pages/chequelist/chequelist';
import { ContactUs } from '../pages/contact-us/contact-us';
import { ExchangePage } from '../pages/exchange/exchange';
import { FastTransferPage } from '../pages/fast-transfer/fast-transfer';
import { HomePage } from '../pages/home/home';
import { InternalTransferPage } from '../pages/internal-transfer/internal-transfer';
import { Login } from '../pages/login/login';
import { Logout } from '../pages/logout/logout';
import { MainHomePage } from '../pages/main-home/main-home';
import { MerchantlistPage } from '../pages/merchantlist/merchantlist';
import { MobileTopupPage } from '../pages/mobile-topup/mobile-topup';
import { OwnTransferPage } from '../pages/own-transfer/own-transfer';
import { TopUpListPage } from '../pages/top-up-list/top-up-list';
import { TopUp } from '../pages/top-up/top-up';
import { WalletTopupPage } from '../pages/wallet-topup/wallet-topup';
import { GlobalProvider } from '../providers/global/global';
import { ProductPage } from '../pages/product/product';
import { CalculatorPage } from '../pages/calculator/calculator';
import { FCM } from '@ionic-native/fcm';
import { NotificationPage } from '../pages/notification/notification';
import { Observable } from 'rxjs';
import { Badge } from '@ionic-native/badge';
import { ChequeInquiryInternalPage } from '../pages/cheque-inquiry-internal/cheque-inquiry-internal';
import { QPayPage } from '../pages/q-pay/q-pay';
import { QuickpayPage } from '../pages/quickpay/quickpay';

export const myConst = {
  bankingApp: {
    ios: {
      storeUrl: 'itms-apps://itunes.apple.com/us/app/nsb-mobile-banking/id1439309200',
      appId: 'NSB mBanking://'
      //https://apps.apple.com/us/app/nsb-mobile-banking/id1439309200
    },
    android: {
      storeUrl: 'https://play.google.com/store/apps/details?id=com.mit.mbanking',
      appId: 'mBanking'
    }
  }
}

declare var cordova: any;
declare var window;

@Component({
  templateUrl: 'app.html',
  providers: [Changefont]
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  hideSidemenuBackButton: boolean = false;
  subMenu: any = '';
  showExitbtn: boolean = false;
  rootPage: any;
  flag: any;
  pages: any = [{ name: '', icon: '', data: [] }];
  userphone: any = '';
  loginMenu: any = [{
    "name": "", icon: '', "data": [
      { title: 'Login', component: Login, icon: 'ios-log-in', 'ststus': 0, taxons: [], "status": 1 },
      { title: 'Location', component: AtmLocation, icon: 'ios-navigate-outline', 'ststus': 0, taxons: [], "status": 0 },
      { title: 'Exchange Rate', component: ExchangePage, icon: 'ios-sync-outline', 'ststus': 0, taxons: [], "status": 0 },
      { title: 'Contact Us', component: ContactUs, icon: 'ios-contactus', 'ststus': 0, taxons: [], "status": 0 },
      { title: 'Cheque Inquiry', component: ChequeStatusPage, icon: 'ios-clipboard-outline', 'ststus': 0, taxons: [], "status": 0 },
      { title: 'Calculator', component: CalculatorPage, icon: 'ios-app', 'ststus': 0, taxons: [], "status": 0 },
      { title: 'Product', component: ProductPage, icon: 'ios-podium-outline', 'ststus': 0, taxons: [], "status": 0 },
    ]
  }];
  mainMenu: any = [{
    "name": "", "data": [
      { title: 'Home', component: MainHomePage, icon: 'ios-home-outline', 'ststus': 1, taxons: [] },
      { title: 'Account Summary', component: HomePage, icon: 'ios-accsummary', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Transaction History', component: AccountTransation, icon: 'ios-transhistory', 'ststus': 1, taxons: [], "status": 0 },
      {
        title: 'Transfer', component: "", icon: 'ios-transfer', taxons: [
          { title: 'Own Transfer', component: OwnTransferPage, icon: 'ios-owntransfer', is_first_level: false },
          { title: 'Internal Transfer', component: InternalTransferPage, icon: 'contacts', is_first_level: false },
          { title: 'Fast Transfer', component: FastTransferPage, icon: 'ios-ftransfer', is_first_level: false },
          { title: 'Beneficiary Maintenance', component: BeneficiaryListPage, icon: 'ios-person-add-outline', taxons: [], 'status': 1, }]
      },
      { title: 'Mobile Top Up', component: MobileTopupPage, icon: 'ios-mobiletopup', 'ststus': 1, taxons: [] },
      { title: 'Payment', component: TopUpListPage, icon: 'ios-bill', 'ststus': 1, taxons: [], 'type': 1 },
      { title: 'Merchant Pay', component: QPayPage, icon: 'ios-card', 'status': 0, taxons: [] },
      //{ title: 'Q-Pay', component: QuickpayPage, icon: 'ios-bill', 'ststus': 1, taxons: [] },
      { title: 'Wallet Top Up', component: WalletTopupPage, icon: 'ios-wallettopup', 'ststus': 1, taxons: [] },
      { title: 'Services', component: ChequelistPage, icon: 'ios-card-outline', 'ststus': 1, taxons: [] },
      { title: 'Cheque Verification', component: ChequeVarificationPage, icon: 'ios-cash-outline', 'ststus': 1, taxons: [] },
      { title: 'Cheque Inquiry', component: ChequeInquiryInternalPage, icon: 'ios-clipboard-outline', 'ststus': 1, taxons: [] },
      { title: 'Change Password', component: ChangepasswordPage, icon: 'ios-changepassword', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Location', component: AtmLocation, icon: 'ios-location', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Exchange Rate', component: ExchangePage, icon: 'ios-currencyexchange', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Contact Us', component: ContactUs, icon: 'ios-contactus', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Calculator', component: CalculatorPage, icon: 'ios-app', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Product', component: ProductPage, icon: 'ios-podium-outline', 'ststus': 1, taxons: [], "status": 0 },
      { title: 'Logout', component: '', icon: 'ios-log-out', 'ios-logout': 1, taxons: [] }
    ]
  }];
  firstpages: any;
  mainpages: any;
  userdata: any;
  loading: any;
  lastTimeLoginSuccess: any = '';
  ipaddress: string;
  font: string;
  userID: any;
  userName: any;
  backend: any;
  textMyanfirst: any;
  textMyanmain: any; mai //"ကုန်ပစ္စည်း", 'ချက် ငွေလွှဲရန်',  ,  "ချက်နံပါတ် စစ်ဆေးရန်" , "ချက်နံပါတ် စစ်ဆေးရန်"
  texSubMyan: any = ["ကိုယ်ပိုင်ဘဏ်စာရင်းသို့ ငွေလွှဲရန်", "ဘဏ်စာရင်းအချင်းချင်း ငွေလွှဲရန်", "အမြန်ငွေလွှဲရန်", "ငွေလက်ခံသူ", "ငွေပေးချေမှု", "အခွန်ပေးဆောင်ခြင်း",];//"ငွေသားပို့ရန်", 'ချက် ငွေလွှဲရန်', "ချက်နံပါတ် စစ်ဆေးရန်", 
  textMyan: any = [
    "ဝင်မည်", "တည်နေရာ",
    "ငွေလဲနှုန်း", "ဆက်သွယ်ရန်",
    "ချက်နံပါတ် စစ်ဆေးရန်", "အတိုးဂဏန်းတွက်စက်",
    "ထုတ်ကုန်များ"
  ];
  textMyanMain: any = [
    "ပင်မစာမျက်နှာ", "စာရင်းချုပ်",
    "စာရင်းအသေးစိတ်", "ငွေလွှဲပြောင်းခြင်း",
    "ဖုန်းငွေဖြည့်သွင်းခြင်း", "ငွေပေးချေမှု","အမြန်ငွေပေးချေမှု",
    "ဝေါ(လ်)လက် ငွေဖြည့်သွင်းခြင်း", "ဝန်ဆောင်မှုများ",
    'ချက် ငွေလွှဲရန်', "ချက်နံပါတ် စစ်ဆေးရန်",
    'လျှို့ဝှက်နံပါတ်ပြောင်းရန်', "တည်နေရာ",
    "ငွေလဲနှုန်း", "ဆက်သွယ်ရန်",
    "အတိုးဂဏန်းတွက်စက်", "ထုတ်ကုန်များ",
    "ထွက်မည်"
  ]; //, "ငွေဖြည့်သွင်းခြင်း"

  shownGroup: any = null;
  //notidata:Observable<any>;
  notidata: any;
  showtext: any = 0;
  noticount: any = 0;

  constructor(
    public platform: Platform, public statusBar: StatusBar,
    public splashScreen: SplashScreen, public menuCtrl: MenuController,
    public events: Events, public storage: Storage,
    public http: Http, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, public changefont: Changefont,
    public global: GlobalProvider, public alertCtrl: AlertController,
    public fcm: FCM, public badge: Badge,
    public app: App,
    //private localNotifications: LocalNotifications//public navCtrl: NavController
  ) {
    this.initializeApp();
    this.mainpages = this.mainMenu;
    this.firstpages = this.loginMenu;
    this.textMyanfirst = this.textMyan;
    this.textMyanmain = this.textMyanMain;
    this.rootPage = Login;
    this.storage.get('ipaddress').then((ipaddress) => {
      if (ipaddress == '' || ipaddress == null) {
        this.storage.set('ipaddress', global.ipaddress);
        this.ipaddress = global.ipaddress;
      } else {
        this.ipaddress = ipaddress;
      }
    });
    this.events.subscribe('login_success', res => {
      this.storage.get('language').then((font) => {
        if (res) {
          this.showExitbtn = true;
          this.storage.get('userData').then((data) => {
            if (data != null || data != "") {
              this.userID = data.userID;
              this.userName = data.userName;
            }
            else {
              this.userID = '';
              this.userName = '';
            }
          });
          this.changelanguage(font, 1);
          this.flag = true;
        }
        else {
          this.showExitbtn = false;
          this.hideSidemenuBackButton = false;
          this.changelanguage(font, 0);
          this.flag = false;
        }
      });
    })
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data, lan.status);
    })
    this.events.subscribe('lastTimeLoginSuccess', res => {
      if (res != '')
        this.lastTimeLoginSuccess = "Last Time Log In : " + res;
      else {
        this.lastTimeLoginSuccess = "";
        this.hideSidemenuBackButton = false;
      }
    })
    platform.registerBackButtonAction(() => {
      let nav = app.getActiveNav();
      let activeView: ViewController = nav.getActive();
      if (activeView != null) {
        if (activeView.component.name == 'ConfirmotpPage') {
          this.nav.setRoot(MerchantlistPage);
        }
        else if (activeView.component.name == 'TopupotpPage') {
          this.nav.setRoot(TopUp);
        }
        else if (activeView.component.name == 'Merchantsuccess') {
          this.nav.setRoot(MerchantlistPage);

        } else if (activeView.component.name == 'TopUpSuccessPage') {
          this.nav.setRoot(TopUp);
        }
        else if (activeView.component.name == 'MerchantErrorPage') {
          this.nav.setRoot(MerchantlistPage);
        }
        else if (activeView.component.name == 'QrPage') {
          this.nav.setRoot(MerchantlistPage);
        }
        else if (activeView.component.name == 'ChequestopsuccessPage') {
          this.nav.setRoot(ChequelistPage);
        }
        else if (activeView.component.name == 'TopuperrorPage') {
          this.nav.setRoot(TopUp);
        }
        else if (nav.canGoBack()) {
          nav.pop();
        } else if (typeof activeView.instance.backButtonAction === 'function') {
          activeView.instance.backButtonAction();
        }
        else {
          this.platform.exitApp();
          //  nav.parent.select(0); // goes to the first tab
        }
      }
    });

  }
  showSubcategories(a) {
    if (a.taxons.length > 0) {
      this.pages = a.taxons;
      this.hideSidemenuBackButton = true;
      this.subMenu = a.title;
    }
    else {
      this.menuCtrl.close();
      this.openPage(a);
    }
  }

  showTopLevelCategories() {
    this.pages = this.mainpages;
    this.hideSidemenuBackButton = false;
  }

  readVersionMobile() {
    let parameter = {
      version: this.global.version,
      appCode: this.global.appCode,
      t1: "",
      n1: 0
    };
    //console.log("Version parameter" +JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/service001/getVersion', parameter).map(res => res.json()).subscribe(result => {
      //console.log("Version result " +JSON.stringify(result));
      if (result.code == "0000") {
        if (result.versionStatus == '1') {
          let confirm = this.alertCtrl.create({
            title: result.versionTitle,
            message: result.versionDesc,
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'CANCEL',
                handler: () => {

                }
              },
              {
                text: 'UPDATE',
                handler: () => {
                  if (this.platform.is('ios')) {
                    window.open(myConst.bankingApp.ios.storeUrl, '_system');
                  } else {
                    window.open(myConst.bankingApp.android.storeUrl, '_system');
                  }
                }
              }
            ]
          });
          confirm.present();
        }
        else if (result.versionStatus == '999') {
          let confirm = this.alertCtrl.create({
            title: result.versionTitle,
            message: result.versionDesc,
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'UPDATE',
                handler: () => {
                  if (this.platform.is('ios')) {
                    window.open(myConst.bankingApp.ios.storeUrl, '_system');
                  } else {
                    window.open(myConst.bankingApp.android.storeUrl, '_system');
                  }
                  this.platform.exitApp();
                }
              }
            ]
          });
          confirm.present();
        }
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 5000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);

      });
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.fcm.subscribeToTopic('nsbmBankingTQM');

      // this.fcm.subscribeToTopic('hello').then(
      //   success => {
      //     console.log("Here is success*" + success);
      //     alert(success)
      //   },
      //   error => {
      //     console.log("This is the issue*" + error);
      //     alert(error);
      //   }
      // ).catch(response => {
      //   alert(response);
      //   console.log('Gists error = ', response);
      // });

      // this.fcm.getToken().then(token => {
      //  this.backend.registerToken(token);
      //  console.log("token="+token);
      //});

      //this.fcm.onTokenRefresh().subscribe(token => {
      //  this.backend.registerToken(token);
      //});

      //this.fcm.unsubscribeFromTopic('hello');

      this.fcm.onNotification().subscribe(data => {
        // this.localNotifications.schedule([{
        //   id: 1,
        //   title: data.title,
        //   text: data.body,
        //  }]);
        // alert("data=" + data);

        if (data.wasTapped) {
          // console.log("Received in background"+ data);

          // this.localNotifications.schedule([{
          //   id: 1,
          //   title: data.title,
          //   text: data.body,
          //  }]);

          this.events.publish('user:created');
        } else {
          // console.log("Received in foreground"+ data);

          // this.localNotifications.schedule([{
          //   id: 1,
          //   title: data.title,
          //   text: data.body,
          //  }]);

          this.events.publish('user:created');
        };
      });

      // this.fcm.onTokenRefresh().subscribe(token => {
      //   this.backend.registerToken(token);
      // });
      // this.fcm.unsubscribeFromTopic('marketing');     

      this.storage.remove("ipaddress");
      this.storage.get('language').then((font) => {
        if (font == null || font == "") {
          this.changelanguage(font, 0);
        }
        else {
          this.changelanguage(font, 0);
        }
      });
      this.storage.get('ipaddress').then((ipaddress) => {
        if (ipaddress == '' || ipaddress == null) {
          this.storage.set('ipaddress', this.global.ipaddress);
          this.ipaddress = this.global.ipaddress;
        } else {
          this.ipaddress = ipaddress;
          //this.getNotiCount();
        }
        this.readVersionMobile();
      });
      this.pages = this.loginMenu;
      this.mainpages = this.mainMenu;
      this.firstpages = this.loginMenu;
      this.textMyanfirst = this.textMyan;
      this.textMyanmain = this.textMyanMain;
      this.flag = false;
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      /* const permissions = cordova.plugins.permissions;
      permissions.hasPermission(permissions.READ_SMS, checkPermissionCallback, null);
      function checkPermissionCallback(status) {
        // console.log('no sms', JSON.stringify(status));
        if (!status.hasPermission) {
          // console.log('sms=', JSON.stringify(status.hasPermission));
          var errorCallback = function () {
            // console.log('no sms permisions');
            this.Platform.exitApp();
          }
          permissions.requestPermission(permissions.READ_SMS, function (status) {
            // console.log('request sms permisions=', JSON.stringify(status));
            if (!status.hasPermission) errorCallback();
          }, errorCallback);
        }
      } */
      const permissions1 = cordova.plugins.permissions;
      permissions1.hasPermission(permissions1.CAMERA, checkPermissionCallback1, null);
      function checkPermissionCallback1(status) {
        // console.log('no sms', JSON.stringify(status));
        if (!status.hasPermission) {
          // console.log('sms=', JSON.stringify(status.hasPermission));
          var errorCallback = function () {
            // console.log('no sms permisions');
            this.Platform.exitApp();
          }
          permissions1.requestPermission(permissions1.CAMERA, function (status) {
            // console.log('request sms permisions=', JSON.stringify(status));
            if (!status.hasPermission) errorCallback();
          }, errorCallback);
        }
      }

      const permissions2 = cordova.plugins.permissions;
      permissions2.hasPermission(permissions2.GET_ACCOUNTS, checkPermissionCallback2, null);
      function checkPermissionCallback2(status) {
        // console.log('no sms', JSON.stringify(status));
        if (!status.hasPermission) {
          // console.log('sms=', JSON.stringify(status.hasPermission));
          var errorCallback = function () {
            // console.log('no sms permisions');
            this.Platform.exitApp();
          }
          permissions2.requestPermission(permissions2.GET_ACCOUNTS, function (status) {
            // console.log('request sms permisions=', JSON.stringify(status));
            if (!status.hasPermission) errorCallback();
          }, errorCallback);
        }
      }

      const permissions3 = cordova.plugins.permissions;
      permissions3.hasPermission(permissions3.READ_CONTACTS, checkPermissionCallback3, null);
      function checkPermissionCallback3(status) {
        // console.log('no sms', JSON.stringify(status));
        if (!status.hasPermission) {
          // console.log('sms=', JSON.stringify(status.hasPermission));
          var errorCallback = function () {
            // console.log('no sms permisions');
            this.Platform.exitApp();
          }
          permissions3.requestPermission(permissions3.READ_CONTACTS, function (status) {
            // console.log('request sms permisions=', JSON.stringify(status));
            if (!status.hasPermission) errorCallback();
          }, errorCallback);
        }
      }
    });

    // this.platform.resume.subscribe(() => {
    //   //this.getNotiCount();
    //   this.getNoti();
    //   //this.getNotiResume();
    // });

  }

  changelanguage(lan, s) {
    let myamarText = [];
    let subMyan = [];
    if (s == 0) {
      this.pages = this.firstpages;
      myamarText = this.textMyanfirst;
    }
    else if (s == 1) {
      this.pages = this.mainpages;
      myamarText = this.textMyanmain;
      subMyan = this.texSubMyan;
    }
    if (lan == 'uni') {
      this.font = "uni";
      this.storage.set('language', "uni");
      let count = 0;
      let innercount = 0;
      this.hideSidemenuBackButton = false;
      for (let i = 0; i < this.pages.length; i++) {
        for (let j = 0; j < this.pages[i].data.length; j++) {
          this.pages[i].data[j].title = myamarText[count];
          count++;
          if (this.pages[i].data[j].taxons.length > 0) {
            for (let k = 0; k < this.pages[i].data[j].taxons.length; k++) {
              this.pages[i].data[j].taxons[k].title = subMyan[innercount];
              innercount++;
            }
          }
        }
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      this.storage.set('language', "zg");
      let count = 0;
      let innercount = 0;
      for (let i = 0; i < this.pages.length; i++) {
        for (let j = 0; j < this.pages[i].data.length; j++) {
          this.pages[i].data[j].title = this.changefont.UnitoZg(myamarText[count]);
          count++;
          if (this.pages[i].data[j].taxons.length > 0) {
            for (let k = 0; k < this.pages[i].data[j].taxons.length; k++) {
              this.pages[i].data[j].taxons[k].title = this.changefont.UnitoZg(subMyan[innercount]);
              innercount++;
            }
          }
        }
      }
    }
    else {
      this.font = "";
      this.storage.set('language', "eng");
      if (s == 0) {
        this.pages = [{
          "name": "", icon: '', "data": [
            { title: 'Login', component: Login, icon: 'ios-log-in', 'ststus': 0, taxons: [], "status": 1 },
            { title: 'Location', component: AtmLocation, icon: 'ios-location', 'ststus': 0, taxons: [], "status": 0 },
            { title: 'Exchange Rate', component: ExchangePage, icon: 'ios-currencyexchange', 'ststus': 0, taxons: [], "status": 0 },
            { title: 'Contact Us', component: ContactUs, icon: 'ios-contactus', 'ststus': 0, taxons: [], "status": 0 },
            { title: 'Cheque Inquiry', component: ChequeStatusPage, icon: 'ios-clipboard-outline', 'ststus': 0, taxons: [], "status": 0 },
            { title: 'Interest Calculator', component: CalculatorPage, icon: 'ios-app', 'ststus': 0, taxons: [], "status": 0 },
            { title: 'Product', component: ProductPage, icon: 'ios-podium-outline', 'ststus': 0, taxons: [], "status": 0 },
          ]
        }];
      }
      else if (s == 1) {
        this.hideSidemenuBackButton = false;
        this.pages = [{
          "name": "", "data": [
            { title: 'Home', component: MainHomePage, icon: 'ios-home-outline', 'ststus': 1, taxons: [] },
            { title: 'Account Summary', component: HomePage, icon: 'ios-accsummary', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Transaction History', component: AccountTransation, icon: 'ios-transhistory', 'ststus': 1, taxons: [], "status": 0 },
            {
              title: 'Transfer', component: "", icon: 'ios-transfer', taxons: [
                { title: 'Own Transfer', component: OwnTransferPage, icon: 'ios-owntransfer', is_first_level: false },
                { title: 'Internal Transfer', component: InternalTransferPage, icon: 'ios-itransfer', is_first_level: false },
                { title: 'Fast Transfer', component: FastTransferPage, icon: 'ios-ftransfer', is_first_level: false },
                { title: 'Beneficiary Maintenance', component: BeneficiaryListPage, icon: 'ios-person-add-outline', 'status': 1, taxons: [] },]
            },
            { title: 'Mobile Top Up', component: MobileTopupPage, icon: 'ios-mobiletopup', 'ststus': 1, taxons: [] },
            //{ title: 'Top Up', component: TopUpListPage, icon: 'phone-portrait', 'ststus': 1, taxons: [], 'type': 2 },
            { title: 'Payment', component: TopUpListPage, icon: 'ios-bill', 'ststus': 1, taxons: [], 'type': 1 },
            /*  {
               title: 'Payment', component: "", icon: 'ios-albums-outline', taxons: [
                 { title: 'Utility Payment', component: QrUtilityPage, icon: 'ios-albums-outline', 'ststus': 1, taxons: [] },
                 { title: 'Tax Payment', component: TaxInfoPage, icon: 'clipboard', 'ststus': 1, taxons: [] },
               ]
             }, */
            { title: 'Merchant-Pay', component: QPayPage, icon: 'ios-card', 'status': 0, taxons: [] },
            //{ title: 'Q-Pay', component: QuickpayPage, icon: 'ios-bill', 'ststus': 1, taxons: [] },
            { title: 'Wallet Top Up', component: WalletTopupPage, icon: 'ios-wallettopup', 'ststus': 1, taxons: [] },
            { title: 'Services', component: ChequelistPage, icon: 'ios-card-outline', 'ststus': 1, taxons: [] },
            { title: 'Cheque Verification', component: ChequeVarificationPage, icon: 'ios-cash-outline', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Cheque Inquiry', component: ChequeInquiryInternalPage, icon: 'ios-clipboard-outline', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Change Password', component: ChangepasswordPage, icon: 'ios-changepassword', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Location', component: AtmLocation, icon: 'ios-location', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Exchange Rate', component: ExchangePage, icon: 'ios-currencyexchange', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Contact Us', component: ContactUs, icon: 'ios-contactus', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Interest Calculator', component: CalculatorPage, icon: 'ios-app', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Product', component: ProductPage, icon: 'ios-podium-outline', 'ststus': 1, taxons: [], "status": 0 },
            { title: 'Logout', component: '', icon: 'ios-logout', 'ststus': 1, taxons: [] }
          ]
        }];
        this.mainpages = this.pages;
      }
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // this.showTopLevelCategories();
    if (page.component != '' && page.component != 'call') {
      if (page.status == '0') {
        this.nav.push(page.component, {
          ststus: page.ststus
        });
      } else if (page.type == '1') {//payment
        this.nav.setRoot(TopUpListPage, {
          data: page.type
        });
      } else if (page.type == '2') {//topup
        this.nav.setRoot(TopUpListPage, {
          data: page.type
        });
      } else {
        this.nav.setRoot(page.component, {
          ststus: page.ststus
        });
      }
    } else if (page.component == 'call') {
      this.global.getphone().then(phone => {
        this.callIT(phone);
      })
    }
    else {
      this.gologout();
    }
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Logging out...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      this.storage.get('userData').then((data) => {
        this.userdata = data;
        let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
        this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            this.loading.dismiss();
            this.storage.remove('userData');
            this.events.publish('lastTimeLoginSuccess', '');
            this.events.publish('login_success', false);
            this.nav.setRoot(Logout, {
              data: data
            });
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
            let code;
            if (error.status == 404) {
              code = '001';
            }
            else if (error.status == 500) {
              code = '002';
            }
            else if (error.status == 403) {
              code = '003';
            }
            else if (error.status == -1) {
              code = '004';
            }
            else if (error.status == 0) {
              code = '005';
            }
            else if (error.status == 502) {
              code = '006';
            }
            else {
              code = '000';
            }
            let msg = "Can't connect right now. [" + code + "]";
            let toast = this.toastCtrl.create({
              message: msg,
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      });
    });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  // getNotiCount(){
  //   let param = { userID: '', sessionID: '' };
  //   this.http.post(this.ipaddress + '/service006/getNotiCount', param).map(res => res.json()).subscribe(result => {

  //    console.log("noticount",JSON.stringify(result));
  //    //this.global.noticount = result.noticount;
  //   // console.log("noticount",JSON.stringify(this.global.noticount));
  //     // if (result.code == "0000") {
  //     //   let tempArray = [];
  //     //   if (!Array.isArray(result.data)) {
  //     //     tempArray.push(result.data);
  //     //     result.data = tempArray;
  //     //   }
  //     //   this.notiData = result.data;
  //     //   console.log("notdata>>", JSON.stringify(this.notiData));


  //     //   this.loading.dismiss();

  //     // }
  //     // else if (result.code == "0016") {
  //     //   console.log("No Data")
  //     // }
  //     // else {
  //     //   this.all.showAlert('Warning!', result.desc);
  //     //   this.loading.dismiss();
  //     // }
  //   },
  //     error => {
  //      // this.all.showAlert('Warning!', this.all.getErrorMessage(error));
  //       //this.loading.dismiss();
  //     });
  // }
}
