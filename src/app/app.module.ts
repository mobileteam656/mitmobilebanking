//import 'intl';
//import 'intl/locale-data/jsonp/en';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { Badge } from '@ionic-native/badge';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Contacts } from '@ionic-native/contacts';
import { Device } from '@ionic-native/device';
import { FCM } from '@ionic-native/fcm';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
//import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//import { SMS } from '@ionic-native/sms';
import { IonicStorageModule } from '@ionic/storage';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { QRCodeModule } from 'angular2-qrcode';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AboutPage } from '../pages/about/about';
import { AccountSummaryDetail } from '../pages/account-summary-detail/account-summary-detail';
import { AccountTransactionDetail } from '../pages/account-transaction-detail/account-transaction-detail';
import { AccountTransation } from '../pages/account-transation/account-transation';
import { AccountTransferConfirmPage } from '../pages/account-transfer-confirm/account-transfer-confirm';
import { AccountTransferDetail } from '../pages/account-transfer-detail/account-transfer-detail';
import { AccountTransferOtpPage } from '../pages/account-transfer-otp/account-transfer-otp';
import { AtmLocation } from '../pages/atm-location/atm-location';
import { BeneficiaryAddPage } from '../pages/beneficiary-add/beneficiary-add';
import { BeneficiaryConfirmPage } from '../pages/beneficiary-confirm/beneficiary-confirm';
import { BeneficiaryDeleteSuccessPage } from '../pages/beneficiary-delete-success/beneficiary-delete-success';
import { BeneficiaryEditPage } from '../pages/beneficiary-edit/beneficiary-edit';
import { BeneficiaryFailPage } from '../pages/beneficiary-fail/beneficiary-fail';
import { BeneficiaryInfoPage } from '../pages/beneficiary-info/beneficiary-info';
import { BeneficiaryListPage } from '../pages/beneficiary-list/beneficiary-list';
import { BeneficiaryOtpPage } from '../pages/beneficiary-otp/beneficiary-otp';
import { BeneficiarySuccessPage } from '../pages/beneficiary-success/beneficiary-success';
import { CalculatorDetailPage } from '../pages/calculator-detail/calculator-detail';
import { CalculatorPage } from '../pages/calculator/calculator';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { ChangepasswordotpPage } from '../pages/changepasswordotp/changepasswordotp';
import { ChequeStatusPage } from '../pages/cheque-status/cheque-status';
import { ChequeStopOtpPage } from '../pages/cheque-stop-otp/cheque-stop-otp';
import { ChequeVarificationPage } from '../pages/cheque-varification/cheque-varification';
import { ChequePage } from '../pages/cheque/cheque';
import { ChequelistPage } from '../pages/chequelist/chequelist';
import { ChequestopPage } from '../pages/chequestop/chequestop';
import { ChequestopconfirmPage } from '../pages/chequestopconfirm/chequestopconfirm';
import { ChequestopsuccessPage } from '../pages/chequestopsuccess/chequestopsuccess';
import { ChequesuccessPage } from '../pages/chequesuccess/chequesuccess';
import { ChoosepackagePage } from '../pages/choosepackage/choosepackage';
//import { QRCodeComponent } from 'ng2-qrcode';
import { ChoosetopupPage } from '../pages/choosetopup/choosetopup';
import { CloudPage } from '../pages/cloud/cloud';
import { ConfirmotpPage } from '../pages/confirmotp/confirmotp';
import { ConfirmpagePage } from '../pages/confirmpage/confirmpage';
import { ContactUs } from '../pages/contact-us/contact-us';
import { CorporateCheckerPage } from '../pages/corporate-checker/corporate-checker';
import { CorporateDetailPage } from '../pages/corporate-detail/corporate-detail';
import { CorporateMakerComfirmPage } from '../pages/corporate-maker-comfirm/corporate-maker-comfirm';
import { CorporateMakerDetailPage } from '../pages/corporate-maker-detail/corporate-maker-detail';
import { CorporateMakerPage } from '../pages/corporate-maker/corporate-maker';
import { CorporateTypePage } from '../pages/corporate-type/corporate-type';
import { DefaultPopOver } from '../pages/default-pop-over/default-pop-over';
import { DemoPage } from '../pages/demo/demo';
import { DetailPage } from '../pages/detail/detail';
import { DomainPage } from '../pages/domain/domain';
import { ExchangePage } from '../pages/exchange/exchange';
import { FastTransferPage } from '../pages/fast-transfer/fast-transfer';
import { FirstPage } from '../pages/first-page/first-page';
import { ForcechangePage } from '../pages/forcechange/forcechange';
import { HomePage } from '../pages/home/home';
import { InternalTransferPage } from '../pages/internal-transfer/internal-transfer';
import { IpchangePage } from '../pages/ipchange/ipchange';
import { LanguagePopOver } from '../pages/language-pop-over/language-pop-over';
import { Language } from '../pages/language/language';
import { ListPage } from '../pages/list/list';
import { LocationDetail } from '../pages/location-detail/location-detail';
import { LoginOtpPage } from '../pages/login-otp/login-otp';
import { Login } from '../pages/login/login';
import { Logout } from '../pages/logout/logout';
import { LovbyPage } from '../pages/lovbypage/lovbypage';
import { MainHomePage } from '../pages/main-home/main-home';
import { MerchantErrorPage } from '../pages/merchant-error-page/merchant-error-page';
import { MerchantlistPage } from '../pages/merchantlist/merchantlist';
import { Merchantsuccess } from '../pages/merchantsuccess/merchantsuccess';
import { MobileTopupPage } from '../pages/mobile-topup/mobile-topup';
import { MptTopupConfirmPage } from '../pages/mpt-topup-confirm/mpt-topup-confirm';
import { MptTopupFinishPage } from '../pages/mpt-topup-finish/mpt-topup-finish';
import { MptTopupOtpPage } from '../pages/mpt-topup-otp/mpt-topup-otp';
import { MptTopupPage } from '../pages/mpt-topup/mpt-topup';
import { MyPopOverPage } from '../pages/my-pop-over-page/my-pop-over-page';
import { NotificationPage } from '../pages/notification/notification';
import { OwnTransferPage } from '../pages/own-transfer/own-transfer';
import { PopoverPage } from '../pages/popover-page/popover-page';
import { ProductDetailPage } from '../pages/product-detail/product-detail';
import { ProductImgViewPage } from '../pages/product-img-view/product-img-view';
import { ProductPage } from '../pages/product/product';
import { QrUtilitySuccessPage } from '../pages/qr-utility-success/qr-utility-success';
import { QrUtilityPage } from '../pages/qr-utility/qr-utility';
import { QuickpayConfirmPage } from '../pages/quickpay-confirm/quickpay-confirm';
import { QuickpaySuccessPage } from '../pages/quickpay-success/quickpay-success';
import { QuickpayPage } from '../pages/quickpay/quickpay';
import { SkynetConfirmPage } from '../pages/skynet-confirm/skynet-confirm';
import { SkynetOtpPage } from '../pages/skynet-otp/skynet-otp';
import { SkynetSuccessPage } from '../pages/skynet-success/skynet-success';
import { SkynetTopupPage } from '../pages/skynet-topup/skynet-topup';
import { SkynetPage } from '../pages/skynet/skynet';
import { TaxBusinessProfilePage } from '../pages/tax-business-profile/tax-business-profile';
import { TaxInfoPage } from '../pages/tax-info/tax-info';
import { TaxInformationDetailPage } from '../pages/tax-information-detail/tax-information-detail';
import { TaxPaymentConfirmPage } from '../pages/tax-payment-confirm/tax-payment-confirm';
import { TaxPaymentFinishPage } from '../pages/tax-payment-finish/tax-payment-finish';
import { TaxPaymentOtpPage } from '../pages/tax-payment-otp/tax-payment-otp';
import { TaxPaymentPage } from '../pages/tax-payment/tax-payment';
import { TopUpForm } from '../pages/top-up-form/top-up-form';
import { TopUpListPage } from '../pages/top-up-list/top-up-list';
import { TopUpSuccessPage } from '../pages/top-up-success/top-up-success';
import { TopUp } from '../pages/top-up/top-up';
import { TopupconfirmPage } from '../pages/topupconfirm/topupconfirm';
import { TopuperrorPage } from '../pages/topuperror/topuperror';
import { TopuplistPage } from '../pages/topuplist/topuplist';
import { TopupotpPage } from '../pages/topupotp/topupotp';
import { TransactionHistoryDetail } from '../pages/transaction-history-detail/transaction-history-detail';
import { TransactionHistoryPage } from '../pages/transaction-history-page/transaction-history-page';
import { TransationHistory } from '../pages/transation-history/transation-history';
import { UtilityConfirmPage } from '../pages/utility-confirm/utility-confirm';
import { UtilityOtpPage } from '../pages/utility-otp/utility-otp';
import { WalletTopupConfirmPage } from '../pages/wallet-topup-confirm/wallet-topup-confirm';
import { WalletTopupOtpPage } from '../pages/wallet-topup-otp/wallet-topup-otp';
import { WalletTopupSuccessPage } from '../pages/wallet-topup-success/wallet-topup-success';
import { WalletTopupPage } from '../pages/wallet-topup/wallet-topup';
import { AllserviceProvider } from '../providers/allservice/allservice';
import { ChangefontProvider } from '../providers/changefont/changefont';
import { ChangelanguageProvider } from '../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../providers/global/global';
import { UtilProvider } from '../providers/util/util';
import { MyApp } from './app.component';
import { SkynetErrorPage } from '../pages/skynet-error/skynet-error';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { EventLoggerProvider } from '../providers/event-logger/event-logger';
import { UrlPage } from '../pages/url/url';
import { ChequeInquiryInternalPage } from '../pages/cheque-inquiry-internal/cheque-inquiry-internal';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';
import { QPayPage } from '../pages/q-pay/q-pay';
import { QPayConfirmPage } from '../pages/q-pay-confirm/q-pay-confirm';
import { QPaySuccessPage } from '../pages/q-pay-success/q-pay-success';
import { QPayInfoPage } from '../pages/q-pay-info/q-pay-info';
import { QPayCagPage } from '../pages/q-pay-cag/q-pay-cag';

@NgModule({
  declarations: [
    MyApp,
    FirstPage,
    HomePage,
    ListPage,
    ContactUs,
    Login,
    Logout,
    TransationHistory,
    TransactionHistoryPage,
    IpchangePage,
    AtmLocation,
    LocationDetail,
    ExchangePage,
    DetailPage,
    MerchantlistPage,
    ChoosepackagePage,
    ChangepasswordPage,
    ForcechangePage,
    ConfirmpagePage,
    ConfirmotpPage,
    Merchantsuccess,
    AccountTransation,
    MyPopOverPage,
    ChangepasswordotpPage,
    Language,
    AccountSummaryDetail,
    TransactionHistoryDetail,
    AccountTransactionDetail,
    PopoverPage,
    DefaultPopOver,
    LanguagePopOver,
    AccountTransferDetail,
    MerchantErrorPage,
    ChequesuccessPage,
    ChequePage,
    ChequestopPage,
    ChequestopconfirmPage,
    ChequestopsuccessPage,
    AccountTransferConfirmPage,
    AccountTransferOtpPage,
    ChequelistPage,
    TopUp,
    TopUpForm,
    TopupconfirmPage,
    TopupotpPage,
    TopuperrorPage,
    TopUpSuccessPage,
    MainHomePage,
    ChequeStopOtpPage,
    CloudPage,
    DomainPage,
    DemoPage,
    AboutPage,
    ChoosetopupPage,
    TopuplistPage,
    BeneficiaryListPage,
    BeneficiaryInfoPage,
    BeneficiaryAddPage,
    BeneficiaryConfirmPage,
    BeneficiaryOtpPage,
    BeneficiarySuccessPage,
    BeneficiaryFailPage,
    BeneficiaryEditPage,
    LovbyPage,
    BeneficiaryDeleteSuccessPage,
    QrUtilityPage,
    QrUtilitySuccessPage,
    FastTransferPage,
    OwnTransferPage,
    InternalTransferPage,
    UtilityConfirmPage,
    UtilityOtpPage,
    WalletTopupPage,
    WalletTopupConfirmPage,
    WalletTopupOtpPage,
    WalletTopupSuccessPage,
    LoginOtpPage,
    MptTopupPage,
    MptTopupConfirmPage,
    MptTopupFinishPage,
    MptTopupOtpPage,
    TaxPaymentPage,
    TaxPaymentConfirmPage,
    TaxPaymentFinishPage,
    TaxPaymentOtpPage,
    TaxInfoPage,
    TopUpListPage,
    MobileTopupPage,
    TaxBusinessProfilePage,
    TaxInformationDetailPage,
    QuickpayPage,
    QuickpayConfirmPage,
    QuickpaySuccessPage,
    CorporateCheckerPage,
    CorporateDetailPage,
    CorporateTypePage,
    CorporateMakerPage,
    CorporateMakerComfirmPage,
    CorporateMakerDetailPage,
    CorporateDetailPage,
    CalculatorPage,
    CalculatorDetailPage,
    ChequeVarificationPage,
    ProductPage,
    ProductDetailPage,
    ProductImgViewPage,
    ChequeStatusPage,
    NotificationPage,
    SkynetPage,
    SkynetTopupPage,
    SkynetConfirmPage,
    SkynetOtpPage,
    SkynetSuccessPage,
    SkynetErrorPage,
    UrlPage,
    ChequeInquiryInternalPage,
    QPayPage,
    QPayInfoPage,
    QPayConfirmPage,
    QPaySuccessPage,
    QPayCagPage
  ],
  imports: [
    BrowserModule,
    QRCodeModule,
    HttpModule,
    HttpClientModule,
    // NgIdleKeepaliveModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'md-arrow-back',
      iconMode: 'md',
      tabsPlacement: 'bottom',
      platforms: {
        android: {
          tabsPlacement: 'top'
        },
        ios: {
          tabsPlacement: 'top',
          menuType: 'overlay'
        },
        windows:
        {
          tabsPlacement: 'top'
        }
      }
    }),
    SlimLoadingBarModule.forRoot(),
    NgIdleKeepaliveModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FirstPage,
    HomePage,
    ListPage,
    ContactUs,
    Login,
    Logout,
    TransationHistory,
    TransactionHistoryPage,
    IpchangePage,
    AtmLocation,
    LocationDetail,
    ExchangePage,
    DetailPage,
    MerchantlistPage,
    ChoosepackagePage,
    ChangepasswordPage,
    ForcechangePage,
    ConfirmpagePage,
    ConfirmotpPage,
    Merchantsuccess,
    AccountTransation,
    MyPopOverPage,
    ChangepasswordotpPage,
    Language,
    AccountSummaryDetail,
    TransactionHistoryDetail,
    AccountTransactionDetail,
    PopoverPage,
    DefaultPopOver,
    LanguagePopOver,
    AccountTransferDetail,
    MerchantErrorPage,
    ChequesuccessPage,
    ChequePage,
    ChequestopPage,
    ChequestopconfirmPage,
    ChequestopsuccessPage,
    AccountTransferConfirmPage,
    AccountTransferOtpPage,
    ChequelistPage,
    TopUp,
    TopUpForm,
    TopupconfirmPage,
    TopupotpPage,
    TopuperrorPage,
    TopUpSuccessPage,
    MainHomePage,
    ChequeStopOtpPage,
    CloudPage,
    DomainPage,
    DemoPage,
    AboutPage,
    ChoosetopupPage,
    TopuplistPage,
    BeneficiaryListPage,
    BeneficiaryInfoPage,
    BeneficiaryAddPage,
    BeneficiaryConfirmPage,
    BeneficiaryOtpPage,
    BeneficiarySuccessPage,
    BeneficiaryFailPage,
    BeneficiaryEditPage,
    LovbyPage,
    BeneficiaryDeleteSuccessPage,
    QrUtilityPage,
    QrUtilitySuccessPage,
    FastTransferPage,
    OwnTransferPage,
    InternalTransferPage,
    UtilityConfirmPage,
    UtilityOtpPage,
    WalletTopupPage,
    WalletTopupConfirmPage,
    WalletTopupOtpPage,
    WalletTopupSuccessPage,
    LoginOtpPage,
    MptTopupPage,
    MptTopupConfirmPage,
    MptTopupFinishPage,
    MptTopupOtpPage,
    TaxPaymentPage,
    TaxPaymentConfirmPage,
    TaxPaymentFinishPage,
    TaxPaymentOtpPage,
    TaxInfoPage,
    TopUpListPage,
    MobileTopupPage,
    TaxBusinessProfilePage,
    TaxInformationDetailPage,
    QuickpayPage,
    QuickpayConfirmPage,
    QuickpaySuccessPage,
    CorporateCheckerPage,
    CorporateDetailPage,
    CorporateTypePage,
    CorporateMakerPage,
    CorporateMakerComfirmPage,
    CorporateMakerDetailPage,
    CalculatorPage,
    CalculatorDetailPage,
    ChequeVarificationPage,
    ProductPage,
    ProductDetailPage,
    ProductImgViewPage,
    ChequeStatusPage,
    NotificationPage,
    SkynetPage,
    SkynetTopupPage,
    SkynetConfirmPage,
    SkynetOtpPage,
    SkynetSuccessPage,
    SkynetErrorPage,
    UrlPage,
    ChequeInquiryInternalPage,
    QPayPage,
    QPayInfoPage,
    QPayConfirmPage,
    QPaySuccessPage,
    QPayCagPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    Geolocation,
    BarcodeScanner,
    Network,
    //NativeGeocoder,
    LocationAccuracy,
    Device,
    // SMS,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AllserviceProvider,
    ChangelanguageProvider,
    ChangefontProvider,
    GlobalProvider,
    Keyboard,
    Contacts,
    File,
    FileOpener,
    FileTransfer,
    FirebaseAnalytics,
    UtilProvider,
    HttpClientModule,
    FCM,
    Badge,
    EventLoggerProvider,
    FirebaseCrashlytics,
    //Clipboard
  ]
})
export class AppModule { }
