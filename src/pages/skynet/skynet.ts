import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { ConfirmpagePage } from '../confirmpage/confirmpage';
import { Login } from '../login/login';
import { TopUpListPage } from '../top-up-list/top-up-list';

@Component({
  selector: 'page-skynet',
  templateUrl: 'skynet.html',
  providers: [ChangelanguageProvider]
})

export class SkynetPage {

  font: string;
  textMyan: any = [
    "အကောင့်နံပါတ်", "Package အမျိုးအစား", "သက်တမ်း",
    "အခွန်နှုန်း", "ဝန်ဆောင်ခနှုန်း", "ငွေပမာဏ",
    "စုစုပေါင်း ငွေပမာဏ", "QRဖတ်ရန်", "ကဒ်နံပါတ်",
    "ပေးချေမည်", "ရုပ်သံလိုင်း ငွေပေးချေမှု", "ဘဏ်စာရင်းလက်ကျန်ငွေ",
    "စာရင်းအမှတ် ရွေးချယ်ပါ", "Packageအမျိုးအစား ရွေးချယ်ပါ", "သက်တမ်းအမျိုးအစား ရွေးချယ်ပါ",
    "ကတ်နံပါတ် ရိုက်သွင်းပါ",

    "Bill အမျိုးအစားရွေးချယ်ပါ",
    "Biller အမှတ်စဉ်ရိုက်ထည့်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "Operator အမျိုးအစားရွေးချယ်ပါ",
    "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "အသုံးပြုသူအမှတ်စဉ်ရိုက်သွင်းပါ", "ငွေတောင်းခံလွှာနံပါတ်ရိုက်သွင်းပါ",
    "အကြောင်းအရာရွေးချယ်ပါ", "အရေအတွက်ရွေးချယ်ပါ", "ဈေးနှုန်းရိုက်သွင်းပါ",
    "စုစုပေါင်းငွေပမာဏလိုအပ်နေပါသည်", "Biller အမျိုးအစား", "အသုံးပြုသူအမှတ်စဉ်",
    "CNP ဝန်ဆောင်ခ", "ဘဏ်ဝန်ဆောင်ခ", "ရုပ်သံလိုင်းဝန်ဆောင်ခ",
    "အကြောင်းအရာ", "စစ်ဆေးမည်", "ဒဏ်ငွေကြေး",
    "ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ်", "မီတာ နံပါတ်", "မီတာ နံပါတ် ရိုက်သွင်းပါ",
    "ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ် ရွေးချယ်ပါ", "ဒဏ်ငွေကြေး ရိုက်သွင်းပါ",
    "Account Balance", "ဘဏ်ဝန်ဆောင်ခ ရိုက်သွင်းပါ"
  ];
  textEng: any = [
    "Account", "Package", "Month",
    "Commercial Tax", "Service Charges", "Amount",
    "Total Amount", "QR SCAN", "Card Number",
    "SUBMIT", "SkyNet-DTH Recharge", "Account Balance",
    "Choose Account Number.", "Choose Package Type.", "Choose Month.",
    "Enter Card Number.",

    "Choose Biller Type.",
    "Enter Biller Reference No.", "Enter Amount.", "Choose Operator Type.",
    "Enter Phone Number.", "Enter Customer No.", "Enter Invoice No.",
    "Choose Items", "Enter Quantity.", "Enter Price.",
    "Total Amount Required", "Bill Type", "Customer Reference No.",
    "CNP Charges", "Bank Charges", "Merchant Charges",
    "Narrative", "CHECK", "Penalty",
    "Outstanding Bill", "Meter Number", "Enter Meter Number.",
    "Choose Outstanding Bill.", "Enter Penalty.",
    "Account Balance", "Enter Bank Charges."
  ];
  showFont: string[] = [];

  billdata: any = {
    merchanttype: '', processingCode: '',
    merchantid: '', accounttype: '',
    amountTax: '', amountServiceCharges: '',
    amount: '', amountTotal: '',
    cardno: '',
    //=============
    availableamount: '', packagetype: '',
    monthtype: '', peroid: '',
    did: '', pid: '',
    tid: '', accessToken: ''
  };

  useraccount: any;//depositAcc,avlBal,ccy
  msgCnpAcc: any;
  msgCnpamt: any;

  temppackagename: string = '';
  packagetype: any;//id,name,rateList
  msgCnpbill: any;

  tempmonthname: string = '';
  monthtype: any;//did,m,r,pid,amountServiceCharges,amountTotal,amountTax,tid
  msgCnpCusNo: any;

  errormessage: string;
  //=======================
  ipaddress: string;

  // public loading;

  passtemp: any;//merchantName, merchantID, processingCode, logo
  userData: any;
  flag: string;

  merchantList: any;

  paymentType: string = '';

  constructor(
    public navCtrl: NavController, public barcodeScanner: BarcodeScanner,
    public storage: Storage, public http: Http,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public events: Events, public network: Network,
    public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider,
    public global: GlobalProvider, public changefont: Changefont,
    public navParams: NavParams
  ) {
    this.paymentType = '1';

    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = result;
      }

      this.storage.get('userData').then((userData) => {
        this.userData = userData;

        this.getMerchantList();

        this.getAccountSummary();
      });
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "uni";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLeave() {
    //this.slimLoader.reset();
  }

  getMerchantList() {
    //this.slimLoader.start(() => { });

    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      paymentType: this.paymentType
    };
    // console.log("getMerchant request=" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/getMerchant', param).map(res => res.json()).subscribe(
      data => {
        // console.log("getMerchant response=" + JSON.stringify(data));
        if (data.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(data.merchantList)) {
            tempArray.push(data.merchantList);
            data.merchantList = tempArray;
          }

          this.merchantList = data.merchantList;

          for (let i = 0; i < this.merchantList.length; i++) {
            if (this.merchantList[i].processingCode == '080400') {
              this.passtemp = this.merchantList[i];
              //break;
            }
          }

          if (this.passtemp != undefined && this.passtemp != null && this.passtemp != "") {
            this.billdata.merchanttype = this.passtemp.merchantName;
            this.billdata.merchantid = this.passtemp.merchantID;
            this.billdata.processingCode = this.passtemp.processingCode;
            this.billdata.logo = this.passtemp.logo;

            this.storage.get('skyNetCardNo').then((result) => {
              this.billdata.cardno = result;
            });

            this.checkNetwork();

            if (this.flag == "success") {
              this.getPackage();
            } else {
              this.all.showAlert('Warning!', "Check your internet connection!");
            }
          } else {
            //this.slimLoader.complete();
          }
        } else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,

            message: data.desc,
            buttons: [{
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');

                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }]
          })
          confirm.present();

          //this.slimLoader.complete();
        } else {
          this.all.showAlert('Warning!', data.desc);

          //this.slimLoader.complete();
        }
      },
      error => {
        // console.log("getMerchant error=" + JSON.stringify(error));

        this.all.showAlert('Warning!', this.all.getErrorMessage(error));

        //this.slimLoader.complete();
      }
    );
  }

  getPackage() {
    //this.slimLoader.start(() => { });

    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      merchantID: this.billdata.merchantid,
      processingCode: this.billdata.processingCode
    };
    // console.log("getPackages request=" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/getPackages', param).map(res => res.json()).subscribe(
      data => {
        // console.log("getPackages response=" + JSON.stringify(data));
        if (data.code == "0000") {
          this.packagetype = data.result.dataList;
          this.billdata.accessToken = data.result.accessToken;

          //this.slimLoader.complete();
        } else if (data.code == "0016") {
          this.packagetype = [];
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [{
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }]
          })
          confirm.present();

          //this.slimLoader.complete();
        } else {
          this.packagetype = [];

          this.all.showAlert('Warning!', data.desc);

          //this.slimLoader.complete();
        }
      },
      error => {
        // console.log("getPackages error=" + JSON.stringify(error));

        this.all.showAlert('Warning!', this.all.getErrorMessage(error));

        //this.slimLoader.complete();
      }
    );
  }

  /* getAccountSummary() {
    this.slimLoader.start(() => { });

    let parameter = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID
    };
    this.http.post(this.ipaddress + '/service001/getAccountSummary', parameter).map(res => res.json()).subscribe(
      result => {
        // console.log("result:AccountSummary" + JSON.stringify(result));
        if (result.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(result.dataList)) {
            tempArray.push(result.dataList);
            result.dataList = tempArray;
          }
          this.useraccount = [];
          for (let i = 0; i < result.dataList.length; i++) {
            if (result.dataList[i].accType != 'Fixed Deposit Account') {
              this.useraccount.push(result.dataList[i]);
            }
          }
          //  this.loading.dismiss();
          this.slimLoader.complete();
        } else if (result.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: result.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {

                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ]
          })
          confirm.present();
          this.slimLoader.complete();
        } else {
          this.all.showAlert('Warning!', result.desc);
          this.slimLoader.complete();
        }
      },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      }
    );
  } */

  getAccountSummary() {
    // this.loading = this.loadingCtrl.create({
    //   dismissOnPageChange: true
    // });
    // this.loading.present();
    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID
    };
    // console.log("getTransferAccountList request=" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', param).map(res => res.json()).subscribe(
      data => {
        // console.log("getTransferAccountList response=" + JSON.stringify(data));
        if (data.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(data.dataList)) {
            tempArray.push(data.dataList);
            data.dataList = tempArray;
          }
          this.useraccount = [];
          for (let i = 0; i < data.dataList.length; i++) {
            this.useraccount.push(data.dataList[i]);
          }
          // this.loading.dismiss();
        } else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [{
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }],
            cssClass: 'warningAlert',
          })
          confirm.present();
          // this.loading.dismiss();
        } else {
          this.all.showAlert("Warning!", data.desc);
          // this.loading.dismiss();
        }
      },
      error => {
        // console.log("getTransferAccountList error=" + JSON.stringify(error));

        this.all.showAlert("Warning!", this.all.getErrorMessage(error));

        // this.loading.dismiss();
      }
    );
  }

  accountChange(accounttype) {
    //this.errormessage = '';
    //this.billdata.accounttype = data;
    for (let i = 0; i < this.useraccount.length; i++) {
      if (accounttype == this.useraccount[i].debitAcc) {
        this.billdata.availableamount = this.useraccount[i].avlBal;
      }
    }
  }

  packageChange(packageid) {
    for (let i = 0; i < this.packagetype.length; i++) {
      if (packageid == this.packagetype[i].id) {
        this.billdata.packagetype = this.packagetype[i].name;
        this.monthtype = this.packagetype[i].rateList;
      }
    }

    //this.errormessage = '';
    this.tempmonthname = '';
    this.billdata.amountTax = '';
    this.billdata.amountServiceCharges = '';
    this.billdata.amount = '';
    this.billdata.amountTotal = '';
    this.billdata.monthtype = '';
    this.billdata.peroid = '';
    this.billdata.did = '';
    this.billdata.pid = '';
    this.billdata.tid = '';
  }

  monthChange(month) {
    //this.errormessage = '';
    for (let i = 0; i < this.monthtype.length; i++) {
      if (month == this.monthtype[i].did) {
        this.billdata.monthtype = this.monthtype[i].m;
        this.billdata.amount = this.monthtype[i].r;
        this.billdata.peroid = this.monthtype[i].m;
        this.billdata.did = this.monthtype[i].did;
        this.billdata.pid = this.monthtype[i].pid;
        this.billdata.tid = this.monthtype[i].tid;
        this.billdata.amountTax = this.monthtype[i].amountTax;
        this.billdata.amountTotal = this.monthtype[i].amountTotal;
        this.billdata.amountServiceCharges = this.monthtype[i].amountServiceCharges;
      }
    }
  }

  QRscan() {
    this.barcodeScanner.scan().then(
      (barcodeData) => {
        this.billdata.cardno = barcodeData.text;
        //this.errormessage = '';
      },
      (err) => {
      }
    );
  }

  clicksubmit() {
    this.checkNetwork();

    if (this.flag == "success") {
      let flag0 = false;
      let flag1 = false;
      let flag2 = false;
      let flag3 = false;

      if (this.billdata.accounttype == '') {
        this.msgCnpAcc = this.showFont[12];
        flag0 = false;
      } else {
        this.msgCnpAcc = '';
        flag0 = true;
      }

      if (this.billdata.packagetype == '') {
        this.msgCnpbill = this.showFont[13];
        flag1 = false;
      } else {
        this.msgCnpbill = '';
        flag1 = true;
      }

      if (this.billdata.monthtype == '') {
        flag2 = false;
        this.msgCnpCusNo = this.showFont[14];
      } else {
        this.msgCnpCusNo = '';
        flag2 = true;
      }

      if (this.billdata.cardno == '') {
        this.msgCnpamt = this.showFont[15];
        flag3 = false;
      } else {
        this.msgCnpamt = '';
        flag3 = true;
      }

      if (flag0 && flag1 && flag2 && flag3) {
        this.storage.get('skyNetCardNo').then((result) => {
          if (result == null || result == '') {
            this.storage.set('skyNetCardNo', this.billdata.cardno);
          } else {
            this.storage.remove('skyNetCardNo');
            this.storage.set('skyNetCardNo', this.billdata.cardno);
          }
        });

        this.getOtpConfirm();
      }
    } else {
      this.flag = 'none';
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  getOtpConfirm() {
    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      type: '1',
      merchantID: this.billdata.merchantid
    };
    // console.log("checkOTPSetting request=" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(
      data => {
        // console.log("checkOTPSetting response=" + JSON.stringify(data));
        if (data.code == "0000") {
          //this.slimLoader.complete();

          this.navCtrl.push(ConfirmpagePage, {
            data: this.billdata,
            detail: this.billdata,
            otp: data.rKey,
            sKey: data.sKey
          });
        } else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [{
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }]
          })
          confirm.present();

          //this.slimLoader.complete();
        } else {
          this.all.showAlert('Warning!', data.desc);

          //this.slimLoader.complete();
        }
      },
      error => {
        // console.log("checkOTPSetting error=" + JSON.stringify(error));

        this.all.showAlert('Warning!', this.all.getErrorMessage(error));

        //this.slimLoader.complete();
      }
    );
  }

  backButton() {
    this.navCtrl.setRoot(TopUpListPage, {
      data: '1'
    });
  }

}
