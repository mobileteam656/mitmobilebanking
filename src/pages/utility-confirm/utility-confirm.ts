import 'rxjs/add/operator/map';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilityOtpPage } from '../utility-otp/utility-otp';
import { Login } from '../login/login';
import { MerchantErrorPage } from '../merchant-error-page/merchant-error-page';
import { QrUtilitySuccessPage } from '../qr-utility-success/qr-utility-success';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-utility-confirm',
  templateUrl: 'utility-confirm.html',
})
export class UtilityConfirmPage {
  textMyan: any = ["အတည်ပြုခြင်း", "အကောင့်နံပါတ်", "အမှတ်စဉ်", "ဘေလ်နံပါတ်", "အမည်", "အခွန် ငွေပမာဏ", "အခွန်ဌာန", "အခွန် အမျိုးအစား", "ကုန်ဆုံးမည့် ရက်စွဲ", "ဒဏ်ကြေး", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်","ကော်မရှင် ငွေပမာဏ","စုစုပေါင်း ငွေပမာဏ","နောက်ကျသည့် ရက်ပေါင်း"];
  textEng: any = ["Confirmation", "Account Number", "Reference Number", "Bill ID", "Customer Name", "Bill Amount", "Department Name", "Tax Description", "Due Date", "Penalty Amount", "Narrative", "CANCEL", "CONFIRM","Commission Amount","Total Amount","Belated Days"];
  showFont: string[] = [];
  font: string = '';
  obj: any;
  userData: any;
  ipaddress: any;
  isChecked: boolean = false;
  passOtp: any;
  passSKey: any;
  loading: any;
  _billObj: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public events: Events, public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider,private firebaseAnalytics: FirebaseAnalytics,
    public storage: Storage, public alertCtrl: AlertController, public http: Http , public changefont: Changefont,
    public toastCtrl: ToastController, private slimLoader: SlimLoadingBarService, private global: GlobalProvider) {
    this._billObj = this.navParams.get("data");
    this.passOtp = this.navParams.get("otp");
    this.passSKey = this.navParams.get('sKey');

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    /* this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    }); */

    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad UtilityConfirmPage');
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
  cancel() {
    this.navCtrl.pop();
  }

  confirm() {
    if (this.passOtp == 'true') {
      this.loading = this.loadingCtrl.create({
        content: "Processing...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 1, merchantID: this._billObj.merchantID, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.push(UtilityOtpPage, {
            dataOTP: data,
            data: this._billObj,
            sKey: this.passSKey
          })
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {

                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {});
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
    else {
      this.goTransfer();
    }
  }

  goTransfer() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    //this._billObj.amountTotal = this._billObj.amount;
    let parameter = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      sKey: this.passSKey,
      fromAccount: this._billObj.account,
      toAccount: this._billObj.toAcc,
      penaltyAccount:this._billObj.penaltyAccount,
      //merchantID: 'M00114',
      amount: this.formatToDouble(this._billObj.amount),
      bankCharges: this.formatToDouble(this._billObj.comAmount),
      totalAmount:this.formatToDouble(this._billObj.totalAmount),
      penalty:this.formatToDouble(this._billObj.penaltyAmount),
      narrative: this._billObj.narrative,
      refNo: this._billObj.refNo,
      field1: '',
      field2: '',
      billId: this._billObj.billId,
      cusName: this._billObj.cusName,
      ccyCode: this._billObj.ccy,
      deptName: this._billObj.deptName,
      taxDesc: this._billObj.taxDesc,
      t1: this._billObj.t1,
      t2: this._billObj.t2,
      dueDate: this._billObj.dueDate,
      paidBy: this.global.appName,
      vendorCode: this._billObj.vendorCode,
      merchantID:this._billObj.merchantID,
      belateday:this._billObj.belatedDays
    };
    //console.log("request goUtilityTransfer>>" + JSON.stringify(parameter))
    this.http.post(this.ipaddress + '/service003/goUtilityTransfer', parameter).map(res => res.json()).subscribe(res => {
      // console.log("response gopayment  data>>" + JSON.stringify(res));
      this.firebaseAnalytics.logEvent('utility_payment', {
        "userid": this.userData.userID})
            .then((res: any) => { console.log(res); })
            .catch((error: any) => console.log(error));

      if (res.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.setRoot(QrUtilitySuccessPage, {
          data: res,
          detail: this._billObj,
          fromPage: 'qrUtility',
          detailmerchant: { "processingCode": "100600" }
        });
      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // console.log('OK clicked');
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.loading.dismiss();
        this.navCtrl.setRoot(MerchantErrorPage, {
          data: res,
          detail: this._billObj,
          detailmerchant: 'qrUtility'
        });
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
        // this.loading.dismiss();
      });
  }
  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }
}
