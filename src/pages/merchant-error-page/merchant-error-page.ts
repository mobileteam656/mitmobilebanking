import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, NavController, NavParams, Platform } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { MerchantlistPage } from '../merchantlist/merchantlist';
import { QrUtilityPage } from '../qr-utility/qr-utility';

/**
 * Generated class for the MerchantErrorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-merchant-error-page',
  templateUrl: 'merchant-error-page.html',
  providers: [ChangelanguageProvider]
})
export class MerchantErrorPage {

  textMyan: any = ['လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'Package', 'ကတ်နံပါတ်', 'စာရင်းနံပါတ်', 'ကျသင့်ငွေ', 'ပိတ်မည်'];
  textEng: any = ["Transaction Approved", "Package", "Card Number", "Account Number", "Transaction Amount", "Close"];
  showFont: string[] = [];
  font: string;
  passtemp1: any;
  passtemp2: any;
  passtemp3: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage, public platform: Platform, public changeLanguage: ChangelanguageProvider) {
    this.passtemp1 = this.navParams.get('data');
    this.passtemp2 = this.navParams.get('detail');
    this.passtemp3 = this.navParams.get('detailmerchant');
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  ionViewDidLoad() {
  }

  getOK() {
     if (this.passtemp3 == "qrUtility") {
      this.navCtrl.setRoot(QrUtilityPage,{
        data:this.passtemp2
      })
    }
    else {
      this.navCtrl.setRoot(MerchantlistPage)
    }
  }

}
