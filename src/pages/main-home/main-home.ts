import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { AlertController, Events, LoadingController, MenuController, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/Rx';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { AccountSummaryDetail } from '../account-summary-detail/account-summary-detail';
import { AccountTransation } from '../account-transation/account-transation';
import { AtmLocation } from '../atm-location/atm-location';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ContactUs } from '../contact-us/contact-us';
import { CorporateTypePage } from '../corporate-type/corporate-type';
import { ExchangePage } from '../exchange/exchange';
import { FastTransferPage } from '../fast-transfer/fast-transfer';
import { HomePage } from '../home/home';
import { InternalTransferPage } from '../internal-transfer/internal-transfer';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { Logout } from '../logout/logout';
import { MobileTopupPage } from '../mobile-topup/mobile-topup';
import { PopoverPage } from '../popover-page/popover-page';
import { TopUpListPage } from '../top-up-list/top-up-list';
import { WalletTopupPage } from '../wallet-topup/wallet-topup';
import { OwnTransferPage } from '../own-transfer/own-transfer';
import { NotificationPage } from '../notification/notification';

declare var window;

@Component({
  selector: 'page-main-home',
  templateUrl: 'main-home.html',
})

export class MainHomePage {
  userData: any = {};
  allaccount: any;
  status: any;
  loading: any;
  flag: string;
  useraccount: any;
  currentaccount: any = [];
  savingaccount: any = [];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  shwe: string;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  font: string;
  showFont: any = [];
  textEng: any = ['mBanking', 'Summary', 'Transaction History', 'Internal Transfer', 'QR Payment', 'Services', 'Contact Us', 'Payment', 'Topup', 'Agent', 'QR Receipt', 'Logout', 'Utility Payment', 'Fast Transfer', 'Remittance', 'Change Password', 'Location', 'Currency Exchange', 'Contact Us', 'Wallet Top Up', 'I Have', 'I Owe','Mobile Top Up','Corporate','Own Transfer'];
  textMyan = ['mBanking', 'စာရင်းချုပ်', 'စာရင်းအသေးစိတ်', 'ဘဏ်စာရင်းအချင်းချင်း ငွေလွှဲရန်', 'QR ငွေပေးချေမှု', 'ဝန်ဆောင်မှုများ', 'ဖုန်းခေါ်ဆိုရန်', "ငွေပေးချေမှု", "ဖုန်းငွေဖြည့်သွင်းခြင်း", "အေးဂျင့်", "QR ငွေလက်ခံမှု", "ထွက်မည်", 'ငွေပေးချေမှု', 'အမြန်ငွေလွှဲရန်', 'ငွေသားပို့ရန်', 'လျှို့ဝှက်နံပါတ်ပြောင်းရန်', 'တည်နေရာ', 'ငွေလဲနှုန်း', 'ဆက်သွယ်ရန်', 'ဝေါ(လ်)လက် ငွေဖြည့်သွင်းခြင်း', 'လက်ကျန်ငွေ', 'ပေးရန်ရှိငွေ','ဖုန်းငွေဖြည့်သွင်းခြင်း','ကော်ပိုရိတ်','ကိုယ်ပိုင်ဘဏ်စာရင်းသို့ ငွေလွှဲရန်'];
  idleCtrl: boolean;
  popover: any;
  totalsFloat: any = 0;
  moneyFloat: any;
  temp2: any;
  currency: any;
  debitTotal: any = 0;
  load: any = '...';
  data: any = [];
  userName:any;
  alertPresented: any;
  count: any =0;
  flagnoti: boolean ;
  constructor(public navCtrl: NavController, public menu: MenuController, public idle: Idle, public navParams: NavParams, public storage: Storage,
    public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public events: Events, public network: Network,
    public popoverCtrl: PopoverController, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, private keepalive: Keepalive,
    public platform: Platform, public global: GlobalProvider, public changefont: Changefont) {
      //this.count = this.global.noticount;
      //events.subscribe('user:created', ( ) => {
       // this.getNotiCount();
        // user and time are the same arguments passed in `events.publish(user, time)`
        //console.log('Welcome', user, 'at', time);
    //  });
    let temp = this.navParams.get('data');
    this.currency = 'MMK';
    this.alertPresented = false;
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changelanguage(this.font);
      this.data = this.getData();
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changelanguage(this.font);
      this.data = this.getData();
    });
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.userName=this.userData.userName;
      this.storage.get('ipaddress').then((result1) => {
        if (result == null || result == '') {
        } else {
          this.ipaddress = result1;
        }
        this.getAccountSummary();
      });
    });
  }

  getData() {
    return [
      {
        row: [
          {
            col: [
              { name: this.showFont[1], Component: HomePage, Css: 'card color', icon: "ios-accsummary", status:0 },
              { name: this.showFont[2], Component: AccountTransation, Css: 'card width', icon: "ios-transhistory", status:0 }
            ],
          }, {
            col: [
              { name: this.showFont[24], Component: OwnTransferPage, Css: "card", icon: 'ios-owntransfer' },
              //{ name: this.showFont[13], Component: FastTransferPage, Css: "card color", icon: "ios-ftransfer" }
              { name: this.showFont[13], Component: FastTransferPage, Css: "card color width", icon: "ios-ftransfer" },
            ],
          }, {
            col: [
              { name: this.showFont[3], Component: InternalTransferPage, Css: "card color", icon: "ios-itransfer" },
              { name: this.showFont[7], Component: TopUpListPage, Css: "card width", icon: "ios-bill", type:'1' },              
            ]
          }],
      }, {
        row: [
          {
            col: [
              { name: this.showFont[22], Component: MobileTopupPage, Css: "card color", icon: "ios-mobiletopup"},
              //{ name: this.showFont[23], Component: CorporateTypePage, Css: "card color", icon: "briefcase" },
              { name: this.showFont[19], Component: WalletTopupPage, Css: "card width", icon: "ios-wallettopup" },              
            ],
          }, {
            col: [
              { name: this.showFont[16], Component: AtmLocation, Css: "card", icon: "ios-location" , status:0},
              { name: this.showFont[17], Component: ExchangePage, Css: "card color width", icon: "ios-currencyexchange", status:0 }
            ],
          }, {
            col: [
              { name: this.showFont[18], Component: ContactUs, Css: "card color", icon: "ios-contactus" , status:0},
              { name: this.showFont[11], Component: "", Css: "card width", icon: "ios-logout" }
            ]
          }],
      }];
  }

  ionViewDidLoad() {
    this.storage.get('ipaddress').then((ipaddress) => {
      if (ipaddress == '' || ipaddress == null) {
        this.storage.set('ipaddress', this.global.ipaddress);
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = ipaddress;
        //this.getNotiCount();
      }
      //this.readVersionMobile();
    });
  }

  getAccountSummary() {
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service001/getAccountSummary', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.currency = 'MMK';
        this.totalsFloat = result.haveAmountTotal;
		//this.debitTotal = result.oweAmountTotal;
        this.debitTotal = result.oweAmountTotal.replace("-","");
        this.load = '0.00';
      } else if (result.code == "0016") {
        //this.logoutAlert("Warning!",result.desc);
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert'
        })
        confirm.present();
      } else {
        this.status = 0;
        this.allaccount = [];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        } else if (error.status == 500) {
          code = '002';
        } else if (error.status == 403) {
          code = '003';
        } else if (error.status == -1) {
          code = '004';
        } else if (error.status == 0) {
          code = '005';
        } else if (error.status == 502) {
          code = '006';
        } else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      });
  }

  idleWatch() {
    this.idle.setIdle(2);
    this.idle.setTimeout(5 * 60);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
    });
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (this.idleCtrl) {
        this.idleCtrl = false;
        this.gologout();
        this.idle.stop();
      }
    });
    this.idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!'
    });
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      var data = countdown / 60;
      this.min = data.toString().split('.')[0];
      this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
      this.sec = (Math.round(this.sec * 100) / 100);
      this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
    });
    this.keepalive.interval(15);
    this.keepalive.onPing.subscribe(() => {
      this.lastPing = new Date();
    });
    this.reload();
  }

  checkNetwork(cb) {
    if (this.network.type == "none") {
      this.flag = 'none';
      cb(this.flag);
    } else {
      this.flag = 'success';
      cb(this.flag);
    }
  }

  ionViewCanEnter() {
  }

  ionViewDidLeave() {
    this.reload();
    this.idleCtrl = true;
    this.slimLoader.reset();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  doRefresh(refresher) {
    this.currentaccount = [];
    this.savingaccount = [];
    this.getAccountSummary();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  gotoDetail(a) {
    this.navCtrl.push(AccountSummaryDetail,
      {
        data: a
      });
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }],
          cssClass:'confirmAlert'
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {
    this.idleCtrl = false;
    this.idle.stop();
    this.loading = this.loadingCtrl.create({
      content: "Loging out...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.storage.remove('userData');
          this.events.publish('lastTimeLoginSuccess', '');
          this.events.publish('login_success', false);
          this.navCtrl.setRoot(Logout, {
            data: data
          });
        } else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let code;
          if (error.status == 404) {
            code = '001';
          } else if (error.status == 500) {
            code = '002';
          } else if (error.status == 403) {
            code = '003';
          } else if (error.status == -1) {
            code = '004';
          } else if (error.status == 0) {
            code = '005';
          } else if (error.status == 502) {
            code = '006';
          } else {
            code = '000';
          }
          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }

  addThousandSeparators(number) { 
    let whole, fraction;
    let decIndex = number.toString().lastIndexOf('.');
    if (decIndex > -1) {
      whole = number.toString().substr(0, decIndex);
      fraction = number.toString().substr(decIndex);
    } else {
      whole = number.toString();
    }
    let rgx = /(\d+)(\d{3})/
    while (rgx.test(whole)) {
      whole = whole.replace(rgx, '$1' + ',' + '$2')
    }
    return fraction ? whole + fraction : whole
  }
  
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  goPage(col) {
    if (col.Component == '') {
      this.gologout();
    } else if (col.status=='0') {
      this.navCtrl.push(col.Component, {
        ststus: '1'
      });
    } else if (col.type == '1') {
      this.navCtrl.setRoot(TopUpListPage, {
        data: col.type
      });
    } else if (col.type == '2') {
      this.navCtrl.setRoot(TopUpListPage, {
        data: col.type
      });
    }  else {
      this.navCtrl.setRoot(col.Component)
    }
  }

  logoutAlert(title, message){
    let vm = this
    if (!vm.alertPresented) {
      vm.alertPresented = true
      vm.alertCtrl.create({
        title: title,
        message: message,
        buttons: [{
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
            vm.alertPresented = false
          }
        }],
        cssClass: 'warningAlert',
        enableBackdropDismiss: false
      }).present();
    }
  }

  getNotiCount(){
    // let param = { userID: '', sessionID: '' };
    // this.http.post(this.ipaddress + '/service006/getNotiCount', param).map(res => res.json()).subscribe(result => {
     
    //  console.log("noticount",JSON.stringify(result));
    //  this.count = result.noticount;
    //  if(this.count == 0){
    //     this.global.flagnoti = false;
    //  }else{
    //     this.global.flagnoti = true;
    //  }
   
    // },
    //   error => {
      
    //   });
  }

  goNoti(){
    this.navCtrl.push(NotificationPage);
  }
}
