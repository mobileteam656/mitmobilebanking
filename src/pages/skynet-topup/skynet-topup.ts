import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { SkynetConfirmPage } from '../skynet-confirm/skynet-confirm';
import { TopUpListPage } from '../top-up-list/top-up-list';
import { UtilProvider } from '../../providers/util/util';

@Component({
    selector: 'page-skynet-topup',
    templateUrl: 'skynet-topup.html',
})
export class SkynetTopupPage {

    textMyan: any = [
        "Skynet Payment", "အကောင့်နံပါတ် မှ",
        "ဘဏ်စာရင်း လက်ကျန်ငွေ", "ကဒ်နံပါတ်",
        "ပက်​ကေ့နာမည်", "ကြာချိန်",
        "ပြန်စမည်", "လုပ်ဆောင်မည်",
        "ပက်​ကေ့ အမျိုးအစား", "​ဇာတ်ကားနာမည်",
        "သက်တမ်းကုန်ဆုံးရက်", "စတင်ရက်စွဲအချိန်",
        "အဆုံးရက်စွဲအချိန်", "​ဝယ်ယူခဲ့သည့် ပက်​ကေ့",
        "ငွေပမာဏ", "ကဒ်နံပါတ် မှားယွင်းနေပါသည်",
        "မှတ်ချက်း အစိုးရအခွန် ၅% ပါဝင်ပြီး",
    ];
    textEng: any = [
        "Skynet Payment", "From Account No.",
        "Account Balance", "Card No.",
        "Available Packages", "Duration",
        "RESET", "SUBMIT",
        "Package Type", "Movie Name",
        "Expiry Date Time", "Start Date Time",
        "End Date Time", "Paid Package",
        "Amount", "Invalid Card No.",
        "Note: Including 5% Government commercial tax","Bank Charges",
        "Total Amount",
    ];
    textData: any = [];

    textErrorEng: any = [
        "Please choose Account No.", "Please fill Card No.",
        "Please choose Package.", "Please choose Voucher.",
        "Please choose Movie.", "Please choose Package Type.",
        "Invalid Card No.", "There is no movies. You already purchased."
    ]
    textErrorMyan: any = [
        "ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ကဒ်နံပါတ် ရိုက်ထည့်ပါ",
        "ကျေးဇူးပြုပြီး ပက်​ကေ့နာမည် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ​ဘောက်ချာ အမျိုးအစား ရွေးချယ်ပါ",
        "ကျေးဇူးပြုပြီး ​ဇာတ်ကားနာမည် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ​ပက်​ကေ့ အမျိုးအစား ရွေးချယ်ပါ",
        "ကဒ်နံပါတ် မှားယွင်း​နေပါသည်", "There is no movies. You already purchased."
    ];
    textError: string[] = [];

    font: any = '';

    transferData: any = {};
    fromaccMsg: any = '';
    cardNoMsg: any = '';
    subscriptionMsg: any = '';
    voucherMsg: any = '';
    fromAccountlist: any = [];
    ccy: any = "";
    accountBal: any = "";
    checked=false;
    userdata: any;
    ipaddress: string;
    loading: any;

    servicelist: any = [];
    installlist: any = [];
    voucherlist: any = [];
    voucherlist1: any = [];

    merchantID: any = "";
    merchantCode: any = "";

    dataFromMerchantList: any;

    bankCharges: any = "";

    subscriptionno: any = "";

    packagetypelist = [
        {
            key: "normal",
            value: "Monthly Package"
        },
        {
            key: "ppv",
            value: "Pay Per View Package"
        }
    ];

    packageTypeMsg: any = '';

    movielist: any = [];

    movieMsg: any = '';
    commissionAmount:any;
    callService = "no";
    disableCardNo = "no";
    weeklydata = 'no';
    expdate = 'no';
    photo = 'yes';
    amount: any;
    caculateAmount:any;
    totalAmount:any;
    ppvamount: any;
    startdate: any;
    enddate: any;
    expirydate: any;

    constructor(
        public navCtrl: NavController, public navParams: NavParams,
        public events: Events, public storage: Storage,
        public alertCtrl: AlertController, public all: AllserviceProvider,
        public loadingCtrl: LoadingController, public global: GlobalProvider,
        public http: Http, public platform: Platform,
        public changefont: Changefont, public util: UtilProvider
    ) {
        this.dataFromMerchantList = this.navParams.get('data');

        if (this.dataFromMerchantList != undefined && this.dataFromMerchantList != null && this.dataFromMerchantList != '') {
            this.merchantID = this.dataFromMerchantList.merchantID;
            this.merchantCode = this.dataFromMerchantList.processingCode;
        } else {
            this.merchantID = "";
            this.merchantCode = "";
        }

        this.events.subscribe('changelanguage', lan => {
            this.changelanguage(lan.data)
        });

        this.storage.get('language').then((font) => {
            this.changelanguage(font);
        });
    }

    ionViewDidLoad() {
        this.accountBal = "";
        this.ccy = "";

        this.callService = "no";
        this.disableCardNo = "no";

        this.storage.get('userData').then((val) => {
            this.userdata = val;

            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                    this.ipaddress = this.global.ipaddress;
                }
                else {
                    this.ipaddress = result;
                }

                this.getTransferAccountList();
            })
        });
    }

    getTransferAccountList() {
        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        this.loading.present();

        let parameter = {
            userID: this.userdata.userID,
            sessionID: this.userdata.sessionID
        };

        this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(
            result => {
                if (result.code == "0000") {
                    let tempArray = [];
                    if (!Array.isArray(result.dataList)) {
                        tempArray.push(result.dataList);
                        result.dataList = tempArray;
                    }
                    this.fromAccountlist = result.dataList;
                    this.loading.dismiss();
                }
                else if (result.code == "0016") {
                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,
                        message: result.desc,
                        buttons: [{
                            text: 'OK',
                            handler: () => {
                                this.storage.remove('userData');
                                this.events.publish('login_success', false);
                                this.events.publish('lastTimeLoginSuccess', '');
                                this.navCtrl.setRoot(Login, {});
                                this.navCtrl.popToRoot();
                            }
                        }],
                        cssClass: 'warningAlert',
                    })
                    confirm.present();
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert("Warning!", result.desc);
                    this.loading.dismiss();
                }
            },
            error => {
                this.all.showAlert("Warning!", this.all.getErrorMessage(error));
                this.loading.dismiss();
            }
        );
    }

    changePackageType(type) {
        if (this.callService == 'yes') {
            if (type != 'ppv') {
                this.getServiceAndVoucher();
            } else {
                this.getCatalogListAndAvailablePPV();
            }
        }

        this.packageTypeMsg = "";
        this.subscriptionMsg = "";
        this.voucherMsg = "";
        this.movieMsg = "";
    }

    changeMovieName(moviecode) {
        this.movieMsg = '';

        for (let i = 0; i < this.movielist.length; i++) {
            if (moviecode == this.movielist[i].moviecode) {
                this.transferData.moviecode = this.movielist[i].moviecode;
                this.transferData.moviename = this.movielist[i].moviename;
                this.transferData.startdate = this.movielist[i].startdate;
                this.startdate = this.transferData.startdate.replace("T", " ");
                this.transferData.enddate = this.movielist[i].enddate;
                this.enddate = this.transferData.enddate.replace("T", " ");
                this.transferData.amount = this.movielist[i].amount;
                this.amount = this.util.formatAmount(this.transferData.amount) + " MMK";
            }
        }
    }

    getCatalogListAndAvailablePPV() {
        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        this.loading.present();

        let parameter = {
            userid: this.userdata.userID,
            sessionid: this.userdata.sessionID,
            life_cycle_state: this.transferData.life_cycle_state_for_catalog_list,
            provisioning_provider_identifier__alternative_code: this.transferData.alternative_code_provisioning_for_catalog_list,
            subscription_identifier__number: this.subscriptionno,
            termed_service_identifier__alternative_code: this.transferData.alternative_code_termed_for_catalog_list
        };

        this.http.post(this.ipaddress + '/serviceskynet/getCatalogListAndAvailablePPV', parameter).map(res => res.json()).subscribe(
            result => {
                if (result.code == "0000") {
                    let tempArray1 = [];
                    if (!Array.isArray(result.movielist)) {
                        tempArray1.push(result.movielist);
                        result.movielist = tempArray1;
                    }
                    this.movielist = result.movielist;
                    this.bankCharges=result.bankCharges;
                    this.commissionAmount= this.util.formatAmount(this.bankCharges) + " MMK";
                    console.log("this.is BC"+this.bankCharges);
                    console.log("movice log"+JSON.stringify(result));
                    this.transferData.moviecode = this.movielist[0].moviecode;
                    this.transferData.moviename = this.movielist[0].moviename;
                    this.transferData.startdate = this.movielist[0].startdate;
                    this.startdate = this.transferData.startdate.replace("T", " ");
                    this.transferData.enddate = this.movielist[0].enddate;
                    this.enddate = this.transferData.enddate.replace("T", " ");
                    this.transferData.amount = this.movielist[0].amount;
                    this.ppvamount = this.util.formatAmount(this.transferData.amount) + " MMK";
                    this.totalAmount=this.util.formatAmount(parseFloat(this.transferData.amount)+parseFloat(this.bankCharges) ) + " MMK";
                    this.transferData.usage_service_catalog_identifier__id = result.usage_service_catalog_identifier__id;
                    this.weeklydata = "yes";
                    this.loading.dismiss();
                }
                else if (result.code == "0016") {
                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,
                        message: result.desc,
                        buttons: [{
                            text: 'OK',
                            handler: () => {
                                this.storage.remove('userData');
                                this.events.publish('login_success', false);
                                this.events.publish('lastTimeLoginSuccess', '');
                                this.navCtrl.setRoot(Login, {});
                                this.navCtrl.popToRoot();
                            }
                        }],
                        cssClass: 'warningAlert',
                    })
                    confirm.present();
                    this.loading.dismiss();
                }
                else if (result.code == "0014") {
                    this.all.showAlert("Warning!", this.textError[7]);
                    this.loading.dismiss();
                    this.weeklydata = "no";
                }
                else {
                    this.all.showAlert("Warning!", result.desc);
                    this.loading.dismiss();
                }
            },
            error => {
                this.all.showAlert("Warning!", this.all.getErrorMessage(error));
                this.loading.dismiss();
            }
        );
    }

    getServiceAndVoucher() {
        let f2 = false;
        let s;
        let tempory;
        if (this.transferData.cardNo != undefined && this.transferData.cardNo != null && this.transferData.cardNo != '') {
            this.cardNoMsg = '';
            f2 = true;

            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: true
            });
            this.loading.present();

            let parameter = {
                userid: this.userdata.userID,
                sessionid: this.userdata.sessionID,
                cardno: this.transferData.cardNo
            };

            this.http.post(this.ipaddress + '/serviceskynet/getServiceAndVoucher', parameter).map(res => res.json()).subscribe(
                result => {
                    if (result.code == "0000") {
                        this.photo = 'no';

                        let tempArray1 = [];
                        if (!Array.isArray(result.servicelist)) {
                            tempArray1.push(result.servicelist);
                            result.servicelist = tempArray1;
                        }

                        let tempArray2 = [];
                        if (!Array.isArray(result.installlist)) {
                            tempArray2.push(result.installlist);
                            result.installlist = tempArray2;
                        }

                        let tempArray3 = [];
                        if (!Array.isArray(result.voucherlist)) {
                            tempArray3.push(result.voucherlist);
                            result.voucherlist = tempArray3;
                        }

                        this.servicelist = result.servicelist;
                        this.installlist = result.installlist;
                        this.voucherlist = result.voucherlist;
                        tempory=this.voucherlist[0].value;
                        console.log("tm",tempory);
                        console.log(this.servicelist);
                        console.log( this.installlist );
                        console.log("ver",this.voucherlist);

                        this.transferData.servicealternativecode = this.servicelist[0].alternativecode;
                        this.transferData.servicename = this.servicelist[0].servicecode;
                        this.transferData.voucheralternative_code = this.voucherlist[0].alternative_code;
                        this.transferData.amount = this.voucherlist[0].value;
                        this.amount = this.util.formatAmount(this.transferData.amount) + " MMK";
                         //Am
                        // this.commissionAmount = this.util.formatAmount(this.transferData.commissionAmount);
                        // this.totalAmount = this.util.formatAmount(this.transferData.totalAmount);
                         
                        this.transferData.life_cycle_state_for_catalog_list = result.life_cycle_state_for_catalog_list;
                        this.transferData.alternative_code_provisioning_for_catalog_list = result.alternative_code_provisioning_for_catalog_list;
                        this.transferData.alternative_code_termed_for_catalog_list = result.alternative_code_termed_for_catalog_list;

                        if (result.expirydate == undefined || result.expirydate == null || result.expirydate == '') {
                            this.expdate = 'no';
                            this.transferData.expirydate = result.expirydate;
                        } else {
                            this.expdate = 'yes';
                            this.transferData.expirydate = result.expirydate;
                            this.expirydate = this.transferData.expirydate.replace("T", " ");
                        }

                        this.transferData.currentpackage = result.currentpackage;

                        this.transferData.packagetype = "normal";

                        let bankChargesTemp = result.bankCharges;
                         console.log(this.bankCharges);
                        if (bankChargesTemp != undefined && bankChargesTemp != null && bankChargesTemp != '') {
                            this.bankCharges =bankChargesTemp;
                            this.commissionAmount=this.util.formatAmount(bankChargesTemp) + " MMK";
                        } else {
                            this.bankCharges = 0;
                            this.commissionAmount=0;
                        }
                        if(this.amount!= undefined && this.amount != null && this.amount != '')
                        {
                            this.totalAmount=this.util.formatAmount(parseFloat(this.transferData.amount)+parseFloat(bankChargesTemp) ) + " MMK";
                            console.log(this.transferData.amount);
                        }
                        let subscriptionnoTemp = result.subscriptionno;

                        if (subscriptionnoTemp != undefined && subscriptionnoTemp != null && subscriptionnoTemp != '') {
                            this.subscriptionno = subscriptionnoTemp;
                        } else {
                            this.subscriptionno = 0;
                        }

                        this.callService = "yes";
                        this.disableCardNo = "yes";

                        this.loading.dismiss();
                    }
                    else if (result.code == "0016") {
                        let confirm = this.alertCtrl.create({
                            title: 'Warning!',
                            enableBackdropDismiss: false,
                            message: result.desc,
                            buttons: [{
                                text: 'OK',
                                handler: () => {
                                    this.storage.remove('userData');
                                    this.events.publish('login_success', false);
                                    this.events.publish('lastTimeLoginSuccess', '');
                                    this.navCtrl.setRoot(Login, {});
                                    this.navCtrl.popToRoot();
                                }
                            }],
                            cssClass: 'warningAlert',
                        })
                        confirm.present();
                        this.loading.dismiss();
                    }
                    else if (result.code == "0014") {
                        this.photo = 'yes';
                        this.all.showAlert("Warning!", this.textError[6]);
                        this.loading.dismiss();
                    }
                    else {
                        this.all.showAlert("Warning!", result.desc);
                        this.loading.dismiss();
                    }
                },
                error => {
                    this.all.showAlert("Warning!", this.all.getErrorMessage(error));
                    this.loading.dismiss();
                }
            );
        }
        else {
            this.cardNoMsg = this.textError[1];
            f2 = false;
            this.photo = 'yes';
        }
    }

    transfer() {
        //let num = /^[0-9-\+]*$/;
        let f1, f2, f3, f4, f5, f6 = false;

        //from account
        if (this.transferData.fromAcc != null && this.transferData.fromAcc != '' && this.transferData.fromAcc != undefined) {
            this.fromaccMsg = '';
            f1 = true;
        }
        else {
            this.fromaccMsg = this.textError[0];
            f1 = false;
        }

        //card no
        if (this.transferData.cardNo != null && this.transferData.cardNo != '' && this.transferData.cardNo != undefined) {
            this.cardNoMsg = '';
            f2 = true;
        }
        else {
            this.cardNoMsg = this.textError[1];
            f2 = false;
        }

        if (this.disableCardNo == 'yes') {
            f6 = true;

            //package type
            if (this.transferData.packagetype != null && this.transferData.packagetype != '' && this.transferData.packagetype != undefined) {
                this.packageTypeMsg = '';
                f5 = true;
            }
            else {
                this.packageTypeMsg = this.textError[5];
                f5 = false;
            }

            if (this.transferData.packagetype != 'ppv') {
                //install item and subscription list
                if (this.transferData.servicealternativecode != null && this.transferData.servicealternativecode != '' && this.transferData.servicealternativecode != undefined) {
                    this.subscriptionMsg = '';
                    f3 = true;
                }
                else {
                    this.subscriptionMsg = this.textError[2];
                    f3 = false;
                }

                //voucher list
                if (this.transferData.voucheralternative_code != null && this.transferData.voucheralternative_code != '' && this.transferData.voucheralternative_code != undefined) {
                    this.voucherMsg = '';
                    f4 = true;
                }
                else {
                    this.voucherMsg = this.textError[3];
                    f4 = false;
                }
            } else {
                //movie list
                if (this.transferData.moviecode != null && this.transferData.moviecode != '' && this.transferData.moviecode != undefined) {
                    this.movieMsg = '';
                    f3 = true;
                    f4 = true;
                }
                else {
                    this.movieMsg = this.textError[4];
                    f3 = false;
                    f4 = false;
                }
            }
        } else {
            f6 = false;
            if (this.cardNoMsg == undefined || this.cardNoMsg == null || this.cardNoMsg == '') {
                this.cardNoMsg = this.textError[6];
            }
        }

        if (f1 && f2 && f3 && f4 && f5 && f6) {
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: true
            });
            this.loading.present();

            let param = {
                userID: this.userdata.userID,
                sessionID: this.userdata.sessionID,
                type: '1',
                merchantID: this.merchantID
            };

            this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(
                data => {
                    if (data.code == "0000") {
                        this.loading.dismiss();
                        // let commAmt = this.bankCharges;
                        // let totalAmt = parseFloat(this.transferData.amount) + parseFloat(commAmt);
                        let temp: any;
                        if (this.transferData.packagetype != 'ppv') {
                            let commAmt = this.bankCharges;
                            let totalAmt = parseFloat(this.transferData.amount) + parseFloat(commAmt);
                            temp = {
                                fromAcc: this.transferData.fromAcc,
                                fromName: this.userdata.userName,
                                cardNo: this.transferData.cardNo,
                                packageName: this.transferData.servicename,
                                voucherType: this.transferData.voucheralternative_code,
                                amount: this.transferData.amount,
                                commissionAmount: commAmt,
                                totalAmount: totalAmt,
                                ccy: this.ccy,
                                merchantID: this.merchantID,
                                merchantCode: this.merchantCode,
                                subscriptionno: this.subscriptionno,
                                //toAcc: this.transferData.toAcc,
                                packagetype: this.transferData.packagetype
                            };
                        } else {
                            let commAmt =this.bankCharges;
                            let totalAmt = parseFloat(this.transferData.amount) + parseFloat(commAmt);
                            temp = {
                                fromAcc: this.transferData.fromAcc,
                                fromName: this.userdata.userName,
                                cardNo: this.transferData.cardNo,
                                //packageName: this.transferData.servicename,
                                //voucherType: this.transferData.voucheralternative_code,
                                amount: this.transferData.amount,
                                commissionAmount:commAmt,
                                totalAmount: totalAmt,
                                ccy: this.ccy,
                                merchantID: this.merchantID,
                                merchantCode: this.merchantCode,
                                subscriptionno: this.subscriptionno,
                                //toAcc: this.transferData.toAcc,
                                packagetype: this.transferData.packagetype,
                                moviename: this.transferData.moviename,
                                moviecode: this.transferData.moviecode,
                                startdate: this.transferData.startdate,
                                enddate: this.transferData.enddate,
                                usage_service_catalog_identifier__id: this.transferData.usage_service_catalog_identifier__id
                            };
                        }

                        this.navCtrl.push(SkynetConfirmPage, {
                            data: temp,
                            otp: data.rKey,
                            sKey: data.sKey
                        });
                    }
                    else if (data.code == "0016") {
                        let confirm = this.alertCtrl.create({
                            title: 'Warning!',
                            enableBackdropDismiss: false,
                            message: data.desc,
                            buttons: [{
                                text: 'OK',
                                handler: () => {
                                    this.storage.remove('userData');
                                    this.events.publish('login_success', false);
                                    this.events.publish('lastTimeLoginSuccess', '');
                                    this.navCtrl.setRoot(Login, {});
                                    this.navCtrl.popToRoot();
                                }
                            }],
                            cssClass: 'warningAlert',
                        })
                        confirm.present();
                        this.loading.dismiss();
                    }
                    else {
                        this.all.showAlert('Warning!', data.desc);
                        this.loading.dismiss();
                    }
                },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                }
            );
        }
    }

    reset() {
        this.transferData.fromAcc = "";
        this.transferData.cardNo = "";
        this.transferData.packagetype = "";
        this.transferData.servicealternativecode = "";
        this.transferData.voucheralternative_code = "";
        this.transferData.moviecode = "";
        this.transferData.moviename = "";
        this.transferData.startdate = "";
        this.transferData.enddate = "";
        this.transferData.amount = "";
        this.transferData.life_cycle_state_for_catalog_list = "";
        this.transferData.alternative_code_provisioning_for_catalog_list = "";
        this.transferData.alternative_code_termed_for_catalog_list = "";
        this.transferData.usage_service_catalog_identifier__id = "";
        this.transferData.servicename = "";

        this.movielist = [];
        this.servicelist = [];
        this.voucherlist = [];

        this.accountBal = "";
        this.ccy = "";

        this.fromaccMsg = '';
        this.cardNoMsg = '';
        this.voucherMsg = '';
        this.subscriptionMsg = '';
        this.packageTypeMsg = '';
        this.movieMsg = '';

        this.disableCardNo = "no";
        this.callService = "no";
        this.photo = "yes";
    }

    changelanguage(font) {
        if (font == "eng") {
            this.font = "";
            for (let i = 0; i < this.textEng.length; i++) {
                this.textData[i] = this.textEng[i];
            }
            for (let i = 0; i < this.textErrorEng.length; i++) {
                this.textError[i] = this.textErrorEng[i];
            }
            // if (this.fromaccMsg != '') {
            //     this.fromaccMsg = this.textError[0];
            // }
            // if (this.cardNoMsg != '') {
            //     this.cardNoMsg = this.textError[1];
            // }
            // if (this.subscriptionMsg != '') {
            //     this.subscriptionMsg = this.textError[2];
            // }
            // if (this.voucherMsg != '') {
            //     this.voucherMsg = this.textError[3];
            // }
            // if (this.movieMsg != '') {
            //     this.movieMsg = this.textError[4];
            // }
            // if (this.packageTypeMsg != '') {
            //     this.packageTypeMsg = this.textError[5];
            // }
        }
        else if (font == "zg") {
            this.font = "zg";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
            }
            for (let i = 0; i < this.textErrorMyan.length; i++) {
                this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
            }
            // if (this.fromaccMsg != '') {
            //     this.fromaccMsg = this.textError[0];
            // }
            // if (this.cardNoMsg != '') {
            //     this.cardNoMsg = this.textError[1];
            // }
            // if (this.subscriptionMsg != '') {
            //     this.subscriptionMsg = this.textError[2];
            // }
            // if (this.voucherMsg != '') {
            //     this.voucherMsg = this.textError[3];
            // }
            // if (this.movieMsg != '') {
            //     this.movieMsg = this.textError[4];
            // }
            // if (this.packageTypeMsg != '') {
            //     this.packageTypeMsg = this.textError[5];
            // }
        }
        else {
            this.font = "uni";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.textData[i] = this.textMyan[i];
            }
            for (let i = 0; i < this.textErrorMyan.length; i++) {
                this.textError[i] = this.textErrorMyan[i];
            }
            // if (this.fromaccMsg != '') {
            //     this.fromaccMsg = this.textError[0];
            // }
            // if (this.cardNoMsg != '') {
            //     this.cardNoMsg = this.textError[1];
            // }
            // if (this.subscriptionMsg != '') {
            //     this.subscriptionMsg = this.textError[2];
            // }
            // if (this.voucherMsg != '') {
            //     this.voucherMsg = this.textError[3];
            // }
            // if (this.movieMsg != '') {
            //     this.movieMsg = this.textError[4];
            // }
            // if (this.packageTypeMsg != '') {
            //     this.packageTypeMsg = this.textError[5];
            // }
        }
    }

    ionViewCanLeave() {
        this.platform.registerBackButtonAction(() => {
            this.navCtrl.pop();
        });
    }

    backButton() {
        this.navCtrl.setRoot(TopUpListPage, {
            data: '1'
        });
    }

    changeAcc(account) {
        this.fromaccMsg = '';

        for (let i = 0; i < this.fromAccountlist.length; i++) {
            if (account == this.fromAccountlist[i].depositAcc) {
                this.accountBal = this.fromAccountlist[i].avlBal;
                this.ccy = this.fromAccountlist[i].ccy;
            }
        }
        this.checked=true;
    }

    changeVoucher(voucheralternative_code) {
        this.voucherMsg = '';

        for (let i = 0; i < this.voucherlist.length; i++) {
            if (voucheralternative_code == this.voucherlist[i].alternative_code) {
                this.transferData.voucheralternative_code = this.voucherlist[i].alternative_code;
                this.transferData.amount = this.voucherlist[i].value;
                this.amount = this.util.formatAmount(this.transferData.amount) + ' MMK';
                this.caculateAmount=this.voucherlist[i].value;
               
            }
        }
        this.totalAmount=this.util.formatAmount(parseFloat(this.caculateAmount)+parseFloat(this.bankCharges) ) + " MMK";
        console.log("VC"+this.totalAmount);
    
    }

    changeService(servicealternativecode) {
        this.subscriptionMsg = '';

        for (let i = 0; i < this.servicelist.length; i++) {
            if (servicealternativecode == this.servicelist[i].alternativecode) {
                this.transferData.servicealternativecode = this.servicelist[i].alternativecode;
                this.transferData.servicename = this.servicelist[i].servicecode;
            }
        }

        //get voucher list
        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        this.loading.present();

        let parameter = {
            useri: this.userdata.userID,
            sessionid: this.userdata.sessionID,
            alternativecode: this.transferData.servicealternativecode
        };

        this.http.post(this.ipaddress + '/serviceskynet/getVoucher', parameter).map(res => res.json()).subscribe(
            result => {
                if (result.code == "0000") {
                    let tempArray = [];
                    if (!Array.isArray(result.voucherlist)) {
                        tempArray.push(result.voucherlist);
                        result.voucherlist = tempArray;
                    }
                    this.voucherlist = result.voucherlist;

                    this.loading.dismiss();
                }
                else if (result.code == "0016") {
                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,
                        message: result.desc,
                        buttons: [{
                            text: 'OK',
                            handler: () => {
                                this.storage.remove('userData');
                                this.events.publish('login_success', false);
                                this.events.publish('lastTimeLoginSuccess', '');
                                this.navCtrl.setRoot(Login, {});
                                this.navCtrl.popToRoot();
                            }
                        }],
                        cssClass: 'warningAlert',
                    })
                    confirm.present();
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert("Warning!", result.desc);
                    this.loading.dismiss();
                }
            },
            error => {
                this.all.showAlert("Warning!", this.all.getErrorMessage(error));
                this.loading.dismiss();
            }
        );
    }

}
