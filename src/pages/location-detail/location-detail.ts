import { Component,ViewChild,ElementRef } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
/**
 * Generated class for the LocationDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;


@Component({
  selector: 'page-location-detail',
  templateUrl: 'location-detail.html',
  providers : [ChangelanguageProvider]
})
export class LocationDetail {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  data={lat:'',lng:'',name:'',address:''};
  lat:any;
  lng:any;

  latLng:any;
  font:string;
  showFont:any=[];
 /* textEng: any=["Shwe Bank"];
  textMyan : any = ["ရွှေဘဏ်"];*/
textEng: any=["mBanking"];
  textMyan : any = ["နေပြည်တော် စည်ပင်ဘဏ်"];

 /*textEng: any=["TCB mBanking"];
  textMyan : any = ["ထွန်းကော်မာရှယ်ဘဏ်"];*/
  /* textEng: any=["mBanking 360"];
  textMyan : any = ["mBanking 360"]; */
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation,public toastCtrl:ToastController,public changeLanguage:ChangelanguageProvider,public storage:Storage) {
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }
/*  changelanguage(lan) {
    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }

    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }

    }
  }*/
  ionViewDidLoad() {
    // console.log('ionViewDidLoad LocationDetail');
    this.data = this.navParams.get('data');
    // console.log("location="+JSON.stringify(this.data))
    this.lat=this.data.lat;
    this.lng=this.data.lng;
    this.loadMap();
  }
  loadMap(){

    //  this.geolocation.getCurrentPosition().then((position) => {
    try{
      this.latLng = new google.maps.LatLng(this.lat, this.lng);
      // console.log("lat  ===",this.lat);
      // console.log(" log ===",this.lng);
      let mapOptions = {
        center: this.latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position:this.latLng
      });

      let content = "<h6>"+this.data.name+"</h6>";

      let infoWindow = new google.maps.InfoWindow({
        content: content
      });

      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });
    }catch(e){
      // console.log("error>>"+JSON.stringify(e));
      let toast = this.toastCtrl.create({
        message:"Fail to show map.",
         duration: 3000,
         position: 'bottom',
      //  showCloseButton: true,
      //  closeButtonText: 'OK',
        dismissOnPageChange: true,
      });
      /*toast.onDidDismiss(() => {
       this.navCtrl.push(Page1);
       });*/
      toast.present(toast);
    }



    /* }, (err) => {
     // console.log(err);
     });*/

  }
}
