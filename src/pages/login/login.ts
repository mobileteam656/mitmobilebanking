import { Component, HostListener, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
//import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Device } from '@ionic-native/device';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CloudPage } from '../cloud/cloud';
import { DefaultPopOver } from '../default-pop-over/default-pop-over';
import { DemoPage } from '../demo/demo';
import { DomainPage } from '../domain/domain';
import { ForcechangePage } from '../forcechange/forcechange';
import { IpchangePage } from '../ipchange/ipchange';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { LoginOtpPage } from '../login-otp/login-otp';
import { MainHomePage } from '../main-home/main-home';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';

declare var window;

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ChangelanguageProvider]
})
export class Login {
  @ViewChild('user') user;
  @ViewChild('pass') pass;
  flag: string;
  signinData: any;
  font: string;
  loginData = {
    'userph': '',
    'userpsw': ''
  };
  uName: boolean = false;
  pWord: boolean = false;
  deviceID: string = '';
  location: string = '';
  overallText: any;
  popover: any;
  advertiseData: any = []
  noticount: any = '';
  /* textEng: any=['User ID','Password','Enter User ID','Enter Password','Login','Shwe Bank','Enter correct User ID'];
   textMyan : any = ['အိုင်ဒီ နံပါတ်','လျှို့ဝှက်နံပါတ်','အိုင်ဒီနံပါတ်ရိုက်ထည့်ပါ',"လျှို့ဝှက်နံပါတ်ရိုက်ထည့်ပါ",'ဝင်မည်','ရွှေဘဏ်','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ'];
 */
  /*  textEng: any=['User ID','Password','Enter User ID','Enter Password','Login','NSB Agent','Enter correct User ID'];
    textMyan : any = ['အိုင်ဒီ နံပါတ်','လျှို့ဝှက်နံပါတ်','အိုင်ဒီနံပါတ်ရိုက်ထည့်ပါ',"လျှို့ဝှက်နံပါတ်ရိုက်ထည့်ပါ",'ဝင်မည်','နေပြည်တော် စည်ပင်ဘဏ်','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ'];
  */
  /*textEng: any=['User ID','Password','Enter User ID','Enter Password','Login','TCB mBanking','Enter correct User ID'];
   textMyan : any = ['အိုင်ဒီ နံပါတ်','လျှို့ဝှက်နံပါတ်','အိုင်ဒီနံပါတ်ရိုက်ထည့်ပါ',"လျှို့ဝှက်နံပါတ်ရိုက်ထည့်ပါ",'ဝင်မည်','ထွန်းကော်မာရှယ်ဘဏ်','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ','မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ'];
 */

  textEng: any = ['User ID', 'Password', 'Enter User ID', 'Enter Password', 'Sign in', 'mBanking', 'Enter correct User ID', 'Access Code', 'PIN', 'Enter Access Code', 'Enter PIN'];
  textMyan: any = ['အိုင်ဒီနံပါတ်', 'လျှို့ဝှက်နံပါတ်', 'အိုင်ဒီနံပါတ် ရိုက်ထည့်ပါ', "လျှို့ဝှက်နံပါတ် ရိုက်ထည့်ပါ", 'ဝင်မည်', 'mBanking', 'မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ', 'Access Code', 'PIN', 'Enter Access Code', 'Enter PIN'];

  showFont: any = [];

  loginFormEng = [{ phonenumber: 'User ID', passwordno: 'Password', enternumber: 'Enter User ID', enterpassword: 'Enter Password', button: 'Login' }];
  loginFormMyan = [{ phonenumber: 'အိုင်ဒီနံပါတ်', passwordno: 'လျှို့ဝှက်နံပါတ်', enternumber: 'အိုင်ဒီနံပါတ် ရိုက်ထည့်ပါ', enterpassword: 'လျှို့ဝှက်နံပါတ် ရိုက်ထည့်ပါ', button: 'ဝင်မည်' }];

  ipaddress: string;
  public loading;
  loginmessage: string;
  showFooter: boolean = true;
  result: any;
  loginStatus: any;
  userID: any;
  isenabled: boolean;
  normalizePhone: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage, public changeLanguage: ChangelanguageProvider,
    private slimLoader: SlimLoadingBarService, public util: UtilProvider, public all: AllserviceProvider, public popoverCtrl: PopoverController, public platform: Platform, public geolocation: Geolocation, 
    //public geocoder: NativeGeocoder, 
    public locac: LocationAccuracy, public device: Device, public global: GlobalProvider, public keyboard: Keyboard,
    public alertCtrl: AlertController, public firebase: FirebaseAnalytics, private firebaseCrashlytics: FirebaseCrashlytics) {
  
    // this.events.subscribe('noti', (result) => {
    //   if (result != null)
    //     this.noticount = 1;
    //   this.result = result;
    //   // console.log("Ip address>>" + result);
    // });
    this.events.subscribe('ipaddress', (ipaddress) => {
      this.ipaddress = ipaddress;
      // console.log("Ip address>>" + this.ipaddress);
    });
    if (this.device.uuid != '' && this.device.uuid != undefined) {
      this.deviceID = this.device.uuid;
    }
    this.storage.get('loginStatus').then((result) => {
      if (result == '' || result == null || result == undefined) {
        this.loginStatus = '2';
      } else {
        this.loginStatus = result
      }
    });
    // this.geolocate();
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });

    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }
  geolocate() {
    /* this.loading = this.loadingCtrl.create({
       content: "Accessing location...",
       dismissOnPageChange: true
       //   duration: 3000
     });
     this.loading.present();*/
    let options = {
      enableHighAccuracy: true
    };
    this.locac.canRequest().then(() => {
      // console.log("res>>" + res);
      //   if (res) {
      // the accuracy option will be ignored by iOS
      this.locac.request(this.locac.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
        this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
          this.getcountry(position);
        }).catch((err) => {
          // this.loading.dismiss();
          console.error(JSON.stringify(err));
        });
      }, (error) => {
        // this.loading.dismiss();
        console.error(JSON.stringify(error));
      });
    })
  }

  getcountry(pos) {
    //  this.loading.dismiss();
    this.location = pos.coords.latitude + "/" + pos.coords.longitude;
    // this.geocoder.reverseGeocode(pos.coords.latitude, pos.coords.longitude).then(() => {
    // });
  }


  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
    //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  ionViewDidLeave() {

    // console.log("ionViewDidLeave Login apge");
    //  this.idle.stop();
    this.slimLoader.reset();
  }

  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(DefaultPopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data == "0") {
        this.navCtrl.push(IpchangePage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      } else if (data == "3") {
        window.open('https://play.google.com/store/apps/details?id=com.mit.mbanking', '_system');
      }

    });
  }


  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 0 };
        temp.data = data;
        temp.status = 0;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  presentPopoverCloud(ev) {

    this.popover = this.popoverCtrl.create(CloudPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data == 0) {
        this.presentPopoverDemo(ev);
      }
      else if (data == 1) {
        this.navCtrl.push(DomainPage);
      }
      else if (data == 2) {
        this.navCtrl.push(IpchangePage);
      }


    });
  }

  presentPopoverDemo(ev) {
    this.popover = this.popoverCtrl.create(DemoPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      this.events.publish("demo", data);
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad Login');
    this.keyboard.onKeyboardShow().subscribe(() => { this.showFooter = false })
    this.keyboard.onKeyboardHide().subscribe(() => { this.showFooter = true })
    //this.keyboard.disableScroll(false);
    this.checkNetwork();
    this.storage.get('ipaddress').then((ipaddress) => {
      if (ipaddress == '' || ipaddress == null) {
        this.storage.set('ipaddress', this.global.ipaddress);
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = ipaddress;
      }
      this.getAdLink();
      this.getLoginAuthentication();
    });
    /* this.network.onDisconnect().subscribe(data => {
       this.flag='none';
       let toast = this.toastCtrl.create({
         message:"Check your internet connection!",
         // duration: 2000,
         // position: 'bottom'
         showCloseButton: true,
         closeButtonText: 'OK'
       });
       toast.present(toast);
       // console.log(data)

     }, error => console.error(error));*/
  }
  inputChange(a) {
    if (a === 1) {
      this.uName = true;
      this.pWord = false;
    } else if (a === 2) {
      this.uName = false;
      this.pWord = true;
    }
    this.loginmessage = "";
  }
  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
  goLogin() {
    this.http.get(this.ipaddress + "/service002/getLoginAuthentication").map(res => res.json()).subscribe(result => {
      this.isenabled = true;
      if (result.code == '0000') {
        this.loginStatus = result.status;
        if (this.loginStatus == '1') {
          this.goLoginMO();
        } else if (this.loginStatus == '2' || this.loginStatus == '4') {
          this.goLoginUP();
          // console.log("login with userId and password");
        } else if (this.loginStatus == '3' || this.loginStatus == '5') {
          this.goLoginUPO();
          // console.log("login with userId, password and otp");
        }
      } else {
        this.all.showAlert('Warning!', result.desc);
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
      })
  }

  getLoginAuthentication() {


    this.http.get(this.ipaddress + "/service002/getLoginAuthentication").map(res => res.json()).subscribe(result => {

      if (result.code == '0000') {
        this.storage.remove('loginStatus');
        this.loginStatus = result.status;
        this.storage.set('loginStatus', this.loginStatus);
        if (this.loginStatus == '1') {
        } else if (this.loginStatus == '2') {
          // console.log("login with userId and password");
        } else if (this.loginStatus == '3') {
          // console.log("login with userId, password and otp");
        }

      } else {

        // this.advertiseData = [{"logo": "assets/fruit.jpg"}];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
    },
      error => {
        // console.log("error getAdLink =" + error.status);
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      })

  }
  goLoginUPO() {//login with userid, password and otp
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.loginData.userph == '' || this.loginData.userph == null || this.loginData.userph == undefined) {
      this.isenabled = false;
      if (this.loginStatus == 5) {
        this.loginmessage = this.showFont[10];
      } else {
        this.loginmessage = this.showFont[2];
      }
    } else if (this.loginData.userpsw == '' || this.loginData.userpsw == null || this.loginData.userpsw == undefined) {
      if (this.loginStatus == 5) {
        this.loginmessage = this.showFont[11];
      } else {
        this.loginmessage = this.showFont[3];
      }
      this.isenabled = false;
    } else {
      if (num.test(this.loginData.userph)) {
        this.isenabled = false;
        this.normalizePhone = this.util.normalizePhone(this.loginData.userph);
        if (this.normalizePhone.flag == true) {
          flag = true;
          this.loginmessage = '';
          this.loginData.userph = this.normalizePhone.phone;
          this.userID = this.util.removePlus(this.loginData.userph);
        } else {
          flag = true;
          this.loginmessage = '';
          this.userID = this.loginData.userph
        }
      } else {
        this.isenabled = false;
        flag = true;
        this.loginmessage = '';
        this.userID = this.loginData.userph
      }
      if (flag) {
        this.isenabled = true;
        this.slimLoader.start(() => {
        });
        let param = {
          userID: this.userID,
          password: this.loginData.userpsw,
          deviceID: this.deviceID,
          location: this.location,
        };
        this.http.post(this.ipaddress + '/service002/getLoginOTP', param).map(res => res.json()).subscribe(data => {
          this.isenabled = false;
          let otpData = { userID: this.userID, password: this.loginData.userpsw, sessionID: data.sessionID, otpcode: data.otpCode, rKey: data.rKey, sKey: data.sKey };
          let loginData = data;
          if (data.code == "0000") {
            // console.log("OTP Code is:" + JSON.stringify(otpData));
            /* this.navCtrl.push(LoginOtpPage, {
              data: otpData,
              userData: loginData
            }); */
            //open if you want to send otp code as sms(start)


            this.navCtrl.setRoot(LoginOtpPage, {
              data: otpData,
              userData: loginData
            })
            this.slimLoader.complete();
            //this.loading.dismiss();
          }
          else if (data.code == "0016") {
            this.logoutAlert(data.desc);
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.isenabled = false;
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.slimLoader.complete();
          });
      } else {
        this.isenabled = false;
        this.all.showAlert('Warning!', "Check your internet connection!");
      }
      this.network.onDisconnect().subscribe(() => {
        this.isenabled = false;
        this.flag = 'none';
        this.all.showAlert('Warning!', "Check your internet connection!");
      }, error => console.error(error));
    }
  }

  goLoginUP() {    
    //    if(this.flag=="success"){
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.loginData.userph == '' || this.loginData.userph == null || this.loginData.userph == undefined) {
      // this.loginmessage="Enter mobile number";
      this.loginmessage = this.showFont[2];
      this.isenabled = false;
    } else if (this.loginData.userpsw == '' || this.loginData.userpsw == null || this.loginData.userpsw == undefined) {
      //this.loginmessage="Enter password";
      this.loginmessage = this.showFont[3];
      this.isenabled = false;
    } else {
      if (num.test(this.loginData.userph)) {
        this.isenabled = false;
        if (this.loginData.userph.indexOf("+") == 0 && (this.loginData.userph.length == 12 || this.loginData.userph.length == 13 || this.loginData.userph.length == 11)) {
          //In the array!
          //this.register.regtype = "phone";
          this.loginmessage = '';
          flag = true;
        }
        else if (this.loginData.userph.indexOf("7") == 0 && this.loginData.userph.length == 9) {
          this.loginmessage = '';
          this.loginData.userph = '+959' + this.loginData.userph;
          flag = true;
        }
        else if (this.loginData.userph.indexOf("9") == 0 && this.loginData.userph.length == 9) {
          this.loginmessage = '';
          this.loginData.userph = '+959' + this.loginData.userph;
          flag = true;
        }
        else if (this.loginData.userph.indexOf("+") != 0 && this.loginData.userph.indexOf("7") != 0 && this.loginData.userph.indexOf("9") != 0 && (this.loginData.userph.length == 8 || this.loginData.userph.length == 9 || this.loginData.userph.length == 7)) {
          this.loginmessage = '';
          this.loginData.userph = '+959' + this.loginData.userph;
          flag = true;
        } else if (this.loginData.userph.indexOf("09") == 0 && (this.loginData.userph.length == 10 || this.loginData.userph.length == 11 || this.loginData.userph.length == 9)) {
          this.loginmessage = '';
          this.loginData.userph = '+959' + this.loginData.userph.substring(2);
          flag = true;
        }
        else if (this.loginData.userph.indexOf("959") == 0 && (this.loginData.userph.length == 11 || this.loginData.userph.length == 12 || this.loginData.userph.length == 10)) {
          this.loginmessage = '';
          this.loginData.userph = '+959' + this.loginData.userph.substring(3);
          flag = true;
        }
        else {
          flag = false;
          this.loginmessage = this.showFont[6];
        }
      }
      else {
        this.isenabled = false;
        flag = true;
        this.loginmessage = '';
        this.userID = this.loginData.userph
        /* this.isenabled = false;
        flag = false;
        this.loginmessage = this.showFont[6]; */
      }
      if (flag) {
        this.isenabled = true;
        let ph = this.loginData.userph.replace(/\+/g, '');
        this.slimLoader.start(() => {
          // console.log('Loading complete');
        });
        /* this.loading = this.loadingCtrl.create({
           content: "Please wait...",
           dismissOnPageChange: true
           //   duration: 3000
         });
         this.loading.present();*/
        let iv = this.all.getIvs();
        let dm = this.all.getIvs();
        let salt = this.all.getIvs();
        let param = {
          userID: ph,
          password: this.loginData.userpsw,
          deviceID: this.deviceID,
          location: this.location,
          version: this.global.version,
          appCode: this.global.appCode,
          "iv": iv, "dm": dm, "salt": salt
        };
        param.password = this.all.getEncryptText(iv, salt, dm, param.password)
        // console.log("param signin  data>>" + JSON.stringify(param));

        // this.firebase.logEvent('login_userid_request', {
        //   "userid": ph
        // }).then((res: any) => {
        //   console.log(res);
        // }).catch((error: any) => {
        //   console.log(error);
        // });
       // const crashlytics = this.firebaseCrashlytics.initialize();
        //this.firebaseCrashlytics.initialize();
       // this.firebaseCrashlytics.logException('No internet connection');

        this.http.post(this.ipaddress + '/service001/mobileSignin', param).map(res => res.json()).subscribe(
          data => {
            //lastTimeLoginFailed,lastTimeLoginSuccess
            //console.log("response  data>>" + JSON.stringify(data));

            // this.firebase.logEvent('login_userid_response', {
            //   "userid": ph,
            //   "code": data.code,
            //   "desc": data.desc
            // }).then((res: any) => {
            //   console.log(res);
            // }).catch((error: any) => {
            //   console.log(error);
            // });

            this.isenabled = false;
            let login = data.code;

            if (login == '0000') {
              let tempArray = [];
              //  if (data.debitAcc != null) {

              if (!Array.isArray(data.debitAcc)) {
                tempArray.push(data.debitAcc);
                data.debitAcc = tempArray;
              }
              let userData = {
                sessionID: data.sessionID,
                userID: data.userID,
                userName: data.userName
              };
              this.storage.get('userData').then((result) => {
                if (result == null || result == '') {
                  this.storage.set('userData', userData);
                }
                else {
                  this.storage.remove('userData');
                  this.storage.set('userData', userData);
                }
                //  this.signinData=data;
                this.events.publish('login_success', true);
                this.events.publish('lastTimeLoginSuccess', data.lastTimeLoginSuccess);
                this.slimLoader.complete();
                this.navCtrl.setRoot(MainHomePage, {
                  data: 'login'
                });
              });
            }
            else if (login == '0001') {
              let alert = this.alertCtrl.create({
                title: 'Information',
                subTitle: data.desc,
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      let temp1 = { userID: data.userID, sessionID: data.sessionID };
                      this.navCtrl.setRoot(ForcechangePage, {
                        data: temp1
                      });
                    }
                  },
                ],
                enableBackdropDismiss: false
              });
              alert.present();
              this.slimLoader.complete();
            }//forcechangepwd
            else if (login == '0002') {
              let alert = this.alertCtrl.create({
                title: 'Information',
                subTitle: data.desc,
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      let temp1 = { userID: data.userID, sessionID: data.sessionID };
                      this.navCtrl.setRoot(ForcechangePage, {
                        data: temp1
                      });
                    }
                  },
                  {
                    text: 'CANCEL',
                    handler: () => {
                      let tempArray = [];
                      if (!Array.isArray(data.debitAcc)) {
                        tempArray.push(data.debitAcc);
                        data.debitAcc = tempArray;
                      }
                      let userData = {
                        sessionID: data.sessionID,
                        userID: data.userID,
                        userName: data.userName
                      };
                      this.storage.get('userData').then((result) => {
                        if (result == null || result == '') {
                          this.storage.set('userData', userData);
                        }
                        else {
                          this.storage.remove('userData');
                          this.storage.set('userData', userData);
                          // console.log("set userdata : " + JSON.stringify(data));

                        }
                        this.events.publish('login_success', true);
                        this.events.publish('lastTimeLoginSuccess', data.lastTimeLoginSuccess);
                        this.slimLoader.complete();

                        this.navCtrl.setRoot(MainHomePage, {
                          data: 'login'
                        });
                      });
                    }
                  },
                ],
                enableBackdropDismiss: false
              });
              alert.present();
            }//forcechangepwd
            else if (login == '0015') {    //force change password
              this.slimLoader.complete();
              let temp = { userID: ph, sessionID: data.sessionID };
              this.navCtrl.push(ForcechangePage, {
                data: temp
              });
            }
            else {            
              // const crashlytics = this.firebaseCrashlytics.initialise();
              // crashlytics.logException('Internet conection error..');
              this.loginData.userpsw = '';
              let toast = this.toastCtrl.create({
                message: data.desc,
                duration: 3000,
                position: 'bottom',
                // showCloseButton: true,
                dismissOnPageChange: true,
                //  closeButtonText: 'OK'
              });
              toast.present(toast);
              this.slimLoader.complete();
            }
          },
          error => {
            const crashlytics = this.firebaseCrashlytics.initialise();
            crashlytics.logException('Mobile Login');
            //const crashlytics = this.firebaseCrashlytics.initialize();
            this.isenabled = false;
            // console.log("signin error=" + error.status);
            /* ionic App error
             ............001) url link worng, not found method (404)
             ........... 002) server not response (500)
             ............003) cross fillter not open (403)
             ............004) server stop (-1)
             ............005) app lost connection (0)
             */
            let code;
            if (error.status == 404) {
              code = '001';
            }
            else if (error.status == 500) {
              code = '002';
            }
            else if (error.status == 403) {
              code = '003';
            }
            else if (error.status == -1) {
              code = '004';
            }
            else if (error.status == 0) {
              code = '005';
            }
            else if (error.status == 502) {
              code = '006';
            }
            else {
              code = '000';
            }
            let msg = "Can't connect right now. [" + code + "]";
            let toast = this.toastCtrl.create({
              message: msg,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.slimLoader.complete();

            this.firebase.logEvent('login_userid_error', {
              "userid": ph,
              "error": msg
            }).then((res: any) => {
              console.log(res);
            }).catch((error: any) => {
              console.log(error);
            });
          }
        );
      }
    }
    /*   }
     else{
         let toast = this.toastCtrl.create({
           message:'No internet connection. Please check it!',
           // duration: 2000,
           // position: 'bottom'
           showCloseButton: true,
           dismissOnPageChange: true,
           closeButtonText: 'OK'
         });
         toast.present(toast);
       }*/

  }

  goLoginMO() {//login with phone number and otp
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.loginData.userph == '' || this.loginData.userph == null || this.loginData.userph == undefined) {
      this.loginmessage = this.showFont[2];
      this.isenabled = false;
    } else {
      if (num.test(this.loginData.userph)) {
        this.isenabled = false;
        this.normalizePhone = this.util.normalizePhone(this.loginData.userph);
        if (this.normalizePhone.flag == true) {
          flag = true;
          this.loginmessage = '';
          this.loginData.userph = this.normalizePhone.phone;
          this.userID = this.util.removePlus(this.loginData.userph);
        } else {
          flag = true;
          this.loginmessage = '';
          this.userID = this.loginData.userph
        }
      }
      if (flag) {
        this.isenabled = true;
        this.slimLoader.start(() => {
        });
        let param = {
          phone: this.userID,
          userID: this.userID,
          password: '',
          deviceID: this.deviceID,
          location: this.location,
        };
        this.http.post(this.ipaddress + '/service002/mobileloginbyphone', param).map(res => res.json()).subscribe(data => {
          this.isenabled = false;
          let otpData = { userID: this.userID, password: this.loginData.userpsw, sessionID: data.sessionID, otpcode: data.otpCode, rKey: data.rKey, sKey: data.sKey };
          let loginData = data;
          if (data.code == "0000") {
            // console.log("OTP Code is:" + JSON.stringify(otpData));
            /* this.navCtrl.push(LoginOtpPage, {
              data: otpData,
              userData: loginData
            }); */
            //open if you want to send otp code as sms(start)


            this.navCtrl.setRoot(LoginOtpPage, {
              data: otpData,
              userData: loginData,
              from: 'phone_otp'
            })
            this.slimLoader.complete();
            //this.loading.dismiss();
          }
          else if (data.code == "0016") {
            this.logoutAlert(data.desc);
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.slimLoader.complete();
            this.isenabled = false;
          });
      } else {
        this.all.showAlert('Warning!', "Check your phone number!");
        this.isenabled = false;
      }
      this.network.onDisconnect().subscribe(() => {
        this.flag = 'none';
        this.all.showAlert('Warning!', "Check your internet connection!");
        this.isenabled = false;
      }, error => console.error(error));
    }
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    // console.log("call number>>" + passedNumber);
    window.location = "tel:" + passedNumber;
  }

  onChange(i, a) {
    if (i == "09") {
      this.loginData.userph = "+959";
    }
    else if (i == "959") {
      this.loginData.userph = "+959";
    }
    if (a == 1) {
      this.uName = true;
      this.pWord = false;
    } else {
      this.uName = false;
      this.pWord = true;
    }
  }

  getAdLink() {
    // console.log("tempArray testing>>" + JSON.stringify(this.advertiseData));

    this.http.get(this.ipaddress + "/service001/getAdLink").map(res => res.json()).subscribe(result => {
      // console.log("return data getAdLink =" + JSON.stringify(result));

      if (result.code == '0000') {
        let tempArray = [];

        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }

        this.advertiseData = result.dataList;

        // console.log("tempArray>>" + JSON.stringify(result.dataList));

      } else {

        // this.advertiseData = [{"logo": "assets/fruit.jpg"}];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }

    },
      error => {

        // console.log("error getAdLink =" + error.status);
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      })
  }

  showNoti() {
    if (this.noticount != '') {
      this.noticount = '';
      let confirm = this.alertCtrl.create({
        title: this.result.notiTitle,
        message: this.result.notiDesc,
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            handler: () => {
              // console.log('Cancel clicked');
            }
          },
        ]
      });
      confirm.present();
    }
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }

  @HostListener('keydown', ['$event']) onInputChange(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      // console.log(this.user.active);
      if (this.uName == true) {
        this.pass.setFocus();
        this.uName = false;
        this.pWord = true;
      } else if (this.pWord == true) {
        //this.keyboard.close();
        this.pWord = false;
      }
    }
  }

  active(a) {
    if (a === 1) {
      this.uName = true;
      this.pWord = false;
    } else if (a === 2) {
      this.uName = false;
      this.pWord = true;
    }
  }

  // crash(){
  //   this.firebaseCrashlytics.crash();
  // }

}
