import 'rxjs/add/operator/map';

import { Component, HostListener } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { BeneficiaryConfirmPage } from '../beneficiary-confirm/beneficiary-confirm';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { Keyboard } from '@ionic-native/keyboard';
import { LovbyPage } from '../lovbypage/lovbypage';
declare var window;
@Component({
  selector: 'page-beneficiary-add',
  templateUrl: 'beneficiary-add.html',
  providers: [Changefont]
})
//အိုင်ဒီနံပါတ်
export class BeneficiaryAddPage {
  textMyan: any = ["လက်ခံသူ ထည့်သွင်းခြင်း", "လက်ခံသူ အချက်အလက်", "လက်ခံသူ အမည်တို ", "အကောင့်နံပါတ်", "လက်ခံသူ အမည်", "လက်ခံသူ အီးမေးလ်", "လက်ခံသူ ဖုန်းနံပါတ်", "ပယ်ဖျက်မည်", "စာရင်းသွင်းမည်", "အကောင့်နံပါတ် ဖြည့်သွင်းပါ", "လက်ခံသူ အမည်ဖြည့်သွင်းပါ", "အကောင့်နံပါတ် စစ်ဆေးပါ", "အမည် ဖြည့်သွင်းပါ", "လက်ခံသူ အမျိုးအစား", "Branch", "ဘဏ်အမည်", "ဘဏ်ကုဒ်", "ဘဏ်လိပ်စာ", "လက်ခံသူ လိပ်စာ", "ဖုန်းနံပါတ် ဖြည့်သွင်းပါ", "လိပ်စာ ဖြည့်သွင်းပါ", "ဘဏ်အချက်အလက် ဖြည့်သွင်းပါ"];
  textEng: any = ["Add Beneficiary", "Beneficiary Info", "Beneficiary Short Name(eg 'MMA')", "Account Number", "Beneficiary Name", "Beneficiary Email", "Beneficiary Phone", "CANCEL", "SUBMIT", "Account Number is required!", "Beneficiary Name is required!", "Please check Account Number", "Beneficiary Name is required", "Beneficiary Type", "Branch", "Bank Name", "Bank Code", "Bank Address", "Beneficiary Address", "Beneficiary Phone Number is required", "Beneficiary Address is required", "Bank Information is required"];
  showFont: string[] = [];
  font: string = '';
  errormsg1: any;
  errormsg2: any;
  errormsg3: any;
  errormsg4: any;
  errormsg5: any;
  errormsg6: any;
  errormsg7: any;
  errormsg8: any;
  obj: any = { beneficiaryID: '', accountNo: '', name: '', email: '', phone: '', beneficiaryType: '', branch: '', bankName: '', bankCode: '', bankAddress1: '', bankAddress2: '', bankAddress3: '', bankAddress4: '', bankBranch: '', beneAddress: '' };
  userdata: any;
  ipaddress: any;
  isChecked: boolean = false;
  beneficiaryType: any = [];
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  popover: any;
  txnType: any;
  modal: any;
  hardwareBackBtn: any = true;
  flag: string;
  beneTypeValue: string = '';
  accNo: any;
  checkAccNo: any;
  bankList: any = [];
  reqData: any = { "value": [], "type": 0, "title": "", 'ans': '' };
  bankAddress: any;
  beneType: any;
  bindingBeneficiary:any=[];
  //this.userProfile = this.navParams.get("data");
  constructor(public navCtrl: NavController, public navParams: NavParams, public all: AllserviceProvider,
    public events: Events, public changefont: Changefont, public changeLanguage: ChangelanguageProvider,
    public storage: Storage, public alertCtrl: AlertController, public http: Http,
    public toastCtrl: ToastController, private slimLoader: SlimLoadingBarService,
    public loadingCtrl: LoadingController, public popoverCtrl: PopoverController,
    public platform: Platform, public global: GlobalProvider, public idle: Idle,
    public network: Network, public keyboard: Keyboard) {

    this.bindingBeneficiary=this.navParams.get('data'); 
    this.beneType = this.navParams.get('beneType');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.storage.get('userData').then((data) => {
      this.userdata = data;
      if(this.bindingBeneficiary!=null)
    {
    //this.obj=this.bindingBeneficiary;
    // this.obj.beneficiaryID=this.bindingBeneficiary.name;
     this.obj.accountNo=this.bindingBeneficiary.toAcc;
    //  this.obj.branch=this.bindingBeneficiary.branch;
    //  this.obj.name=this.bindingBeneficiary.name;
    // this.checkAccount();
    }
      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.getBeneficiaryType();
        this.getBankList();
      });
    });

    
    // this.keyboard.onKeyboardHide().subscribe(() => { this.checkAccNo.setFocus() })
  }

  ionViewDidLoad() {


  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  checkAccount() {
    let flag = true;
    if (this.obj.accountNo != undefined && this.obj.accountNo != '') {
      flag = true;
    } else {
      this.errormsg2 = this.showFont[9];
      flag = false;
    }
    if (flag) {
      this.errormsg2 = '';
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, accountNo: this.obj.accountNo, type: 1 };

      this.http.post(this.ipaddress + '/service002/checkAccount', param).map(res => res.json()).subscribe(data => {
        //console.log("response check account : " + JSON.stringify(data));
        if (data.code == "0000") {
          this.isChecked = true;
          this.slimLoader.complete();
          this.obj.name = data.accountName;
          this.obj.branch = data.branch;
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
          this.slimLoader.complete();
        }
        else {
          this.all.showAlert("Warning!", data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    }
  }

  cancel() {
    this.obj = {};
    this.beneTypeValue = this.beneficiaryType[0].value;
    this.obj.beneficiaryType = this.beneficiaryType[0];
    this.isChecked = false;
    this.errormsg1 = '', this.errormsg2 = '', this.errormsg3 = '', this.errormsg4 = '', this.errormsg5, this.errormsg6 = '', this.errormsg7 = '', this.errormsg8 = '';

  }
  setBeneficiaryType(b) {
    for (let i = 0; i < this.beneficiaryType.length; i++) {
      if (b == this.beneficiaryType[i].value) {
        this.obj.beneficiaryType = this.beneficiaryType[i];
      }
    }
    if (b = '2') {
      this.obj.bankName = '';
      this.obj.bankCode = '';
      this.obj.bankAddress1 = '';
      this.obj.bankAddress2 = '';
      this.obj.bankAddress3 = '';
      this.obj.bankAddress4 = '';
      this.bankAddress = '';
      this.obj.bankBranch = '';
      this.errormsg6 = '', this.errormsg7 = '', this.errormsg8 = '';
    }
  }
  add() {
    this.checkNetwork();
    if (this.flag == "success") {
      let flag1 = true;
      let flag2 = true;
      let flag3 = true;
      let flag4 = true;
      let flag5 = true, flag6 = true, flag7 = true, flag8 = true;
      if (this.obj.beneficiaryID != '' && this.obj.beneficiaryID != null && this.obj.beneficiaryID != undefined) {
        flag1 = true;
        this.errormsg1 = '';
      } else {
        flag1 = false;
        this.errormsg1 = this.showFont[10];
      }
      if (this.obj.accountNo != '' && this.obj.accountNo != null && this.obj.accountNo != undefined) {
        flag2 = true;
        this.errormsg2 = '';
      } else {
        flag2 = false;
        this.errormsg2 = this.showFont[9];
      }
      if (this.obj.name != '' && this.obj.name != null && this.obj.name != undefined) {
        flag3 = true;
        this.errormsg3 = '';
      } else {
        flag3 = false;
        this.errormsg3 = this.showFont[12];
      }
      if (this.beneTypeValue == '2') {
        if (this.isChecked) {
          flag4 = true;
          this.errormsg2 = '';
        } else {
          flag4 = false;
          this.errormsg2 = this.showFont[11];
        }
      }
      if (this.beneTypeValue == '3') {
        if (this.obj.phone != '' && this.obj.phone != null && this.obj.phone != undefined) {
          flag5 = true;
          this.errormsg6 = '';
        } else {
          flag5 = false;
          this.errormsg6 = this.showFont[19];
        }
        if (this.obj.beneAddress != '' && this.obj.beneAddress != null && this.obj.beneAddress != undefined) {
          flag6 = true;
          this.errormsg7 = '';
        } else {
          flag6 = false;
          this.errormsg7 = this.showFont[20];
        }
        if (this.obj.bankCode != '' && this.obj.bankCode != null && this.obj.bankCode != undefined) {
          flag7 = true;
          this.errormsg8 = '';
        } else {
          flag7 = false;
          this.errormsg8 = this.showFont[21];
        }

      }
      if (flag1 && flag2 && flag3 && flag4 && flag5 && flag6 && flag7) {
        let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: 12, merchantID: '' };
        this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            this.slimLoader.complete();
            this.navCtrl.push(BeneficiaryConfirmPage, {
              data: this.obj,
              otp: data.rKey,
              sKey: data.sKey
            })
          }
          else if (data.code == "0016") {
            let confirm = this.alertCtrl.create({
              title: 'Warning!',
              enableBackdropDismiss: false,
              message: data.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {

                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {
                    });
                    this.navCtrl.popToRoot();
                  }
                }
              ],
              cssClass:"warningAlert"
            })
            confirm.present();
            this.slimLoader.complete();
          }
          else {
            this.all.showAlert("Warning!", data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.all.showAlert("Warning!", this.all.getErrorMessage(error));
            this.slimLoader.complete();
          });
      }
    } else {
      this.all.showAlert("Warning!", "Check your internet connection!");
    }
  }

  inputChange(data) {
    switch (data) {
      case 1:
        this.errormsg1 = "";
        break;
      case 2:
        this.isChecked = false;
        this.errormsg2 = "";
        break;
      case 3:
        this.errormsg6 = "";
        break;
      case 4:
        this.errormsg7 = "";
        break;
      case 5:
        this.errormsg3 = "";
        break;
      default: break;
    }
  }

  getBankList() {
    this.checkNetwork();

    if (this.flag == "success") {
      this.errormsg2 = '';
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service002/getBankList', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(data.bankInfo)) {
            tempArray.push(data.dataList);
            data.bankInfo = tempArray;
          }
          this.bankList = data.bankInfo;
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {

                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
          this.slimLoader.complete();
        }
        /* else {
          this.all.showAlert("Warning!",data.desc);
          this.slimLoader.complete();
        } */
      },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    } else {
      this.all.showAlert("Warning!", "Check your internet connection!");
    }
  }

  getBeneficiaryType() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.errormsg2 = '';
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service002/getBeneficiaryType', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(data.dataList)) {
            tempArray.push(data.dataList);
            data.dataList = tempArray;
          }
          this.beneficiaryType = data.dataList;
          this.obj.beneficiaryType = this.beneficiaryType[0];
          if(this.beneType==undefined || this.beneType==''){
            this.beneTypeValue = this.obj.beneficiaryType.value;
          }else {
            this.beneTypeValue=this.beneType
          }
         
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {

                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
          this.slimLoader.complete();
        }
        else {
          this.all.showAlert("Warning!", data.desc);
          this.slimLoader.complete();
        }
      },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    } else {
      this.all.showAlert("Warning!", "Check your internet connection!");
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {


      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        // this.navCtrl.push(Language)
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {


      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    //if(this.hardwareBackBtn)
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {
      // let hardwareBackBtn = true;
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;

            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert("Warning!", data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'BeneficiaryAddPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning!.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'BeneficiaryAddPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  goBankList() {
    if (this.bankList.length < 1 || this.bankList[0] == undefined) {
      this.all.showAlert("Warning!", "Bank list not found.");
    } else {
      for (let i = 0; i < this.bankList.length; i++) {
        this.reqData.value.push({ code: this.bankList[i].bankCode, desc: this.bankList[i].bankName });
      }
      this.reqData.type = '3';
      this.reqData.title = 'Bank List';
      this.navCtrl.push(LovbyPage, {
        data: this.reqData
      });
    }

  }

  ionViewWillEnter() {

    if (this.reqData.ans != undefined && this.reqData != '' && this.reqData != null && this.reqData.title == 'Bank List') {
      this.bankAddress = '';
      for (let i = 0; i < this.bankList.length; i++) {
        if (this.reqData.ans === this.bankList[i].bankCode) {
          this.obj.bankName = this.bankList[i].bankName;
          this.obj.bankCode = this.bankList[i].bankCode;
          this.obj.bankAddress1 = this.bankList[i].bankAddress1;
          this.obj.bankAddress2 = this.bankList[i].bankAddress2;
          this.obj.bankAddress3 = this.bankList[i].bankAddress3;
          this.obj.bankAddress4 = this.bankList[i].bankAddress4;
          this.obj.bankBranch = this.bankList[i].branchCode;
        }
      }
      if (this.obj.bankAddress1 != '') {
        this.bankAddress += this.obj.bankAddress1
      }
      if (this.obj.bankAddress2 != '') {
        this.bankAddress += ", " + this.obj.bankAddress2
      }
      if (this.obj.bankAddress3 != '') {
        this.bankAddress += ", " + this.obj.bankAddress3
      }
      if (this.obj.bankAddress4 != '') {
        this.bankAddress += ", " + this.obj.bankAddress4
      }
      this.reqData.title = '';
      this.reqData.value = [];
      this.reqData.ans = '';
      this.errormsg8 = '';
    }
  }


}
