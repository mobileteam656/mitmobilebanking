import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, PopoverController, Select } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { PopoverPage } from '../popover-page/popover-page';
import { TaxPaymentConfirmPage } from '../tax-payment-confirm/tax-payment-confirm';
import { TaxBusinessProfilePage } from '../tax-business-profile/tax-business-profile';
import { TaxInformationDetailPage } from '../tax-information-detail/tax-information-detail';

declare var window;

@Component({
  selector: 'page-tax-payment',
  templateUrl: 'tax-payment.html',
})
export class TaxPaymentPage {
  @ViewChild('myselect') select: Select;
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အခွန်နံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "လုပ်ဆောင်မည်", "အခွန်ပေးဆောင်ခြင်း", "ပယ်ဖျက်မည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "ပရိုဖိုင်နာမည်", "လုပ်ငန်း", "ကုမ္ပဏီအမျိုးအစား", "လုပ်ငန်းကုဒ်", "ဖုန်းနံပါတ်", "အခွန်ငွေပမာဏ", "လုပ်ငန်းအချက်အလက်", "အခွန်အမျိုးအစား", "ပေးဆောင်ခြင်းအမျိုးအစား", "အခွန်နှစ်ကာလ","အခွန်ကာလ"];
  textEng: any = ["From Account No.", "TIN No.", "Amount", "Narrative", "SUBMIT", "Tax Payment", "CANCEL", "Account Balance", "Profile Name", "Main Business", "Company Type", "Industry Code", "Phone Number", "Tax Amount", "Business Profile", "Tax Type", "Payment Type", "Income Year",  "Tax Period"];
  textErrorEng: any = ["Please choose account number.", "Please fill TIN number.", "Please fill amount.", "Please check TIN Number", "Insufficient Balance","Please choose Tax Type","Please choose Payment Type","Please choose Income Year","Please choose Tax Period"];
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး အခွန်နံပါတ် ရိုက်ထည့်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "အခွန်နံပါတ် စစ်ဆေးပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ","ကျေးဇူးပြုပြီး အခွန်အမျိုးအစား ရွေးချယ်ပါ","ကျေးဇူးပြုပြီး ပေးဆောင်ခြင်းအမျိုးအစား ရွေးချယ်ပါ","ကျေးဇူးပြုပြီး အခွန်နှစ်ကာလ ရွေးချယ်ပါ","ကျေးဇူးပြုပြီး အခွန်ကာလ ရွေးချယ်ပါ",];
  textError: string[] = [];
  textData: any = [];
  font: any = '';
  fromaccMsg: any;
  tinMsg: any;
  amountMsg: any;
  taxTypeMsg:any;
  paymentMsg:any;
  incomeMsg:any;
  periodMsg:any;
  reqData: any = { "value": [], "type": 0, "title": "", 'ans': '' };
  taxData: any = {};
  isChecked: boolean;
  accountBal: any;
  popover: any;
  loading: any;
  ipaddress: any;
  userData: any;
  fromAccountlist: any = [];
  amountBal: any;
  paymentList: any = [];
  taxList: any = [];
  businessList: any = [];
  incomeYear: any = [];
  taxPeriod:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public popoverCtrl: PopoverController, public storage: Storage,
    public http: Http, public platform: Platform, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public changefont: Changefont, public global: GlobalProvider, public all: AllserviceProvider,
    public util: UtilProvider) {
    this.accountBal = "";
    this.taxData = this.navParams.get("data");
    this.paymentList = this.navParams.get("paymentList");
    this.taxList = this.navParams.get("taxList");
    this.businessList = this.navParams.get("businessList");
    this.incomeYear = this.navParams.get("incomeYear");
    this.taxPeriod=this.navParams.get("taxPeriod");
    this.amountBal = "";
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.getAccountSummary();
      })
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  checkTinNo() {
    let flag = false;
    if (this.taxData.tinNo != undefined && this.taxData.tinNo != '') {
      flag = true;
    } else {
      flag = false;
      this.tinMsg = this.textError[1];
    }
    if (flag) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      this.tinMsg = '';
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, tinNo: this.taxData.tinNo, type: 1 };
      this.http.post(this.ipaddress + '/service002/checkTinNo', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.isChecked = true;
          this.taxData.profileName = data.profileName
          this.taxData.mainBusiness = data.mainBusiness
          this.taxData.companyType = data.companyType
          this.taxData.industryCode = data.industryCode
          this.taxData.phoneNumber = data.phoneNumber
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
          this.taxData.name = '';
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  validation() {
    /* amountMsg: any;
    taxTypeMsg:any;
    paymentMsg:any;
    incomeMsg:any;
    periodMsg:any; */
    let f1 = false, f2 = false, f3 = false;//f4=false, f5=false, f6=false, f7=false
    if (this.taxData.fromAcc != null && this.taxData.fromAcc != '' && this.taxData.fromAcc != undefined) {
      this.fromaccMsg = '';
      f1 = true;
    }
    else {
      this.fromaccMsg = this.textError[0];
      f1 = false;
    }

   /*  if (this.taxData.taxTypeCode != null && this.taxData.taxTypeCode != '' && this.taxData.taxTypeCode != undefined) {
      this.taxTypeMsg = ''
      f4 = true;
    }
    else {
      this.taxTypeMsg = this.textError[5];
      f4 = false;
    }

    if (this.taxData.paymentTypeCode != null && this.taxData.paymentTypeCode != '' && this.taxData.paymentTypeCode != undefined) {
      this.paymentMsg = ''
      f5 = true;
    }
    else {
      this.paymentMsg = this.textError[6];
      f5 = false;
    }

    if (this.taxData.incomeYearCode != null && this.taxData.incomeYearCode != '' && this.taxData.incomeYearCode != undefined) {
      this.incomeMsg = ''
      f6 = true;
    }
    else {
      this.incomeMsg = this.textError[7];
      f6 = false;
    }

    if (this.taxData.taxPeriodCode != null && this.taxData.taxPeriodCode != '' && this.taxData.taxPeriodCode != undefined) {
      this.periodMsg = ''
      f7 = true;
    }
    else {
      this.periodMsg = this.textError[8];
      f7 = false;
    } */

    if (this.taxData.taxAmount != null && this.taxData.taxAmount != '' && this.taxData.taxAmount != undefined) {
      let amount = parseInt(this.taxData.taxAmount);
      if (this.taxData.taxAmount.substring(0, 1) == "-" || amount == 0) {
        this.amountMsg = this.textError[2];
        f3 = false;
      } else if (isNaN(amount)) {
        this.amountMsg = this.textError[2];
        f3 = false;
      } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.taxData.taxAmount)) {
        this.amountMsg = this.textError[4];
        f3 = false;
      } else {
        this.amountMsg = '';
        f3 = true;
      }
    }
    else {
      this.amountMsg = this.textError[2];
      f3 = false;
    }
    if (f1 && f3 ) {//&& f4 && f5 && f6 && f7
      this.goTransfer();
    } else {
      this.all.showAlert('Warning!', 'Please check require field.');
    }
  }

  goTransfer() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 1, merchantID: this.taxData.merchantID };
    this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.push(TaxPaymentConfirmPage, {
          data: this.taxData,
          otp: data.rKey,
          sKey: data.sKey,
        });
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  inputChange(data) {
    if (data == 1) {
      this.tinMsg = "";
    } else if (data == 2) {
      this.amountMsg = "";
    }
  }

  changeAcc(s, data) {
    if (s == 1) {
      this.fromaccMsg = '';
      for (let i = 0; i < this.fromAccountlist.length; i++) {
        if (data == this.fromAccountlist[i].depositAcc) {
          this.amountBal = this.fromAccountlist[i].avlBal;
          this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
        }
      }
    }
  }

  getData(i, data) {
    if (i == 1) {
      this.taxData.taxOfficeName = data.taxOfficeName;
      this.taxData.companyType = data.companyType;
      this.taxData.industryCode = data.industryCode;
      this.taxData.typeOfBusi = data.typeOfBusi;
      this.taxData.taxType = data.taxType;
      this.taxData.mdaccNo = data.mdaccNo;
      this.taxData.creditorBranchName = data.creditorBranchName;
      this.taxData.creditorBranchCode = data.creditorBranchCode;
    } else if (i == 2) {
      this.paymentMsg="";
     
      this.taxData.paymentTypeCode = data.paymentTypeCode;
      this.taxData.paymentTypeDesc = data.paymentTypeDesc;
    }
    else if (i == 3) {
      this.taxTypeMsg="";
    /*   for(let i;i<this.taxList.length;i++){
        if(data==this.taxList[i].)
      } */
      this.taxData.taxTypeCode = data.taxTypeCode;
      this.taxData.taxTypeDesc = data.taxTypeDesc;
    }
    else if (i == 4) {
      this.incomeMsg="";
      this.taxData.incomeYearCode = data.incomeYearCode;
      this.taxData.incomeYearDesc = data.incomeYearDesc;
    }
    else if (i == 5) {
      this.periodMsg="";
      this.taxData.taxPeriodCode = data.taxPeriodCode;
      this.taxData.taxPeriodDesc = data.taxPeriodDesc;
    }
  }

  reset() {
    this.taxData.fromAcc = "";
    this.taxData.taxAmount = "";
    this.taxData.narrative = "";
    this.taxData.industryCode = "";
    this.taxData.companyType = "";
    this.taxData.creditorBranchCode = "";
    this.taxData.creditorBranchName = "";
    this.taxData.typeOfBusi = "";
    this.taxData.mdaccNo = "";
    this.taxData.taxOfficeName = "";
    this.taxData.taxType = "";
    this.fromaccMsg = '';
    this.amountMsg = '';
    this.accountBal = "";
    this.amountBal = "";
    this.taxData.paymentTypeCode = "";
    this.taxData.paymentTypeDesc = "";
    this.taxData.taxTypeCode = "";
    this.taxData.taxTypeDesc = "";
    this.taxData.incomeYearCode = "";
    this.taxData.incomeYearDesc = "";
    this.taxData.taxPeriodCode = "";
    this.taxData.taxPeriodDesc = "";
    this.taxTypeMsg="";
    this.paymentMsg="";
    this.incomeMsg="";
    this.periodMsg="";
    this.navCtrl.pop();
    /* this.taxData.fromAcc = "";
    this.taxData.toAcc = "";
    this.taxData.amount = "";
    this.taxData.refNo = "";
    this.taxData.profileName = "";
    this.taxData.mainBusiness = "";
    this.taxData.companyType = "";
    this.taxData.industryCode = "";
    this.taxData.phoneNumber = "";
    this.taxData.taxAmount = "";
    this.fromaccMsg = '';
    this.tinMsg = '';
    this.amountMsg = '';
    this.accountBal = "";
    this.isChecked = false; */
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }

  goInformation() {
    this.navCtrl.push(TaxInformationDetailPage, {
      taxData: this.taxData
    });
  }

  goBusinessProfile() {
    this.navCtrl.push(TaxBusinessProfilePage, {
      data: this.businessList,
      taxData: this.taxData
    });
  }
}
