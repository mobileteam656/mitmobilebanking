import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { GlobalProvider } from '../../providers/global/global';
import { BeneficiaryAddPage } from '../beneficiary-add/beneficiary-add';
import { BeneficiaryInfoPage } from '../beneficiary-info/beneficiary-info';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { MainHomePage } from '../main-home/main-home';
declare var window;


@Component({
  selector: 'page-beneficiary-list',
  templateUrl: 'beneficiary-list.html',
  providers: [Changefont]
})
export class BeneficiaryListPage {
  textMyan: any = ["ငွေလက်ခံသူစာရင်း"];
  textEng: any = ["Beneficiary List",];
  textErrorEng: any = ["Please choose account number.", "Please choose account number.", "Please fill amount.", "Shouldn't match from account no. and to account no."]
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ငွေပေးမည့်အကောင့်နှင့် ငွေလက်ခံမည့်အကောင့် တူ၍မရပါ"];
  textError: string[] = [];
  textData: any = [];
  font: any = '';
  modal: any;
  hardwareBackBtn: any = true;
  beneficiaryList: any;
  tempData: any;

  userData: any;
  ipaddress: string;
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  transferData: any = {};
  transferData1: any = {};
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  popover: any;
  txnType: any;
  flag: any;
  searchTerm: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public network: Network, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService) {
    this.txnType = 'own';
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      //Am 
       this.checkNetwork();
      })
    });
  }

  ionViewCanEnter() {
    // this.idleWatch();
  }

  getBeneficiaryList() {
    this.slimLoader.start(() => {
    });
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getBeneficiary', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        let temp = [];
        let count = 0;
        this.beneficiaryList = result.dataList;
        this.tempData = this.beneficiaryList;
        this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.slimLoader.complete();
      }
      //Am start
      // else {
      //   if (result.desc == 'No record found!') {
      //     let alert = this.alertCtrl.create({
      //       message: 'Please Add Beneficiary',
      //       enableBackdropDismiss: false,
      //       buttons: [{
      //         text: 'No',
      //         handler: () => {
      //         }
      //       },
      //       {
      //         text: 'Yes',
      //         handler: () => {
      //           this.navCtrl.push(BeneficiaryAddPage);
      //         }
      //       }],
      //       cssClass: 'confirmAlert',
      //     });
      //     alert.present();
      //   } else {
      //     this.all.showAlert("Warning!", result.desc);
      //   }
      //   this.slimLoader.complete();
      // } end
    },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
  }

  beneficiaryInfo(b) {
    this.navCtrl.push(BeneficiaryInfoPage, {
      data: b
    })
  }

  addBeneficiay(b) {
    this.navCtrl.push(BeneficiaryAddPage, {
      data: b
    })
  }

  getItems(ev: any) {
    this.beneficiaryList = this.tempData;
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.beneficiaryList = this.beneficiaryList.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'BeneficiaryListPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'BeneficiaryListPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }],
          cssClass: 'confirmAlert'
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert("Warning!", data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
      this.storage.get('userData').then((data) => {
        this.userData = data;
        this.getBeneficiaryList();
      });
    }
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
