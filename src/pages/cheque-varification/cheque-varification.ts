import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Contacts } from '@ionic-native/contacts';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';

@Component({
  selector: 'page-cheque-varification',
  templateUrl: 'cheque-varification.html',
  providers: [ChangelanguageProvider],
})
export class ChequeVarificationPage {

  popover: any;
  font: string;
  showFont: any = [];
  loading: any;
  ipaddress: any;
  userdata: any;
  textMyan: any = ["အကောင့် နံပါတ်", "ချက် နံပါတ်", "ငွေပမာဏ", "အတည်ပြုရန်", "ချက် ငွေလွှဲရန်",
  "အကောင့် ငွေပမာဏ", "Status", "Expiry Date is ", "."];
  textEng: any = ["Account No.", "Cheque No.", "Amount", "Verify", "Cheque Verification",
  "Account Balance", "Status", "Expiry Date is ", "."];

  fromAccountlist: any = [];
  amountBal: any;
  amountBal1: any;
  accountBal: any;
  alertPresented: boolean;

  amount: any;
  ccy = "";
  chequeno = "";
  chequetlistFromServer: any = [];
  chequetlistForUI: any = [];
  status: any;
  fromAccountNo = "";
  accError = "";
  chequeError = "";
  amtErr = "";

  showDueDate = false;
  dueDate = "";

  constructor(
    public global: GlobalProvider, public navCtrl: NavController,
    public navParams: NavParams, public barcodeScanner: BarcodeScanner,
    public storage: Storage, public http: Http,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public events: Events,
    public network: Network, private slimLoader: SlimLoadingBarService,
    public changeLanguage: ChangelanguageProvider, private contacts: Contacts,
    public all: AllserviceProvider, public util: UtilProvider,
    public changefont: Changefont, public popoverCtrl: PopoverController,
    public platform: Platform
  ) {
    this.amountBal = "";
    this.alertPresented = false;

    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        } else {
          this.ipaddress = result;
        }
        this.getChequeAccountList();
      })
    });

    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });

    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  changeAcc(account) {
    if (this.showDueDate) {
      this.amount = "";
    }

    this.showDueDate = false;

    this.chequetlistForUI = [];
    this.chequeno = "";
    this.status = "";

    this.accError = "";
    this.chequeError = "";
    this.amtErr = "";

    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].chequeAcc) {
        if (account != '-') {
          this.getChequeList(account);
          this.amountBal = this.fromAccountlist[i].avlBal;
          this.amountBal1 = this.util.removeComma(this.fromAccountlist[i].avlBal);
          this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy;
          this.ccy = this.fromAccountlist[i].ccy;
        } else if (account == '-') {
          this.chequetlistForUI = [];
          let param = { "value": "-", "desc": "-" };
          this.chequetlistForUI.push(param);
        }
      }
    }
  }

  //change cheque
  changeCheque(chequeno) {
    this.showDueDate = false;

    for (let i = 0; i < this.chequetlistFromServer.length; i++) {
      if (chequeno == this.chequetlistFromServer[i].chequeNo) {
        this.status = this.chequetlistFromServer[i].status;
      }
    }
  }

  getChequeAccountList() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
    this.http.post(this.ipaddress + '/service001/getChequeAccounts', parameter).map(res => res.json()).subscribe(
      result => {
        this.loading.dismiss();

        this.chequetlistForUI = [];
        let param = { "value": "-", "desc": "-" };
        this.chequetlistForUI.push(param);

        if (result.code == "0000") {
          if (result.dataList != undefined && result.dataList != null && result.dataList != '') {
            let tempArray = [];
            if (!Array.isArray(result.dataList)) {
              tempArray.push(result.dataList);
              result.dataList = tempArray;
            }
            this.fromAccountlist = [];
            this.fromAccountlist = result.dataList;
            //console.log(JSON.stringify(this.fromAccountlist));{"avlBal":"509,000,038.00","ccy":"MMK","chequeAcc":"0020101100037979"}
            if (this.fromAccountlist.length <= 0) {
              let param = { "avlBal": "-", "ccy": "-", "chequeAcc": "-" };
              this.fromAccountlist = [];
              this.fromAccountlist.push(param);
              //this.all.showAlert('Warning!', 'There is no account to show.');
            }
          } else {
            let param = { "avlBal": "-", "ccy": "-", "chequeAcc": "-" };
            this.fromAccountlist = [];
            this.fromAccountlist.push(param);
            //this.all.showAlert('Warning!', 'There is no account to show.');
          }
        } else if (result.code == "0014") {
          let param = { "avlBal": "-", "ccy": "-", "chequeAcc": "-" };
          this.fromAccountlist = [];
          this.fromAccountlist.push(param);

          this.all.showAlert('Warning!', result.desc);
        } else if (result.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: result.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
        } else {
          this.all.showAlert("Warning!", result.desc);
        }
      },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.loading.dismiss();
      }
    );
  }

  //Get Cheque List
  getChequeList(accountNo) {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, accountNo: accountNo };
    this.http.post(this.ipaddress + '/service005/getChequeList', parameter).map(res => res.json()).subscribe(
      result => {
        this.loading.dismiss();

        if (result.code == "0000") {
          if (result.dataList != undefined && result.dataList != null && result.dataList != "") {
            let tempArray = [];
            if (!Array.isArray(result.dataList)) {
              tempArray.push(result.dataList);
              result.dataList = tempArray;
            }

            let chequetlistTemp = result.dataList;

            this.chequetlistFromServer = [];
            this.chequetlistForUI = [];

            let isExistData = 0;

            if (chequetlistTemp.length > 0) {
              for (let i = 0; i < chequetlistTemp.length; i++) {
                if (chequetlistTemp[i].status == "Unpaid") {
                  isExistData = 1;
                  this.chequetlistFromServer.push(chequetlistTemp[i]);

                  let param = { "value": "", "desc": "" };
                  param.value = chequetlistTemp[i].chequeNo;
                  param.desc = chequetlistTemp[i].chequeNo;
                  this.chequetlistForUI.push(param);
                }
              }

              if (isExistData == 0) {
                let param = { "value": "-", "desc": "-" };
                this.chequetlistForUI.push(param);
              }
            } else {
              let param = { "value": "-", "desc": "-" };
              this.chequetlistForUI.push(param);
            }
          } else {
            this.chequetlistForUI = [];
            let param = { "value": "-", "desc": "-" };
            this.chequetlistForUI.push(param);
          }
        } else if (result.code == "0014") {
          this.chequetlistForUI = [];
          let param = { "value": "-", "desc": "-" };
          this.chequetlistForUI.push(param);

          this.all.showAlert('Warning!', result.desc);
        } else if (result.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: result.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'confirmAlert'
          })
          confirm.present();
        } else {
          this.all.showAlert("Warning!", result.desc);
        }
      },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.loading.dismiss();
      }
    );
  }

  goVerify() {
    this.accError = "";
    this.chequeError = "";
    this.amtErr = "";

    let goServer = true;

    if (this.fromAccountNo == null || this.fromAccountNo == '' || this.fromAccountNo == undefined) {
      this.accError = "Please choose Account No.";
      goServer = false;
    }

    if (this.chequeno == null || this.chequeno == '' || this.chequeno == undefined) {
      this.chequeError = "Please choose Cheque No.";
      goServer = false;
    }

    if (this.amount == null || this.amount == '' || this.amount == undefined) {
      this.amtErr = "Please fill Amount.";
      goServer = false;
    }

    if (this.accError == '' && this.chequeError == '' && this.amtErr == '') {
      if (this.fromAccountNo == '-') {
        this.all.showAlert('Warning!', "There is no Account No.");
        goServer = false;
      } else if (this.chequeno == '-') {
        this.all.showAlert('Warning!', "There is no Cheque No.");
        goServer = false;
      } else {
        let amountTemp = this.amount * 1;

        if (!isNaN(amountTemp)) {
          if (this.amount <= 0) {
            this.all.showAlert('Warning!', "Amount should be greater than zero.");
            goServer = false;
          }

          if (this.amountBal1 < this.amount) {
            this.all.showAlert('Warning!', "Insufficient Amount.");
            goServer = false;
          }
        } else {
          this.all.showAlert('Warning!', "Amount should be number only.");
          goServer = false;
        }
      }
    }

    if (goServer) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();

      let parameter = {
        userID: this.userdata.userID, sessionID: this.userdata.sessionID,
        accountNo: this.fromAccountNo, chequeNo: this.chequeno,
        amount: this.amount, t2: this.ccy
      };
      this.http.post(this.ipaddress + '/service005/setChequeAmount', parameter).map(res => res.json()).subscribe(
        result => {
          this.loading.dismiss();

          if (result.code == "0000") {
            let confirm = this.alertCtrl.create({
              title: 'Cheque Verification',
              enableBackdropDismiss: false,
              message: 'This cheque has been verified successfully. <br/>Expiry date is ' + result.dueDate + '.',
              buttons: [
                {
                  text: 'OK'
                }
              ],
              cssClass: 'infoAlert',
            });
            confirm.present();

            this.showDueDate = true;
            this.dueDate = result.dueDate;
          } else if (result.code == "0014") {
            this.all.showAlert('Warning!', result.desc);
          } else if (result.code == "0016") {
            let confirm = this.alertCtrl.create({
              title: 'Warning!',
              enableBackdropDismiss: false,
              message: result.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {
                    });
                    this.navCtrl.popToRoot();
                  }
                }
              ],
              cssClass: 'confirmAlert'
            })
            confirm.present();
          } else {
            this.all.showAlert("Warning!", result.desc);
          }
        },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.loading.dismiss();
        }
      );
    }
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }

}
