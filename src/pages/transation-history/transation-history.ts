import 'rxjs/add/operator/map';

import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  App,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
  Select,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { TransactionHistoryPage } from '../transaction-history-page/transaction-history-page';

declare var window;

@Component({
  selector: 'page-transation-history',
  templateUrl: 'transation-history.html',
  providers: [ChangelanguageProvider]
})
export class TransationHistory {
  @ViewChild('myselect') select: Select;
  userdata = { userID: '', useraccount: '', merchant: '', sessionID: '' };
  accountlist: any;
  gaming: any = 4;
  merchanttype: any;
  range: any = false;
  date: any;
  duration: any = 0;
  dateval: string;
  merchantlist: any;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  fromDate: any;
  toDate: any;
  merchantName: any;
  processingCode: any;
  btncolor: any = true;
  btncolor1: any = false;
  btncolor2: any = false;
  btncolor3: any = false;
  public loading;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  showFont: string[] = [];
  textEng: any = ['Bill Transaction', 'Merchant', 'Select Account', 'Filter By Date', 'Last 10 Transactions', 'Last 2 Transaction', 'Last 5 Transaction', 'Default', '2 Days', '5 Days', 'Custom', 'Start Date', 'End Date', 'Search'];
  textMyan: any = ['ငွေပေးချေမှုစာရင်း', 'ရုပ်သံလိုင်းအမျိုးအစား', 'စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ရက်အလိုက်စီစဉ်သည်', 'နောက်ဆုံး၁၀ကြောင်း', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'နဂိုအခြေအနေ', '၂ရက်', '၅ရက်', 'စိတ်ကြိုက်', 'စတင်သည့်ရက်', 'ပြီးဆုံးသည့်ရက်', 'ရှာမည်'];
  font: string;
  popover: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public datePipe: DatePipe, public network: Network, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, public http: Http, public all: AllserviceProvider, public storage: Storage, public idle: Idle, public alertCtrl: AlertController,
    public events: Events, public popoverCtrl: PopoverController, public app: App, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    public platform: Platform, public global: GlobalProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
      this.dateval = this.showFont[4];
    });

  }

  /* changelanguage(lan) {
 
     if (lan == 'uni') {
       this.font = "uni";
       //  this.storage.set('language', "uni");
 
       for (let j = 0; j <  this.textMyan.length;j++) {
         this.showFont[j] = this.textMyan[j];
       }
 
     }
     else if (lan == 'zg') {
       this.font = "zg";
       for (let j = 0; j <  this.textMyan.length;j++) {
         this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
       }
     }
     else{
       this.font='';
       for (let j = 0; j <  this.textEng.length;j++) {
         this.showFont[j] = this.textEng[j];
       }
     }
   } */

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TransationHistory')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TransationHistory') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        //   
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  merchantChange(data) {
    for (let i = 0; i < this.merchantlist.length; i++) {
      if (data == this.merchantlist[i].merchantID) {
        this.processingCode = this.merchantlist[i].processingCode;

        break;
      }

    }

  }
  getMerchant() {
    /*  this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange :true
        //   duration: 3000
      });
      this.loading.present();*/
    this.slimLoader.start(() => {

    });
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "paymentType": '1'
    };

    this.http.post(this.ipaddress + '/service001/getMerchant', param).map(res => res.json()).subscribe(data => {

      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.merchantList)) {
          tempArray.push(data.merchantList);
          data.merchantList = tempArray;
        }
        this.merchantlist = data.merchantList;
        // this.loading.dismiss();
        this.slimLoader.complete();
      }
      else if (data.code == "0016") {
        this.logoutAlert( data.desc);
        this.slimLoader.complete();
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        //  this.loading.dismiss();
        this.slimLoader.complete();
      }

    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.slimLoader.complete();
      });
  }
  accountChange(data) {

  }
  ionViewDidLoad() {

    //// this.idleWatch();
    this.storage.get('userData').then((val) => {

      this.userdata.userID = val.userID;
      this.userdata.sessionID = val.sessionID;
      this.accountlist = val.debitAcc;
      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
          this.getMerchant();
        }
      });

    });

  }
  ionViewDidLeave() {

    this.slimLoader.reset();
  }
  gorange() {
    this.duration = 3;
    this.range = true;
    this.btncolor = false;
    this.btncolor1 = false;
    this.btncolor2 = false;
    this.btncolor3 = true;
  }

  getduration(data) {
    if (data == 0) {
      this.dateval = this.showFont[4];
      this.btncolor = true;
      this.btncolor1 = false;
      this.btncolor2 = false;
      this.btncolor3 = false;
    }
    if (data == 1) {
      this.dateval = this.showFont[5];
      this.btncolor = false;
      this.btncolor1 = true;
      this.btncolor2 = false;
      this.btncolor3 = false;
    }
    else if (data == 2) {
      this.dateval = this.showFont[6];
      this.btncolor = false;
      this.btncolor1 = false;
      this.btncolor2 = true;
      this.btncolor3 = false;
    }
    this.fromDate = '';
    this.toDate = '';
    this.duration = data;
    this.range = false;
  }

  goSearch() {
    if (this.fromDate != '' && this.toDate != '' && this.duration == 3) {
      this.dateval = this.datePipe.transform(this.fromDate, 'dd-MM-yyyy') + " to " + this.datePipe.transform(this.toDate, 'dd-MM-yyyy');
    }

    this.slimLoader.start(() => {

    });
    let param = {
      userID: this.userdata.userID,
      acctNo: this.userdata.useraccount,
      sessionID: this.userdata.sessionID,
      durationType: this.duration,
      fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      totalCount: 0,
      currentPage: 1,
      pageSize: 10,
      pageCount: 0,
      status: 'all',
      merchantID: this.merchanttype,
      processingCode: this.processingCode
    };


    this.http.post(this.ipaddress + '/service001/billPaymentTransactionListing', param).map(res => res.json()).subscribe(data => {

      if (data.code == '0000') {
        //  this.loading.dismiss();
        this.slimLoader.complete();
        this.navCtrl.push(TransactionHistoryPage, {
          data: data,
          detail: param,
          date: this.dateval
        });
      }
      else if (data.code == "0016") {
        this.logoutAlert( data.desc);
        this.slimLoader.complete();
      }
      else {
        this.range = false;
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 4000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.slimLoader.complete();
        // this.loading.dismiss();
      }


    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.slimLoader.complete();
      });

  }
  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(PopoverPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
        //  this.navCtrl.push(Language)
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    //if(this.hardwareBackBtn)
    /* checks if modal is open */
    if (this.modal && this.modal.index === 0) {
      /* closes modal */
      this.modal.dismiss();
    } else {
      // let hardwareBackBtn = true;
      /* exits the app, since this is the main/first tab */
      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit?',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;

            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }
  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
