import 'rxjs/add/operator/map';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
  Select,
} from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ChequestopconfirmPage } from '../chequestopconfirm/chequestopconfirm';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the ChequePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-cheque',
  templateUrl: 'chequestop.html',
  providers: [ChangelanguageProvider]

})
export class ChequestopPage {
  @ViewChild('myselect') select: Select;
  userData: any;
  ipaddress: string;
  loading: any;
  useraccount: any;
  chequeNo: any;
  cheqaccountMsg: string;
  accounterrorMsg: string;
  accountlist: any;
  font: string;
  popover: any;
  cheacc: any;
  textEng: any = ['Select Account', 'Cheque Number', 'Choose Account Number', 'Enter Cheque Number', 'Are you sure you want to exit', 'Yes', 'No', 'Cancel', 'Submit', 'Stop Cheque Request', "Account Balance"];
  textMyan: any = ['အကောင့်နံပါတ် ရွေးချယ်ပါ', 'ချက်နံပါတ်', 'စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ချက်နံပါတ်ရိုက်ထည့်ပါ', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ', 'ပယ်ဖျက်သည်', 'သဘောတူသည်', 'ချက်ပေးချေမှုပယ်ဖျက်ခြင်း', "ဘဏ်စာရင်းလက်ကျန်ငွေ"];
  showFont: any = [];
  accountBal: any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public all: AllserviceProvider, public navParams: NavParams, private alertCtrl: AlertController, public storage: Storage, public events: Events, public changeLanguage: ChangelanguageProvider, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public platform: Platform, public popoverCtrl: PopoverController) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      this.storage.get('userData').then((data) => {
        this.userData = data;
        this.getCustomerAccount();
      });
    });
  }

  getCustomerAccount() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service001/getChequeAccounts', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.dataList)) {
          tempArray.push(data.dataList);
          data.dataList = tempArray;
        }
        this.loading.dismiss();
        this.accountlist = data.dataList;
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  inputChange(data) {
    this.cheqaccountMsg = '';
  }

  accountChange(account) {
    this.accounterrorMsg = '';
    for (let i = 0; i < this.accountlist.length; i++) {
      if (account == this.accountlist[i].chequeAcc) {
        this.accountBal = this.accountlist[i].avlBal + " " + this.accountlist[i].ccy
      }
    }
  }

  okbtn() {
    let flag1 = true, flag2 = true;
    if (this.useraccount == '' || this.useraccount == undefined) {
      this.accounterrorMsg = this.showFont[2];
      flag1 = false;
    }
    if (this.chequeNo == '' || this.chequeNo == undefined) {
      this.cheqaccountMsg = this.showFont[3];
      flag2 = false;
    }
    if (flag1 && flag2) {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '8', merchantID: '' };
      this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          let param = {
            userID: this.userData.userID,
            sessionID: this.userData.sessionID,
            accountNo: this.useraccount,
            chequeNo: this.chequeNo,
            otp: data.rKey,
            sKey: data.sKey
          };
          this.navCtrl.push(ChequestopconfirmPage, {
            data: param,
            sKey: data.sKey
          });
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert'
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  ionViewDidLoad() {
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

}
