import 'rxjs/add/operator/map';

import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  App,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
  Select,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { AccountTransactionDetail } from '../account-transaction-detail/account-transaction-detail';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MyPopOverPage } from '../my-pop-over-page/my-pop-over-page';
import { PopoverPage } from '../popover-page/popover-page';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { MainHomePage } from '../main-home/main-home';
declare var window;


@Component({
  selector: 'page-account-transation',
  templateUrl: 'account-transation.html',
  providers: [ChangelanguageProvider]

})
export class AccountTransation {
  @ViewChild('myselect') select: Select;
  userdata: any;
  accountlist: any;
  loading: any;
  useraccount: any;
  currentpage: any = 1;
  pageSize: any = 10;
  duration: any = 0;
  durationmsg: any;
  passTemp: any = {};
  todayDate: any = '';
  resultdata: any = [];
  range: any = false;
  next: number = 1;
  previous: number = 1;
  status: any;
  ipaddress: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  modal: any;
  hardwareBackBtn: any = true;
  font: string;
  tempFont: number = 3;
  showFont: string[] = [];
  popover: any;
  fromDate: any = new Date().toISOString();
  toDate: any = new Date().toISOString();
  fabMargin: any = '';
  isDownload: boolean = true;
  fileData: any;
  textEng: any = ['Transaction', 'Select Account', 'Filter by Date', 'Last 10 Transaction', 'Last 2 Days', 'Last 5 Days', 'No result Found', 'Next', 'Previous', 'Default', '2 Days', '5 Days', 'Custom', 'Start Date', 'End Date', 'Search', 'Are you sure you want to exit', 'Yes', 'No', 'Ref'];
  textMyan: any = ['စာရင်းအသေးစိတ်', 'စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ရက်အလိုက်စီစဉ်သည်', 'နောက်ဆုံး၁၀ကြောင်း', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'အချက်အလက်မရှိပါ', 'ရှေ့သို့', 'နောက်သို့', 'နောက်ဆုံး ၁၀ ချက်', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'စိတ်ကြိုက်ရွေးချယ်မည်', 'စတင်သည့်ရက်', 'ပြီးဆုံးသည့်ရက်', 'ရှာမည်', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ', 'အမှတ်စဉ်'];
  accDesc: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http, public datePipe: DatePipe, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public events: Events, public idle: Idle, public all: AllserviceProvider, public app: App, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    public platform: Platform, public global: GlobalProvider, public file: File, public fileOpener: FileOpener, private transfer: FileTransfer, ) {
    let date = new Date();
    this.isDownload = true;
    this.fabMargin = { 'margin-bottom': '' };
    this.todayDate = this.datePipe.transform(date, 'dd MM yyyy');
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });

    });
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.getAccountSummary();
      });
    });

  }

  getAccountSummary() {
    this.slimLoader.start(() => {
    });
    let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, };
    this.http.post(this.ipaddress + '/service001/getAccountSummary', parameter).map(res => res.json()).subscribe(result => {
      // console.log("result:AccountSummary" + JSON.stringify(result));
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.accountlist = [];
        for (let i = 0; i < result.dataList.length; i++) {
               this.accountlist.push(result.dataList[i]);
        }

        // console.log("accountlist is : " + this.accountlist);
        //  this.loading.dismiss();
        this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })
        confirm.present();
        // this.loading.dismiss();
        this.slimLoader.complete();
      }
      else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        // this.loading.dismiss();
        this.slimLoader.complete();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
    });
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'AccountTransation')
        this.gologout();


    });
    this.idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!';
    });
    this.idle.onTimeoutWarning.subscribe((countdown) => {

      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'AccountTransation') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  ionViewDidLoad() {

    //// this.idleWatch();
    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });

  }
  ionViewDidLeave() {

    this.slimLoader.reset();
  }


  accountChange(s) {
    this.useraccount = s;

    this.tempFont = 3;
    this.duration = 0;
    this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
    this.goSearch();
  }

  goduration(s) {
    this.duration = s;

  }

  datafilter(ev) {

    this.popover = this.popoverCtrl.create(MyPopOverPage, {});

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      //    this.resultdata={};


      this.duration = data;
      this.currentpage = 1;
      if (data != 3 && data != null) {
        if (data == 0) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
          this.tempFont = 3;
        }
        if (data == 1) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[4] + " )";
          this.tempFont = 4;
        }
        else if (data == 2) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[5] + " )";
          this.tempFont = 5;
        }

        this.range = false;
        /* this.fromDate = '';
        this.toDate = ''; */
        this.currentpage = 1;
        this.pageSize = 10;
        this.goSearch();
      }
      else if (data == 3) {
        this.range = true;
        this.tempFont = 6;
      }

    });
  }

  nextData() {
    this.next++;
    if (this.next <= this.passTemp.pageCount) {
      this.currentpage = this.currentpage + 1;
      this.previous = this.next;
      this.goSearch();
    }

  }

  previousData() {
    this.previous--;
    if (this.previous >= 1) {
      this.currentpage = this.previous;
      this.next = this.previous;
      this.goSearch();
    }
  }

  goSearch() {
    if (this.fromDate != '' && this.toDate != '' && this.duration == '3') {
      this.durationmsg = " ( " + this.datePipe.transform(this.fromDate, 'dd-MM-yyyy') + " to " + this.datePipe.transform(this.toDate, 'dd-MM-yyyy') + " )";

      /* if(this.fromDate.toString().indexOf("-") > -1) {
         this.fromDate = this.datePipe.transform(this.fromDate, 'yyyyMMdd');
         this.toDate = this.datePipe.transform(this.toDate, 'yyyyMMdd');
       } */

    }

    this.slimLoader.start(() => {

    });
    var param = {
      userID: this.userdata.userID,
      sessionID: this.userdata.sessionID,
      customerNo: '',
      durationType: this.duration,
      fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      totalCount: 0,
      acctNo: this.useraccount.depositAcc,
      currentPage: this.currentpage,
      pageSize: this.pageSize,
      pageCount: 0,
    };

    this.http.post(this.ipaddress + '/service001/getAccountActivity', param).map(res => res.json()).subscribe(data => {

      if (data.code == '0000') {
        this.isDownload = false;
        this.range = false;
        this.status = 1;
        if (data.pageCount > 1 && this.currentpage == 1) {
          this.next = 1;
          this.previous = 1;
          this.fabMargin = { 'margin-bottom': '10%' };
        } else if (this.currentpage > 1) {
          this.fabMargin = { 'margin-bottom': '10%' };
        } else {
          this.fabMargin = { 'margin-bottom': '0' };
        }
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
        }
        this.passTemp = data;
        this.fileData = data.data;
        let group_to_values = data.data.reduce(function (obj, item) {
          obj[item.txnDate] = obj[item.txnDate] || [];
          obj[item.txnDate].push(item);
          return obj;
        }, {});
        let groups = Object.keys(group_to_values).map(function (key) {
          return { txnDate: key, data: group_to_values[key] };
        });


        this.resultdata = groups;



        //   this.loading.dismiss();
        this.slimLoader.complete();
        /* data.durationmsg  =  this.durationmsg;
         this.navCtrl.push(TransactionHistoryPage,{
         data : data
         }) */
      }
      else if (data.code == "0016") {
        this.status = 0;
        this.isDownload = true;
        this.fabMargin = { 'margin-bottom': '0' };
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })

        confirm.present();
        //   this.loading.dismiss();
        this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.range = false;
        this.passTemp = {};
        this.resultdata = [];
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        //  this.loading.dismiss();
        this.slimLoader.complete();
      }

    },
      error => {
        this.status = 0;
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.slimLoader.complete();
        // this.loading.dismiss();
      });
  }

  gotoDetail(a) {
    this.navCtrl.push(AccountTransactionDetail,
      {
        data: a

      });
  }

  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(PopoverPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        // this.navCtrl.push(Language)
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }
  backButtonAction() {
    if (this.modal && this.modal.index === 0) {

      this.modal.dismiss();
    } else {

      /* exits the app, since this is the main/first tab */
      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;

            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }

  createFile(type) {
    this.slimLoader.start(() => { });
    let param = {
      userID: this.userdata.userID,
      sessionID: this.userdata.sessionID,
      data: this.fileData,
      type: type,
      accountNo: this.useraccount.depositAcc,
    };
    this.http.post(this.ipaddress + '/service002/createFile', param).map(res => res.json()).subscribe(data => {
      this.slimLoader.complete();
      if (data.code == '0000') {
        if (this.platform.is('ios')) {
          this.downloadPdf(data.fileName.replace(/\\/g, '/'), type);//ios
        } else {
          this.downloadPdf(data.fileName, type);//android
        }
      } else if (data.code == "0016") {
        this.status = 0;
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  downloadPdf(fileName, type) {

    let path = null;
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }

    let fName = "";
    if (type == '1') {
      fName = "account_activity.pdf"
    } else if (type == '2') {
      fName = "account_activity.xlsx"
    }

    let link = this.ipaddress.substr(0, this.ipaddress.lastIndexOf('/') + 1);
    const transfer = this.transfer.create();
    let fileExtn = fName.split('.').reverse()[0];
    let fileMIMEType = this.getMIMEtype(fileExtn);

    transfer.download(link + fileName, path + fName).then(entry => {
      this.fileOpener.open(path + fName, fileMIMEType);
    });
  }

  getMIMEtype(extn) {
    let ext = extn.toLowerCase();
    let MIMETypes = {
      'txt': 'text/plain',
      'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc': 'application/msword',
      'pdf': 'application/pdf',
      'jpg': 'image/jpeg',
      'bmp': 'image/bmp',
      'png': 'image/png',
      'xls': 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf': 'application/rtf',
      'ppt': 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    return MIMETypes[ext];
  }
  ionViewCanLeave() {
    this.select.close();

    this.platform.registerBackButtonAction(() => {
      // console.log("Active Page=" + this.navCtrl.getActive().name);
      this.select.close();
      this.navCtrl.pop();
    });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
