import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AccountTransferDetail } from '../account-transfer-detail/account-transfer-detail';
import { AccountTransferOtpPage } from '../account-transfer-otp/account-transfer-otp';
import { Login } from '../login/login';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';
/**
 * Generated class for the AccountTransferConfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-account-transfer-confirm',
    templateUrl: 'account-transfer-confirm.html',
    providers: [ChangelanguageProvider]
})
export class AccountTransferConfirmPage {
    accType: any;
    passTemp: any;
    passType: any;
    textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "အတည်ပြုခြင်း", "ငွေလွှဲ အမျိုးအစား", "လွှဲပြောင်းမည့် အမျိုးအစား"];
    textEng: any = ["From Account No.", "To Account No.", "Amount", "Narrative", "CANCEL", "CONFIRM", "Confirmation", "Transfer Type", "Account Type"];
    showFont: any = [];
    font: any = '';
    data: any;
    userdata: any;
    loading: any;
    flag: string;
    detail: any;
    ipaddress: string;
    passOtp: any;
    passSKey: any;
    fromPage: any; amount: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
        public alertCtrl: AlertController, public all: AllserviceProvider, public util: UtilProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public changefont: Changefont,
        private firebaseAnalytics: FirebaseAnalytics,private firebaseCrashlytics: FirebaseCrashlytics) {
        this.passTemp = this.navParams.get("data");
        // console.log("Account Transfer Form Data is here : " + JSON.stringify(this.passTemp));
        this.passType = this.navParams.get("type");
        this.passOtp = this.navParams.get("otp");
        this.passSKey = this.navParams.get('sKey');
        this.fromPage = this.navParams.get('fromPage');
        this.amount = this.util.formatAmount(this.passTemp.amount);
        this.events.subscribe('changelanguage', lan => {
            this.changelanguage(lan.data)
          });
      
          this.storage.get('language').then((font) => {
            this.changelanguage(font);
          });
        /* this.storage.get('language').then((font) => {
            this.font = font;
            this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
                this.showFont = data;
            });
        }); */

        this.storage.get('userData').then((data) => {
            this.userdata = data;

            this.storage.get('ipaddress').then((result) => {

                if (result == null || result == '') {

                }
                else {
                    this.ipaddress = result;
                }
            });
        });
    }

    ionViewDidLoad() {

        this.checkNetwork();
    }
    changelanguage(font) {
        if (font == "eng") {
          this.font = "";
          for (let i = 0; i < this.textEng.length; i++) {
            this.showFont[i] = this.textEng[i];
          }
        }
        else if (font == "zg") {
          this.font = "zg";
          for (let i = 0; i < this.textMyan.length; i++) {
            this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
          }
        }
        else {
          this.font = "uni";
          for (let i = 0; i < this.textMyan.length; i++) {
            this.showFont[i] = this.textMyan[i];
          }
        }
      }
    Okaybtn() {
        
        if (this.passOtp == 'true') {
            this.loading = this.loadingCtrl.create({
                content: "Processing...",
                dismissOnPageChange: true
            });
            this.loading.present();
            let transferType
            if (this.passType == 'own') {
                transferType = 3;
            }
            else if (this.passType == 'internal') {
                transferType = 4;
            }
            let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: transferType, merchantID: '', sKey: this.passSKey };

            this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {

                if (data.code == "0000") {
                    this.loading.dismiss();
                    this.navCtrl.push(AccountTransferOtpPage, {
                        data: data,
                        type: this.passType,
                        detail: this.passTemp,
                        sKey: this.passSKey,
                        fromPage: this.fromPage
                    })
                }
                else if (data.code == "0016") {
                    this.logoutAlert(data.desc);
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert('Warning!', data.desc);
                    this.loading.dismiss();
                }

            },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }
        else {
            this.goTransfer();
        }
    }

    goTransfer() {
        this.loading = this.loadingCtrl.create({
            content: "Processing...",
            dismissOnPageChange: true
        });
        this.loading.present();
        let transferType = '';
        if (this.passType == 'own') {
            transferType = '1';
            let parameter = {
                userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: this.passTemp.fromAcc,
                toAccount: this.passTemp.toAcc, merchantID: '', amount: this.passTemp.amount, bankCharges: 0, narrative: this.passTemp.refNo,
                 refNo: '', field1: transferType, field2: '', sKey: this.passSKey,fromName:this.passTemp.fromName,toName:this.passTemp.toName
            };
            this.http.post(this.ipaddress + '/service003/goOwnTransfer', parameter).map(res => res.json()).subscribe(data1 => {
                // this.firebase.logEvent('own_transfer', {})
                //   .then((res: any) => { console.log(res); })
                //   .catch((error: any) => console.log(error));

                // this.firebaseAnalytics.logEvent('own_transfer', {"userid": this.userdata.userID})
                // .then((res: any) => console.log(res))
                // .catch((error: any) => console.error(error));

                if (data1.code == "0000") {
                    this.loading.dismiss();
                    this.passTemp.amount = data1.amount;
                    this.navCtrl.setRoot(AccountTransferDetail, {
                        data: data1,
                        detail: this.passTemp,
                        type: this.passType,
                        fromPage: this.fromPage
                    })
                }
                else if (data1.code == "0016") {
                    this.logoutAlert(data1.desc);
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert('Warning!', data1.desc);
                    this.loading.dismiss();
                }

            },
                error => {
                    const crashlytics = this.firebaseCrashlytics.initialise();
                    crashlytics.logException('Own Transfer'+this.all.getErrorMessage(error));
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }
        else if (this.passType == 'internal') {
            transferType = '2';
            let parameter = {
                userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: this.passTemp.fromAcc,
                toAccount: this.passTemp.toAcc, merchantID: '', amount: this.passTemp.amount, bankCharges: 0, narrative: this.passTemp.refNo, refNo: '', 
                field1: transferType, field2: '', sKey: this.passSKey,fromName:this.passTemp.fromName,toName:this.passTemp.toName
            };
            this.http.post(this.ipaddress + '/service003/goInternalTransfer', parameter).map(res => res.json()).subscribe(data1 => {
               
                // this.firebaseAnalytics.logEvent('internal_transfer', {"userid": this.userdata.userID})
                // .then((res: any) => console.log(res))
                // .catch((error: any) => console.error(error));

                if (data1.code == "0000") {
                    this.loading.dismiss();
                    this.passTemp.amount = data1.amount;
                    this.navCtrl.setRoot(AccountTransferDetail, {
                        data: data1,
                        detail: this.passTemp,
                        type: this.passType,
                        fromPage: this.fromPage
                    })
                }
                else if (data1.code == "0016") {
                    this.logoutAlert(data1.desc);
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert('Warning!', data1.desc);
                    this.loading.dismiss();
                }
            },
                error => {
                    const crashlytics = this.firebaseCrashlytics.initialise();
                    crashlytics.logException('Internal Transfer'+this.all.getErrorMessage(error));
                   // this.firebaseCrashlytics.initialize();
                   // this.firebaseCrashlytics.logException('Internal Transfer'+ this.all.getErrorMessage(error));
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }

    }
    cancelbtn() {
        this.navCtrl.pop();
    }

    checkNetwork() {

        if (this.network.type == "none") {
            this.flag = 'none';
        } else {
            this.flag = 'success';
        }
    }

    logoutAlert(message){
        let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.storage.remove('userData');
                        this.events.publish('login_success', false);
                        this.events.publish('lastTimeLoginSuccess', '');
                        this.navCtrl.setRoot(Login, {
                        });
                        this.navCtrl.popToRoot();
                    }
                }
            ],
            cssClass: 'warningAlert',
        })
        confirm.present();
    }
}
