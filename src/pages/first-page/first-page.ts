import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, Events} from 'ionic-angular';
//import { Tabs } from '../tabs/tabs';

import { HomePage } from '../home/home';
/**
 * Generated class for the FirstPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-first-page',
  templateUrl: 'first-page.html',
})
export class FirstPage {
  myanlanguage : string = "မြန်မာ";
  englanguage : string = "English";
  language : string = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl:MenuController,
  public events: Events) {
   // this.menuCtrl.swipeEnable(false);
    this.language = this.englanguage;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad FirstPage');

  }

  goLogin(){
    this.events.publish('login_success', true);
    this.navCtrl.setRoot(HomePage)
  }
  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }
}
