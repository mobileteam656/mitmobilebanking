import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-corporate-detail',
  templateUrl: 'corporate-detail.html',
  providers: [ChangelanguageProvider]
})
export class CorporateDetailPage {

  detailData: any[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  showFont:string [] = [];
  textEng: any=['Details','Ref No','Name','From Account','To Account','Amount','Date/Time','Reject','Approve','Approved','Rejected'];
  textMyan : any = ['အသေးစိတ်အချက်အလက်','အမှတ်စဉ်','အမည်','အကောင့်မှ','အကောင့်သို့','ငွေပမာဏ','နေ့ရက်/အချိန်','အတည်မပြု','အတည်ပြု','Approved','Rejected'];
  font:string;
  public loading;
  ipaddress:string;
  userdata:any;
  alertCtrl: any;
  public alertPresented : any;
  constructor(public changefont: Changefont, public global: GlobalProvider,public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams,public idle:Idle,public events:Events,public storage:Storage,public changeLanguage:ChangelanguageProvider,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public http:Http,
    private firebaseAnalytics: FirebaseAnalytics) {
    this.detailData=navParams.get("data");
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }  

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  goApproveReject(obj,status) {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    var param = {
      "autokey": obj.autokey,
      "approveuserid": this.userdata.userID,
      "approveusername": '',
      "status": status
    };
    console.log(JSON.stringify("Change Status=" + param));
    this.http.post(this.ipaddress + '/service005/updateApproveTransaction', param).map(res => res.json()).subscribe(data => {
      if (data.code == '0000') {
        // obj.status = data.status;
        this.transferDeposit(obj, data.status);
        console.log(JSON.stringify("resultdata" + obj.status));
        this.loading.dismiss();        
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })
        this.loading.dismiss();
        confirm.present();
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        //toast.present(toast);
        this.loading.dismiss();
      }

    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        //toast.present(toast);
        this.loading.dismiss();
      });
  }
  transferDeposit(adata, upstatus) {
    let param = {
      userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: adata.t1,
      toAccount: adata.t2, amount: adata.amount, bankCharges: 0, narrative: "", refNo: adata.autokey,
      field1: '2', field2: ''
    };
    console.log(JSON.stringify("transferDeposit" + param));
    this.http.post(this.ipaddress + '/service003/goAgentTransferDeposit', param).map(res => res.json()).subscribe(data => {
      // this.firebase.logEvent('quick_pay', {})
      //     .then((res: any) => { console.log(res); })
      //     .catch((error: any) => console.log(error));

      if (data.code == '0000') {
        adata.status = upstatus;
        this.navCtrl.pop();
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })

        confirm.present();
        //this.slimLoader.complete();
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      }

    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      });
  }
  gologout() {
    
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = {userID: this.userdata.userID, sessionID: this.userdata.sessionID};

      
      this.http.post(this.ipaddress+'/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.loading.dismiss();
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess','');
            this.storage.remove('userData');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              // duration: 2000,
              // position: 'bottom'
              showCloseButton: true,
              dismissOnPageChange: true,
              closeButtonText: 'Ok'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            showCloseButton: true,
            dismissOnPageChange: true,
            closeButtonText: 'Ok'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }
  

  backButton() {
    this.navCtrl.pop();
  }
  
}

