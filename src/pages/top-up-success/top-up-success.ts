import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { TopUp } from '../top-up/top-up';

/**
 * Generated class for the TopUpSuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-top-up-success',
  templateUrl: 'top-up-success.html',
    providers : [ChangelanguageProvider]
})
export class TopUpSuccessPage {
  textMyan : any = ["လုပ်ဆောင်မှုအတည်ပြုခြင်း","အကောင့်နံပါတ်","Operator အမျိုးအစား","ဖုန်းနံပါတ်","ငွေပမာဏ","ပိတ်မည်","အမှတ်စဉ်","ဘဏ် အခွန် အမှတ်စဉ် နံပါတ်","လုပ်ဆောင်ခဲ့သည့်ရက်"];
  textEng: any = ["Top Up - Success",  "Account Number", "Operator Type", "Phone Number", "Amount","Close","Bank Reference Number","Bank Tax Reference Number","Transaction Date"];
  passtemp : any ;
  passtemp1 : any ;
  showFont : string [] = [];
  font:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage, public changeLanguage:ChangelanguageProvider) {
    this.passtemp = this.navParams.get('data');
    this.passtemp1 = this.navParams.get('detail');
    
    
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
  }

  ionViewDidLoad() {
    
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");

      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/
  getOK(){
    /*  if(this.passtemp3 == "1"){
     this.navCtrl.setRoot(QrDemo)
     }
     else{ */

   this.navCtrl.setRoot(TopUp)

    /*  } */

  }

}
