import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ConfirmpagePage } from '../confirmpage/confirmpage';
import { Login } from '../login/login';
import { Global } from '../global/global';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-choosepackage',
  templateUrl: 'choosepackage.html',
  providers: [ChangelanguageProvider]
})

export class ChoosepackagePage {
  textMyan: any = ["အကောင့်နံပါတ်", "Package အမျိုးအစား", "သက်တမ်း", "အခွန်နှုန်း", "ဝန်ဆောင်ခနှုန်း", "ငွေပမာဏ", "စုစုပေါင်းငွေပမာဏ", "QRဖတ်ရန်", "ကဒ်နံပါတ်", "ပေးချေမည်", "စာရင်းအမှတ်ရွေးချယ်ပါ", "Package အမျိုးအစားရွေးချယ်ပါ", "သက်တမ်းအမျိုးအစားရွေးချယ်ပါ", "ကတ်နံပါတ်ရိုက်သွင်းပါ", "Bill အမျိုးအစားရွေးချယ်ပါ", "Biller အမှတ်စဉ်ရိုက်ထည့်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "အသုံးပြုသူအမှတ်စဉ်ရိုက်သွင်းပါ", "ငွေတောင်းခံလွှာနံပါတ်ရိုက်သွင်းပါ", "အကြောင်အရာရွေးချယ်ပါ", "အရေအတွက်ရွေးချယ်ပါ", "ဈေးနှုန်းရိုက်သွင်းပါ", "စုစုပေါင်းငွေပမာဏလိုအပ်နေပါသည်", "Biller အမျိုးအစား", "အသုံးပြုသူအမှတ်စဉ်", "CNP ဝန်ဆောင်ခ", "ဘဏ်ဝန်ဆောင်ခ", "ရုပ်သံလိုင်းဝန်ဆောင်ခ", "အကြောင်အရာ", "စစ်ဆေးမည်", "ဒဏ်ငွေကြေး", "ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ်", "မီတာ နံပါတ်",
    "မီတာ နံပါတ် ရိုက်သွင်းပါ", "ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ် ရွေးချယ်ပါ", "ဒဏ်ငွေကြေး ရိုက်သွင်းပါ", "ဘဏ်ဝန်ဆောင်ခ ရိုက်သွင်းပါ", "ဘဏ်စာရင်းလက်ကျန်ငွေ"];
  textEng: any = ["Account", "Package", "Month", "Commercial Tax", "Service Charges", "Amount", "Total Amount", "QR Scan", "Card Number", "SUBMIT", "Choose Account Number", "Choose Package Type", "Choose Month", "Enter Card Number", "Choose Biller Type", "Enter Biller Reference No.", "Enter Amount", "Choose Operator Type", "Enter Phone Number", "Enter Customer No.", "Enter Invoice No.", "Choose Items", "Enter Quantity", "Enter Price", "Total Amount Required", "Bill Type", "Customer Reference No.", "CNP Charges", "Bank Charges", "Merchant Charges", "Narrative", "Check", "Penalty", "Outstanding Bill", "Meter Number",
    "Enter Meter Number", "Choose Outstanding Bill", "Enter Penalty", "Enter Bank Charges", "Account Balance"];
  showFont: string[] = [];
  font: string;
  public loading;
  passtemp: any;
  userData: any;
  useraccount: any = [];
  errormessage: string;
  temppackagename: string = '';
  tempmonthname: string = '';
  referencename: string = '';

  packageData: any;
  packagetype: any;
  monthtype: any;
  mertchantID: string;
  userdata: any;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  msgCnpAcc: any;
  msgCnpbill: any;
  msgCnpamt: any;
  msgCnptotamt: any;
  msgCnpCusNo: any;
  ipaddress: string;
  primColor: any = { 'background-color': '#3B5998' };
  customCss: any = '';
  billdata: any = { accounttype: '', availableamount: '', merchanttype: '', packagetype: '', monthtype: '', cardno: '', amount: '', amountTax: '', amountServiceCharges: '', peroid: '', amountTotal: '' };
  //cnpText : any = ["Bill Type","Customer Reference No.","Amount","CNP Charges","Bank Charges","Merchant Charges","Total Amount","Narrative","Check"];
  cnpData: any = { accounttype: '', billType: '', CusrefNo: '', cnpCharge: '', bankCharges: '', amount: '', totalAmount: '', narrative: '' };
  SkypayText: any = ["Operator Type", "Phone Number", "Amount", "Bank Charges", "Merchant Charges", "Narrative"];
  skypayData: any = {};
  mptText: any = ["Customer No.", "Invoice No.", "Amount", "Bank Charges", "Narrative"];
  mptData: any = {};
  breweryText: any = ["Items", "Quantity", "Price", "Amount", "Bank Charges", "Merchant Charges", "Narrative"];
  breweryData: any = {};
  waterBillData: any = { accounttype: '', meterNo: '', outstandBill: '', penalty: '', bankCharges: '', amount: '', totalAmount: '', narrative: '' };
  waterMeterCheckData: any;
  referenceType: any;
  isenabled: boolean = false;
  amountType: any = [{ name: '1000', value: '1000' }, { name: '3000', value: '3000' }, { name: '5000', value: '5000' }, { name: '10000', value: '10000' }];

  msgwBillAcc: any;
  msgwBillMno: any;
  msgwBilloutstand: any;
  msgwBillamt: any;
  msgwBillbCharges: any;
  msgwBillPenalty: any;
  msgwBilltotamt: any;
  amt: any;
  bcharges: any;
  penalty: any;
  outstandBill: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public barcodeScanner: BarcodeScanner,
    public storage: Storage, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public events: Events, public all: AllserviceProvider, public network: Network, public idle: Idle,
    private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, private global: GlobalProvider) {
    this.passtemp = this.navParams.get('data');
    this.billdata.merchanttype = this.passtemp.merchantName;
    this.billdata.merchantid = this.passtemp.merchantID;
    this.billdata.processCode = this.passtemp.processingCode;


    if (this.passtemp.colorCode == '') {
      this.primColor = { 'background-color': '#3B5998' };
    } else {
      this.primColor = { 'background-color': this.passtemp.colorCode };
    }
    if (this.billdata.processCode == '080400') {
      this.customCss = 'resizeskynet';
      this.storage.get('skyNetCardNo').then((result) => {

        this.billdata.cardno = result;
      });
    }
    else
      this.customCss = 'resize';
    this.checkNetwork();
    if (this.flag == "success") {
      this.storage.get('userData').then((result) => {
        this.userData = result;
        this.storage.get('ipaddress').then((result) => {
          if (result == null || result == '') {
            this.ipaddress = this.global.ipaddress;
          }
          else {
            this.ipaddress = result;
            if (this.billdata.processCode == '080400')
              this.getPackage();
            else if (this.billdata.processCode != '080400' && this.billdata.processCode != '000100')
              this.getreferenceData();
          }
          this.getAccountSummary();
        });

      });

    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }

    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });

  }


  accountChange(data) {
    this.errormessage = '';
    this.billdata.accounttype = data;
    for (var i = 0; i < this.useraccount.length; i++) {
      if (data == this.useraccount[i].debitAcc) {
        this.billdata.availableamount = this.useraccount[i].avlBal;
      }
    }
  }

  clicksubmit() {

    this.checkNetwork();
    if (this.flag == "success") {
      let flag = false;
      let flag1 = false;
      let flag2 = false;
      let flag3 = false;
      let flag4 = false;
      let flag5 = false;
      let flag6 = false;
      if (this.billdata.processCode == '080400') {
        if (this.billdata.accounttype == '') {
          this.msgCnpAcc = this.showFont[10];
          flag = false;
        }
        else {
          this.msgCnpAcc = '';
          flag = true;
        }
        if (this.billdata.packagetype == '') {
          this.msgCnpbill = this.showFont[11];
          flag1 = false;
        }
        else {
          this.msgCnpbill = '';
          flag1 = true;
        }
        if (this.billdata.monthtype == '') {
          flag2 = false;
          this.msgCnpCusNo = this.showFont[12];
        }
        else {
          this.msgCnpCusNo = '';
          flag2 = true;
        }
        if (this.billdata.cardno == '') {
          this.msgCnpamt = this.showFont[13];
          flag3 = false;
        }
        else {
          this.msgCnpamt = '';
          flag3 = true;
        }
        if (flag && flag1 && flag2 && flag3) {
          console.error("Before send to confirm billdata>>" + JSON.stringify(this.billdata));
          this.storage.get('skyNetCardNo').then((result) => {
            if (result == null || result == '') {
              this.storage.set('skyNetCardNo', this.billdata.cardno);
            }
            else {
              this.storage.remove('skyNetCardNo');
              this.storage.set('skyNetCardNo', this.billdata.cardno);
            }
          });
          this.idle.stop();
          this.getOtpConfirm(this.billdata);
        }

      }
      else if (this.billdata.processCode == '050200') {


        if (this.cnpData.accounttype == '') {
          this.msgCnpAcc = this.showFont[10];
          flag = false;
        }
        else {
          this.msgCnpAcc = '';
          flag = true;
        }
        if (this.cnpData.billType == '') {
          this.msgCnpbill = this.showFont[14];
          flag1 = false;
        }
        else {
          this.msgCnpbill = '';
          flag1 = true;
        }

        //   if(this.isenabled){
        if (this.cnpData.CusrefNo == '' || this.cnpData.CusrefNo == undefined) {
          flag2 = false;
          this.msgCnpCusNo = this.showFont[19];
        }
        else {
          this.msgCnpCusNo = '';
          flag2 = true;
        }
        /*  }
          else{
              this.msgCnpCusNo = '';
              flag2 = true;
          }*/
        if (this.cnpData.amount == '' || this.cnpData.amount == undefined) {
          this.msgCnpamt = this.showFont[16];
          flag3 = false;
        }
        else {
          this.msgCnpamt = '';
          flag3 = true;
        }
        if (this.cnpData.totalAmount == '' || this.cnpData.totalAmount == undefined) {
          this.msgCnptotamt = this.showFont[24];
          flag4 = false;
        }
        else {
          this.msgCnptotamt = '';
          flag4 = true;
        }
        if (flag && flag1 && flag2 && flag3 && flag4) {

          //    this.cnpData.amountTotal = parseInt(this.cnpData.amount)+parseInt(this.cnpData.cnpCharge)+parseInt(this.cnpData.bankCharges);


          this.idle.stop();
          this.getOtpConfirm(this.cnpData);
        }
      }
      else if (this.billdata.processCode == '040300') {
        if (this.skypayData.accounttype == '') {
          this.errormessage = this.showFont[10];
        }
        else if (this.skypayData.referenceType == '') {
          this.errormessage = this.showFont[17];
        } else if (this.skypayData.phone == '' || this.skypayData.phone == undefined) {
          this.errormessage = this.showFont[18];
        } else if (this.skypayData.amount == '' || this.skypayData.amount == undefined) {
          this.errormessage = this.showFont[16];
        } else {
          this.skypayData.amountTotal = parseInt(this.skypayData.amount) + parseInt(this.skypayData.bankCharges);
          console.error("Before send to confirm skypayData>>" + JSON.stringify(this.skypayData));
          this.idle.stop();
          this.getOtpConfirm(this.skypayData);
        }
      }
      else if (this.billdata.processCode == '000100') {
        if (this.mptData.accounttype == '') {
          this.errormessage = this.showFont[10];
        }
        else if (this.mptData.customerNo == '') {
          this.errormessage = this.showFont[19];
        } else if (this.mptData.invoiceNo == '' || this.mptData.invoiceNo == undefined) {
          this.errormessage = this.showFont[20];
        } else if (this.mptData.amount == '' || this.mptData.amount == undefined) {
          this.errormessage = this.showFont[16];
        } else {
          this.mptData.amountTotal = parseInt(this.mptData.amount);
          console.error("Before send to confirm mptData>>" + JSON.stringify(this.mptData));
          this.idle.stop();
          this.getOtpConfirm(this.mptData);
        }
      }
      else if (this.billdata.processCode == '030001') {
        if (this.breweryData.accounttype == '') {
          this.errormessage = this.showFont[10];
        }
        else if (this.breweryData.referenceType == '') {
          this.errormessage = this.showFont[21];
        } else if (this.breweryData.quantity == '' || this.breweryData.quantity == undefined) {
          this.errormessage = this.showFont[22];
        } else if (this.breweryData.price == '' || this.breweryData.price == undefined) {
          this.errormessage = this.showFont[23];
        } else if (this.breweryData.amount == '' || this.breweryData.amount == undefined) {
          this.errormessage = this.showFont[16];
        } else {
          this.breweryData.amountTotal = parseInt(this.breweryData.quantity) * parseInt(this.breweryData.amount) + parseInt(this.breweryData.bankCharges);
          console.error("Before send to confirm breweryData>>" + JSON.stringify(this.breweryData));
          this.idle.stop();
          this.getOtpConfirm(this.breweryData);
        }
      }
      else if (this.billdata.processCode == '090500') {

        if (this.waterBillData.accounttype == '' || this.waterBillData.accounttype == undefined || this.waterBillData.accounttype == null) {
          this.msgwBillAcc = this.showFont[10];
          flag = false;
        }
        else {
          this.msgwBillAcc = '';
          flag = true;
        }

        //meter number
        if (this.waterBillData.meterNo == '' || this.waterBillData.meterNo == undefined || this.waterBillData.meterNo == null) {
          this.msgwBillMno = this.showFont[35];
          flag1 = false;
        }
        else {
          this.msgwBillMno = '';
          flag1 = true;
        }

        //outstandingbill
        if (this.waterBillData.outstandBill == '') {
          flag2 = false;
          this.msgwBilloutstand = this.showFont[36];
        }
        else {
          this.msgwBilloutstand = '';
          flag2 = true;
        }

        //amount
        if (this.waterBillData.amount == '' || this.waterBillData.amount == undefined || this.waterBillData.amount == null) {
          this.msgwBillamt = this.showFont[16];
          flag3 = false;
        }
        else {
          this.msgwBillamt = '';
          flag3 = true;
        }


        //bankCharges
        if (this.waterBillData.bankCharges == '' || this.waterBillData.bankCharges == undefined || this.waterBillData.bankCharges == null) {
          this.msgwBillbCharges = this.showFont[38];
          flag4 = false;
        }
        else {
          this.msgwBillbCharges = '';
          flag4 = true;
        }


        //penalty
        if (this.waterBillData.penalty == '' || this.waterBillData.penalty == undefined || this.waterBillData.penalty == null) {
          this.msgwBillPenalty = this.showFont[37];
          flag5 = false;
        }
        else {
          this.msgwBillPenalty = '';
          flag5 = true;
        }

        //totalamount
        if (this.waterBillData.totalAmount == '' || this.waterBillData.totalAmount == undefined || this.waterBillData.totalAmount == null) {
          this.msgwBilltotamt = this.showFont[24];
          flag6 = false;
        }
        else {
          this.msgwBilltotamt = '';
          flag6 = true;
        }

        if (flag && flag1 && flag2 && flag3 && flag4 && flag5 && flag6) {

          this.getOtpConfirm(this.waterBillData);
        }
      }

    } else {
      this.flag = 'none';
      this.all.showAlert('Warning!', "Check your internet connection!");
    }


  }

  getAccountSummary() {
    this.slimLoader.start(() => {
    });
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      // console.log("result:AccountSummary" + JSON.stringify(result));
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.useraccount = result.dataList;
        /* for (let i = 0; i < result.dataList.length; i++) {
          if (result.dataList[i].accType != 'Fixed Deposit Account') {
            this.useraccount.push(result.dataList[i]);
          }
        } */
        //  this.loading.dismiss();
        this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        this.logoutAlert(result.desc);
        this.slimLoader.complete();
      }
      else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        // this.loading.dismiss();
        this.slimLoader.complete();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  getOtpConfirm(res) {
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '1', merchantID: this.passtemp.merchantID };

    this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {

      if (data.code == "0000") {
        this.slimLoader.complete();

        this.navCtrl.push(ConfirmpagePage, {
          data: res,
          detail: this.passtemp,
          otp: data.rKey,
          sKey: data.sKey
        });
      }
      else if (data.code == "0016") {
        this.logoutAlert(data.desc);
        this.slimLoader.complete();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();
      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }
  QRscan() {

    this.barcodeScanner.scan().then((barcodeData) => {

      this.billdata.cardno = barcodeData.text;
      this.errormessage = '';
    }, (err) => {

    });
  }


  packageChange(data) {  //package.id
    for (let i = 0; i < this.packagetype.length; i++) {
      if (this.packagetype[i].id == data) {
        this.billdata.packagetype = this.packagetype[i].name;
        this.monthtype = this.packagetype[i].rateList;
      }
    }
    this.errormessage = '';
    this.tempmonthname = '';
    this.billdata.amountTax = '';
    this.billdata.amountServiceCharges = '';
    this.billdata.amount = '';
    this.billdata.amountTotal = '';
    // this.monthtype = data.rateList;
    //  this.billdata.packagetype=data.name;

  }

  amountChange(event, data) {
    if (!this.isenabled) {
      let x, y;
      if (this.cnpData.cnpCharge == '' || this.cnpData.cnpCharge == undefined)
        this.cnpData.cnpCharge = 0;
      else {
        x = this.cnpData.cnpCharge.replace(/,/g, "");
      }
      if (this.cnpData.bankCharges == '' || this.cnpData.bankCharges == undefined)
        this.cnpData.bankCharges = 0;
      else {
        y = this.cnpData.bankCharges.replace(/,/g, "");
      }
      this.cnpData.totalAmount = parseFloat(this.cnpData.amount) + parseFloat(x) + parseFloat(y);
      this.cnpData.totalAmount = this.addThousandSeparators(this.cnpData.totalAmount.toFixed(2));
    }
  }
  referenceChange(data) {
    if (this.billdata.processCode == '050200') {
      this.cnpData.amount = '';
      this.cnpData.CusrefNo = '';
      this.cnpData.billType = data.name;
      this.cnpData.billTypeValue = data.value;
      this.cnpData.bankCharges = data.bankCharges;
      this.cnpData.cnpCharge = data.merchantCharges;
      if (data.value != 'B-MPT') {
        this.isenabled = true;
        this.cnpData.totalAmount = '';
      }
      else {
        this.msgCnpCusNo = '';
        this.isenabled = false;
        this.cnpData.totalAmount = Number(this.cnpData.cnpCharge) + Number(this.cnpData.bankCharges);
      }
    }
    else if (this.billdata.processCode == '040300') {
      this.skypayData.referenceType = data.value;
      this.skypayData.bankCharges = data.bankCharges;
      //   this.skypayData.merchantCharges = data.merchantCharges;
    }
    else if (this.billdata.processCode == '030001') {
      this.breweryData.referenceType = data.value;
      this.breweryData.bankCharges = data.bankCharges;
      this.breweryData.price = data.price;
    }


  }

  inputChange(data, c) {
    this.errormessage = '';
    if (this.billdata.processCode == '030001') {
      this.breweryData.amount = parseInt(this.breweryData.price) * c;
    }
  }

  monthChange(data) {   //pid
    this.errormessage = '';
    for (let i = 0; i < this.monthtype.length; i++) {
      if (this.monthtype[i].did == data) {
        this.billdata.monthtype = this.monthtype[i].m;
        this.billdata.amount = this.monthtype[i].r;
        this.billdata.peroid = this.monthtype[i].m;
        this.billdata.did = this.monthtype[i].did;
        this.billdata.pid = this.monthtype[i].pid;
        this.billdata.tid = this.monthtype[i].tid;
        this.billdata.amountTax = this.monthtype[i].amountTax;
        this.billdata.amountTotal = this.monthtype[i].amountTotal;
        this.billdata.amountServiceCharges = this.monthtype[i].amountServiceCharges;

      }

    }
    // this.billdata.amount = data.totalamount;



  }

  getPackage() {
    this.slimLoader.start(() => {

    });
    /*this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange :true
      //   duration: 3000
      });
      this.loading.present();*/
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, merchantID: this.passtemp.merchantID, processingCode: this.passtemp.processingCode };

    this.http.post(this.ipaddress + '/service001/getPackages', param).map(res => res.json()).subscribe(data => {

      if (data.code == "0000") {
        this.packagetype = data.result.dataList;
        this.billdata.accessToken = data.result.accessToken;
        this.slimLoader.complete();
      }
      else if (data.code == "0016") {
        this.packagetype = [];
        this.logoutAlert(data.desc);
        this.slimLoader.complete();
      }
      else {
        this.packagetype = [];
        this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();
      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  getreferenceData() {
    this.slimLoader.start(() => {

    });
    /*this.loading = this.loadingCtrl.create({
     content: "Please wait...",
     dismissOnPageChange :true
     //   duration: 3000
     });
     this.loading.present();*/
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, processingCode: this.passtemp.processingCode };

    this.http.post(this.ipaddress + '/service001/getReferenceData', param).map(res => res.json()).subscribe(data => {

      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.dataList)) {
          tempArray.push(data.dataList);
          data.dataList = tempArray;
        }

        for (let i = 0; i < data.dataList.length; i++) {
          if (this.passtemp.processingCode == '050200') {
            if (data.dataList[i].value == 'BILL') {
              if (!Array.isArray(data.dataList[i].dataList)) {
                tempArray.push(data.dataList[i].dataList);
                data.dataList[i].dataList = tempArray;
              }
              this.referenceType = data.dataList[i].dataList;
            }

          }
          else {
            this.referenceType.push(data.dataList[i]);
          }
        }

        this.slimLoader.complete();
      }
      else if (data.code == "0016") {
        this.packagetype = [];
        this.logoutAlert(data.desc);
        this.slimLoader.complete();
      }
      else {
        this.packagetype = [];
        //  this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();
      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }
  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  checkCusNo() {
    this.slimLoader.start(() => {

    });
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, processingCode: this.passtemp.processingCode, cnpType: this.cnpData.billTypeValue, custRefNo: this.cnpData.CusrefNo };

    this.http.post(this.ipaddress + '/service001/checkCNPBillNo', param).map(res => res.json()).subscribe(data => {

      if (data.code == "0000") {
        this.cnpData.bankCharges = data.bankCharges;
        this.cnpData.cnpCharge = data.merchantCharges;
        this.cnpData.amount = data.amount;
        this.cnpData.totalAmount = data.totalAmount;
        this.slimLoader.complete();
      }
      else if (data.code == "0016") {
        this.packagetype = [];
        this.logoutAlert(data.desc);
        this.slimLoader.complete();
      }
      else {
        this.packagetype = [];
        this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();

      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  checkMeterNo() {

    if (this.waterBillData.meterNo != '') {
      this.msgwBillMno = '';
      this.slimLoader.start(() => {

      });
      //merchantid = 'M0001'
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, meterID: this.waterBillData.meterNo, merchantID: this.billdata.merchantid, field1: '', field2: '' };


      this.http.post(this.ipaddress + '/service001/getOutstandingBill', param).map(res => res.json()).subscribe(data => {


        if (data.code == "0000") {
          this.slimLoader.complete();
          let tempArray = [];
          if (!Array.isArray(data.outData)) {
            tempArray.push(data.outData);
            data.outData = tempArray;
          }
          this.waterMeterCheckData = [];
          this.outstandBill = '';
          this.waterMeterCheckData = data.outData;
          this.waterBillData.meterNo = data.meterID;
          this.waterBillData.outstandBill = '';
          this.waterBillData.bankCharges = '';
          this.waterBillData.amount = '';
          this.waterBillData.penalty = '';
          this.waterBillData.totalAmount = '';

        }
        else if (data.code == "0016") {
          this.packagetype = [];
          this.logoutAlert(data.desc);
          this.slimLoader.complete();
        }
        else if (data.code == "0014") {
          this.packagetype = [];
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();

        }
        else {
          this.packagetype = [];
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    } else {
      this.msgwBillMno = this.showFont[35];
    }

  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    });

  }
  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChoosepackagePage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChoosepackagePage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);

        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLeave() {

    this.slimLoader.reset();
  }

  outstanBilChange(data) {
    this.errormessage = '';
    this.outstandBill = data;
    if (data.code == '0000') {
      this.waterBillData.outstandBill = data.billNo;
      this.waterBillData.bankCharges = data.bankChargesAmt;
      this.waterBillData.amount = data.amount;
      this.waterBillData.penalty = data.penaltyAmt;
    } else {
      this.waterBillData.outstandBill = '0';
      this.waterBillData.bankCharges = '0';
      this.waterBillData.amount = '0';
      this.waterBillData.penalty = '0';
      this.all.showAlert('Warning!', data.desc);
    }
    let bcharge = this.waterBillData.bankCharges.replace(/,/g, "");
    this.bcharges = parseFloat(bcharge);

    let amt = this.waterBillData.amount.replace(/,/g, "");
    this.amt = parseFloat(amt);

    let pen = this.waterBillData.penalty.replace(/,/g, "");
    this.penalty = parseFloat(pen);

    this.waterBillData.totalAmount = this.amt + this.bcharges + this.penalty;
    this.waterBillData.totalAmount = this.addThousandSeparators(this.waterBillData.totalAmount.toFixed(2));
  }
  addThousandSeparators(number) { //

    let whole, fraction;
    let decIndex = number.toString().lastIndexOf('.');
    if (decIndex > -1) {
      whole = number.toString().substr(0, decIndex);
      fraction = number.toString().substr(decIndex);
    } else {
      whole = number.toString();
    }
    let rgx = /(\d+)(\d{3})/
    while (rgx.test(whole)) {
      whole = whole.replace(rgx, '$1' + ',' + '$2')
    }
    return fraction ? whole + fraction : whole
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
