
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';
import { UtilProvider } from '../../providers/util/util';

@Component({
  selector: 'page-skynet-success',
  templateUrl: 'skynet-success.html'
})
export class SkynetSuccessPage {

  textEng: any = [
    "Details", "From Account No.",
    "Card No.", "Package Name",
    "Voucher Type", "Amount",
    "Bank Charges", "Total Amount",
    "Bank Reference No.", "Transaction Date",
    "CLOSE", "Movie Name"
  ];
  textMyan: any = [
    "အသေးစိတ်အချက်အလက်", "အကောင့်နံပါတ် မှ",
    "ကဒ်နံပါတ်​", "ပက်​ကေ့နာမည်",
    "​ဘောက်ချာ အမျိုးအစား", "ငွေပမာဏ",
    "ဝန်​ဆောင်ခ", "စုစုပေါင်း ငွေပမာဏ",
    'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်',
    "ပိတ်မည်", "​ဇာတ်ကားနာမည်"
  ];
  showFont: any = [];
  font: any = '';

  merchantSuccessObj: any;
  amount: any;
  commissionAmount: any;
  totalAmount: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,public util: UtilProvider,
    public storage: Storage, public changeLanguage: ChangelanguageProvider
  ) {
    this.merchantSuccessObj = this.navParams.get("data");
    
    this.amount = this.util.formatAmount(this.merchantSuccessObj.amount);
    this.commissionAmount = this.util.formatAmount(this.merchantSuccessObj.commissionAmount);
    this.totalAmount = this.util.formatAmount(this.merchantSuccessObj.totalAmount);

    this.storage.get('language').then((font) => {
      this.font = font;

      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  closeSkynetSuccess() {
    let paramMerchantSuccessObj = {
      merchantID: this.merchantSuccessObj.merchantID,
      processingCode: this.merchantSuccessObj.merchantCode
    };

    this.navCtrl.setRoot(SkynetTopupPage, {
      data: paramMerchantSuccessObj
    });
  }

}
