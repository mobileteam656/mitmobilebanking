import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams,ViewController,Events } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
/**
 * Generated class for the PopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-popover-page',
  templateUrl: 'popover-page.html',
  providers: [Changefont]
})
export class PopoverPage {
  headeritem = "Settings";
  popoverItemList : any [] = [{name: '',key:0},{name: '',key:1}, {name: '',key:2},{name: '',key:3}];
  textMyan:any=[{name: 'လျှို့ဝှက်နံပါတ်ပြောင်းရန်',key:0},{name: 'ဘာသာစကား',key:1}, {name: "ဆက်သွယ်ရန်",key:2},{name: 'ဗားရှင်း 1.0.24 ',key:3}];
  textEng:any=[{name: 'Change Password',key:0},{name: 'Language',key:1}, {name: 'Call',key:2},{name:'Version 1.0.24 ',key:3}];
  showFont:any;
  font:string;
  tempArray:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public changefont:Changefont,public events:Events,public storage:Storage) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.changelanguage(font);
    });
  }

  changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      this.popoverItemList = this.textMyan;
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.changefont.UnitoZg(this.textMyan[j].name);
      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PopoverPage');
  }
  changeLanguage(s) {
    this.viewCtrl.dismiss(s);
  }
}
