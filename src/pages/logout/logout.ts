import { Component } from '@angular/core';
import { NavController, NavParams ,Events} from 'ionic-angular';
import { Storage  } from '@ionic/storage';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';
/**
 * Generated class for the Logout page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
    providers : [ChangelanguageProvider]
})
export class Logout {
  passtemp : any;
  showFont:string [] = [];

 textEng: any=['Thanks for using mBanking!','Last Time Log In','Last Time Log out','Duration','Activity History','mBanking'];
  textMyan : any = ['ကျေးဇူးတင်ပါသည်','နောက်ဆုံးဝင်ရောက်ခဲ့သည့်အချိန်','နောက်ဆုံးထွက်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အကြောင်းအရာ','နေပြည်တော် စည်ပင်ဘဏ်'];

 /* textEng: any=['Thanks for using Shwe Bank!','Last Time Log In','Last Time Log out','Duration','Activity History','Shwe Bank'];
  textMyan : any = ['ကျေးဇူးတင်ပါသည်','နောက်ဆုံးဝင်ရောက်ခဲ့သည့်အချိန်','နောက်ဆုံးထွက်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အကြောင်းအရာ','ရွှေဘဏ်'];
*/
/*    textEng: any=['Thanks for using TCB mBanking!','Last Time Log In','Last Time Log out','Duration','Activity History','TCB mBanking'];
  textMyan : any = ['ကျေးဇူးတင်ပါသည်','နောက်ဆုံးဝင်ရောက်ခဲ့သည့်အချိန်','နောက်ဆုံးထွက်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အကြောင်းအရာ','ထွန်းကော်မာရှယ်ဘဏ်'];
*/

    /* textEng: any=['Thanks for using mBanking 360!','Last Time Log In','Last Time Log out','Duration','Activity History','mBanking 360'];
    textMyan : any = ['ကျေးဇူးတင်ပါသည်','နောက်ဆုံးဝင်ရောက်ခဲ့သည့်အချိန်','နောက်ဆုံးထွက်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အချိန်','လုပ်ဆောင်ခဲ့သည့်အကြောင်းအရာ',"mBanking 360"];
 */
    font:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public events:Events,public changeLanguage:ChangelanguageProvider,private firebaseCrashlytics: FirebaseCrashlytics) {
    this.passtemp = this.navParams.get('data');
    // console.log('passtemp Logout='+JSON.stringify(this.passtemp));
    this.events.subscribe('changelanguage', lan => {
        this.font = lan.data;
        this.changeLanguage.changelanguage(lan.data, this.textEng,this.textMyan).then(data =>
        {
            // console.log("Ngar data " + JSON.stringify(data));
            this.showFont = data;
            // console.log("Show pe lay " + JSON.stringify(this.showFont));
        });
    });
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
        this.font = font;
        this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
        {
            // console.log("Ngar data " + JSON.stringify(data));
            this.showFont = data;
            // console.log("Show pe lay " + JSON.stringify(this.showFont));
        });
    });
  }

  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
     // this.showFont=this.textMyan;
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }

    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont.push( this.changefont.UnitoZg(this.textMyan[j]));
      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  ionViewDidLoad() {
    // console.log('ionViewDidLoad Logout');
  }
  getlogout(){
    // const crashlytics = this.firebaseCrashlytics.initialise();
    // crashlytics.logException('Internet conection error..');
    this.navCtrl.setRoot(Login);
  }

}
