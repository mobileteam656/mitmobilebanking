import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { CorporateMakerDetailPage } from '../corporate-maker-detail/corporate-maker-detail';
import { Login } from '../login/login';

@Component({
  selector: 'page-corporate-maker-comfirm',
  templateUrl: 'corporate-maker-comfirm.html',
  providers: [ChangelanguageProvider]
})
export class CorporateMakerComfirmPage {

  accType: any;
  passTemp: any; 
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "အတည်ပြုခြင်း", "ငွေလွှဲ အမျိုးအစား", "လွှဲပြောင်းမည့် အမျိုးအစား"];
  textEng: any = ["From Account No.", "To Account No.", "Amount", "Narrative", "CANCEL", "CONFIRM", "Confirmation", "Transfer Type", "Account Type","To Name"];
  showFont: any = [];
  font: any = '';
  data: any;
  userdata: any;
  loading: any;
  flag: string;
  detail: any;
  ipaddress: string;
  // passType: any;
  //passOtp: any;
  //passSKey: any;
  //fromPage: any; 
  amount: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public all: AllserviceProvider, public util: UtilProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public changefont: Changefont, ) {
    this.passTemp = this.navParams.get("data");
    //this.passType = this.navParams.get("type");
    //this.passOtp = this.navParams.get("otp");
    //this.passSKey = this.navParams.get('sKey');
    //this.fromPage = this.navParams.get('fromPage');
    this.amount = this.util.formatAmount(this.passTemp.amount);
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    /* this.storage.get('language').then((font) => {
        this.font = font;
        this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
            this.showFont = data;
        });
    }); */

    this.storage.get('userData').then((data) => {
      this.userdata = data;

      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }

  ionViewDidLoad() {

    this.checkNetwork();
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  goComfirm() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
    });
    this.loading.present();
      let parameter = {
        autokey : "0",
        requestuserid : this.userdata.userID,
        requestusername : this.userdata.userName,
        transactiontype : "2",
        status : "1",
        amount : this.passTemp.amount,
        currencycode : "MMK",
        t1 : this.passTemp.fromAcc,
        t2 : this.passTemp.toAcc,
        t3 : this.passTemp.accType,
        t4 : this.passTemp.toName,
        };
        console.log(JSON.stringify("Maker Comfirm" + parameter));
      this.http.post(this.ipaddress + '/service005/saveApproveTransaction', parameter).map(res => res.json()).subscribe(data1 => {

        if (data1.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.setRoot(CorporateMakerDetailPage, {
            data: data1,
            detail: this.passTemp,
            //type: this.passType,
            //fromPage: this.fromPage
          })
        }
        else if (data1.code == "0016") {
          this.logoutAlert(data1.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data1.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });

  }
  cancelbtn() {
    this.navCtrl.pop();
  }

  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
