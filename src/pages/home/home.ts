import 'rxjs/Rx';

import {
    AlertController, Events, LoadingController, MenuController, NavController, NavParams, Platform,
    PopoverController, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { AccountSummaryDetail } from '../account-summary-detail/account-summary-detail';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { PopoverPage } from '../popover-page/popover-page';

declare var window;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ChangelanguageProvider]
})
export class HomePage {
  userData: any = {};
  allaccount: any;
  status: any;
  loading: any;
  flag: string;
  useraccount: any;
  currentaccount: any = [];
  savingaccount: any = [];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  shwe: string;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  font: string;
  textEng: any = ['Account Summary', 'No result Found!', 'Pull to refresh', 'Refreshing', 'Are you sure you want to exit', 'Yes', 'No'];
  textMyan: any = ['စာရင်းချုပ်', 'အချက်အလက်မရှိပါ', 'အချက်အလက်သစ်များရယူရန်အောက်သို့ဆွဲပါ', 'အချက်အလက်သစ်များရယူနေသည်', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ'];
  showFont: any = [];
  idleCtrl: boolean;
  popover: any;
  constructor(public navCtrl: NavController, public menu: MenuController, public idle: Idle, public navParams: NavParams, public storage: Storage,
    public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public events: Events, public network: Network,
    public popoverCtrl: PopoverController, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, private keepalive: Keepalive,
    public platform: Platform, public global: GlobalProvider) {
    let temp = this.navParams.get('data');
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.storage.get('ipaddress').then((result1) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result1;
          this.getAccountSummary();
        }
      });
    });
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service001/getAccountSummary', parameter).map(res => res.json()).subscribe(result => {
      console.log("getAccountSummary>>>", result);
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;          
        }
        /* let accountGroup= result.dataList.reduce(function (obj, item) {
          obj[item.currentBal] = obj[item.avlBal.substring(0, 1)] || [];
          obj[item.currentBal].push(item);
          return obj;
        }, {}); */
        let group_to_values = result.dataList.reduce(function (obj, item) {
          obj[item.accType] = obj[item.accType] || [];
          obj[item.accType].push(item);
          return obj;
        }, {});
        let groups = Object.keys(group_to_values).map(function (key) {
          return { accType: key, data: group_to_values[key] };
        });
        this.allaccount = groups;
        this.status = 1;
        this.loading.dismiss();
        //this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.allaccount = [];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        // this.slimLoader.complete();
        this.loading.dismiss();
      });
  }

  idleWatch() {
    this.idle.setIdle(2);
    this.idle.setTimeout(5 * 60);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
    });
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      if (this.idleCtrl) {
        this.idleCtrl = false;
        this.gologout();
        this.idle.stop();
      }

    });

    this.idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!'
    });
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      var data = countdown / 60;
      this.min = data.toString().split('.')[0];
      this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
      this.sec = (Math.round(this.sec * 100) / 100);
      this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
    });

    this.keepalive.interval(15);

    this.keepalive.onPing.subscribe(() => {
      this.lastPing = new Date();
    });
    this.reload();
  }

  checkNetwork(cb) {
    if (this.network.type == "none") {
      this.flag = 'none';
      cb(this.flag);
    } else {
      this.flag = 'success';
      cb(this.flag);
    }
  }

  ionViewCanEnter() {
  }

  ionViewDidLoad() {
  }

  ionViewDidLeave() {
    this.reload();
    this.idleCtrl = true;
    this.slimLoader.reset();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  doRefresh(refresher) {
    this.currentaccount = [];
    this.savingaccount = [];
    //this.getAccountSummary();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  gotoDetail(a) {
    this.navCtrl.push(AccountSummaryDetail,
      {
        data: a
      });
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {
    this.idleCtrl = false;
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.set('userData', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
      },
        error => {
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }
          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        });
    });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
