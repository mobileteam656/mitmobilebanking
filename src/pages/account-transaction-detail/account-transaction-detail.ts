import 'rxjs/Rx';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { AllserviceProvider } from '../../providers/allservice/allservice';
/**
 * Generated class for the AccountTransactionDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-account-transaction-detail',
  templateUrl: 'account-transaction-detail.html',
  providers : [ChangelanguageProvider]
})
export class AccountTransactionDetail {
  b: any[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  showFont:string [] = [];
  textEng: any=['Account Details','Account Number','Reference Number','Transaction Date','Amount','Description'];
  textMyan : any = ['အကောင့်အသေးစိတ်အချက်အလက်','စာရင်းနံပါတ်','အမှတ်စဉ်','လုပ်ဆောင်ခဲ့သည့်ရက်','ငွေပမာဏ','အကြောင်းအရာ'];
  font:string;
  public loading;
  ipaddress:string;
  userdata:any;
  constructor(public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams,public idle:Idle,public events:Events,public storage:Storage,public changeLanguage:ChangelanguageProvider,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public http:Http) {
    this.b=navParams.get("data");
    

    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });

    });

    this.storage.get('userData').then((val) => {
      this.userdata = val;
    });
  }

  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }

    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }

    }
  }*/
  idleWatch(){
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5*60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='AccountTransactionDetail')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='AccountTransactionDetail'){
        var data=countdown/60;
        this.min=data.toString().split('.')[0];
        this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        this.sec=  (Math.round(this.sec * 100) / 100);
       // 
        this.idleState = 'You\'ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  gologout() {
    
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = {userID: this.userdata.userID, sessionID: this.userdata.sessionID};

      
      this.http.post(this.ipaddress+'/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.loading.dismiss();
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess','');
            this.storage.remove('userData');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              // duration: 2000,
              // position: 'bottom'
              showCloseButton: true,
              dismissOnPageChange: true,
              closeButtonText: 'Ok'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            showCloseButton: true,
            dismissOnPageChange: true,
            closeButtonText: 'Ok'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }
  ionViewDidLeave(){
    
  }

  ionViewDidLoad() {
    
   // // this.idleWatch();
  }

}
