import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Select, PopoverController, Platform, Events, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MainHomePage } from '../main-home/main-home';
import { PopoverPage } from '../popover-page/popover-page';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { TaxPaymentPage } from '../tax-payment/tax-payment';
import { TopUpListPage } from '../top-up-list/top-up-list';
import { TaxBusinessProfilePage } from '../tax-business-profile/tax-business-profile';
import {UtilProvider} from '../../providers/util/util';
declare var window;

@Component({
  selector: 'page-tax-info',
  templateUrl: 'tax-info.html',
})
export class TaxInfoPage {
  textMyan: any = ["အခွန်ပေးဆောင်ခြင်း", "ဖုန်းနံပါတ်", "အခွန်နံပါတ်", "ပြန်စမည်", "လုပ်ဆောင်မည်"];
  textEng: any = ["Tax Payment", "Phone Number", "TIN No.", "RESET", "SUBMIT"];
  textErrorEng: any = ["Please fill Phone number.", "Please fill TIN number."];
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး ဖုန်းနံပါတ် ရိုက်ထည့်ပါ", "ကျေးဇူးပြုပြီး အခွန်နံပါတ် ရိုက်ထည့်ပါ"];
  textError: string[] = [];
  textData: any = [];
  font: any = '';
  tinMsg: any;
  phoneMsg: any;
  taxData: any = {};
  popover: any;
  loading: any;
  ipaddress: any;
  userData: any;
  paymentList: any = [];
  taxList: any = [];
  businessList: any = [];
  incomeYear: any = [];
  taxPeriod: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public popoverCtrl: PopoverController, public storage: Storage,
    public http: Http, public platform: Platform,public util:UtilProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public changefont: Changefont, public global: GlobalProvider, public all: AllserviceProvider) {
    this.taxData.processingCode = '200900';
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      })
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  inputChange(data) {
    if (data == 1) {
      this.phoneMsg = "";
    } else if (data == 2) {
      this.tinMsg = "";
    }
  }

  validation() {
    let f1 = false, f2 = false;
    let num = /^[0-9-\+]*$/;
    if (this.taxData.phoneNo != null && this.taxData.phoneNo != '' && this.taxData.phoneNo != undefined) {
      if (num.test(this.taxData.phoneNo)) {
        if (this.taxData.phoneNo.indexOf("+") == 0 && (this.taxData.phoneNo.length == 12 || this.taxData.phoneNo.length == 13 || this.taxData.phoneNo.length == 11)) {
          this.phoneMsg = '';
          f1 = true;
        }
        else if (this.taxData.phoneNo.indexOf("7") == 0 && this.taxData.phoneNo.length == 9) {
          this.phoneMsg = '';
          this.taxData.phoneNo = '+959' + this.taxData.phoneNo;
          f1 = true;
        }
        else if (this.taxData.phoneNo.indexOf("9") == 0 && this.taxData.phoneNo.length == 9) {
          this.phoneMsg = '';
          this.taxData.phoneNo = '+959' + this.taxData.phoneNo;
          f1 = true;
        }
        else if (this.taxData.phoneNo.indexOf("+") != 0 && this.taxData.phoneNo.indexOf("7") != 0 && this.taxData.phoneNo.indexOf("9") != 0 && (this.taxData.phoneNo.length == 8 || this.taxData.phoneNo.length == 9 || this.taxData.phoneNo.length == 7)) {
          this.phoneMsg = '';
          this.taxData.phoneNo = '+959' + this.taxData.phoneNo;
          f1 = true;
        } else if (this.taxData.phoneNo.indexOf("09") == 0 && (this.taxData.phoneNo.length == 10 || this.taxData.phoneNo.length == 11 || this.taxData.phoneNo.length == 9)) {
          this.phoneMsg = '';
          this.taxData.phoneNo = '+959' + this.taxData.phoneNo.substring(2);
          f1 = true;
        }
        else if (this.taxData.phoneNo.indexOf("959") == 0 && (this.taxData.phoneNo.length == 11 || this.taxData.phoneNo.length == 12 || this.taxData.phoneNo.length == 10)) {
          this.phoneMsg = '';
          this.taxData.phoneNo = '+959' + this.taxData.phoneNo.substring(3);
          f1 = true;
        }
        else {
          f1 = false;
          this.phoneMsg = this.textError[0];
        }
      }
      else {
        f1 = false;
        this.phoneMsg = this.textError[0];
      }
      /*  this.phoneMsg = '';
       f1 = true; */
    } else {
      this.phoneMsg = this.textError[0];
      f1 = false;
    }

    if (this.taxData.tinNo != null && this.taxData.tinNo != '' && this.taxData.tinNo != undefined) {
      this.tinMsg = '';
      f2 = true;
    }
    else {
      this.tinMsg = this.textError[1];
      f2 = false;
    }
    if (f1 && f2) {
      this.checkTinNo();
    }/*  else {
      this.all.showAlert('Warning!', 'Please fill require field.');
    } */
  }

  checkTinNo() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.tinMsg = '';
    this.taxData.fromAcc = undefined;
    this.taxData.taxAmount = "";
    this.taxData.narrative = "";
    this.taxData.industryCode = "";
    this.taxData.companyType = "";
    this.taxData.creditorBranchCode = "";
    this.taxData.creditorBranchName = "";
    this.taxData.typeOfBusi = "";
    this.taxData.mdaccNo = "";
    this.taxData.taxOfficeName = "";
    this.taxData.taxType = "";
    this.taxData.paymentTypeCode = "";
    this.taxData.paymentTypeDesc = "";
    this.taxData.taxTypeCode = "";
    this.taxData.taxTypeDesc = "";
    this.taxData.incomeYearCode = "";
    this.taxData.incomeYearDesc = "";
    this.taxData.taxPeriodCode = "";
    this.taxData.taxPeriodDesc = "";
    let phoneNo=this.util.removePlus(this.taxData.phoneNo);
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, phoneNo: phoneNo, tinNo: this.taxData.tinNo, processingCode: this.taxData.processingCode };
    this.http.post(this.ipaddress + '/service002/checkTinNo', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.taxData.merchantID = data.merchantID
        //from api data
        this.taxData.profileID = data.profileID
        this.taxData.profileName = data.profileName
        this.taxData.regionCode = data.regionCode
        // this.taxData.sessionCode=data.sessionCode
        // this.taxData.mainBusiness = data.mainBusiness
        this.taxData.quarter = data.quarter
        this.taxData.phoneNumber = data.phoneNumber
        this.taxData.faxNumber = data.faxNumber
        this.taxData.emailAddress = data.emailAddress
        this.taxData.townShipCode = data.townShipCode
        this.taxData.registrationNo = data.registrationNo
        this.taxData.street = data.street
        this.taxData.houseNo = data.houseNo
        this.taxData.refNo = data.refNo

        let paymentArray = [];
        if (!Array.isArray(data.paymentData)) {
          paymentArray.push(data.paymentData);
          data.paymentData = paymentArray;
        }
        this.paymentList = data.paymentData
        let taxArray = [];
        if (!Array.isArray(data.taxData)) {
          paymentArray.push(data.taxData);
          data.taxData = taxArray;
        }
        this.taxList = data.taxData
        let bArray = [];
        if (!Array.isArray(data.businessProfileData)) {
          bArray.push(data.businessProfileData);
          data.businessProfileData = bArray;
        }
        this.businessList = data.businessProfileData

        let iArray = [];
        if (!Array.isArray(data.incomeYear)) {
          iArray.push(data.incomeYear);
          data.incomeYear = iArray;
        }
        this.incomeYear = data.incomeYear

        let pArray = [];
        if (!Array.isArray(data.taxPeriod)) {
          pArray.push(data.taxPeriod);
          data.taxPeriod = pArray;
        }
        this.taxPeriod = data.taxPeriod

        this.loading.dismiss();
        //this.navCtrl.push(TaxPaymentPage, {
        this.navCtrl.push(TaxBusinessProfilePage, {
          data: this.taxData,
          paymentList: this.paymentList,
          taxList: this.taxList,
          businessList: this.businessList,
          incomeYear: this.incomeYear,
          taxPeriod: this.taxPeriod
        });
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.taxData.name = '';
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  reset() {
    this.taxData.phoneNo = "";
    this.taxData.tinNo = "";
    this.phoneMsg = "";
    this.tinMsg = "";
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButton() {
    this.navCtrl.setRoot(TopUpListPage, {
      data: '1'
    });
  }

  onChange(event,i) {
    if (i == "09") {
      this.taxData.phoneNo = "+959";
    }
    else if (i == "959") {
      this.taxData.phoneNo = "+959";
    }
  }
}
