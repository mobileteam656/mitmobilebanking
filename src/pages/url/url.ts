import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, NavController } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Login } from '../login/login';

@Component({
  selector: 'page-url',
  templateUrl: 'url.html',
  providers: [ChangelanguageProvider]
})
export class UrlPage {

  textMyan: any =
    [
      "URL", "URL",
      "OK"
    ];
  textEng: any = [
    "URL", "URL",
    "OK"
  ];

  font: any;
  showFont: any;

  url: any;
  errMsgUrl: any;

  constructor(
    private storage: Storage, public events: Events,
    public navCtrl: NavController, public global: GlobalProvider,
    public changeLanguage: ChangelanguageProvider
  ) {
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(
        data => {
          this.showFont = data;
        }
      );
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(
        data => {
          this.showFont = data;
        }
      );
    });
  }

  setUrl() {
    if (this.url == undefined || this.url == null || this.url == '') {
      this.errMsgUrl = "Please enter URL.";
    } else {
      this.global.ipaddress = this.url;
    }
  }

  backButton() {
    this.navCtrl.setRoot(Login);
  }

}
