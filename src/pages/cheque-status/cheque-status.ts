import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-cheque-status',
  templateUrl: 'cheque-status.html',
  providers: [ChangelanguageProvider]
})
export class ChequeStatusPage {

  textMyan: any = [
    "ချက်နံပါတ် စစ်ဆေးရန်", "ချက်နံပါတ်",
    "စစ်ဆေးရန်", "Amount",
    "Expired Date"
  ];
  textEng: any = [
    "Cheque Inquiry", "Cheque No.",
    "Inquiry", "Amount",
    "Expired Date"
  ];
  textError: any = ["Please fill Cheque No."];
  font: string;
  showFont: any = [];

  ipaddress: any;
  loading: any;
  chequeno: any;
  amount: any;
  expiredDate: any;
  toaccMsg: any = '';
  disable_flag: boolean = false;
  deviceID: string = '';
  amountInfo = "";
  dateInfo = "";

  constructor(
    private storage: Storage, public http: Http,
    public changeLanguage: ChangelanguageProvider, public events: Events,
    public global: GlobalProvider, public loadingCtrl: LoadingController,
    public device: Device, public all: AllserviceProvider,
    public alertCtrl: AlertController
  ) {
    console.log("Am");
    this.storage.get('ipaddress').then((result) => {
      if (result == undefined || result == null || result == '') {
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = result;
      }
    });

    if (this.device.uuid != undefined && this.device.uuid != null && this.device.uuid != '') {
      this.deviceID = this.device.uuid;
    }

    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(
        data => {
          this.showFont = data;
        }
      );
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(
        data => {
          this.showFont = data;
        }
      );
    });
  }

  clicksubmit() {
    this.toaccMsg = "";
    this.disable_flag = false;

    if (this.chequeno == null || this.chequeno == '' || this.chequeno == undefined) {
      this.toaccMsg = this.textError[0];
    }

    if (this.toaccMsg == '') {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();

      let parameter = {
        deviceID: this.deviceID,
        chequeNo: this.chequeno
      };

      this.http.post(this.ipaddress + '/service005/getChequeInfo', parameter).map(res => res.json()).subscribe(
        result => {
          this.loading.dismiss();

          if (this.chequeno == null || this.chequeno == '' || this.chequeno == undefined) {
            this.toaccMsg = this.textError[0];
          }
          else if (result.code == "0000") {
            this.disable_flag = true;
            this.toaccMsg = '';
            this.amount = result.amount;
            this.expiredDate = result.date;

            // this.amountInfo = "Amount of Cheque No. " 
            // + this.chequeno + " is " + result.amount + " MMK and its expiry date is " + result.date + '.';

            this.amountInfo = result.amount + ' ' + result.ccy + " is available for Cheque No. " + this.chequeno + '.';
            this.dateInfo = "Expiry date is " + result.date + '.';

            let confirm = this.alertCtrl.create({
              title: 'Cheque Information',
              enableBackdropDismiss: false,
              message: this.amountInfo + ' ' + this.dateInfo,
              buttons: [{
                text: 'OK'
              }],
              cssClass: 'infoAlert',
            });
            confirm.present();
          } else if (result.code == "0014") {
            this.all.showAlert("Warning!", result.desc);
          } else if (result.code == "0016") {
            this.all.showAlert("Warning!", result.desc);
          }
        },
        error => {
          this.all.showAlert("Warning!", this.all.getErrorMessage(error));
          this.loading.dismiss();
        }
      );
    }
  }

}
