import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ConfirmpagePage } from '../confirmpage/confirmpage';
import { Login } from '../login/login';

@Component({
  selector: 'page-choosetopup',
  templateUrl: 'choosetopup.html',
  providers : [ChangelanguageProvider]
})
export class ChoosetopupPage {
  textMyan : any = ["အကောင့်နံပါတ်", "Package အမျိုးအစား", "သက်တမ်း","အခွန်နှုန်း", "ဝန်ဆောင်ခနှုန်း", "ငွေပမာဏ", "စုစုပေါင်းငွေပမာဏ", "QR ဖတ်ရန်", "ကဒ်နံပါတ်", "ပေးချေမည်","စာရင်းအမှတ်ရွေးချယ်ပါ","Package အမျိုးအစားရွေးချယ်ပါ","သက်တမ်းအမျိုးအစားရွေးချယ်ပါ","ကတ်နံပါတ်ရိုက်သွင်းပါ"];
  
  textEng: any = ["Account", "Package", "Month", "Commercial Tax", "Service Charges", "Amount", "Total Amount", "QR Scan", "Card Number", "SUBMIT","Choose Account Number","Choose Package Type","Choose Month","Enter Card Number",
  "Choose Biller Type","Enter Biller Reference No.","Enter Amount","Choose Operator Type","Enter Phone Number","Enter Customer No.","Enter Invoice No.",
    "Choose Items","Enter Quantity","Enter Price"];
  showFont : string [] = [];
  font:string;
  public  loading;
  passtemp : any;
  userData :any;
  useraccount:any;
  errormessage:string;
  temppackagename:string = '';
  tempmonthname:string = '';
  /* referencename:any; */
  cnpname:string = '';

  packageData:any;
  packagetype:any;
  monthtype:any;
  mertchantID:string;
  userdata:any;
  flag:string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  ipaddress:string;
  primColor : any = {'background-color' :'#236ee3'};
  customCss : any ='';
  billdata : any = {accounttype:'',availableamount:'',merchanttype:'',packagetype:'',monthtype:'',cardno:'',amount:'',amountTax:'',amountServiceCharges:'',peroid:'',amountTotal:''};
  cnpText : any = ["Payment Type","Cust Ref No.","Amount","Bank Charges","Total Amount","Merchant Charges","Narrative"];
  cnpData : any = {};
  SkypayText : any = ["Operator Type","Phone Number","Amount","Bank Charges","Merchant Charges","Narrative"];
  skypayData : any = {};
  mptText : any = ["Customer No.","Invoice No.","Amount","Bank Charges","Narrative"];
  mptData : any = {};
  breweryText : any = ["Items","Quantity","Price","Amount","Bank Charges","Merchant Charges","Narrative"];
  breweryData : any ={};
  referenceType : any ={};
  cnpType : any;
  amountType : any = [{name:'1000',value:'1000'},{name:'3000',value:'3000'},{name:'5000',value:'5000'},{name:'10000',value:'10000'}];
  topUpamountType : any = [{name:'1000',value:'1000'},{name:'3000',value:'3000'},{name:'5000',value:'5000'},{name:'10000',value:'10000'},{name:'Other',value:'Other'}];
  checkShow: boolean = false;
  //flagCode :any;
  topUpAmount: any;
  customizeAmount:any;
  customBox: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public barcodeScanner: BarcodeScanner,
              public storage:Storage,public all: AllserviceProvider,public http: Http,public toastCtrl:ToastController,public loadingCtrl:LoadingController,public alertCtrl:AlertController,
  public events:Events,public  network: Network,public idle:Idle,private slimLoader: SlimLoadingBarService, public changeLanguage:ChangelanguageProvider) {
    this.passtemp = this.navParams.get('data');
    this.billdata.merchanttype = this.passtemp.merchantName;
    this.billdata.merchantid = this.passtemp.merchantID;
    this.billdata.processCode = this.passtemp.processingCode;
    
    
    if(this.passtemp.colorCode==''){
      this.primColor= {'background-color' :'#236ee3'};
    }else{
      this.primColor= {'background-color' :this.passtemp.colorCode};
    }
    if(this.billdata.processCode == '080400') {
      this.customCss = 'resizeskynet';
      this.storage.get('skyNetCardNo').then((result) => {
        
        this.billdata.cardno = result;
      });
    }
    else
      this.customCss = 'resize';
    this.checkNetwork();
    if(this.flag=="success"){
      this.storage.get('userData').then((result) => {
        this.userData = result;
        this.useraccount = this.userData.debitAcc;
        
        this.storage.get('ipaddress').then((result) => {
          
          if(result == null || result ==''){
            
          }
          else{
            this.ipaddress=result;
            if(this.billdata.processCode == '080400')
              this.getPackage();
            else if(this.billdata.processCode != '080400' && this.billdata.processCode != '000100' )
              this.getreferenceData();
          }
        });

      });

    }else{
      let toast = this.toastCtrl.create({
        message:"Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }

    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });

    });

   }


  accountChange(data){
    this.errormessage='';
    this.billdata.accounttype = data;
    for(var i=0;i<this.useraccount.length;i++){
      if(data == this.useraccount[i].debitAcc){
        this.billdata.availableamount = this.useraccount[i].avlBal;
      }
    }
  }

  clicksubmit(){

    this.checkNetwork();
    if(this.flag=="success"){
      if(this.billdata.processCode == '080400'){
        if(this.billdata.accounttype==''){
          this.errormessage =this.showFont[10];
        }else if(this.billdata.packagetype =='' || this.billdata.packagetype==undefined){
          this.errormessage =this.showFont[11];
        }else if(this.billdata.monthtype ==''|| this.billdata.monthtype==undefined){
          this.errormessage =this.showFont[12];
        }else if(this.billdata.cardno ==''){
          this.errormessage =this.showFont[13];
        }else{
          console.error("Before send to confirm billdata>>"+JSON.stringify(this.billdata));
          this.storage.get('skyNetCardNo').then((result) => {
            if (result == null || result == '') {
              this.storage.set('skyNetCardNo', this.billdata.cardno);
            }
            else {
              this.storage.remove('skyNetCardNo');
              this.storage.set('skyNetCardNo', this.billdata.cardno);
            }
          });
          this.idle.stop();
          this.navCtrl.push(ConfirmpagePage,{
            data:this.billdata,
            detail : this.passtemp
          });
        }
      }
      else if(this.billdata.processCode == '050200'){
        if(this.cnpData.accounttype==''){
          this.errormessage =this.showFont[10];
        }
        else if(this.cnpData.referenceType==''){
          this.errormessage =this.showFont[14];
        }else if(this.cnpData.refNo =='' || this.cnpData.refNo==undefined){
          this.errormessage =this.showFont[15];
        }else if(this.cnpData.amount ==''|| this.cnpData.amount==undefined){
          this.errormessage =this.showFont[16];
        }else{

          this.cnpData.amountTotal = parseInt(this.cnpData.amount)+parseInt(this.cnpData.cnpCharge)+parseInt(this.cnpData.bankCharges);
          
          
          this.idle.stop();
          this.navCtrl.push(ConfirmpagePage,{
            data:this.cnpData,
            detail : this.passtemp
          });
        }
      }
      else if(this.billdata.processCode == '040300'){
        if(this.skypayData.accounttype==''){
          this.errormessage =this.showFont[10];
        }
        else if(this.skypayData.referenceType==''){
          this.errormessage =this.showFont[17];
        }else if(this.skypayData.phone =='' || this.skypayData.phone==undefined){
          this.errormessage =this.showFont[18];
        }else if(this.skypayData.amount ==''|| this.skypayData.amount==undefined){
          this.errormessage =this.showFont[16];
        }else{
          this.skypayData.amountTotal = parseInt(this.skypayData.amount)+parseInt(this.skypayData.bankCharges);
          console.error("Before send to confirm skypayData>>"+JSON.stringify(this.skypayData));
          this.idle.stop();
          this.navCtrl.push(ConfirmpagePage,{
            data:this.skypayData,
            detail : this.passtemp
          });
        }
      }
      else if(this.billdata.processCode == '000100'){
        if(this.mptData.accounttype==''){
          this.errormessage =this.showFont[10];
        }
        else if(this.mptData.customerNo==''){
          this.errormessage =this.showFont[19];
        }else if(this.mptData.invoiceNo =='' || this.mptData.invoiceNo==undefined){
          this.errormessage =this.showFont[20];
        }else if(this.mptData.amount ==''|| this.mptData.amount==undefined){
          this.errormessage =this.showFont[16];
        }else{
          this.mptData.amountTotal = parseInt(this.mptData.amount);
          console.error("Before send to confirm mptData>>"+JSON.stringify(this.mptData));
          this.idle.stop();
          this.navCtrl.push(ConfirmpagePage,{
            data:this.mptData,
            detail : this.passtemp
          });
        }
      }
      else if(this.billdata.processCode == '030001'){
        if(this.breweryData.accounttype==''){
          this.errormessage =this.showFont[10];
        }
        else if(this.breweryData.referenceType==''){
          this.errormessage =this.showFont[21];
        }else if(this.breweryData.quantity =='' || this.breweryData.quantity==undefined){
          this.errormessage =this.showFont[22];
        }else if(this.breweryData.price ==''|| this.breweryData.price==undefined){
          this.errormessage =this.showFont[23];
        }else if(this.breweryData.amount ==''|| this.breweryData.amount==undefined){
          this.errormessage =this.showFont[16];
        }else{
          this.breweryData.amountTotal = parseInt(this.breweryData.quantity) * parseInt(this.breweryData.amount)+parseInt(this.breweryData.bankCharges);
          console.error("Before send to confirm breweryData>>"+JSON.stringify(this.breweryData));
          this.idle.stop();
          this.navCtrl.push(ConfirmpagePage,{
            data:this.breweryData,
            detail : this.passtemp
          });
        }
      }

    }else{
      this.flag='none';
      let toast = this.toastCtrl.create({
        message:"Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }


  }

  QRscan(){
    
    this.barcodeScanner.scan().then((barcodeData) => {
      
      this.billdata.cardno=barcodeData.text;
      this.errormessage='';
    }, (err) => {

    });
  }

  packageChange(data){  //package.id
    for(let i=0;i<this.packagetype.length;i++){
      if(this.packagetype[i].id== data){
        this.billdata.packagetype = this.packagetype[i].name;
        this.monthtype=this.packagetype[i].rateList;
      }
    }
    this.errormessage='';
    this.tempmonthname='';
    this.billdata.amountTax='';
    this.billdata.amountServiceCharges='';
    this.billdata.amount='';
    this.billdata.amountTotal='';

    
  }
  checkFun(data){
	  if(data == '' || data == undefined){
		  alert("Enter Ref No.");
	  }else{
		   this.loading = this.loadingCtrl.create({
			  content: "Please wait...",
			  dismissOnPageChange: true
			  //   duration: 3000
			});
		let param={
					"userID": this.userData.userID,
					"sessionID": this.userData.sessionID,
					"processingCode": this.billdata.processCode,
					"cnpType": this.cnpname["name"],
					"custRefNo": data
				};
				
		this.http.post(this.ipaddress+'/service001/checkCNPBillNo', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {

			this.cnpData.amount = data.amount;
			this.cnpData.cnpCharge = data.merchantCharges;
			this.cnpData.amountTotal = data.totalAmount;
			this.cnpData.bankCharges = data.bankCharges;
            this.loading.dismiss();
           // this.accountlist = data.dataList;
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
				
	  }
  }
	cnpTypeChange(data){
		
	//	if(this.flagCode == 'BILL'){
			
				if(data.value == 'B-YESC'){
				this.checkShow = true;
			}else{
				this.checkShow = false;
			}	
		/* }else if(data.pid == 'TOPUP'){
			this.checkShow = false;
		} */
		this.cnpData.bankCharges = data.bankCharges;
		this.cnpData.cnpCharge = data.merchantCharges;

	}
	/* topUpaccountChange(data){
		if(data.value != 'T-MPT' && data.value != 'T-TELENOR' && data.value != 'T-OOREDOO'){
			this.customBox = true;
		}else{
			this.customBox = false;
		}
		
	} */
    referenceChange(data){
/* 		
		this.cnpData.bankCharges = '';
		this.cnpData.amount = data.amount;
		this.cnpData.cnpCharge = data.merchantCharges;
		this.cnpData.amountTotal = data.totalAmount;
		this.cnpname = '';
		this.topUpAmount = ''; */
        if(this.billdata.processCode == '050200'){
		/* for(let i=0;i< this.referenceType.dataList.length;i++){
			if(this.referenceType.dataList[i].value == data){
		this.flagCode = this.referenceType.dataList[i].value;
		
	    this.cnpType = this.referenceType.dataList[i].dataList;
		break;
			}
		} */
		
     /*  this.cnpData.referenceType = data.value;
      this.cnpData.bankCharges = data.bankCharges;
      this.cnpData.cnpCharge = data.merchantCharges */;
    }
    else if(this.billdata.processCode == '040300'){
      this.skypayData.referenceType = data.value;
      this.skypayData.bankCharges = data.bankCharges;
   //   this.skypayData.merchantCharges = data.merchantCharges;
    }
    else if(this.billdata.processCode == '030001'){
      this.breweryData.referenceType = data.value;
      this.breweryData.bankCharges = data.bankCharges;
      this.breweryData.price = data.price;
    }


  }

  inputChange(data,c){
    this.errormessage='';
    if(this.billdata.processCode == '030001'){
      this.breweryData.amount = parseInt(this.breweryData.price) * c;
    }
  }

  monthChange(data){   //pid
    this.errormessage='';
    for(let i=0;i<this.monthtype.length;i++){
      if(this.monthtype[i].did== data){
        this.billdata.monthtype = this.monthtype[i].m;
        this.billdata.amount = this.monthtype[i].r;
        this.billdata.peroid = this.monthtype[i].m;
        this.billdata.did = this.monthtype[i].did;
        this.billdata.pid = this.monthtype[i].pid;
        this.billdata.tid = this.monthtype[i].tid;
        this.billdata.amountTax =  this.monthtype[i].amountTax;
        this.billdata.amountTotal= this.monthtype[i].amountTotal;
      this.billdata.amountServiceCharges = this.monthtype[i].amountServiceCharges;
        
  }

}
// this.billdata.amount = data.totalamount;



}

getPackage(){
  this.slimLoader.start(() => {
    
  });
  /*this.loading = this.loadingCtrl.create({
    content: "Please wait...",
    dismissOnPageChange :true
    //   duration: 3000
    });
    this.loading.present();*/
    let param ={ userID:this.userData.userID,sessionID:this.userData.sessionID,merchantID:this.passtemp.merchantID,processingCode:this.passtemp.processingCode};
    
    this.http.post(this.ipaddress+'/service001/getPackages',param).map(res => res.json()).subscribe(data => {
        
        if(data.code=="0000"){
          this.packagetype = data.result.dataList;
          this.billdata.accessToken = data.result.accessToken;
          this.slimLoader.complete();
        }
        else if(data.code == "0016"){
          this.packagetype = [];
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss:false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess','');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.slimLoader.complete();
        }
        else{
          this.packagetype = [];
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.slimLoader.complete();

        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.slimLoader.complete();
      });
  }

  getreferenceData (){
	/* let tmpData = {
	code: '0000',
	desc: 'Successfully',
	dataList: [{
		"name": "Bill Payment",
		"value": "BILL",
		"dataList": [{
			"name": "YESC",
			"value": "B-YESC",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "200"
		},
		{
			"name": "YESC",
			"value": "B-YESC",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "300"
		},
		{
			"name": "MPT",
			"value": "B-MPT",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "200"
		}]
	},
	{
		"name": "Topup",
		"value": "TOPUP",
		"dataList": [{
			"name": "MPT",
			"value": "T-MPT",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "200"
		},
		{
			"name": "TELENOR",
			"value": "T-TELENOR",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "300"
		},
		{
			"name": "OOREDOO",
			"value": "T-OOREDOO",
			"price": "",
			"bankCharges": "100",
			"merchantCharges": "200"
		}]
	}]
}; */
/* 	this.referenceType = tmpData;
	this.referencename = "BILL";
	for(let i=0;i< this.referenceType.dataList.length;i++){
			if(this.referenceType.dataList[i].value == this.referencename){
				this.cnpType = this.referenceType.dataList[i].dataList;
				break;
			}
	}		
	this.flagCode = 'BILL'; */
	 
    this.slimLoader.start(() => {
      
    });
    /*this.loading = this.loadingCtrl.create({
     content: "Please wait...",
     dismissOnPageChange :true
     //   duration: 3000
     });
     this.loading.present();*/
    let param ={ userID:this.userData.userID,sessionID:this.userData.sessionID,processingCode:this.passtemp.processingCode};
    
    this.http.post(this.ipaddress+'/service001/getReferenceData',param).map(res => res.json()).subscribe(data => {
        
        if(data.code=="0000"){
          let tempArray = [];
          if (!Array.isArray(data.dataList)) {
            tempArray.push(data.dataList);
            data.dataList = tempArray;
          }
          this.referenceType = data.dataList;
          this.slimLoader.complete();
        }
        else if(data.code == "0016"){
          this.packagetype = [];
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss:false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess','');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.slimLoader.complete();
        }
        else{
          this.packagetype = [];
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.slimLoader.complete();

        }

      },
        error => {
        let toast = this.toastCtrl.create({
          message:this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.slimLoader.complete();
      });
  }
  checkNetwork(){
  
  if(this.network.type=="none"){
    this.flag='none';
  }else{
    this.flag='success';


  }
}
  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = {userID: this.userData.userID, sessionID: this.userData.sessionID};

      
      this.http.post(this.ipaddress+'/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.loading.dismiss();
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess','');
            this.storage.remove('userData');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }
  idleWatch(){
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5*60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='ChoosepackagePage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='ChoosetopupPage'){
        var data=countdown/60;
        this.min=data.toString().split('.')[0];
        this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        this.sec=  (Math.round(this.sec * 100) / 100);
       
        this.idleState = 'You\'ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLoad() {
    

  //  // this.idleWatch();
    /*this.network.onDisconnect().subscribe(data => {
      this.flag='none';
      let toast = this.toastCtrl.create({
        message:"Check your internet connection!",
        // duration: 2000,
        // position: 'bottom'
        showCloseButton: true,
        closeButtonText: 'OK'
      });
      toast.present(toast);
      

    }, error => console.error(error));*/
  }
  ionViewDidLeave(){
    
    this.slimLoader.reset();
  }


  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
     // this.showFont=this.textMyan;
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
     // this.showFont=this.textEng;
    }
  }*/


}
