import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { SkynetOtpPage } from '../skynet-otp/skynet-otp';
import { SkynetSuccessPage } from '../skynet-success/skynet-success';
import { SkynetErrorPage } from '../skynet-error/skynet-error';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';

@Component({
    selector: 'page-skynet-confirm',
    templateUrl: 'skynet-confirm.html',
    providers: [ChangelanguageProvider]
})
export class SkynetConfirmPage {

    textMyan: any = [
        "အတည်ပြုခြင်း", "အကောင့်နံပါတ် မှ",
        "ကဒ်နံပါတ်​", "ပက်​ကေ့နာမည်",
        "​ဘောက်ချာ အမျိုးအစား", "ငွေပမာဏ",
        "ဝန်​ဆောင်ခ", "စုစုပေါင်း ငွေပမာဏ",
        "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်",
        "​ဇာတ်ကားနာမည်"
    ];
    textEng: any = [
        "Confirmation", "From Account No.",
        "Card No.", "Package Name",
        "Voucher Type", "Amount",
        "Bank Charges", "Total Amount",
        "CANCEL", "CONFIRM",
        "Movie Name"
    ];
    showFont: any = [];
    font: any = '';

    merchantObj: any = {};
    passOtp: any;
    passSKey: any;

    amount: any;
    commissionAmount: any;
    totalAmount: any;

    ipaddress: string;

    userdata: any;
    loading: any;

    constructor(
        public navCtrl: NavController, public navParams: NavParams,
        public events: Events, public http: Http,
        public loadingCtrl: LoadingController, public changefont: Changefont,
        private storage: Storage, public util: UtilProvider,
        public alertCtrl: AlertController, public all: AllserviceProvider,
        public changeLanguage: ChangelanguageProvider, public global: GlobalProvider,
        private firebaseAnalytics: FirebaseAnalytics,private firebaseCrashlytics: FirebaseCrashlytics
    ) {
        this.merchantObj = this.navParams.get("data");
        this.passOtp = this.navParams.get("otp");
        this.passSKey = this.navParams.get('sKey');

        this.amount = this.util.formatAmount(this.merchantObj.amount);
        this.commissionAmount = this.util.formatAmount(this.merchantObj.commissionAmount);
        this.totalAmount = this.util.formatAmount(this.merchantObj.totalAmount);

        this.events.subscribe('changelanguage', lan => {
            this.changelanguage(lan.data)
        });

        this.storage.get('language').then((font) => {
            this.changelanguage(font);
        });

        this.storage.get('userData').then((data) => {
            this.userdata = data;

            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                    this.ipaddress = this.global.ipaddress;
                }
                else {
                    this.ipaddress = result;
                }
            });
        });
    }

    changelanguage(font) {
        if (font == "eng") {
            this.font = "";
            for (let i = 0; i < this.textEng.length; i++) {
                this.showFont[i] = this.textEng[i];
            }
        }
        else if (font == "zg") {
            this.font = "zg";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
            }
        }
        else {
            this.font = "uni";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.showFont[i] = this.textMyan[i];
            }
        }
    }

    confirm() {
        if (this.passOtp == 'true') {
            this.loading = this.loadingCtrl.create({
                content: "Processing...",
                dismissOnPageChange: true
            });
            this.loading.present();

            let param = {
                userID: this.userdata.userID,
                sessionID: this.userdata.sessionID,
                merchantID: this.merchantObj.merchantID,
                sKey: this.passSKey,
                type: 1
            };

            this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(
                data => {
                    if (data.code == "0000") {
                        this.loading.dismiss();

                        let parameter: any;

                        if (this.merchantObj.packagetype != 'ppv') {
                            parameter = {
                                sessionID: this.userdata.sessionID,
                                userID: this.userdata.userID,
                                fromAccount: this.merchantObj.fromAcc,
                                //toAccount: this.merchantObj.toAcc,
                                merchantCode: this.merchantObj.merchantCode,
                                merchantID: this.merchantObj.merchantID,
                                // amount: this.removeComma(this.merchantObj.amount),
                                // bankCharges: this.removeComma(this.merchantObj.commissionAmount),
                                // totalAmount: this.removeComma(this.merchantObj.totalAmount),
                                ccyCode: this.merchantObj.ccy,
                                narrative: "",
                                sKey: this.passSKey,
                                paidBy: this.global.appName,
                                amount: this.merchantObj.amount,
                                bankCharges: this.merchantObj.commissionAmount,
                                totalAmount: this.merchantObj.totalAmount,
                                cardNo: this.merchantObj.cardNo,
                                packageName: this.merchantObj.packageName,
                                voucherType: this.merchantObj.voucherType,
                                subscriptionno: this.merchantObj.subscriptionno,
                                packagetype: this.merchantObj.packagetype,
                            };
                        } else {
                            parameter = {
                                sessionID: this.userdata.sessionID,
                                userID: this.userdata.userID,
                                fromAccount: this.merchantObj.fromAcc,
                                //toAccount: this.merchantObj.toAcc,
                                merchantCode: this.merchantObj.merchantCode,
                                merchantID: this.merchantObj.merchantID,
                                // amount: this.removeComma(this.merchantObj.amount),
                                // bankCharges: this.removeComma(this.merchantObj.commissionAmount),
                                // totalAmount: this.removeComma(this.merchantObj.totalAmount),
                                ccyCode: this.merchantObj.ccy,
                                narrative: "",
                                sKey: this.passSKey,
                                paidBy: this.global.appName,
                                amount: this.merchantObj.amount,
                                bankCharges: this.merchantObj.commissionAmount,
                                totalAmount: this.merchantObj.totalAmount,
                                cardNo: this.merchantObj.cardNo,
                                //packageName: this.merchantObj.packageName,
                                //voucherType: this.merchantObj.voucherType,
                                subscriptionno: this.merchantObj.subscriptionno,
                                packagetype: this.merchantObj.packagetype,
                                moviename: this.merchantObj.moviename,
                                moviecode: this.merchantObj.moviecode,
                                startdate: this.merchantObj.startdate,
                                enddate: this.merchantObj.enddate,
                                usage_service_catalog_identifier__id: this.merchantObj.usage_service_catalog_identifier__id
                            };
                        }

                        this.navCtrl.push(SkynetOtpPage, {
                            data: parameter,
                            otpdata: data,
                            sKey: this.passSKey
                        });
                    }
                    else if (data.code == "0016") {
                        this.logoutAlert(data.desc);
                        this.loading.dismiss();
                    }
                    else {
                        this.all.showAlert('Warning!', data.desc);
                        this.loading.dismiss();
                    }
                },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                }
            );
        }
        else {
            this.goTransfer();
        }
    }

    goTransfer() {
        this.loading = this.loadingCtrl.create({
            content: "Processing...",
            dismissOnPageChange: true
        });
        this.loading.present();

        let parameter: any;

        if (this.merchantObj.packagetype != 'ppv') {
            parameter = {
                sessionID: this.userdata.sessionID,
                userID: this.userdata.userID,
                fromAccount: this.merchantObj.fromAcc,
                //toAccount: this.merchantObj.toAcc,
                merchantCode: this.merchantObj.merchantCode,
                merchantID: this.merchantObj.merchantID,
                // amount: this.removeComma(this.merchantObj.amount),
                // bankCharges: this.removeComma(this.merchantObj.commissionAmount),
                // totalAmount: this.removeComma(this.merchantObj.totalAmount),
                ccyCode: this.merchantObj.ccy,
                narrative: "",
                sKey: this.passSKey,
                paidBy: this.global.appName,
                amount: this.merchantObj.amount,
                bankCharges: this.merchantObj.commissionAmount,
                totalAmount: this.merchantObj.totalAmount,
                cardNo: this.merchantObj.cardNo,
                packageName: this.merchantObj.packageName,
                voucherType: this.merchantObj.voucherType,
                subscriptionno: this.merchantObj.subscriptionno,
                packagetype: this.merchantObj.packagetype,
            };
        } else {
            parameter = {
                sessionID: this.userdata.sessionID,
                userID: this.userdata.userID,
                fromAccount: this.merchantObj.fromAcc,
                //toAccount: this.merchantObj.toAcc,
                merchantCode: this.merchantObj.merchantCode,
                merchantID: this.merchantObj.merchantID,
                // amount: this.removeComma(this.merchantObj.amount),
                // bankCharges: this.removeComma(this.merchantObj.commissionAmount),
                // totalAmount: this.removeComma(this.merchantObj.totalAmount),
                ccyCode: this.merchantObj.ccy,
                narrative: "",
                sKey: this.passSKey,
                paidBy: this.global.appName,
                amount: this.merchantObj.amount,
                bankCharges: this.merchantObj.commissionAmount,
                totalAmount: this.merchantObj.totalAmount,
                cardNo: this.merchantObj.cardNo,
                //packageName: this.merchantObj.packageName,
                //voucherType: this.merchantObj.voucherType,
                subscriptionno: this.merchantObj.subscriptionno,
                packagetype: this.merchantObj.packagetype,
                moviename: this.merchantObj.moviename,
                moviecode: this.merchantObj.moviecode,
                startdate: this.merchantObj.startdate,
                enddate: this.merchantObj.enddate,
                usage_service_catalog_identifier__id: this.merchantObj.usage_service_catalog_identifier__id
            };
        }

        //reopen firebase
         this.firebaseAnalytics.logEvent('topupSkynet_userid_request', {
            "userid": this.userdata.userID
        }).then((res: any) => {
            console.log(res);
        }).catch((error: any) => {
            console.log(error);
        }); 

        this.http.post(this.ipaddress + '/service003/goSkynetTransfer', parameter).map(res => res.json()).subscribe(
            data1 => {
                //reopense firebase
                this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
                  "userid": this.userdata.userID,
                  "code": data1.code,
                  "desc": data1.desc
                }).then((res: any) => {
                  console.log(res);
                }).catch((error: any) => {
                  console.log(error);
                }); 

                if (data1.code == "0000") {
                    this.loading.dismiss();

                    let paramSkynetSuccess: any;

                    if (this.merchantObj.packagetype != "ppv") {
                        paramSkynetSuccess = {
                            fromAcc: this.merchantObj.fromAcc,
                            cardNo: this.merchantObj.cardNo,
                            packageName: this.merchantObj.packageName,
                            voucherType: this.merchantObj.voucherType,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            bankRefNo: data1.bankRefNumber,
                            transDate: data1.transactionDate,
                            merchantID: this.merchantObj.merchantID,
                            merchantCode: this.merchantObj.merchantCode,
                            packagetype: this.merchantObj.packagetype,
                        };
                    } else {
                        paramSkynetSuccess = {
                            fromAcc: this.merchantObj.fromAcc,
                            cardNo: this.merchantObj.cardNo,
                            //packageName: this.merchantObj.packageName,
                            //voucherType: this.merchantObj.voucherType,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            bankRefNo: data1.bankRefNumber,
                            transDate: data1.transactionDate,
                            merchantID: this.merchantObj.merchantID,
                            merchantCode: this.merchantObj.merchantCode,
                            moviename: this.merchantObj.moviename,
                            packagetype: this.merchantObj.packagetype
                        };
                    }

                    this.navCtrl.setRoot(SkynetSuccessPage, {
                        data: paramSkynetSuccess
                    });
                }
                else if (data1.code == "0016") {
                    this.logoutAlert(data1.desc);
                    this.loading.dismiss();
                }
                else {
                    this.loading.dismiss();

                    let paramSkynetError: any;

                    if (this.merchantObj.packagetype != "ppv") {
                        paramSkynetError = {
                            fromAcc: this.merchantObj.fromAcc,
                            cardNo: this.merchantObj.cardNo,
                            packageName: this.merchantObj.packageName,
                            voucherType: this.merchantObj.voucherType,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            errordesc: data1.desc,
                            merchantID: this.merchantObj.merchantID,
                            merchantCode: this.merchantObj.merchantCode,
                            packagetype: this.merchantObj.packagetype
                        };
                    } else {
                        paramSkynetError = {
                            fromAcc: this.merchantObj.fromAcc,
                            cardNo: this.merchantObj.cardNo,
                            // packageName: this.merchantOtpObj.packageName,
                            // voucherType: this.merchantOtpObj.voucherType,
                            amount: this.merchantObj.amount,
                            commissionAmount: this.merchantObj.commissionAmount,
                            totalAmount: this.merchantObj.totalAmount,
                            ccy: this.merchantObj.ccy,
                            errordesc: data1.desc,
                            merchantID: this.merchantObj.merchantID,
                            merchantCode: this.merchantObj.merchantCode,
                            moviename: this.merchantObj.moviename,
                            packagetype: this.merchantObj.packagetype
                        };
                    }

                    this.navCtrl.setRoot(SkynetErrorPage, {
                        data: paramSkynetError
                    });
                }
            },
            error => {
                const crashlytics = this.firebaseCrashlytics.initialise();
                crashlytics.logException('Internal Transfer'+error);
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.loading.dismiss();

                let code = error.status;
                let msg = "Can't connect right now. [" + code + "]";

                this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
                    "userid": this.userdata.userID,
                    "error": msg
                }).then((res: any) => {
                    console.log(res);
                }).catch((error: any) => {
                    const crashlytics = this.firebaseCrashlytics.initialise();
                    crashlytics.logException('Internal Transfer'+error);
                    console.log(error);
                });
            }
        );
    }

    cancel() {
        this.navCtrl.pop();
    }

    logoutAlert(message) {
        let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: message,
            buttons: [{
                text: 'OK',
                handler: () => {
                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {});
                    this.navCtrl.popToRoot();
                }
            }],
            cssClass: 'warningAlert',
        })
        confirm.present();
    }

    // removeComma(amount) {
    //     return amount.replace(/[,]/g, '');
    // }

}
