import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { TopUpSuccessPage } from '../top-up-success/top-up-success';
import { TopuperrorPage } from '../topuperror/topuperror';
import { TopupotpPage } from '../topupotp/topupotp';

/**
 * Generated class for the TopupconfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-topupconfirm',
  templateUrl: 'topupconfirm.html',
  providers: [ChangelanguageProvider]
})
export class TopupconfirmPage {
  userData: any;
  ipaddress: string;
  loading: any;
  flag: string;
  passTemp: any;
  textMyan: any = ["အကောင့်နံပါတ်", "Operator အမျိုးအစား", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "အတည်ပြုမည်", "အတည်ပြုခြင်း"];
  textEng: any = ["Account Number", "Operator Type", "Phone Number", "Amount", "Narrative", "CANCEL", "CONFIRM", "Top Up - Confirmation"];
  topUpData: any;
  showFont: string[] = [];
  font: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  passOtp: any;
  passSKey: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public all: AllserviceProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.topUpData = this.navParams.get('data');
    this.passTemp = this.navParams.get('detail');
    this.passOtp = this.navParams.get("otp");
    this.passSKey = this.navParams.get('sKey');

    //  alert(JSON.stringify(this.topUpData));

    this.storage.get('userData').then((data) => {
      this.userData = data;

      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
        }
      });
    });

    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
  }

  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TopupconfirmPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TopupconfirmPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }


  ionViewDidLoad() {

    this.checkNetwork();
    // this.idleWatch();
  }
  ionViewDidLeave() {

    this.slimLoader.reset();
  }
  confirmbtn() {
    if (this.flag == "success") {
      this.slimLoader.start(() => {

      });
      if (this.passOtp == 'true') {
        let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '2', merchantID: this.passTemp.merchantID, sKey: this.passSKey };

        this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {

            this.slimLoader.complete();
            this.passTemp['rKey'] = data.rKey;
            this.navCtrl.push(TopupotpPage, {
              data: this.topUpData,
              detail: this.passTemp,
              sKey: this.passSKey
            })
          }
          else if (data.code == "0016") {
            let confirm = this.alertCtrl.create({
              title: 'Warning!',
              enableBackdropDismiss: false,

              message: data.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {

                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {});
                    this.navCtrl.popToRoot();
                  }
                }
              ],
              cssClass: 'warningAlert',
            })
            confirm.present();
            this.slimLoader.complete();
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.slimLoader.complete();
          });
      }
      else {
        this.slimLoader.complete();
        this.gotopuppayment();
      }

    } else {
      this.all.showAlert('Warning!', "Can't connect right now.");
    }
  }

  gotopuppayment() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param;
    let phoneNo;
    if (this.passTemp.processingCode == "050200") {
      phoneNo = this.topUpData.phoneNo.substring(4);
    }
    else if (this.passTemp.processingCode == "040300") {
      phoneNo = this.topUpData.phoneNo.substring(4);
      phoneNo = "09" + phoneNo;
    }
    else {
      phoneNo = this.topUpData.phoneNo.replace(/\+/g, '');
    }
    if (this.passTemp.processingCode == '050200') {
      param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID,
        "bankAcc": this.topUpData.accounttype,
        "merchantID": this.passTemp.merchantID,
        "processingCode": this.passTemp.processingCode,
        "amount": this.topUpData.amount,
        "refNumber": phoneNo,
        "field1": "",
        "field2": this.topUpData.value,
        "field3": "",
        "field4": "",
        "field5": "",
        "field6": "",
        "field7": "",
        "field8": "",
        "field9": "",
        "field10": "",
        "amountServiceCharges": "",
        "amountTax": "",
        "amountTotal": this.topUpData.amount,
        "paymentType": "2",
        "narrative": this.topUpData.narrative,
        "sKey": this.passSKey
      };
    } else if (this.passTemp.processingCode == '040300') {
      param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID,
        "bankAcc": this.topUpData.accounttype,
        "merchantID": this.passTemp.merchantID,
        "processingCode": this.passTemp.processingCode,
        "amount": this.topUpData.amount,
        "refNumber": phoneNo,
        "field1": "",
        "field2": this.topUpData.value,
        "field3": this.topUpData.amountType,
        "field4": "",
        "field5": "",
        "field6": "",
        "field7": "",
        "field8": "",
        "field9": "",
        "field10": "",
        "amountServiceCharges": "",
        "amountTax": "",
        "amountTotal": this.topUpData.amount,
        "paymentType": "2",
        "narrative": this.topUpData.narrative,
        "sKey": this.passSKey
      };
    }

    this.http.post(this.ipaddress + '/service003/goTopup', param).map(res => res.json()).subscribe(res => {

      this.loading.dismiss();
      if (res.code == "0000") {
        this.topUpData['processingCode'] = this.passTemp.processingCode;
        this.topUpData['colorCode'] = this.passTemp.colorCode;
        this.topUpData['bankRefNumber'] = res.bankRefNumber;
        this.navCtrl.setRoot(TopUpSuccessPage, {
          data: res,
          detail: this.topUpData

        })

      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        // this.slimLoader.complete();
        this.loading.dismiss();
      }
      else {
        this.slimLoader.complete();
        this.navCtrl.setRoot(TopuperrorPage, {
          data: res,
          detail: this.topUpData
        });
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });


  }
  cancelbtn() {
    this.navCtrl.pop();
  }

  gologout() {
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          // this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    });
  }
}
