import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';

/**
 * Generated class for the TransactionHistoryDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction-history-detail',
  templateUrl: 'transaction-history-detail.html',
  providers : [ChangelanguageProvider]
})
export class TransactionHistoryDetail {
  b: any[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  public loading;
  ipaddress:string;
  userdata:any;
  showFont:string [] = [];
  textEng: any=['Details','Reference Number','Merchant Name','Package Name','Month','Card Number',
                'Transaction Date','Amount','Service Charges','Tax Amount','Total Amount','Description',
                'Bill Type','Operator Type','Customer Reference No.',"Outstanding Bill","Meter Number"];
  textMyan : any = ['အသေးစိတ်အချက်အလက်','အမှတ်စဉ်','ရုပ်သံလိုင်းအမည်','Packageအမျိုးအစား','သက်တမ်း','ကတ်နံပါတ်','လုပ်ဆောင်ခဲ့သည့်ရက်စွဲ','ငွေပမာဏ','ဝန်ဆောင်ခနှုန်း','အခွန်နှုန်း','စုစုပေါင်းငွေပမာဏ','အကြောင်းအရာ',
                    'Biller အမျိုးအစား','Operator အမျိုးအစား',"အသုံးပြုသူအမှတ်စဉ်","ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ်","မီတာ နံပါတ်"];
  font:string;
  passtemp : any;
  constructor(public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams,public idle:Idle,public events:Events,public storage:Storage,public changeLanguage:ChangelanguageProvider,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public http:Http) {
    this.b = this.navParams.get("data");
    this.passtemp  = this.navParams.get("detail");
    
    
    this.storage.get('userData').then((val) => {
      
      this.userdata=val;
    });
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
  }


  idleWatch(){
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5*60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='TransactionHistoryDetail')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='TransactionHistoryDetail'){
        var data=countdown/60;
        this.min=data.toString().split('.')[0];
        this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        this.sec=  (Math.round(this.sec * 100) / 100);
      //  
        this.idleState = 'You\'ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLeave(){
    
  }

  ionViewDidLoad() {
   // // this.idleWatch();
    
  }
  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = {userID: this.userdata.userID, sessionID: this.userdata.sessionID};

      
      this.http.post(this.ipaddress+'/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.loading.dismiss();
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess','');
            this.storage.remove('userData');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }
}
