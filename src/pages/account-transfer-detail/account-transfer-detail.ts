import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { FastTransferPage } from '../fast-transfer/fast-transfer';
import { Http } from '@angular/http';
import { BeneficiaryAddPage } from '../beneficiary-add/beneficiary-add';
import { OwnTransferPage } from '../own-transfer/own-transfer';
import { InternalTransferPage } from '../internal-transfer/internal-transfer';
import { WalletTopupPage } from '../wallet-topup/wallet-topup';
import { MainHomePage } from './../main-home/main-home';
/**
 * Generated class for the AccountTransferDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-account-transfer-detail',
  templateUrl: 'account-transfer-detail.html',
  providers: [ChangelanguageProvider]
})
export class AccountTransferDetail {
  passTemp: any;
  passTemp1: any;
  passTemp2: any;
  textEng: any = ["From account", "To account", "Amount", "Bank Reference No.", "Transaction Date", "Card Expired", "Transaction Approved", "Transfer Type", "Close"];
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ငွေလွှဲ အမျိုးအစား', "ပိတ်မည်"];
  showFont: any = [];
  font: any = '';
  fromPage: any;
  beneficiaryID: any;
  loading: any;
  userdata: any;
  ipaddress: any;
  beneStatus: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
    public http: Http, ) {
    this.passTemp = this.navParams.get("data");
    this.passTemp1 = this.navParams.get("detail");
    this.passTemp2 = this.navParams.get("type");
    this.fromPage = this.navParams.get('fromPage');
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
    this.storage.get('userData').then((data) => {
      this.userdata = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
        if (this.fromPage == 'fast-transfer') {
          this.checkBeneficiary();
        }
      });
    });
  }

  ionViewDidLoad() {

  }
  /*  changelanguage(lan) {
  
      if (lan == 'uni') {
        this.font = "uni";
        for (let j = 0; j <  this.textMyan.length;j++) {
          this.showFont[j] = this.textMyan[j];
        }
      }
      else if (lan == 'zg') {
        this.font = "zg";
        for (let j = 0; j <  this.textMyan.length;j++) {
          this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
        }
  
      }
      else{
        this.font='';
        for (let j = 0; j <  this.textEng.length;j++) {
          this.showFont[j] = this.textEng[j];
        }
      }
    }*/

  /*  getOK(){
     this.navCtrl.setRoot(AccountTransfer);
   } */
  checkBeneficiary() {
    this.beneStatus = false;
    let param = {
      userID: this.userdata.userID,
      sessionID: this.userdata.sessionID,
      accountNo: this.passTemp1.toAcc
    };
    this.http.post(this.ipaddress + '/service002/checkExtBeneficiary', param).map(res => res.json()).subscribe(res => {
      // console.log("beneficiary-add-res>>" + JSON.stringify(res))
      if (res.code != "0014") {
        if (res.code == '0002') {
          this.beneficiaryAlert();
         // this.beneStatus = true;
        }
      }
      else {
        this.all.showAlert('Warning!', res.desc);
        this.loading.dismiss();
      }

    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  beneficiaryAlert() {
    let alert = this.alertCtrl.create({
      title: 'Do you want to add Beneficiary?',
      enableBackdropDismiss: false,
      message: '',
      /*  inputs: [
         {
           name: 'Beneficiary ID',
           placeholder: 'Beneficiary ID',
           value: this.beneficiaryID,
         },
       ], */
      buttons: [{
        text: 'No',
        handler: () => {
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.navCtrl.setRoot(BeneficiaryAddPage, {
            data: this.passTemp1,
            //beneficiaryID:this.beneficiaryID
          })
          console.log(this.passTemp1);
          // this.addBeneficiary();
        }
      }]
    });
    alert.present();
  }

  getOK() {
    if (this.fromPage == 'fast-transfer') {
      this.navCtrl.setRoot(FastTransferPage);
    } else if (this.fromPage == 'own-transfer') {
      this.navCtrl.setRoot(OwnTransferPage);
    } else if (this.fromPage == 'internal-transfer') {
      this.navCtrl.setRoot(InternalTransferPage);
    } else if (this.fromPage == 'wallet-transfer') {
      this.navCtrl.setRoot(WalletTopupPage);
    } 

  }

  addBeneficiary() {
    this.navCtrl.setRoot(BeneficiaryAddPage, {
      data: this.passTemp1,
    })
  }
}
