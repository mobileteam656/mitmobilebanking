import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform,
    PopoverController, Select
} from 'ionic-angular';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Contacts } from '@ionic-native/contacts';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MptTopupConfirmPage } from '../mpt-topup-confirm/mpt-topup-confirm';
import { PopoverPage } from '../popover-page/popover-page';
import { TopUpListPage } from '../top-up-list/top-up-list';

declare var window;

@Component({
  selector: 'page-mpt-topup',
  templateUrl: 'mpt-topup.html',
})
export class MptTopupPage {
  @ViewChild('myselect') select: Select;
  titleMyan: any = ["MPT ငွေဖြည့်သွင်းခြင်း", "Telenor ငွေဖြည့်သွင်းခြင်း", "Ooredoo ငွေဖြည့်သွင်းခြင်း", "MEC ငွေဖြည့်သွင်းခြင်း"];
  titleEng: any = ["MPT Top Up", "Telenor Top Up", "Ooredoo Top Up", "MyTel Top Up", , "MEC Top Up"];
  textMyan: any = ["MPT ငွေဖြည့်သွင်းခြင်း", "အကောင့်နံပါတ် မှ", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "လုပ်ဆောင်မည်", "ပြန်စမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ",];
  textEng: any = ["MPT Top Up", "From Account No.", "Phone Number", "Amount", "Narrative", "SUBMIT", "RESET", "Account Balance"];
  textErrorEng: any = ["Please choose account number.", "Please fill phone number", "Please fill amount.", "Insufficient Balance"];
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
  textError: string[] = [];
  textData: any = [];
  titleData: any = [];
  title: any;
  font: any = '';
  modal: any;
  hardwareBackBtn: any = true;
  userData: any;
  ipaddress: string;
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec: any;
  accountBal: any;
  fromAccountlist: any = [];
  topupData: any = [];
  contact: boolean = false;
  contactData: any = [];
  popover: any;
  billAmountList: any = [];
  amount: any;
  fromaccMsg: any = '';
  amountMsg: any = '';
  phoneMsg: any = '';
  merchantID: any;
  processingCode: any;
  data: any;
  merchantData: any;
  amountBal: any;
  image:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public popoverCtrl: PopoverController, public alertCtrl: AlertController, public changefont: Changefont,
    public loadingCtrl: LoadingController, public idle: Idle, public http: Http, private contacts: Contacts,
    public platform: Platform, public global: GlobalProvider, public all: AllserviceProvider, public util: UtilProvider) {
    this.merchantData = this.navParams.get("data");
    this.processingCode = this.merchantData.processingCode
    this.merchantID = this.merchantData.merchantID;
    this.amountBal = "";
    if (this.processingCode == '200800') {
      this.image='assets/mpt_1.png';
    } else {
      this.image='assets/telenor_1.png';
    }
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      cssClass: 'my-loading-class'
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert(result.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  validation() {
    let flag1 = false, flag2 = false, flag3 = false, flag4 = false, flag5 = false,
      num = /^[0-9-\+]*$/;
    if (this.topupData.account == '' || this.topupData.account == undefined) {
      flag1 = false;
      this.fromaccMsg = this.textError[0];
    } else {
      flag1 = true;
      this.fromaccMsg = '';
    }
    if (this.topupData.phone == '' || this.topupData.phone == undefined) {
      flag2 = false;
      this.phoneMsg = this.textError[1];
    } else {
      this.topupData.phone = this.topupData.phone.toString().replace(/ +/g, "");
      if (num.test(this.topupData.phone)) {
        if (this.topupData.phone.indexOf("+") == 0 && (this.topupData.phone.length == "12" || this.topupData.phone.length == "13" || this.topupData.phone.length == "11")) {
          flag2 = true;
          this.phoneMsg = '';
        }
        else if (this.topupData.phone.indexOf("7") == 0 && this.topupData.phone.length == "9") {
          this.topupData.phone = '+959' + this.topupData.phone;
          flag2 = true;
          this.phoneMsg = '';
        }
        else if (this.topupData.phone.indexOf("9") == 0 && this.topupData.phone.length == "9") {
          this.topupData.phone = '+959' + this.topupData.phone;
          flag2 = true;
          this.phoneMsg = '';
        }
        else if (this.topupData.phone.indexOf("+") != 0 && this.topupData.phone.indexOf("7") != 0 && this.topupData.phone.indexOf("9") != 0 && (this.topupData.phone.length == "8" || this.topupData.phone.length == "9" || this.topupData.phone.length == "7")) {
          this.topupData.phone = '+959' + this.topupData.phone;
          flag2 = true;
          this.phoneMsg = '';
        }
        else if (this.topupData.phone.indexOf("09") == 0 && (this.topupData.phone.length == 10 || this.topupData.phone.length == 11 || this.topupData.phone.length == 9)) {
          this.phoneMsg = '';
          this.topupData.phone = '+959' + this.topupData.phone.substring(2);
          flag2 = true;
        }
        else if (this.topupData.phone.indexOf("959") == 0 && (this.topupData.phone.length == 11 || this.topupData.phone.length == 12 || this.topupData.phone.length == 10)) {
          this.phoneMsg = '';
          this.topupData.phone = '+959' + this.topupData.phone.substring(3);
          flag2 = true;
        }
        else {
          flag2 = false;
          this.phoneMsg = 'Invalid phone number.';
        }
      }
      else {
        flag2 = false;
        this.phoneMsg = this.textError[1];
      }
    }
    if (this.topupData.amount != '' && this.topupData.amount != undefined) {
      let amount = parseInt(this.topupData.amount);
      if (this.topupData.amount.substring(0, 1) == "-" || amount == 0) {
        this.amountMsg = this.textError[2];
        flag3 = false;
      } else if (isNaN(amount)) {
        this.amountMsg = this.textError[2];
        flag3 = false;
      } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.topupData.amount)) {
        this.amountMsg = this.textError[3];
        flag3 = false;
      } else {
        this.amountMsg = "";
        flag3 = true;
      }
    }
    else {
      this.amountMsg = this.textError[2];
      flag3 = false;
    }
    if (flag1 && flag2 && flag3) {
      this.goConfirm();
    }
  }

  goConfirm() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      cssClass: 'my-loading-class'
    });
    this.loading.present();
    this.topupData.merchantID = this.merchantID;
    this.topupData.processingCode = this.processingCode;
    this.topupData.operator = this.merchantData.merchantName;
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '2', merchantID: this.merchantID };
    this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.push(MptTopupConfirmPage, {
          data: this.topupData,
          otp: data.rKey,
          sKey: data.sKey
        });
      }
      else if (data.code == "0016") {
        this.logoutAlert(data.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  getContact() {
    this.contacts.pickContact().then((data) => {
      if (data.phoneNumbers.length > 1) {
        this.contact = true;
        this.contactData = data.phoneNumbers;
        this.phoneMsg = "";
        for (let i = 0; i < this.contactData.length; i++) {
          this.contactData[i].value = this.contactData[i].value.toString().replace(/ +/g, "");
          if (this.contactData[i].value.indexOf("7") == 0 && this.contactData[i].value.length == "9") {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("9") == 0 && this.contactData[i].value.length == "9") {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("+") != 0 && this.contactData[i].value.indexOf("7") != 0 && this.contactData[i].value.indexOf("9") != 0 && (this.contactData[i].value.length == "8" || this.contactData[i].value.length == "9" || this.contactData[i].value.length == "7")) {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("09") == 0 && (this.contactData[i].value.length == 10 || this.contactData[i].value.length == 11 || this.contactData[i].value.length == 9)) {
            this.contactData[i].value = '+959' + this.contactData[i].value.substring(2);
          }
          else if (this.contactData[i].value.indexOf("959") == 0 && (this.contactData[i].value.length == 11 || this.contactData[i].value.length == 12 || this.contactData[i].value.length == 10)) {
            this.contactData[i].value = '+959' + this.contactData[i].value.substring(3);
          }
        }
      } else {
        this.contact = false;
        this.topupData.phone = data.phoneNumbers[0].value;
      }
    });
  }

  getReferenceData() {
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, processingCode: this.processingCode };
    this.http.post(this.ipaddress + '/service002/getMPTReferenceData', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.lovList)) {
          tempArray.push(data.lovList);
          data.lovList = tempArray;
        }
        this.billAmountList = data.lovList;
        //this.merchantID=data.merchantID;
      }
      else if (data.code == "0016") {
        this.logoutAlert(data.desc);
      }
      else {
        this.all.showAlert('Warning!', data.desc);
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
      });
  }

  getamount(data) {
    this.topupData.amountType = data.value;
    this.topupData.amount = data.name;
    this.amountMsg = "";
    
  }

  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  }

  reset() {
    this.topupData.account = "";
    this.topupData.phone = "";
    this.topupData.amount = "";
    this.topupData.narrative = "";
    this.fromaccMsg = '';
    this.amountMsg = '';
    this.accountBal = "";
    this.phoneMsg = "";
    this.data = [];
    this.contact = false;
    this.amountBal = "";
    
  }

  onChangePh(i) {
    this.phoneMsg = "";
    if (i == "09") {
      this.topupData.phone = "+959";
    }
    else if (i == "959") {
      this.topupData.phone = "+959";
    }
  }

  inputChange(data) {
    if (data == 1) {
      this.phoneMsg = "";
    } else if (data == 2) {
      this.amountMsg = "";
    }
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
      for (let i = 0; i < this.titleEng.length; i++) {
        this.titleData[i] = this.titleEng[i];
      }
      if (this.processingCode == '200800') {
        this.title = this.titleData[0]
      } else {
        this.title = this.titleData[1]
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
      for (let i = 0; i < this.titleMyan.length; i++) {
        this.titleData[i] = this.changefont.UnitoZg(this.titleMyan[i]);
      }
      if (this.processingCode == '200800') {
        this.title = this.titleData[0]
      } else {
        this.title = this.titleData[1]
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
      for (let i = 0; i < this.titleMyan.length; i++) {
        this.titleData[i] = this.titleMyan[i];
      }
      if (this.processingCode == '200800') {
        this.title = this.titleData[0]
      } else {
        this.title = this.titleData[1]
      }
    }
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.getAccountSummary();
        this.getReferenceData();
      })
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButton() {
    this.navCtrl.setRoot(TopUpListPage, {
      data: '2'
    });
  }
}
