import { Component } from '@angular/core';
import { NavController, NavParams, Platform, Events, ToastController, LoadingController, AlertController, PopoverController } from 'ionic-angular';
import { ProductDetailPage } from '../product-detail/product-detail';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { UtilProvider } from '../../providers/util/util';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Idle } from '@ng-idle/core';
import { Http } from '@angular/http';
import { Changefont } from '../changefont/changeFont';
import { HttpHeaders } from '@angular/common/http';
import { QPayInfoPage } from '../q-pay-info/q-pay-info';
import { QPayCagPage } from '../q-pay-cag/q-pay-cag';

@Component({
  selector: 'page-q-pay',
  templateUrl: 'q-pay.html',
})
export class QPayPage {
  ipaddress: any;
  loading: any;
  headers: { responseType: string; headers: HttpHeaders; };
  choosenNumberValue: any;
  chVal: string;
  response_number: number;
  merchantlist: any = []; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService,
    public util: UtilProvider, public changeLanguage: ChangelanguageProvider) {

      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        } else {
          this.ipaddress = result;
        }
  
        this.merchantList();
      })
  
   
  } 

  soapCall() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', 'http://52.187.13.89:443/PrjFcdbDepositService/fdeposit/merchantlist?wsdl');
    let sr =
        `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:quic="http://quickpay.fcdb.nirvasoft.com/">
        <soapenv:Header/>
        <soapenv:Body>
           <quic:getMerchantList/>
        </soapenv:Body>
     </soapenv:Envelope>`;

    // xmlhttp.onreadystatechange =  () => {
    //     if (xmlhttp.readyState == 4) {
    //         if (xmlhttp.status == 200) {
    //             let xml = xmlhttp.responseXML;
    //             alert(xmlhttp.responseText);
    //         }else{
    //           alert('XML response ' + xmlhttp.responseXML);
    //           alert(xmlhttp.status);
    //         }
    //     }
    // }


    xmlhttp.onreadystatechange =  () => {
      if (xmlhttp.readyState == 4) {
          if (xmlhttp.status == 200) {
              var xml = xmlhttp.responseXML;
              this.merchantlist = xml;//Here I'm getting the value contained by the <return> node
              console.log(this.merchantlist); //I'm printing my result square number
          }
      }
  }

    // Send the POST request
    xmlhttp.setRequestHeader('Content-Type', 'application/soap+xml');
    xmlhttp.responseType = "document";
    xmlhttp.send(sr);
  }

  merchantList2(){
    this.headers = {
      responseType: "text",
      headers: new HttpHeaders()
        .set('Content-Type', 'text/xml; charset=utf-8')
    };
    this.http.post('http://52.187.13.89:443/PrjFcdbDepositService/fdeposit/merchantlist?wsdl',  this.headers)
    .subscribe((response) => {
      console.log(response);
     //let result = new DOMParser().parseFromString(res,"text/xml").getElementsByTagName("**WebMethodName**Result")[0].innerHTML;
    }, (error) => {
     
    });
  }

  merchantList3(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    
    this.http.get("http://52.187.13.89:443/PrjFcdbDepositService/fdeposit/merchantlist?wsdl").subscribe(result => {
      this.loading.dismiss();
      console.log("Q-pay merchant>>>",result);
      // if (result.code == "0000") {
      //   if(result.data==null || result.data==undefined){
      //     this.play=true;
      //     this.productlist="";
      //     this.productlist.title="Sorry,There is no data"
      //     this.all.showAlert('Warning!', result.desc);
      //     this.loading.dismiss;
      //   }else{
      //     let tempArray = [];
      //     if (!Array.isArray(result.data)) {
      //       tempArray.push(result.data);
      //       result.data = tempArray;
      //     }
      //     this.play=false;
      //     this.productlist = result.data;
      //     this.loading.dismiss();
      //   }
        
      // }
      // else if (result.code == "0016") {
      //   // console.log("No Data")
      // }
      // else {
      //   // this.all.showAlert('Warning!', result.desc);
      //   // this.loading.dismiss();
      // }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  merchantList() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: '' }; //'http://122.248.120.16:8080/AppService/module001'
    this.http.post( this.ipaddress + '/service001/getMerchantList', param).map(res => res.json()).subscribe(result => {
      this.loading.dismiss();
      console.log(result);
      if (result.code == "0000") {
        // if(result.merchantList==null || result.merchantList==undefined){
        //   //this.play=true;
        //   this.merchantlist="";
        //   // this.productlist.title="Sorry,There is no data"
        //   // this.all.showAlert('Warning!', result.desc);
        //   this.loading.dismiss;
        // }else{
        //   let tempArray = [];
        //   if (!Array.isArray(result.merchantList)) {
        //     tempArray.push(result.merchantList);
        //     result.merchantList = tempArray;
        //   }
        //   //this.play=false;
        //   this.merchantlist = result.merchantList;
        //   this.loading.dismiss();
        let tempArray = [];
        if (!Array.isArray(result.merchantList)) {
          tempArray.push(result.merchantList);
          result.merchantList = tempArray;          
        }
        /* let accountGroup= result.dataList.reduce(function (obj, item) {
          obj[item.currentBal] = obj[item.avlBal.substring(0, 1)] || [];
          obj[item.currentBal].push(item);
          return obj;
        }, {}); */
        let group_to_values = result.merchantList.reduce(function (obj, item) {
          obj[item.description] = obj[item.description] || [];
          obj[item.description].push(item);
          return obj;
        }, {});
        let groups = Object.keys(group_to_values).map(function (key) {
          return { description: key, data: group_to_values[key] };
        });
        this.merchantlist = groups;
        console.log("groupmerchant>>>",this.merchantlist)
        //this.status = 1;
        this.loading.dismiss();
        //this.slimLoader.complete();
        }
        
      //}
      else if (result.code == "0016") {
        // console.log("No Data")
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QPayPage');
  }

  goDetail(m) {
   // console.log("merchantlist>>",m)
    this.navCtrl.push(QPayCagPage, {
      merchantlist: m,
      //merchantName: name
    });
  }

}
