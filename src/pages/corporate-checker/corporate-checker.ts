import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, App, Events, LoadingController, NavController, NavParams, Platform, PopoverController, Select, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { CorporateDetailPage } from '../corporate-detail/corporate-detail';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { Changefont } from '../changefont/changeFont';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

declare var window;
@Component({
  selector: 'page-corporate-checker',
  templateUrl: 'corporate-checker.html',
  providers: [ChangelanguageProvider]
})
export class CorporateCheckerPage {
  userdata: any;
  accountlist: any;
  loading: any;
  useraccount: any;
  currentpage: any = 1;
  pageSize: any = 10;
  duration: any = 0;
  durationmsg: any;
  passTemp: any = {};
  //todayDate: any = '';
  resultdata: any = [];
  changedata: any = [];
  range: any = false;
  next: number = 1;
  previous: number = 1;
  status: any;
  ipaddress: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  modal: any;
  hardwareBackBtn: any = true;
  font: string;
  tempFont: number = 3;
  showFont: string[] = [];
  popover: any;
  fromDate: any = new Date().toISOString();
  toDate: any = new Date().toISOString();
  fabMargin: any = '';
  isDownload: boolean = true;
  fileData: any = [];
  textEng: any = ['Transaction Approval', 'Select Account', 'Filter by Date', 'Last 10 Transaction', 'Last 2 Days', 'Last 5 Days', 'No result Found', 'Next', 'Previous', 'Default', '2 Days', '5 Days', 'Custom', 'Start Date', 'End Date', 'Search', 'Are you sure you want to exit', 'Yes', 'No', 'Ref No', 'Company Name', 'To Account', 'Reject', 'Approve', 'Approved', 'Rejected', 'Amount', 'Date/Time'];
  textMyan: any = ['ကော်ပိုရိတ် ငွေလွှဲစာရင်းအတည်ပြုရန်', 'စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ရက်အလိုက်စီစဉ်သည်', 'နောက်ဆုံး၁၀ကြောင်း', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'အချက်အလက်မရှိပါ', 'ရှေ့သို့', 'နောက်သို့', 'နောက်ဆုံး ၁၀ ချက်', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'စိတ်ကြိုက်ရွေးချယ်မည်', 'စတင်သည့်ရက်', 'ပြီးဆုံးသည့်ရက်', 'ရှာမည်', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ', 'အမှတ်စဉ်', 'Company Name', 'To Account','အတည်မပြု','အတည်ပြု','Approved', 'Rejected', 'ငွေပမာဏ', 'နေ့ရက်/အချိန်'];
  accDesc: any = [];
  daterange = false;
  statuslist = [{ "value": "0", "caption": "All" }, { "value": "1", "caption": "Pending" }, { "value": "2", "caption": "Approved" }, { "value": "3", "caption": "Rejected" }];
  selectedStatus = "";
  public alertPresented : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http, public datePipe: DatePipe, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public events: Events, public idle: Idle, public all: AllserviceProvider, public app: App, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    public changefont: Changefont, public platform: Platform, public global: GlobalProvider, public file: File, public fileOpener: FileOpener, private transfer: FileTransfer, 
    private firebaseAnalytics: FirebaseAnalytics) {
    //let date = new Date();
    // this.isDownload = true;    
    //this.fabMargin = { 'margin-bottom': '' };
    //this.todayDate = this.datePipe.transform(date, 'dd MM yyyy');  
  }
  ionViewDidEnter() {    
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.selectedStatus = '1';
        this.changedata = [];
        this.getAllTransaction();
      });
    });
  }

  ionViewDidLoad() {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

  }
  ionViewDidLeave() {

    this.slimLoader.reset();
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }


  /* accountChange(s) {
    this.useraccount = s;

    this.tempFont = 3;
    this.duration = 0;
    this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
    this.getAllTransaction();
  } */

  goduration(s) {
    this.duration = s;

  }

  /* datafilter(ev) {

    this.popover = this.popoverCtrl.create(MyPopOverPage, {});

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      this.duration = data;
      this.currentpage = 1;
      if (data != 3 && data != null) {
        if (data == 0) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
          this.tempFont = 3;
        }
        if (data == 1) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[4] + " )";
          this.tempFont = 4;
        }
        else if (data == 2) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[5] + " )";
          this.tempFont = 5;
        }

        this.range = false;
        this.currentpage = 1;
        this.pageSize = 10;
        this.getAllTransaction();
      }
      else if (data == 3) {
        this.range = true;
        this.tempFont = 6;
      }

    });
  } */

  /* nextData() {
    this.next++;
    if (this.next <= this.passTemp.pageCount) {
      this.currentpage = this.currentpage + 1;
      this.previous = this.next;
      this.getAllTransaction();
    }

  }

  previousData() {
    this.previous--;
    if (this.previous >= 1) {
      this.currentpage = this.previous;
      this.next = this.previous;
      this.getAllTransaction();
    }
  } */

  getAllTransaction() {
    /* if (this.fromDate != '' && this.toDate != '' && this.duration == '3') {
      this.durationmsg = " ( " + this.datePipe.transform(this.fromDate, 'dd-MM-yyyy') + " to " + this.datePipe.transform(this.toDate, 'dd-MM-yyyy') + " )";

    } */
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    var param = {
      requestuserid: this.userdata.userID,
      //sessionID: this.userdata.sessionID,
      //durationType: this.duration,
      //fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      //toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      //totalCount: 0,      
      //currentPage: this.currentpage,
      //pageSize: this.pageSize,
      //pageCount: 0,
    };

    this.http.post(this.ipaddress + '/service005/getAllTransactionList', param).map(res => res.json()).subscribe(data => {

      if (data.code == '0000') {
        //this.isDownload = false;
        //this.range = false;
        //this.status = 1;
        /* if (data.pageCount > 1 && this.currentpage == 1) {
          this.next = 1;
          this.previous = 1;
          this.fabMargin = { 'margin-bottom': '10%' };
        } else if (this.currentpage > 1) {
          this.fabMargin = { 'margin-bottom': '10%' };
        } else {
          this.fabMargin = { 'margin-bottom': '0' };
        } */
        if (data.list != null || data.list != undefined || data.list != "") {
          let tempArray = [];
          if (!Array.isArray(data.list)) {
            tempArray.push(data.list);
            data.list = tempArray;
          }
          //let mydate =data.list[0].createddatetime(0,9);
          //data.list.txnDate =this.datePipe.transform(mydate, 'ddMMyyyy')
          this.passTemp = data;
          this.fileData = data.list;
          /* let group_to_values = data.list.reduce(function (obj, item) {
            obj[item.createddatetime] = obj[item.txnDate] || [];
            obj[item.createddatetime].push(item);
            return obj;
          }, {});
          let groups = Object.keys(group_to_values).map(function (key) {
            return { createddatetime: key, data: group_to_values[key] };
          }); */
          //this.resultdata = groups;
          console.log(JSON.stringify("" +this.fileData.length));
          let j = 0;
          for (let i = 0; i < this.fileData.length; i++) {
            if (this.fileData[i].status == this.selectedStatus) {
              this.changedata[j] = this.fileData[i];
              j++;
            }
          }
          console.log(JSON.stringify("resultdata" + this.changedata.length));
        } else {
          this.fileData = [];
          this.changedata = [];
        }
        //this.slimLoader.complete();
        this.loading.dismiss();
      }
      else if (data.code == "0016") {
        this.status = 0;
        this.isDownload = true;
        this.fabMargin = { 'margin-bottom': '0' };
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })

        confirm.present();
        this.loading.dismiss();
        //this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.range = false;
        this.passTemp = {};
        this.resultdata = [];
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
        //this.slimLoader.complete();
      }

    },
      error => {
        this.status = 0;
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      });
  }
  goStatusChange(s) {
    this.changedata = [];
    let j = 0;
    if (s != '0') {
      for (let i = 0; i < this.fileData.length; i++) {
        if (s == this.fileData[i].status) {
          this.changedata[j] = this.fileData[i];
          j++
        }
      }
    } else this.changedata = this.fileData;
  }

  goApproveReject(obj, status) {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    var param = {
      "autokey": obj.autokey,
      "approveuserid": this.userdata.userID,
      "approveusername": this.userdata.Name,
      "status": status
    };
    console.log(JSON.stringify("Change Status=" + param));
    this.http.post(this.ipaddress + '/service005/updateApproveTransaction', param).map(res => res.json()).subscribe(data => {
      if (data.code == '0000') {
        // obj.status = data.status;
        this.transferDeposit(obj, data.status);
        console.log(JSON.stringify("resultdata" + obj.status));
        this.loading.dismiss();
      }
      else if (data.code == "0016") {
        this.status = 0;
        this.isDownload = true;
        this.fabMargin = { 'margin-bottom': '0' };
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })

        confirm.present();
        //this.slimLoader.complete();
        this.loading.dismiss();
      }
      else {
        this.status = 0;
        this.range = false;
        this.passTemp = {};
        this.resultdata = [];
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      }

    },
      error => {
        this.status = 0;
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      });
  }
  transferDeposit(adata, upstatus) {
    let param = {
      userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: adata.t1,
      toAccount: adata.t2, amount: adata.amount, bankCharges: 0, narrative: "", refNo: adata.autokey,
      field1: '2', field2: ''
    };
    console.log(JSON.stringify("transferDeposit" + param));
    this.http.post(this.ipaddress + '/service003/goAgentTransferDeposit', param).map(res => res.json()).subscribe(data => {
      // this.firebase.logEvent('quick_pay', {})
      //     .then((res: any) => { console.log(res); })
      //     .catch((error: any) => console.log(error));

      this.firebaseAnalytics.logEvent('quick_pay', {})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));

      if (data.code == '0000') {
        adata.status = upstatus;
        let j = 0;
        this.changedata = [];
        this.goStatusChange(this.selectedStatus);
        //this.slimLoader.complete();
        this.showAlert(data.desc);
      }
      else if (data.code == "0016") {
        this.status = 0;
        this.isDownload = true;
        this.fabMargin = { 'margin-bottom': '0' };
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: "warningAlert"
        })

        confirm.present();
        //this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.range = false;
        this.passTemp = {};
        this.resultdata = [];
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      }

    },
      error => {
        this.status = 0;
        this.fabMargin = { 'margin-bottom': '0' };
        this.isDownload = true;
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        this.loading.dismiss();
      });
  }

  gotoDetail(a) {
    this.navCtrl.push(CorporateDetailPage,
      {
        data: a

      });
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }
  /* ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {      
      this.select.close();
      this.navCtrl.pop();
    });
  } */
  showAlert(message) {
    let vm = this
    if (!vm.alertPresented) {
      vm.alertPresented = true
      vm.alertCtrl.create({
       // title: title,
        message: message,
        buttons: [{
          text: 'OK',
          handler: () => {
            vm.alertPresented = false
          }
        }],
        cssClass: 'warningAlert',
        enableBackdropDismiss: false
      }).present();
    }
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
