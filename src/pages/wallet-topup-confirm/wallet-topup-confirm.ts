import 'rxjs/add/operator/map';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { WalletTopupOtpPage } from '../wallet-topup-otp/wallet-topup-otp';
import { Login } from '../login/login';
import { WalletTopupSuccessPage } from '../wallet-topup-success/wallet-topup-success';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-wallet-topup-confirm',
  templateUrl: 'wallet-topup-confirm.html',
})
export class WalletTopupConfirmPage {
  textMyan: any = ["အတည်ပြုခြင်း", "အကောင့်နံပါတ်", "ဝေါ(လ်)လက် နံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "အမည်"];
  textEng: any = ["Confirmation", "Account Number", "Wallet ID", "Amount", "Narrative", "CANCEL", "CONFIRM", "Name"];
  showFont: string[] = [];
  font: string = '';
  obj: any;
  userData: any;
  ipaddress: any;
  isChecked: boolean = false;
  passOtp: any;
  passSKey: any;
  loading: any;
  _walletObj: any = {};
  fromPage: any;
  amount: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public events: Events, public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider,
    public storage: Storage, public alertCtrl: AlertController, public http: Http, public changefont: Changefont,
    public toastCtrl: ToastController, private slimLoader: SlimLoadingBarService,public util:UtilProvider,
    private firebaseAnalytics: FirebaseAnalytics) {
    this._walletObj = this.navParams.get("data");
    this.passOtp = this.navParams.get("otp");
    this.passSKey = this.navParams.get('sKey');
    this.fromPage = this.navParams.get('fromPage');
    this.amount = this.util.formatAmount(this._walletObj.amount)+"  "+"MMK";
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan.data)
      });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    /*  this.storage.get('language').then((font) => {
       this.font = font;
       this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
         this.showFont = data;
       });
     }); */

    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad UtilityConfirmPage');
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
  cancel() {
    this.navCtrl.pop();
  }

  confirm() {
    if (this.passOtp == 'true') {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 16, merchantID: this._walletObj.merchantID, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.push(WalletTopupOtpPage, {
            dataOTP: data,
            data: this._walletObj,
            sKey: this.passSKey
          })
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
    else {
      this.goTransfer();
    }
  }

  goTransfer() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let parameter = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      fromAccount: this._walletObj.account,
      toAccount: "",
      merchantID: "",
      bankCharges: "",
      refNo: this._walletObj.walletID,
      sKey: this.passSKey,
      amount: this.formatToDouble(this._walletObj.amount),
      narrative: this._walletObj.narrative,
      field1: "2",
      field2: "",
      fromName:this._walletObj.fromName,
      toName:this._walletObj.toName
    };
    // console.log("request gopayment =" + JSON.stringify(parameter))
    this.http.post(this.ipaddress + '/service003/goTopupToWallet', parameter).map(res => res.json()).subscribe(res => {
      // console.log("response gopayment  data>>" + JSON.stringify(res));
    
      this.firebaseAnalytics.logEvent('wallet_topup', {"userid": this.userData.userID})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
      if (res.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.setRoot(WalletTopupSuccessPage, {
          data: res,
          detail: this._walletObj,
          fromPage: this.fromPage,
        });
      }
      else if (res.code == "0016") {
        this.logoutAlert(res.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', res.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }
  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
