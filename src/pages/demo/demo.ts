import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the DemoPage page.
 *
 * See http:
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-demo',
  templateUrl: 'demo.html',
  providers : [ChangelanguageProvider,GlobalProvider]
})
export class DemoPage {
  headeritem : any = "Demo";
  textMyan:any=[{name: 'Full version',key:0},{name: 'FLexcube',key:1}, {name: 'iCBS',key:2}];
  textEng:any=[{name: 'Full version',key:0},{name: 'FLexcube',key:1}, {name:'iCBS',key:2}];
  font : any ='';
  tagid : any;
  popoverItemList1 : any [] = [{name: '',key:0},{name: '',key:1}, {name: '',key:2}];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
              public changelanguage:ChangelanguageProvider,public storage:Storage,public global:GlobalProvider) {
    this.storage.get('demo').then((data) => {
      this.tagid = data;
    });
    this.storage.get('language').then((font) => {
      this.font =font;
      this.changelanguage.changeLanguageForPopup(font, this.textEng,this.textMyan).then(data =>
      {
        this.popoverItemList1 = data;
      });
    });
  }
  ionViewDidLoad() {
  }
  changeLink(s){
    this.storage.set("demo",s);
    this.viewCtrl.dismiss(s);
  }
}
