import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ForcechangePage } from '../forcechange/forcechange';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';

declare var window: any;
declare var cordova: any;

@Component({
  selector: 'page-login-otp',
  templateUrl: 'login-otp.html',
  providers: [ChangelanguageProvider]
})
export class LoginOtpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  showFont: string[] = [];
  font: string;
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData: any;
  public loading;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  passData: any;
  userID: any;
  from:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController,
    public storage: Storage, public alertCtrl: AlertController, public events: Events, public network: Network, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider) {
    this.regdata = this.navParams.get("data");
    this.from=this.navParams.get("from");
    this.userData = this.navParams.get("userData");
    this.userID = this.userData.userID;
    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
      }
      else {
        this.ipaddress = result;
      }
    });
    //this.sms.hasPermission();
    /* window.SMS.startWatch(function () {
      // console.log('Watching');
    }, function () {
      // console.log('Not Watching');
    });
    setTimeout(() => {
      // console.log("hi");
    }, 6000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      //
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
      //
     //this.slimLoader.complete();
    }) */
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  ionViewDidLoad() {
  }

  getconfirm() {
    if (this.otpcode != "" && this.otpcode != null && this.otpcode != undefined) {
      this.loading = this.loadingCtrl.create({
        content: "Processing",
        dismissOnPageChange: true
      });
      this.loading.present();
      let userID = this.regdata.userID;
      let paramCheckOTP = { "userID": userID, "sessionID": this.regdata.sessionID, "otpCode": this.otpcode, "rKey": this.regdata.rKey, "sKey": this.regdata.sKey };
      this.http.post(this.ipaddress + '/service002/checkLoginOTP', paramCheckOTP).map
        (res => res.json()).subscribe(data => {
          let login = data.code;
          if (login == '0000') {
            let tempArray = [];
            if (!Array.isArray(data.debitAcc)) {
              tempArray.push(data.debitAcc);
              data.debitAcc = tempArray;
            }
            let loginData={
              sessionID: data.sessionID,
              userID: data.userID,
              userName: this.userData.userName
            }
            this.storage.get('userData').then((result) => {
              if (result == null || result == '') {
                this.storage.set('userData', loginData);
              }
              else {
                this.storage.remove('userData');
                this.storage.set('userData', loginData);
              }
              this.events.publish('login_success', true);
              this.events.publish('lastTimeLoginSuccess', this.userData.lastTimeLoginSuccess);
             //this.slimLoader.complete();
              this.navCtrl.setRoot(MainHomePage, {
                data: 'login'
              });
            });
          }
          else if (login == '0001') {
            let alert = this.alertCtrl.create({
              title: 'Information',
              subTitle: this.userData.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    let temp1 = { userID: this.userID, sessionID: this.userData.sessionID };
                    this.navCtrl.setRoot(ForcechangePage, {
                      data: temp1
                    });
                  }
                },
              ],
              enableBackdropDismiss: false
            });
            alert.present();
           //this.slimLoader.complete();
          }//forcechangepwd
          else if (login == '0002') {
            let alert = this.alertCtrl.create({
              title: 'Information',
              subTitle: this.userData.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    let temp1 = { userID: this.userID, sessionID: this.userData.sessionID };
                    this.navCtrl.setRoot(ForcechangePage, {
                      data: temp1
                    });
                  }
                },
                {
                  text: 'CANCEL',
                  handler: () => {
                    let tempArray = [];
                    if (!Array.isArray(data.debitAcc)) {
                      tempArray.push(data.debitAcc);
                      this.userData.debitAcc = tempArray;
                    }
                    let loginData={
                      sessionID: data.sessionID,
                      userID: data.userID,
                      userName: this.userData.userName
                    }
                    this.storage.get('userData').then((result) => {
                      if (result == null || result == '') {
                        this.storage.set('userData', loginData);
                      }
                      else {
                        this.storage.remove('userData');
                        this.storage.set('userData', loginData);
                      }
                      this.events.publish('login_success', true);
                      this.events.publish('lastTimeLoginSuccess', this.userData.lastTimeLoginSuccess);
                      this.navCtrl.setRoot(MainHomePage, {
                        data: 'login'
                      });
                    });
                  }
                },
              ],
              enableBackdropDismiss: false
            });
            alert.present();
          }//forcechangepwd
          else if (login == '0015') {
           //this.slimLoader.complete();
            let temp = { userID: this.userID, sessionID: this.userData.sessionID };
            this.navCtrl.setRoot(ForcechangePage, {
              data: temp
            });
          }
          else {
            //this.loginData.userpsw = '';
            this.all.showAlert('Warning!', data.desc);
            //this.gologout();
            this.loading.dismiss();
           //this.slimLoader.complete();
          }
        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.loading.dismiss();
          });
    }
  }

  gologout() {
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
        }
        else {
          //this.all.showAlert('Warning!', data.desc);
          //this.loading.dismiss();
        }
      },
        error => {
          //this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          //this.loading.dismiss();
        });
    });
  }

  gobackLogin() {
    this.gologout();
    /* if(this.from=='phone_otp'){
      this.navCtrl.setRoot(LoginPhonePage, {
      });
    }else{
      this.navCtrl.setRoot(Login, {
      });
    } */
    this.navCtrl.setRoot(Login, {
    });
    this.navCtrl.popToRoot();
  }


  sendOtpCode() {
    this.checkNetwork();
    if (this.flag == "success") {
      /* this.slimLoader.start(() => {
      }); */
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '15', merchantID: '', sKey: this.userData.sKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.userData.rKey = data.rKey;
          /* window.SMS.startWatch(function () {
          }, function () {
          });
          setTimeout(() => {
          }, 8000);
          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;
            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];
           //this.slimLoader.complete();
          }); */
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.events.publish('login_success', false);
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass:'warningAlert'
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
         //this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
         //this.slimLoader.complete();
        });
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
    this.network.onDisconnect().subscribe(data => {
      this.flag = 'none';
      this.all.showAlert('Warning!', "Check your internet connection!");
    }, error => console.error(error));
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
}
