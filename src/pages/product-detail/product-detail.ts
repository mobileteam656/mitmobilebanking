import { Component } from '@angular/core';
import {  NavController, NavParams, Platform, Events, PopoverController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
// import { ProductImgViewPage } from '../product-img-view/product-img-view';
import { Changefont } from '../changefont/changeFont';
import { Idle } from '@ng-idle/core';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ProductPage } from '../product/product';
 /* Generated class for the ProductDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetailPage {
  textEng:any=["Product Detail"];
  textMyan:any=["ကုန်ပစ္စည်းအသေးစိတ်"];
  ststus: any;
  userdata: any;
  ipaddress: any;
  font: string;
  textFont: any=[];
  loading: any;
  productlist: any=[];
  billerid: any;
  product: any=[];
  imglink: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService,
    public util: UtilProvider,public changeLanguage: ChangelanguageProvider) {
    this.product=navParams.get('data');
    this.imglink = this.global.imglink;
    //console.log("Product"+this.product);
    this.ststus = this.navParams.get("ststus");
    if (this.ststus == '1') {
      this.storage.get('userData').then((val) => {
        this.userdata = val;
        this.storage.get('ipaddress').then((result) => {
          if (result == null || result == '') {
            this.ipaddress = this.global.ipaddress;
          }
          else {
            this.ipaddress = result;
          }
        })
      });


    }
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.textFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.textFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ProductDetailPage');
  }

}
