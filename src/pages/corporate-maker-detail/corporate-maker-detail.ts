import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { FastTransferPage } from '../fast-transfer/fast-transfer';
import { Http } from '@angular/http';
import { BeneficiaryAddPage } from '../beneficiary-add/beneficiary-add';
import { OwnTransferPage } from '../own-transfer/own-transfer';
import { InternalTransferPage } from '../internal-transfer/internal-transfer';
import { WalletTopupPage } from '../wallet-topup/wallet-topup';
import { MainHomePage } from '../main-home/main-home';
import { Changefont } from '../changefont/changeFont';
@Component({
  selector: 'page-corporate-maker-detail',
  templateUrl: 'corporate-maker-detail.html',
  providers: [ChangelanguageProvider]
})
export class CorporateMakerDetailPage {
  passTemp: any;
  passTemp1: any;
  //passTemp2: any;
  textEng: any = ["From account", "To account", "Amount", "Bank Reference No.", "Transaction Date", "Card Expired", "Transaction Approved", "Transfer Type", "Close","MMK"];
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ငွေလွှဲ အမျိုးအစား', 'ပိတ်မည်','MMK'];
  showFont: any = [];
  font: any = '';
  //fromPage: any;
  beneficiaryID: any;
  loading: any;
  userdata: any;
  ipaddress: any;
  beneStatus: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public events: Events, public changefont: Changefont, public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
    public http: Http, ) {
    this.passTemp = this.navParams.get("data");
    this.passTemp1 = this.navParams.get("detail");
    //this.passTemp2 = this.navParams.get("type");
    //this.fromPage = this.navParams.get('fromPage');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((data) => {
      this.userdata = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }  

}
