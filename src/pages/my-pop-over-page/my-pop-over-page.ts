import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController,Events } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
/**
 * Generated class for the MyPopOverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-pop-over-page',
  templateUrl: 'my-pop-over-page.html',
  providers : [ChangelanguageProvider]
})
export class MyPopOverPage {
  popoverItemList : any = [{name: '',key:0},{name: '',key:1}, {name: '',key:2}, {name: '',key:3}];
  font:string;
  textMyan:any=[{name: 'နောက်ဆုံး၁၀ကြောင်း',key:0},{name: 'လွန်ခဲ့သော၂ရက်',key:1}, {name: 'လွန်ခဲ့သော၅ရက်',key:2},{name: 'စိတ်ကြိုက်ရွေးချယ်မည်',key:3}];
  textEng:any=[{name: 'Default',key:0},{name: '2 Days',key:1}, {name: '5 Days',key:2}, {name: 'Custom',key:3}];
  showFont:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public changelanguage:ChangelanguageProvider,public events:Events,public storage:Storage) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changelanguage.changeLanguageForPopup(lan.data, this.textEng,this.textMyan).then(data =>
      {
        // console.log("Ngar data " + JSON.stringify(data));
        this.popoverItemList = data;
        // console.log("Show pe lay " + JSON.stringify(this.popoverItemList));
      });
    });
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font =font;
      this.changelanguage.changeLanguageForPopup(font, this.textEng,this.textMyan).then(data =>
      {
        // console.log("Ngar data " + JSON.stringify(data));
        this.popoverItemList = data;
        // console.log("Show pe lay " + JSON.stringify(this.popoverItemList));
      });
    });
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
     // this.popoverItemList=this.textMyan;
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.changefont.UnitoZg(this.textMyan[j].name);
      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
  }*/

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MyPopOverPage');
  }
  changeLanguage(s) {
    this.viewCtrl.dismiss(s);
  }
}
