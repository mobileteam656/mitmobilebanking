import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MainHomePage } from '../main-home/main-home';

@Component({
  selector: 'page-quickpay-success',
  templateUrl: 'quickpay-success.html',
})
export class QuickpaySuccessPage {

  textMyan: any = ["Bill Aggregator Details", "Customer(Ref No)", "Company/Customer Name", "ငွေပမာဏ", "ပယ်ဖျက်မည်", "ပေးမည်", "အကောင့်ရွေးချယ်ပါ", "နေရပ်လိပ်စာ", "အမည်", "ငွေပမာဏ", "မှတ်ပုံတင်", "ဖုန်းနံပါတ်", "ငွေပေးချေမှု အောင်မြင်ပါသည်", "ပိတ်သည်", "အကြောင်းအရာ"];
  textEng: any = [ "Bill Aggregator Details", "Payment Code(Ref No)", "Company/Customer Name", "Amount","CANCEL" , "SUBMIT","Select Account","Wave Account No","Name","Amount","NRC","Phone No","Transfer Successful.","CLOSE","Narrative"];    
  showFont: string[] = [];
  billerid = "";

  data: any[] = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
    this.billerid = this.navParams.get("billerid");
     this.data = this.navParams.get("data");
    //  console.log("data==" , JSON.stringify(this.data));
    // this.name = this.navParams.get("name");
    // this.amount = this.navParams.get("amount");
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad QuickpayPage');
  }
 
  goClose(){
    this.navCtrl.setRoot(MainHomePage);
  }

}

