import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChequeStopOtpPage } from '../cheque-stop-otp/cheque-stop-otp';
import { ChequestopsuccessPage } from '../chequestopsuccess/chequestopsuccess';
import { Login } from '../login/login';
import { AllserviceProvider } from '../../providers/allservice/allservice';

@Component({
    selector: 'page-chequestopconfirm',
    templateUrl: 'chequestopconfirm.html',
    providers: [ChangelanguageProvider]
})
export class ChequestopconfirmPage {
    font: string;
    showFont: any = [];
    ipaddress: string;
    loading: any;
    userData: any;
    passSKey : any;
    textEng: any = ['Account Number', 'Cheque Number', 'Status', 'Confirmation', 'Cancel', 'Confirm'];
    textMyan: any = ['စာရင်းနံပါတ်', 'ချက်နံပါတ်', 'ရလဒ်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ပယ်ဖျက်သည်', 'လုပ်ဆောင်မည်'];
    constructor(public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams, public storage: Storage, public changeLanguage: ChangelanguageProvider, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
        public alertCtrl: AlertController, public events: Events) {
        this.userData = this.navParams.get('data');
        this.passSKey = this.navParams.get('sKey');
        this.storage.get('ipaddress').then((ipaddress) => {
            
            this.ipaddress = ipaddress;

        });
        this.storage.get('language').then((font) => {
            
            this.font = font;
            this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
                
                this.showFont = data;
                
            });

        });
    }

    ionViewDidLoad() {
        
    }
    cancelbtn() {
        this.navCtrl.pop();
    }


    okbtn() {
        this.loading = this.loadingCtrl.create({
            content: "Pleas Wait...",
            dismissOnPageChange: true
            //   duration: 3000
        });
        this.loading.present();
        let param = {
            userID: this.userData.userID,
            sessionID: this.userData.sessionID,
            accountNo: this.userData.accountNo,
            chequeNo: this.userData.chequeNo,
            sKey: this.passSKey
        };
        if (this.userData.otp == 'true') {
            let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '8', merchantID: '', sKey: this.passSKey };
            

            this.http.post(this.ipaddress + '/service001/getOTP', parameter).map(res => res.json()).subscribe(data => {
                

                if (data.code == "0000") {
                    this.loading.dismiss();
                    this.navCtrl.push(ChequeStopOtpPage, {
                        data: param,
                        detail: this.userData,
                        otprKey: data,
                        sKey: this.passSKey
                    })

                }
                else if (data.code == "0016") {

                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,

                        message: data.desc,
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {
                                    
                                    this.storage.remove('userData');
                                    this.events.publish('login_success', false);
                                    this.events.publish('lastTimeLoginSuccess', '');
                                    this.navCtrl.setRoot(Login, {});
                                    this.navCtrl.popToRoot();
                                }
                            }
                        ],
                        cssClass:'warningAlert'
                    })
                    confirm.present();
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert('Warning!', data.desc);
                    this.loading.dismiss();
                }

            },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }
        else {
            
            this.http.post(this.ipaddress + '/service001/stopCheque', param).map(res => res.json()).subscribe(data => {
                
                if (data.code == "0000") {
                    this.userData.status = data.status;
                    this.loading.dismiss();
                    //this.navCtrl.push(ChequestopsuccessPage,{data:this.userData});
                    this.navCtrl.setRoot(ChequestopsuccessPage, { data: this.userData });

                }
                else {
                    this.all.showAlert('Warning!', data.desc);
                    this.loading.dismiss();
                }

            },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }
    }
}
