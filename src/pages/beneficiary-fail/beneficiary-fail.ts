import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, NavController, NavParams, Platform } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { BeneficiaryListPage } from '../beneficiary-list/beneficiary-list';

@Component({
  selector: 'page-beneficiary-fail',
  templateUrl: 'beneficiary-fail.html',
})
export class BeneficiaryFailPage {

  textMyan : any = ['ရလဒ်',"လုပ်ဆောင်မှု မအောင်မြင်ပါ", 'ပိတ်မည်'];
  textEng: any = ["Result",  "Transaction Failed","Close"];
  showFont : string [] = [];
  font:string;
  passtemp1 : any;
  passtemp2 : any;
  passtemp3 : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public events:Events,public storage:Storage,public platform:Platform, public changeLanguage:ChangelanguageProvider) {
    this.passtemp1 = this.navParams.get('data');
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        this.showFont = data;
      });
    });
  }

  ionViewDidLoad() {
  }

  getOK(){
      this.navCtrl.setRoot(BeneficiaryListPage)
  }
}

