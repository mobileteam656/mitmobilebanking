import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
@Component({
  selector: 'page-tax-information-detail',
  templateUrl: 'tax-information-detail.html',
})
export class TaxInformationDetailPage {
  textMyan: any = ["အခွန်အချက်အလက်", "TIN No.", "Profile ID", "Profile Name", "Registration No.", "Phone Number", "Email", "Fax Number", "No.", "Street", "Quarter", "Township Code", "Region Code", "Main Business", "Session Code", "Ref No.", "Company Type", "Industry Code", "Tax Amount", "Business Profile", "Tax Type", "Payment Type", "Income Year", "Tax Period", "Address 1", "Address 2", "Creditor Branch Code", "Creditor Branch Name", "Type of Business", "MD Account No.", "Tax Office Name", "TIN Info"];
  //["အခွန်အချက်အလက်", "အခွန်နံပါတ်","ပရိုဖိုင်အိုင်ဒီ", "ပရိုဖိုင်နာမည်", "မှတ်ပုံတင်အမှတ်","ဖုန်းနံပါတ်","အီးမေးလ်","ဖက်(စ်)","အိမ်နံပါတ်","လမ်း","ရပ်ကွက်","မြို့နယ်","ဒေသ","လုပ်ငန်း","Session Code","Ref No.","ကုမ္ပဏီအမျိုးအစား", "လုပ်ငန်းကုဒ်",  "လုပ်ငန်းအချက်အလက်", "အခွန်အမျိုးအစား", "ပေးဆောင်ခြင်းအမျိုးအစား", "အခွန်နှစ်ကာလ","အခွန်ကာလ","လိပ်စာ ၁","လိပ်စာ ၂"];
  textEng: any = ["Tax Information", "TIN No.", "Profile ID", "Profile Name", "Registration No.", "Phone Number", "Email", "Fax Number", "No.", "Street", "Quarter", "Township Code", "Region Code", "Main Business", "Session Code", "Ref No.", "Company Type", "Industry Code", "Tax Amount", "Business Profile", "Tax Type", "Payment Type", "Income Year", "Tax Period", "Address 1", "Address 2", "Creditor Branch Code", "Creditor Branch Name", "Type of Business", "MD Account No.", "Tax Office Name", "TIN Info"];
  textData: any = [];
  font: any = '';
  taxData: any = {};
  address1: any = '';
  address2: any = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public changefont: Changefont, public storage: Storage) {
    this.taxData = this.navParams.get("taxData");
    this.address1 = "No. " + this.taxData.houseNo + ", " + this.taxData.street
    if (this.taxData.quarter != '-') {
      this.address1 += ", " + this.taxData.street + " Quarter"
    }
    this.address2 = this.taxData.townShipCode + ", " + this.taxData.regionCode
  }

  ionViewDidLoad() {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
}
