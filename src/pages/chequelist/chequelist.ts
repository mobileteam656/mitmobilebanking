import 'rxjs/Rx';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ChequePage } from '../cheque/cheque';
import { ChequestopPage } from '../chequestop/chequestop';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { GlobalProvider } from '../../providers/global/global';
import { MainHomePage } from '../main-home/main-home';

/**
 * Generated class for the ChequelistPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-chequelist',
  templateUrl: 'chequelist.html',
  providers: [Changefont]

})
export class ChequelistPage {
  popover: any;
  loading: any;
  modal: any;
  userdata: any;
  ipaddress: string;
  hardwareBackBtn: any = true;
  font: string;
  showFont: any = [];
  textEng: any = ['Cheque Status Inquiry', 'Stop Cheque Request', 'Services'];
  textMyan: any = ['ချက်အခြေအနေ စုံစမ်းခြင်း', 'ချက်ပေးချေမှု ပယ်ဖျက်ခြင်း', "ဝန်ဆောင်မှုများ"];
  constructor(public global: GlobalProvider, public navCtrl: NavController, public all: AllserviceProvider, public navParams: NavParams, private alertCtrl: AlertController, public storage: Storage, public events: Events, public changefont: Changefont, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public platform: Platform, public popoverCtrl: PopoverController,
    public http: Http) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data);
    });
    this.storage.get('language').then((font) => {

      this.changelanguage(font);

    });
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
        }
      })
    });
  }
  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(PopoverPage, {
    });

    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        //  this.navCtrl.push(Language)
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }
  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }
  clickcheque(page) {
    if (page == 1) this.navCtrl.push(ChequePage);
    else this.navCtrl.push(ChequestopPage);
  }
  changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j < this.textMyan.length; j++) {
        this.showFont[j] = this.textMyan[j];
      }
      //  this.showFont = this.textMyan;
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j < this.textMyan.length; j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else {
      this.font = '';
      for (let j = 0; j < this.textEng.length; j++) {
        this.showFont[j] = this.textEng[j];
      }
    }

  }

  ionViewDidLoad() {

  }

  backButtonAction() {
    //if(this.hardwareBackBtn)

    if (this.modal && this.modal.index === 0) {

      this.modal.dismiss();
    } else {
      // let hardwareBackBtn = true;

      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;

            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
