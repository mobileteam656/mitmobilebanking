import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { UtilProvider } from '../../providers/util/util';
import { TaxPaymentOtpPage } from '../tax-payment-otp/tax-payment-otp';
import { TaxPaymentFinishPage } from '../tax-payment-finish/tax-payment-finish';

@Component({
  selector: 'page-tax-payment-confirm',
  templateUrl: 'tax-payment-confirm.html',
})
export class TaxPaymentConfirmPage {
  userData: any;
  ipaddress: string;
  loading: any;
  flag: string;
  textMyan: any = ["အကောင့်နံပါတ်", "အခွန်နံပါတ်", "ပရိုဖိုင်နာမည်", "လုပ်ငန်း", "ကုမ္ပဏီအမျိုးအစား", "လုပ်ငန်းကုဒ်", "ဖုန်းနံပါတ်", "အခွန်ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "အတည်ပြုမည်", "အတည်ပြုခြင်း"];
  textEng: any = ["Account Number", "TIN No.", "Profile Name", "Main Business", "Company Type", "Industry Code", "Phone Number", "Tax Amount", "Narrative", "CANCEL", "CONFIRM", "Confirmation"];
  taxData: any;
  showFont: string[] = [];
  font: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  isOtp: any;
  sKey: any;
  amount: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public util: UtilProvider, public all: AllserviceProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.taxData = this.navParams.get('data');
    this.isOtp = this.navParams.get("otp");
    this.sKey = this.navParams.get('sKey');
    this.amount = this.util.formatAmount(this.taxData.taxAmount) + "  " + "MMK";
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  confirm() {
    if (this.isOtp == 'true') {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '1', merchantID: this.taxData.merchantID, sKey: this.sKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.push(TaxPaymentOtpPage, {
            data: this.taxData,
            sKey: this.sKey,
            rKey: data.rKey
          })
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
    else {
      this.goTaxPayment();
    }
  }

  goTaxPayment() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = {
      "userID": this.userData.userID,
      "sessionID": this.userData.sessionID,
      "bankAcc": this.taxData.fromAcc,
      "amount": this.taxData.taxAmount,
      "merchantID": this.taxData.merchantID,
      "processingCode": this.taxData.processingCode,
      "refNumber": this.taxData.phoneNumber,
      "field1": "",
      "field2": "2",
      /* "field3":this.taxData.tinNo,
      "field4":this.taxData.profileName,
      "field5":this.taxData.mainBusiness,
      "field6":this.taxData.companyType,
      "field7":this.taxData.industryCode,
      "field8":this.taxData.phoneNumber,
      "field9":this.taxData.refNo, 
      "field10":"",*/
      "field3": this.taxData.tinNo,
      "field4": this.taxData.profileID,
      "field5": this.taxData.profileName,
      "field6": this.taxData.registrationNo,
      "field7": this.taxData.phoneNumber,
      "field8": this.taxData.emailAddress,
      "field9": this.taxData.faxNumber,
      "field10": this.taxData.houseNo,
      "field11": this.taxData.street,
      "field12": this.taxData.quarter,
      "field13": this.taxData.townShipCode,
      "field14": this.taxData.regionCode,
      "field15": this.taxData.mainBusiness,
      "field16": this.taxData.industryCode,
      "field17": this.taxData.companyType,
      "field18": this.taxData.typeOfBusi,
      "field19": this.taxData.taxType,
      "field20": this.taxData.creditorBranchCode,
      "field21": this.taxData.creditorBranchName,
      "field22": this.taxData.taxOfficeName,
      "field23": this.taxData.mdaccNo,
      "field24": this.taxData.taxTypeCode,
      "field25": this.taxData.taxTypeDesc,
      "field26": this.taxData.paymentTypeCode,
      "field27": this.taxData.paymentTypeDesc,
      "field28": this.taxData.incomeYearCode,
      "field29": this.taxData.incomeYearDesc,
      "field30": this.taxData.taxPeriodCode,
      "field31": this.taxData.taxPeriodDesc,
      "field32": this.taxData.sessionCode,
      "field33": this.taxData.refNo,
      "amountServiceCharges": "",
      "paymentType": "1",
      "narrative": this.taxData.narrative,
      "sKey": this.sKey
    }
    this.http.post(this.ipaddress + '/service003/goTaxPayment ', param).map(res => res.json()).subscribe(res => {
      this.loading.dismiss();
      if (res.code == "0000") {
        this.navCtrl.setRoot(TaxPaymentFinishPage, {
          data: res,
          detail: this.taxData
        })
      }
      else if (res.code == "0016") {
        this.logoutAlert(res.desc);
        this.loading.dismiss();
      }
      else {
        this.loading.dismiss();
        this.navCtrl.setRoot(TaxPaymentFinishPage, {
          data: res,
          detail: this.taxData
        });
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  cancel() {
    this.navCtrl.pop();
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
