import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { MainHomePage } from '../main-home/main-home';
import { MptTopupPage } from '../mpt-topup/mpt-topup';
import { QrUtilityPage } from '../qr-utility/qr-utility';
import { TaxInfoPage } from '../tax-info/tax-info';
import { QuickpayPage } from '../quickpay/quickpay';
import { SkynetPage } from '../skynet/skynet';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';

declare var window;

@Component({
  selector: 'page-top-up-list',
  templateUrl: 'top-up-list.html',
})
export class TopUpListPage {
  textMyan: any = ["ငွေဖြည့်သွင်းခြင်း", "ငွေပေးချေမှု","Bill Aggregator"];
  textEng: any = ["Top Up", "Payment","Bill Aggregator"];
  textData: any = [];
  font: any = '';
  userData: any;
  ipaddress: any;
  flag: any;
  tempData: any;
  topUpList: any;
  loading: any;
  status: any;
  type: any;
  title: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public network: Network, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService) {
    this.type = this.navParams.get('data');
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userData = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.checkNetwork();
      })
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
    if (this.type == '1') {
      this.title = this.textData[1]
    } else if (this.type == '2') {
      this.title = this.textData[0]
    }
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
      this.storage.get('userData').then((data) => {
        this.userData = data;
        this.getTopUpList();
      });
    }
  }

  getTopUpList() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      cssClass: 'my-loading-class'
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID, paymentType: this.type };
    this.http.post(this.ipaddress + '/service001/getMerchant', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.merchantList)) {
          tempArray.push(result.merchantList);
          result.merchantList = tempArray;
        }
        let temp = [];
        let count = 0;
        this.topUpList = result.merchantList;
        for (let i = 0; i < this.topUpList.length; i++) {
          if (this.topUpList[i].processingCode != '000000') {
            temp[count] = this.topUpList[i];
            count++;
          }
        }
        this.topUpList = temp;
        this.status = 1;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.status = 0;
        this.all.showAlert("Warning!", result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }

  goPage(data) {
    if(this.type=='2'){
      this.navCtrl.setRoot(MptTopupPage, {
        data: data
      });
    }
   /*  if (data.processingCode == '200800') {
      this.navCtrl.setRoot(MptTopupPage, {
        data: data
      });
    } */ else if (data.processingCode == '100600') {
      this.navCtrl.setRoot(QrUtilityPage, {
        data: data
      });
    } else if (data.processingCode == '200900') {
      this.navCtrl.setRoot(TaxInfoPage, {
        data: data
      });
    } else if (data.processingCode == '080400') {
      this.navCtrl.setRoot(SkynetTopupPage, {
        data: data
      });
      //SkynetTopupPage
      //SkynetPage
    }
  }
  goNext(){
    this.navCtrl.push(QuickpayPage);
  }
}
