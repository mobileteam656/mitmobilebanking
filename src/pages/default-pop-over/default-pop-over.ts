import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, NavController, NavParams, ViewController } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-default-pop-over',
  templateUrl: 'default-pop-over.html',
  providers : [ChangelanguageProvider,GlobalProvider]
})
export class DefaultPopOver {
  textMyan:any=[{name: 'URL ပြောင်းမည်',key:0},{name: 'ဘာသာစကား',key:1}, {name: 'ဆက်သွယ်ရန်',key:2},{name: 'ဗားရှင်း 1.0.24 ',key:3}];
  textEng:any=[{name: 'Change URL',key:0},{name: 'Language',key:1}, {name: 'Call',key:2},{name:'Version 1.0.24 ',key:3}];

  font : any ='';
  popoverItemList : any [] = [{name: '',key:0},{name: '',key:1}, {name: '',key:2},{name: '',key:3}];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
              public changelanguage:ChangelanguageProvider,public events:Events,public storage:Storage,public global:GlobalProvider) {
  //  this.popoverItemList =
  //  this.textMyan[3].name = this.global.versionMyan;
  //  this.textEng[3].name = this.global.versionEng;
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changelanguage.changeLanguageForPopup(lan.data, this.textEng,this.textMyan).then(data =>
      {
        
        this.popoverItemList = data;
        
      });
    });
    this.storage.get('language').then((font) => {
      
      this.font =font;
      this.changelanguage.changeLanguageForPopup(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.popoverItemList = data;
        
      });
    });

  }

  ionViewDidLoad() {
    
  }

  /*changelanguage(lan) {
    if (lan == 'uni') {
      this.font = "uni";

      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.changefont.UnitoZg(this.textMyan[j].name);
      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
    
  }*/


  changeLanguage(s) {
    this.viewCtrl.dismiss(s);
  }
}
