import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the CloudPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-cloud',
  templateUrl: 'cloud.html',
  providers : [ChangelanguageProvider,GlobalProvider]
})
export class CloudPage {
  headeritem : any = "Cloud";
  textMyan:any=[{name: 'Demo',key:0},{name: 'Domain',key:1}, {name: 'URL',key:2},{name: 'Local',key:3}];
  textEng:any=[{name: 'Demo',key:0},{name: 'Domain',key:1}, {name:'URL',key:2},{name: 'Local',key:3}];
  font : any ='';
  tagid : any;
  popoverItemList : any [] = [{name: '',key:0},{name: '',key:1}, {name: '',key:2},{name: '',key:3}];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
              public changelanguage:ChangelanguageProvider,public storage:Storage,public global:GlobalProvider) {
    this.storage.get('cloud').then((data) => {
      this.tagid = data;
    });
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font =font;
      this.changelanguage.changeLanguageForPopup(font, this.textEng,this.textMyan).then(data =>
      {
        // console.log("Ngar data " + JSON.stringify(data));
        this.popoverItemList = data;
        // console.log("Show pe lay " + JSON.stringify(this.popoverItemList));
      });
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad CloudPage');
  }

  changeLink(s){
    this.storage.set("cloud",s);
    this.viewCtrl.dismiss(s);
  }

}
