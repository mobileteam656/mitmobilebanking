import { Component } from '@angular/core';
import { NavController, NavParams,ViewController, Events  } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the LanguagePopOver page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-language-pop-over',
  templateUrl: 'language-pop-over.html',
  providers: [Changefont]
})
export class LanguagePopOver {
  headeritem : any ='';
  headeritemEng : any = 'Language';
  headeritemMyn: any = 'ဘာသာစကား';
  textMyan:any=[{name: 'English',key:'eng'},{name: 'မြန်မာ',key:'uni'}];//, {name: 'ဇော်ဂျီ',key:'zg'}
  textEng:any=[{name: 'English',key:'eng'}, {name: 'Myanmar',key:'uni'}];//,{name:'Zawgyi',key:'zg'}
  font : any ='';
  popoverItemList : any [] = [{name: '',key:'eng'},{name: '',key:'uni'}];//,{name: '',key:'zg'}
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
              public changefont:Changefont,public events:Events,public storage:Storage) {
    //  this.popoverItemList =
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.changelanguage(font);
    });

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad LanguagePopOver');
  }

  changelanguage(lan) {
    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
      this.headeritem = this.headeritemMyn;
    }
    else if (lan == 'zg') {
      this.font = "zg";
      this.popoverItemList = this.textMyan;
      this.headeritem = this.changefont.UnitoZg(this.headeritemMyn);
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.popoverItemList[j].name = this.changefont.UnitoZg(this.textMyan[j].name);

      }
    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
      this.headeritem = this.headeritemEng;
    }
    // console.log('Your text is', JSON.stringify(this.popoverItemList));
  }


  changeLanguage(s) {
    this.viewCtrl.dismiss(s);
  }
}
