import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Login } from '../login/login';
import { SkynetErrorPage } from '../skynet-error/skynet-error';
import { SkynetSuccessPage } from '../skynet-success/skynet-success';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-skynet-otp',
  templateUrl: 'skynet-otp.html',
})
export class SkynetOtpPage {

  textMyan: any = [
    "အတည်ပြုသည်", "အတည်ပြု ကုဒ်နံပါတ်",
    "အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ် ပို့ထားပြီးပါပြီ",
    "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်"
  ];
  textEng: any = [
    "Verify", "Confirmation Code",
    "CONFIRM", "Confirmation code has been sent.",
    "Please wait.", "RESEND",
  ];
  showFont: string[] = [];
  font: string;

  merchantOtpObj: any;
  otpDataObj: any;
  passSKey: any;

  ipaddress: string;
  userdata: any;

  otpcode: any = '';

  flag: string;
  public loading;

  constructor(
    private global: GlobalProvider, public navCtrl: NavController,
    public navParams: NavParams, public http: Http,
    public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public all: AllserviceProvider, public storage: Storage,
    public alertCtrl: AlertController, public events: Events,
    public network: Network, public idle: Idle,
    public platform: Platform, public changeLanguage: ChangelanguageProvider,
    private firebaseAnalytics: FirebaseAnalytics
  ) {
    this.merchantOtpObj = this.navParams.get('data');
    this.otpDataObj = this.navParams.get('otpdata');
    this.passSKey = this.navParams.get('sKey');

    this.storage.get('userData').then((userdata) => {
      this.userdata = userdata;

      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      });
    });

    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  confirmOtp() {
    if (this.otpcode != undefined && this.otpcode != null && this.otpcode != '') {
      this.checkNetwork();

      if (this.flag == "success") {
        this.loading = this.loadingCtrl.create({
          content: "Processing...",
          dismissOnPageChange: true
        });
        this.loading.present();

        let param = {
          userID: this.userdata.userID,
          sessionID: this.userdata.sessionID,
          rKey: this.otpDataObj.rKey,
          otpCode: this.otpcode,
          sKey: this.passSKey
        };

        this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(
          data => {
            if (data.code == "0000") {
              this.goTransfer1();
            }
            else if (data.code == "0016") {
              this.logoutAlert(data.desc);
              this.loading.dismiss();
            }
            else {
              this.all.showAlert('Warning!', data.desc);
              this.loading.dismiss();
            }
          },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.loading.dismiss();
          }
        );
      } else {
        this.showToast("Check your internet connection!");
      }
    } else {
      this.showToast("Please fill OTP!");
    }
  }

  goTransfer() {
    // this.loading = this.loadingCtrl.create({
    //   content: "Processing...",
    //   dismissOnPageChange: true
    // });
    // this.loading.present();

    let parameter: any;

    if (this.merchantOtpObj.packagetype != 'ppv') {
      parameter = {
        sessionID: this.userdata.sessionID,
        userID: this.userdata.userID,
        fromAccount: this.merchantOtpObj.fromAcc,
        //toAccount: this.merchantOtpObj.toAcc,
        merchantCode: this.merchantOtpObj.merchantCode,
        merchantID: this.merchantOtpObj.merchantID,
        // amount: this.removeComma(this.merchantOtpObj.amount),
        // bankCharges: this.removeComma(this.merchantOtpObj.commissionAmount),
        // totalAmount: this.removeComma(this.merchantOtpObj.totalAmount),
        ccyCode: this.merchantOtpObj.ccy,
        narrative: "",
        sKey: this.passSKey,
        paidBy: this.global.appName,
        amount: this.merchantOtpObj.amount,
        bankCharges: this.merchantOtpObj.commissionAmount,
        totalAmount: this.merchantOtpObj.totalAmount,
        cardNo: this.merchantOtpObj.cardNo,
        packageName: this.merchantOtpObj.packageName,
        voucherType: this.merchantOtpObj.voucherType,
        subscriptionno: this.merchantOtpObj.subscriptionno,
        packagetype: this.merchantOtpObj.packagetype,
      };
    } else {
      parameter = {
        sessionID: this.userdata.sessionID,
        userID: this.userdata.userID,
        fromAccount: this.merchantOtpObj.fromAcc,
        //toAccount: this.merchantOtpObj.toAcc,
        merchantCode: this.merchantOtpObj.merchantCode,
        merchantID: this.merchantOtpObj.merchantID,
        // amount: this.removeComma(this.merchantOtpObj.amount),
        // bankCharges: this.removeComma(this.merchantOtpObj.commissionAmount),
        // totalAmount: this.removeComma(this.merchantOtpObj.totalAmount),
        ccyCode: this.merchantOtpObj.ccy,
        narrative: "",
        sKey: this.passSKey,
        paidBy: this.global.appName,
        amount: this.merchantOtpObj.amount,
        bankCharges: this.merchantOtpObj.commissionAmount,
        totalAmount: this.merchantOtpObj.totalAmount,
        cardNo: this.merchantOtpObj.cardNo,
        //packageName: this.merchantOtpObj.packageName,
        //voucherType: this.merchantOtpObj.voucherType,
        subscriptionno: this.merchantOtpObj.subscriptionno,
        packagetype: this.merchantOtpObj.packagetype,
        moviename: this.merchantOtpObj.moviename,
        moviecode: this.merchantOtpObj.moviecode,
        startdate: this.merchantOtpObj.startdate,
        enddate: this.merchantOtpObj.enddate,
        usage_service_catalog_identifier__id: this.merchantOtpObj.usage_service_catalog_identifier__id
      };
    }

    //reopen firebase
    /* this.firebaseAnalytics.logEvent('topupSkynet_userid_request', {
        "userid": this.userdata.userID
    }).then((res: any) => {
        console.log(res);
    }).catch((error: any) => {
        console.log(error);
    }); */

    this.http.post(this.ipaddress + '/service003/goSkynetTransfer', parameter).map(res => res.json()).subscribe(
      data1 => {
        //reopen firebase
        /* this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
          "userid": this.userdata.userID,
          "code": data1.code,
          "desc": data1.desc
        }).then((res: any) => {
          console.log(res);
        }).catch((error: any) => {
          console.log(error);
        }); */
        if (data1.code == "0000") {
          this.loading.dismiss();

          let paramSkynetSuccess: any;

          if (this.merchantOtpObj.packagetype != "ppv") {
            paramSkynetSuccess = {
              fromAcc: this.merchantOtpObj.fromAcc,
              cardNo: this.merchantOtpObj.cardNo,
              packageName: this.merchantOtpObj.packageName,
              voucherType: this.merchantOtpObj.voucherType,
              amount: this.merchantOtpObj.amount,
              commissionAmount: this.merchantOtpObj.commissionAmount,
              totalAmount: this.merchantOtpObj.totalAmount,
              ccy: this.merchantOtpObj.ccy,
              bankRefNo: data1.bankRefNumber,
              transDate: data1.transactionDate,
              merchantID: this.merchantOtpObj.merchantID,
              merchantCode: this.merchantOtpObj.merchantCode,
              packagetype: this.merchantOtpObj.packagetype,
            };
          } else {
            paramSkynetSuccess = {
              fromAcc: this.merchantOtpObj.fromAcc,
              cardNo: this.merchantOtpObj.cardNo,
              //packageName: this.merchantOtpObj.packageName,
              //voucherType: this.merchantOtpObj.voucherType,
              amount: this.merchantOtpObj.amount,
              commissionAmount: this.merchantOtpObj.commissionAmount,
              totalAmount: this.merchantOtpObj.totalAmount,
              ccy: this.merchantOtpObj.ccy,
              bankRefNo: data1.bankRefNumber,
              transDate: data1.transactionDate,
              merchantID: this.merchantOtpObj.merchantID,
              merchantCode: this.merchantOtpObj.merchantCode,
              moviename: this.merchantOtpObj.moviename,
              packagetype: this.merchantOtpObj.packagetype
            };
          }

          this.navCtrl.setRoot(SkynetSuccessPage, {
            data: paramSkynetSuccess
          });
        }
        else if (data1.code == "0016") {
          this.logoutAlert(data1.desc);
          this.loading.dismiss();
        }
        else {
          this.loading.dismiss();

          let paramSkynetError: any;

          if (this.merchantOtpObj.packagetype != "ppv") {
            paramSkynetError = {
              fromAcc: this.merchantOtpObj.fromAcc,
              cardNo: this.merchantOtpObj.cardNo,
              packageName: this.merchantOtpObj.packageName,
              voucherType: this.merchantOtpObj.voucherType,
              amount: this.merchantOtpObj.amount,
              commissionAmount: this.merchantOtpObj.commissionAmount,
              totalAmount: this.merchantOtpObj.totalAmount,
              ccy: this.merchantOtpObj.ccy,
              errordesc: data1.desc,
              merchantID: this.merchantOtpObj.merchantID,
              merchantCode: this.merchantOtpObj.merchantCode,
              packagetype: this.merchantOtpObj.packagetype
            };
          } else {
            paramSkynetError = {
              fromAcc: this.merchantOtpObj.fromAcc,
              cardNo: this.merchantOtpObj.cardNo,
              // packageName: this.merchantOtpObj.packageName,
              // voucherType: this.merchantOtpObj.voucherType,
              amount: this.merchantOtpObj.amount,
              commissionAmount: this.merchantOtpObj.commissionAmount,
              totalAmount: this.merchantOtpObj.totalAmount,
              ccy: this.merchantOtpObj.ccy,
              errordesc: data1.desc,
              merchantID: this.merchantOtpObj.merchantID,
              merchantCode: this.merchantOtpObj.merchantCode,
              moviename: this.merchantOtpObj.moviename,
              packagetype: this.merchantOtpObj.packagetype
            };
          }

          this.navCtrl.setRoot(SkynetErrorPage, {
            data: paramSkynetError
          });
        }
      },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();

        let code = error.status;
        let msg = "Can't connect right now. [" + code + "]";

        this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
          "userid": this.userdata.userID,
          "error": msg
        }).then((res: any) => {
          console.log(res);
        }).catch((error: any) => {
          console.log(error);
        });
      }
    );
  }

  goTransfer1() {
    this.loading = this.loadingCtrl.create({
        content: "Processing...",
        dismissOnPageChange: true
    });
    this.loading.present();

    let parameter: any;

    if (this.merchantOtpObj.packagetype != 'ppv') {
        parameter = {
            sessionID: this.userdata.sessionID,
            userID: this.userdata.userID,
            fromAccount: this.merchantOtpObj.fromAccount,
            //toAccount: this.merchantOtpObj.toAcc,
            merchantCode: this.merchantOtpObj.merchantCode,
            merchantID: this.merchantOtpObj.merchantID,
            // amount: this.removeComma(this.merchantOtpObj.amount),
            // bankCharges: this.removeComma(this.merchantOtpObj.commissionAmount),
            // totalAmount: this.removeComma(this.merchantOtpObj.totalAmount),
            ccyCode: this.merchantOtpObj.ccyCode,
            narrative: "",
            sKey: this.passSKey,
            paidBy: this.global.appName,
            amount: this.merchantOtpObj.amount,
            bankCharges: this.merchantOtpObj.bankCharges,
            totalAmount: this.merchantOtpObj.totalAmount,
            cardNo: this.merchantOtpObj.cardNo,
            packageName: this.merchantOtpObj.packageName,
            voucherType: this.merchantOtpObj.voucherType,
            subscriptionno: this.merchantOtpObj.subscriptionno,
            packagetype: this.merchantOtpObj.packagetype,
        };
    } else {
        parameter = {
            sessionID: this.userdata.sessionID,
            userID: this.userdata.userID,
            fromAccount: this.merchantOtpObj.fromAccount,
            //toAccount: this.merchantOtpObj.toAcc,
            merchantCode: this.merchantOtpObj.merchantCode,
            merchantID: this.merchantOtpObj.merchantID,
            // amount: this.removeComma(this.merchantOtpObj.amount),
            // bankCharges: this.removeComma(this.merchantOtpObj.commissionAmount),
            // totalAmount: this.removeComma(this.merchantOtpObj.totalAmount),
            ccyCode: this.merchantOtpObj.ccyCode,
            narrative: "",
            sKey: this.passSKey,
            paidBy: this.global.appName,
            amount: this.merchantOtpObj.amount,
            bankCharges: this.merchantOtpObj.bankCharges,
            totalAmount: this.merchantOtpObj.totalAmount,
            cardNo: this.merchantOtpObj.cardNo,
            //packageName: this.merchantOtpObj.packageName,
            //voucherType: this.merchantOtpObj.voucherType,
            subscriptionno: this.merchantOtpObj.subscriptionno,
            packagetype: this.merchantOtpObj.packagetype,
            moviename: this.merchantOtpObj.moviename,
            moviecode: this.merchantOtpObj.moviecode,
            startdate: this.merchantOtpObj.startdate,
            enddate: this.merchantOtpObj.enddate,
            usage_service_catalog_identifier__id: this.merchantOtpObj.usage_service_catalog_identifier__id
        };
    }

    //reopen firebase
    /* this.firebaseAnalytics.logEvent('topupSkynet_userid_request', {
        "userid": this.userdata.userID
    }).then((res: any) => {
        console.log(res);
    }).catch((error: any) => {
        console.log(error);
    }); */

    this.http.post(this.ipaddress + '/service003/goSkynetTransfer', parameter).map(res => res.json()).subscribe(
        data1 => {
            //reopen firebase
            /* this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
              "userid": this.userdata.userID,
              "code": data1.code,
              "desc": data1.desc
            }).then((res: any) => {
              console.log(res);
            }).catch((error: any) => {
              console.log(error);
            }); */

            if (data1.code == "0000") {
                this.loading.dismiss();

                let paramSkynetSuccess: any;

                if (this.merchantOtpObj.packagetype != "ppv") {
                    paramSkynetSuccess = {
                        fromAcc: this.merchantOtpObj.fromAccount,
                        cardNo: this.merchantOtpObj.cardNo,
                        packageName: this.merchantOtpObj.packageName,
                        voucherType: this.merchantOtpObj.voucherType,
                        amount: this.merchantOtpObj.amount,
                        commissionAmount: this.merchantOtpObj.bankCharges,
                        totalAmount: this.merchantOtpObj.totalAmount,
                        ccy: this.merchantOtpObj.ccyCode,
                        bankRefNo: data1.bankRefNumber,
                        transDate: data1.transactionDate,
                        merchantID: this.merchantOtpObj.merchantID,
                        merchantCode: this.merchantOtpObj.merchantCode,
                        packagetype: this.merchantOtpObj.packagetype,
                    };
                } else {
                    paramSkynetSuccess = {
                        fromAcc: this.merchantOtpObj.fromAccount,
                        cardNo: this.merchantOtpObj.cardNo,
                        //packageName: this.merchantOtpObj.packageName,
                        //voucherType: this.merchantOtpObj.voucherType,
                        amount: this.merchantOtpObj.amount,
                        commissionAmount: this.merchantOtpObj.bankCharges,
                        totalAmount: this.merchantOtpObj.totalAmount,
                        ccy: this.merchantOtpObj.ccyCode,
                        bankRefNo: data1.bankRefNumber,
                        transDate: data1.transactionDate,
                        merchantID: this.merchantOtpObj.merchantID,
                        merchantCode: this.merchantOtpObj.merchantCode,
                        moviename: this.merchantOtpObj.moviename,
                        packagetype: this.merchantOtpObj.packagetype
                    };
                }

                this.navCtrl.setRoot(SkynetSuccessPage, {
                    data: paramSkynetSuccess
                });
            }
            else if (data1.code == "0016") {
                this.logoutAlert(data1.desc);
                this.loading.dismiss();
            }
            else {
                this.loading.dismiss();

                let paramSkynetError: any;

                if (this.merchantOtpObj.packagetype != "ppv") {
                    paramSkynetError = {
                        fromAcc: this.merchantOtpObj.fromAcc,
                        cardNo: this.merchantOtpObj.cardNo,
                        packageName: this.merchantOtpObj.packageName,
                        voucherType: this.merchantOtpObj.voucherType,
                        amount: this.merchantOtpObj.amount,
                        commissionAmount: this.merchantOtpObj.commissionAmount,
                        totalAmount: this.merchantOtpObj.totalAmount,
                        ccy: this.merchantOtpObj.ccy,
                        errordesc: data1.desc,
                        merchantID: this.merchantOtpObj.merchantID,
                        merchantCode: this.merchantOtpObj.merchantCode,
                        packagetype: this.merchantOtpObj.packagetype
                    };
                } else {
                    paramSkynetError = {
                        fromAcc: this.merchantOtpObj.fromAcc,
                        cardNo: this.merchantOtpObj.cardNo,
                        // packageName: this.merchantOtpObj.packageName,
                        // voucherType: this.merchantOtpObj.voucherType,
                        amount: this.merchantOtpObj.amount,
                        commissionAmount: this.merchantOtpObj.commissionAmount,
                        totalAmount: this.merchantOtpObj.totalAmount,
                        ccy: this.merchantOtpObj.ccy,
                        errordesc: data1.desc,
                        merchantID: this.merchantOtpObj.merchantID,
                        merchantCode: this.merchantOtpObj.merchantCode,
                        moviename: this.merchantOtpObj.moviename,
                        packagetype: this.merchantOtpObj.packagetype
                    };
                }

                this.navCtrl.setRoot(SkynetErrorPage, {
                    data: paramSkynetError
                });
            }
        },
        error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.loading.dismiss();

            let code = error.status;
            let msg = "Can't connect right now. [" + code + "]";

            this.firebaseAnalytics.logEvent('topupSkynet_userid_response', {
                "userid": this.userdata.userID,
                "error": msg
            }).then((res: any) => {
                console.log(res);
            }).catch((error: any) => {
                console.log(error);
            });
        }
    );
}

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [{
        text: 'OK',
        handler: () => {
          this.storage.remove('userData');
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.navCtrl.setRoot(Login, {});
          this.navCtrl.popToRoot();
        }
      }],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }

  getOtp() {
    this.checkNetwork();

    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();

      let param = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        type: 1,
        merchantID: this.merchantOtpObj.merchantID,
        sKey: this.passSKey
      };

      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(
        data => {
          this.loading.dismiss();

          if (data.code == "0000") {
            this.merchantOtpObj.rKey = data.rKey;
          }
          else if (data.code == "0016") {
            this.logoutAlert(data.desc);
          }
          else {
            this.all.showAlert('Warning!', data.desc);
          }
        },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        }
      );
    } else {
      this.showToast("Check your internet connection!");
    }
  }

  showToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
  }

}
