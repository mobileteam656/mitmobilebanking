import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DomainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-domain',
  templateUrl: 'domain.html',
})
export class DomainPage {
  domain  :any = "";
  key : any = "";
  errormessagetext : any = '';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DomainPage');
  }
  changeaddress(){
    if(this.domain == ''){
      this.errormessagetext = "Invalid! Please try again."
    }
    else if(this.key ==''){
      this.errormessagetext = "Invalid! Please try again."
    }
    else {
      this.errormessagetext = '';
    }
  }

}
