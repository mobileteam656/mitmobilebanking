import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { BeneficiaryListPage } from '../beneficiary-list/beneficiary-list';
/**
 * Generated class for the BeneficiaryDeleteSuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-beneficiary-delete-success',
  templateUrl: 'beneficiary-delete-success.html',
})
export class BeneficiaryDeleteSuccessPage {
  textMyan: any = ['ရလဒ်', "လုပ်ဆောင်မှု အောင်မြင်ပါသည်", 'ပိတ်မည်'];
  textEng: any = ["Result", "Transaction Success", "Close"];
  showFont: string[] = [];
  font: string;
  passtemp1: any;
  passtemp2: any;
  passtemp3: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public changeLanguage: ChangelanguageProvider) {
    this.passtemp1 = this.navParams.get('data');
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  getOK() {
   
      this.navCtrl.setRoot(BeneficiaryListPage)
    

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad BeneficiaryDeleteSuccessPage');
  }

}
