import { Component } from '@angular/core';
import {  NavController, NavParams ,Events,PopoverController,AlertController} from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Storage } from '@ionic/storage';
import {DefaultPopOver} from '../default-pop-over/default-pop-over';
import {LanguagePopOver} from '../language-pop-over/language-pop-over';
import { IpchangePage } from '../ipchange/ipchange';
import { CloudPage } from '../cloud/cloud';
declare var window;

/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  providers : [ChangelanguageProvider]
})
export class AboutPage {
  popover:any;
  textMyan: any = ["အကြောင်းအရာ"];
  textEng: any = ["About"];
  showFont:any=[];
  font:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,private storage : Storage,
              public changeLanguage:ChangelanguageProvider,public popoverCtrl:PopoverController,public events: Events,public global:GlobalProvider,public alertCtrl:AlertController) {
  }

    ionViewDidLoad() {
      this.storage.get('language').then((font) => {
        this.font = font;
        this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
          this.showFont = data;
        });
      });
      this.events.subscribe('changelanguage', lan => {
        this.font = lan.data;
        this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
          this.showFont = data;
        });
      });
    // console.log('ionViewDidLoad AboutPage');
  }

  gotoshwewebsite(){
    if (this.platform.is('android')) {
      window.open('http://www.mit.com.mm', '_system');
    }
    else  if (this.platform.is('ios')) {
      window.open('http://www.mit.com.mm'); // or itms://
    }
    if (this.platform.is('mobileweb')) {
      // console.log("running in a browser on mobile!");
    }
  }

  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(DefaultPopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if(data == "0"){
        //  this.navCtrl.push(IpchangePage);
        this.presentPopoverCloud(ev);
      }
      else if(data == "1"){
        this.presentPopoverLanguage(ev);
      }
      else if(data == "2"){
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
        /* let confirm = this.alertCtrl.create({
          title: "mBanking 360",
          message: "Version "+this.global.version,
          enableBackdropDismiss : false,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                //  window.open(myConst.bankingApp.android.storeUrl, '_system');
              }
            }
          ]
        });
        confirm.present(); */
      }

    });
  }
  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }
  presentPopoverCloud(ev) {

    this.popover = this.popoverCtrl.create(CloudPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if(data == "0"){
        this.navCtrl.push(IpchangePage);
      }
      else if(data == "1"){
        this.presentPopoverLanguage(ev);
      }      


    });
  }

  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if(data != null){
        this.storage.set('language',data);
        let temp = {data:'',status:0};
        temp.data = data;
        temp.status = 0;
        this.events.publish('changelanguage', temp);
      }


    });
  }

}
