import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuickpayConfirmPage } from '../quickpay-confirm/quickpay-confirm';
import { CalculatorDetailPage } from '../calculator-detail/calculator-detail';
import { MainHomePage } from '../main-home/main-home';

@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})
export class CalculatorPage {

  textMyan: any = ["ဂဏန်းတွက်စက်"];
  textEng: any = ["Interest Calculator"];
  showFont: string[] = [];
  //billerType = ["Savings Interest Calculator", "Call Deposit Interest Calculator", "Special Calculator"];
  billerType = ["Savings Interest Calculator", "Fixed Deposit Account Calculator"];
  billerid = "";
  errormsg = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public events: Events, public storage: Storage,
  ) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad QuickpayPage');
  }

  goPay(type) {
    this.navCtrl.push(CalculatorDetailPage, {
      billerid: type
    });
  }

  goCancel() {
    this.billerid = "";
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }

}

