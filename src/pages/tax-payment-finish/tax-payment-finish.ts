import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { TaxInfoPage } from '../tax-info/tax-info';
import { UtilProvider } from '../../providers/util/util';
@Component({
  selector: 'page-tax-payment-finish',
  templateUrl: 'tax-payment-finish.html',
})
export class TaxPaymentFinishPage {

  textMyan: any = ["အသေးစိတ်အချက်အလက်", "အကောင့်နံပါတ်", "အခွန်နံပါတ်", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အမှတ်စဉ်", "ဘဏ် အခွန် အမှတ်စဉ် နံပါတ်", "လုပ်ဆောင်ခဲ့သည့်ရက်", "ပိတ်မည်","လုပ်ဆောင်မှုအတည်ပြုခြင်း","လုပ်ဆောင်မှုအတည်ပြုခြင်း"];
  textEng: any = ["Details", "Account Number", "TIN No.", "Phone Number", "Amount", "Bank Reference Number", "Bank Tax Reference Number", "Transaction Date", "CLOSE","Tax Payment Success","Tax Payment Fail"];
  data: any;
  detail: any;
  showFont: any = [];
  font: string;
  cssStyle: any;
  title: any = "";
  amount: any;
  constructor(public navCtrl: NavController, public all: AllserviceProvider,public util:UtilProvider, public navParams: NavParams, public storage: Storage, public changeLanguage: ChangelanguageProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public http: Http) {
    this.data = this.navParams.get('data');
    this.detail = this.navParams.get('detail');
    this.amount=this.util.formatAmount(this.detail.taxAmount)+"  "+"MMK";
    if (this.data.code == '0000') {
      this.cssStyle = { 'background-color': 'lightgreen', 'padding': '20px', 'white-space': 'inherit' };
    } else {
      this.cssStyle = { 'background-color': '#c13f1a', 'padding': '20px', 'white-space': 'inherit' };
    }
  }

  ionViewDidLoad() {
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
        if (this.data.code == '0000') {
          this.title = this.showFont[9];
        } else {
          this.title = this.showFont[10];
        }
      });
    });
  }

  close() {
    this.navCtrl.setRoot(TaxInfoPage);
  }

}
