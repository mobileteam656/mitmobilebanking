import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Contacts } from '@ionic-native/contacts';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Login } from '../login/login';
import { TopupconfirmPage } from '../topupconfirm/topupconfirm';

@Component({
    selector: 'page-top-up-form',
    templateUrl: 'top-up-form.html',
    providers: [ChangelanguageProvider]
})
export class TopUpForm {
    passtemp: any;
    textEng: any = ["Account", "Operator Type", "Phone Number.", "Amount", "Narrative", "SUBMIT", "Choose Account No.", "Choose Operator Type", "Enter Phone No.", "Choose Amount", "Enter Amount", "Account Balance"];
    textMyan: any = ["အကောင့်နံပါတ်", "အော်ပရေတာ အမျိုးအစား", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ထည့်သွင်းမည်", "စာရင်းအမှတ်ရွေးချယ်ပါ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "ငွေပမာဏရွေးချယ်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "ဘဏ်စာရင်းလက်ကျန်ငွေ"];
    showFont: any = [];
    font: any = '';
    cnpData: any = {};
    userData: any;
    useraccount: any = [];
    ipaddress: any;
    referenceType: any = [];
    referenceTypeBill: any = [];
    amountType: any;
    msgCnpAcc: string;
    msgCnpAmtOther: string;
    msgCnpPhNo: string;
    msgCnpRef: string;
    msgCnpamt: string;
    referencename: any;
    amt: any;
    moreContact: any = false;
    moreContactData: any = [];
    contactPh: any = true;
    primColor: any = { 'background-color': '#3B5998' };
    constructor(public navCtrl: NavController, public navParams: NavParams, public barcodeScanner: BarcodeScanner,
        public storage: Storage, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
        public events: Events, public network: Network, public all: AllserviceProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
        private contacts: Contacts, private global: GlobalProvider) {
        this.passtemp = this.navParams.get('data');
        if (this.passtemp.processingCode == '050200') {
            this.referenceTypeBill = [{ name: '1000', value: '1000' }, { name: '3000', value: '3000' }, { name: '5000', value: '5000' }, { name: '10000', value: '10000' }, { name: 'Other', value: 'other' }];
        }
        if (this.passtemp.colorCode == '') {
            this.primColor = { 'background-color': '#3B5998' };
        } else {
            this.primColor = { 'background-color': this.passtemp.colorCode };
        }
        this.storage.get('userData').then((result) => {
            this.userData = result;
            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                    result = this.global.ipaddress;
                }
                else {
                    this.ipaddress = result;
                    this.getreferenceData();
                }
                this.getAccountSummary();
            });

        });
        this.storage.get('language').then((font) => {
            this.font = font;
            this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

                this.showFont = data;

            });
        });

    }

    getContact() {
        this.contacts.pickContact().then((data) => {
            if (data.phoneNumbers.length > 1) {
                this.contactPh = false;
                this.moreContact = true;
                this.moreContactData = data.phoneNumbers;
                for (let i = 0; i < this.moreContactData.length; i++) {
                    this.moreContactData[i].value = this.moreContactData[i].value.toString().replace(/ +/g, "");
                    if (this.moreContactData[i].value.indexOf("7") == 0 && this.moreContactData[i].value.length == "9") {
                        this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
                    }
                    else if (this.moreContactData[i].value.indexOf("9") == 0 && this.moreContactData[i].value.length == "9") {
                        this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
                    }
                    else if (this.moreContactData[i].value.indexOf("+") != 0 && this.moreContactData[i].value.indexOf("7") != 0 && this.moreContactData[i].value.indexOf("9") != 0 && (this.moreContactData[i].value.length == "8" || this.moreContactData[i].value.length == "9" || this.moreContactData[i].value.length == "7")) {
                        this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
                    }
                    else if (this.moreContactData[i].value.indexOf("09") == 0 && (this.moreContactData[i].value.length == 10 || this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 9)) {
                        this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(2);
                    }
                    else if (this.moreContactData[i].value.indexOf("959") == 0 && (this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 12 || this.moreContactData[i].value.length == 10)) {
                        this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(3);
                    }
                }

            } else {
                this.contactPh = true;
                this.moreContact = false;
                this.cnpData.phoneNo = data.phoneNumbers[0].value;
            }
        });
    }

    ionViewDidLoad() {
    }

    referenceChange(data) {
        if (this.passtemp.processingCode == '040300') {
            this.referenceTypeBill = data.dataList;
        }
    }

    getamountType(data) {
        if (data == 'other') {
            this.amountType = data;
        } else {
            this.amountType = '';
            this.msgCnpAmtOther = '';
            this.cnpData.amountother = '';
        }
    }

    getamount(data) {
        this.cnpData.amountType = data.value;
        this.cnpData.amount = data.name;
    }

    getreferenceData() {
        this.slimLoader.start(() => {

        });
        /*this.loading = this.loadingCtrl.create({
         content: "Please wait...",
         dismissOnPageChange :true
         //   duration: 3000
         });
         this.loading.present();*/
        let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, processingCode: this.passtemp.processingCode };

        this.http.post(this.ipaddress + '/service001/getReferenceData', param).map(res => res.json()).subscribe(data => {

            if (data.code == "0000") {
                let tempArray = [];
                if (!Array.isArray(data.dataList)) {
                    tempArray.push(data.dataList);
                    data.dataList = tempArray;
                }

                for (let i = 0; i < data.dataList.length; i++) {
                    if (this.passtemp.processingCode == '050200') {
                        if (data.dataList[i].value == 'TOPUP') {
                            if (!Array.isArray(data.dataList[i].dataList)) {
                                tempArray.push(data.dataList[i].dataList);
                                data.dataList[i].dataList = tempArray;
                            }
                            this.referenceType = data.dataList[i].dataList;
                        }

                    }
                    else {
                        this.referenceType.push(data.dataList[i]);
                    }
                }

                this.slimLoader.complete();
            }
            else if (data.code == "0016") {
                this.logoutAlert(data.desc);
                this.slimLoader.complete();
            }
            else {
                this.all.showAlert('Warning!', data.desc);
                this.slimLoader.complete();

            }

        },
            error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.slimLoader.complete();
            });
    }
    clicksubmit() {
        let flag1;
        let flag2;
        let flag3;
        let flag4;
        let flag5;
        let num = /^[0-9-\+]*$/;
        if (this.cnpData.accounttype == '' || this.cnpData.accounttype == undefined) {
            flag1 = false;
            this.msgCnpAcc = this.showFont[6];
        } else {
            flag1 = true;
            this.msgCnpAcc = '';
        }
        if (this.referencename == '' || this.referencename == undefined) {
            flag2 = false;
            this.msgCnpRef = this.showFont[7];
        } else {
            flag2 = true;
            this.msgCnpRef = '';
        }
        if (this.cnpData.phoneNo == '' || this.cnpData.phoneNo == undefined) {
            flag3 = false;
            this.msgCnpPhNo = this.showFont[8];
        } else {
            this.cnpData.phoneNo = this.cnpData.phoneNo.toString().replace(/ +/g, "");
            if (num.test(this.cnpData.phoneNo)) {
                if (this.cnpData.phoneNo.indexOf("+") == 0 && (this.cnpData.phoneNo.length == "12" || this.cnpData.phoneNo.length == "13" || this.cnpData.phoneNo.length == "11")) {
                    flag3 = true;
                    this.msgCnpPhNo = '';
                }
                else if (this.cnpData.phoneNo.indexOf("7") == 0 && this.cnpData.phoneNo.length == "9") {
                    this.cnpData.phoneNo = '+959' + this.cnpData.phoneNo;
                    flag3 = true;
                    this.msgCnpPhNo = '';
                }
                else if (this.cnpData.phoneNo.indexOf("9") == 0 && this.cnpData.phoneNo.length == "9") {
                    this.cnpData.phoneNo = '+959' + this.cnpData.phoneNo;
                    flag3 = true;
                    this.msgCnpPhNo = '';
                }
                else if (this.cnpData.phoneNo.indexOf("+") != 0 && this.cnpData.phoneNo.indexOf("7") != 0 && this.cnpData.phoneNo.indexOf("9") != 0 && (this.cnpData.phoneNo.length == "8" || this.cnpData.phoneNo.length == "9" || this.cnpData.phoneNo.length == "7")) {
                    this.cnpData.phoneNo = '+959' + this.cnpData.phoneNo;
                    flag3 = true;
                    this.msgCnpPhNo = '';
                }
                else if (this.cnpData.phoneNo.indexOf("09") == 0 && (this.cnpData.phoneNo.length == 10 || this.cnpData.phoneNo.length == 11 || this.cnpData.phoneNo.length == 9)) {
                    this.msgCnpPhNo = '';
                    this.cnpData.phoneNo = '+959' + this.cnpData.phoneNo.substring(2);
                    flag3 = true;
                }
                else if (this.cnpData.phoneNo.indexOf("959") == 0 && (this.cnpData.phoneNo.length == 11 || this.cnpData.phoneNo.length == 12 || this.cnpData.phoneNo.length == 10)) {
                    this.msgCnpPhNo = '';
                    this.cnpData.phoneNo = '+959' + this.cnpData.phoneNo.substring(3);
                    flag3 = true;
                }
                else {
                    flag3 = false;
                    this.msgCnpPhNo = 'Invalid phone number.';
                }
            }
            else {
                flag3 = false;
                this.msgCnpPhNo = this.showFont[8];
            }
        }
        if (this.cnpData.amount == '' || this.cnpData.amount == undefined) {
            if (this.passtemp.processingCode == '050200') {
                if (this.cnpData.name == '' || this.cnpData.name == undefined) {
                    flag4 = false;
                    this.msgCnpamt = this.showFont[9];
                }
                else {
                    if (this.amountType == 'other') {
                        this.cnpData['amount'] = this.cnpData.amountother;
                    } else {
                        this.cnpData['amount'] = this.cnpData.name;
                    }
                    flag4 = true;
                    this.msgCnpamt = '';
                }
            }
            else {
                flag4 = false;
                this.msgCnpamt = this.showFont[9];
            }
        } else {
            flag4 = true;
            this.msgCnpamt = '';
        }

        if (this.amountType == 'other') {
            if (this.cnpData.amountother == '' || this.cnpData.amountother == undefined) {
                flag5 = false;
                this.msgCnpAmtOther = this.showFont[10];
            } else {
                flag5 = true;
                this.msgCnpAmtOther = '';
            }
        } else {
            flag5 = true;
            this.msgCnpAmtOther = '';
        }


        if (flag1 && flag2 && flag3 && flag4 && flag5) {
            this.cnpData['operator'] = this.referencename['name'];
            this.cnpData['value'] = this.referencename['value'];
            this.cnpData['bankCharges'] = this.referencename['bankCharges'];
            this.cnpData['merchantCharges'] = this.referencename['merchantCharges'];
            if (this.passtemp.processingCode == '050200') {
                if (this.amountType == 'other') {
                    this.cnpData['amount'] = this.cnpData.amountother;
                } else {
                    this.cnpData['amount'] = this.cnpData.name;
                }
            }
            if (this.cnpData.narrative == '' || this.cnpData.narrative == undefined)
                this.cnpData.narrative = "";

            this.getOtpConfirm();
            // this.navCtrl.push(TopupconfirmPage,{data:this.cnpData,detail:this.passtemp});
        }
    }
    getOtpConfirm() {
        let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '2', merchantID: this.passtemp.merchantID };

        this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {

            if (data.code == "0000") {
                this.slimLoader.complete();
                this.navCtrl.push(TopupconfirmPage, {
                    data: this.cnpData,
                    detail: this.passtemp,
                    otp: data.rKey,
                    sKey: data.sKey
                });
            }
            else if (data.code == "0016") {
                this.logoutAlert(data.desc);
                this.slimLoader.complete();
            }
            else {
                this.all.showAlert('Warning!', data.desc);
                this.slimLoader.complete();
            }

        },
            error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.slimLoader.complete();
            });
    }
    onChangeSPh(i) {
        if (i == "09") {
            this.cnpData.phoneNo = "+959";
        }
        else if (i == "959") {
            this.cnpData.phoneNo = "+959";
        }
    }

    getAccountSummary() {
        this.slimLoader.start(() => {
        });
        let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
        this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
            if (result.code == "0000") {
                let tempArray = [];
                if (!Array.isArray(result.dataList)) {
                    tempArray.push(result.dataList);
                    result.dataList = tempArray;
                }
                this.useraccount = result.dataList;
                //  this.loading.dismiss();
                this.slimLoader.complete();
            }
            else if (result.code == "0016") {
                this.logoutAlert(result.desc);
                this.slimLoader.complete();
            }
            else {
                let toast = this.toastCtrl.create({
                    message: result.desc,
                    duration: 3000,
                    position: 'bottom',
                    //  showCloseButton: true,
                    dismissOnPageChange: true,
                    // closeButtonText: 'OK'
                });
                toast.present(toast);
                // this.loading.dismiss();
                this.slimLoader.complete();
            }
        },
            error => {
                let toast = this.toastCtrl.create({
                    message: this.all.getErrorMessage(error),
                    duration: 3000,
                    position: 'bottom',
                    dismissOnPageChange: true,
                });
                toast.present(toast);
                this.slimLoader.complete();
            });
    }

    logoutAlert(message) {
        let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.storage.remove('userData');
                        this.events.publish('login_success', false);
                        this.events.publish('lastTimeLoginSuccess', '');
                        this.navCtrl.setRoot(Login, {
                        });
                        this.navCtrl.popToRoot();
                    }
                }
            ],
            cssClass: 'warningAlert',
        })
        confirm.present();
    }
}
