import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';

@Component({
  selector: 'page-changepasswordotp',
  templateUrl: 'changepasswordotp.html',
  providers: [ChangelanguageProvider]
})
export class ChangepasswordotpPage {
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData = { userID: '', sessionID: '' };
  passtemp1: any;
  passtemp2: any;
  ipaddress: string;
  data: any;
  errormessage: string;
  loading;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  font: string;
  passSKey: any;
  textEng: any = ['Bank', 'Confirm', 'Confirmation code has been sent', 'Please wait.', 'Resend', 'Enter code number', 'Please log in with your new password.', 'OK'];
  textMyan: any = ['ဘဏ်', 'အတည်ပြုသည်', 'လူကြီးမင်းဖုန်းသို့အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ', 'ခေတ္တစောင့်ဆိုင်းပေးပါ', 'နံပါတ်ပြန်လည်ရယူမည်', 'အတည်ပြုနံပါတ်ရိုက်သွင်းပါ', 'Please log in with your new password.', 'OK'];
  showFont: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public storage: Storage, public idle: Idle, public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.passSKey = this.navParams.get('sKey');
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
      this.loading.dismiss();
    }) */
  }

  ionViewDidLeave() {
  }

  ionViewDidLoad() {
    this.data = this.navParams.get('data');
    this.storage.get('userData').then((val) => {
      this.userData.userID = val.userID;
      this.userData.sessionID = val.sessionID;
      // // this.idleWatch();
      this.storage.get('ipaddress').then((ipaddress) => {
        this.ipaddress = ipaddress;
      });
    });
  }

  inputChange(data) {
    this.errormessage = "";
  }

  getconfirm() {
    if (this.otpcode == '' || this.otpcode == undefined) {
      this.errormessage = this.showFont[5];
    } else {
      this.errormessage = "";
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = {
        userID: this.userData.userID,
        sessionID: this.userData.sessionID,
        //   phoneno: this.data.phoneno,
        otpCode: this.otpcode,
        //   password: this.data.oldpsw,
        //      newpassword: this.data.confirmpsw,
        //     confirmnewpassword: this.data.confirmpsw,
        rKey: this.data.rKey,
        sKey: this.passSKey
      };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.changePassword();
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }

  sendOtpCode() {
    /* this.loading = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange :true
       //   duration: 3000
     });
     this.loading.present();*/
    /* this.slimLoader.start(() => {
    }); */
    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      type: '',
      sKey: this.passSKey
    };
    this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.data.rKey = data.rKey;
        this.loading.dismiss();
        /* window.SMS.startWatch(function () {
        }, function () {
        });
        setTimeout(() => {
        }, 8000);
        window.document.addEventListener('onSMSArrive', res => {
          this.smsArived = res;
          
          let arr = this.smsArived.data.body.split(' ');
          this.otpcode = arr[3];
         //this.slimLoader.complete();
          //  this.loading.dismiss();
        }) */
      }
      else if (data.code == "0016") {
        this.logoutAlert(data.desc);
        //this.slimLoader.complete();
        // this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        //  this.loading.dismiss();
      }

    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          showCloseButton: true,
          dismissOnPageChange: true,
          closeButtonText: 'OK'
        });
        toast.present(toast);
        //this.slimLoader.complete();
        // this.loading.dismiss();
      });
  }


  changePassword() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
    let param = {
      "userID": this.userData.userID,
      "sessionID": this.userData.sessionID,
      "rKey": this.data.rKey,
      "otpCode": this.otpcode,
      "oldPassword": this.all.getEncryptText(iv, salt, dm, this.data.oldpsw),
      "newPassword": this.all.getEncryptText(iv, salt, dm, this.data.confirmpsw),
      "sKey": this.passSKey,
      "iv": iv, "dm": dm, "salt": salt
    };
    this.http.post(this.ipaddress + '/service001/changePassword', param).map(res => res.json()).subscribe(response => {
      if (response.code == "0000") {
        let confirm = this.alertCtrl.create({
          title: 'Your password has been changed',
          message: this.showFont[6], // Your password has been reset
          buttons: [
            {
              text: this.showFont[7],
              handler: () => {
                this.gologout();
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else if (response.code == "0016") {
        this.logoutAlert(response.desc);
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: response.desc,
          position: 'bottom',
          showCloseButton: true,
          dismissOnPageChange: true,
          closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChangepasswordotpPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChangepasswordotpPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
