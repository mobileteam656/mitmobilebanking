import 'rxjs/add/operator/map';

import {
  AlertController, Events, LoadingController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { Login } from '../login/login';
import { MptTopupFinishPage } from '../mpt-topup-finish/mpt-topup-finish';
import { MptTopupOtpPage } from '../mpt-topup-otp/mpt-topup-otp';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-mpt-topup-confirm',
  templateUrl: 'mpt-topup-confirm.html',
})
export class MptTopupConfirmPage {
  userData: any;
  ipaddress: string;
  loading: any;
  flag: string;
  textMyan: any = ["အကောင့်နံပါတ်", "Operator အမျိုးအစား", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "အတည်ပြုမည်", "အတည်ပြုခြင်း"];
  textEng: any = ["Account Number", "Operator Type", "Phone Number", "Amount", "Narrative", "CANCEL", "CONFIRM", "Confirmation"];
  topUpData: any;
  showFont: string[] = [];
  font: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  isOtp: any;
  sKey: any;
  amount: any;
  fromPage: any;
  alertPresented: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public util: UtilProvider, public all: AllserviceProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    private firebaseAnalytics: FirebaseAnalytics) {
    this.alertPresented = false;
    this.topUpData = this.navParams.get('data');
    this.isOtp = this.navParams.get("otp");
    this.sKey = this.navParams.get('sKey');
    this.fromPage = this.navParams.get('fromPage');
    this.amount = this.util.formatAmount(this.topUpData.amount) + "  " + "MMK";
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  confirm() {
    if (this.isOtp == 'true') {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '2', merchantID: this.topUpData.merchantID, sKey: this.sKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.push(MptTopupOtpPage, {
            data: this.topUpData,
            sKey: this.sKey,
            rKey: data.rKey,
            fromPage: this.fromPage
          })
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
    else {
      this.goTopUp();
    }
  }

  goTopUp() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let phoneNo = this.topUpData.phone.replace(/\+/g, '');
    let param = {
      "userID": this.userData.userID,
      "sessionID": this.userData.sessionID,
      "bankAcc": this.topUpData.account,
      "merchantID": this.topUpData.merchantID,
      "processingCode": this.topUpData.processingCode,
      "amount": this.topUpData.amount,
      "refNumber": phoneNo,
      "field1": "",
      "field2": "2",
      "field3": "",
      "field4": "",
      "field5": "",
      "field6": "",
      "field7": "",
      "field8": "",
      "field9": "",
      "field10": "",
      "amountServiceCharges": "",
      "amountTax": "",
      "amountTotal": this.topUpData.amount,
      "paymentType": "2",
      "narrative": this.topUpData.narrative,
      "sKey": this.sKey,
      "amountType": this.topUpData.amountType
    }
    this.firebaseAnalytics.logEvent('topupMPT_userid_request', {
      "userid": this.userData.userID
    }).then((res: any) => {
      console.log(res);
    }).catch((error: any) => {
      console.log(error);
    });
    this.http.post(this.ipaddress + '/service003/goMptTopup', param).map(res => res.json()).subscribe(
      res => {
        this.firebaseAnalytics.logEvent('topupMPT_userid_response', {
          "userid": this.userData.userID,
          "code": res.code,
          "desc": res.desc
        }).then((res: any) => {
          console.log(res);
        }).catch((error: any) => {
          console.log(error);
        });

        this.loading.dismiss();
        if (res.code == "0000") {
          /*  this.topUpData['processingCode'] = this.passTemp.processingCode;
           this.topUpData['colorCode'] = this.passTemp.colorCode; */
          this.topUpData['bankRefNumber'] = res.bankRefNumber;
          this.navCtrl.setRoot(MptTopupFinishPage, {
            data: res,
            detail: this.topUpData,
            fromPage: this.fromPage
          })
        }
        else if (res.code == "0016") {
          this.logoutAlert(res.desc);
        }
        else {
          this.navCtrl.setRoot(MptTopupFinishPage, {
            data: res,
            detail: this.topUpData,
            fromPage: this.fromPage
          });
        }
      },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();

        let code = error.status;
        let msg = "Can't connect right now. [" + code + "]";

        this.firebaseAnalytics.logEvent('topupMPT_userid_response', {
          "userid": this.userData.userID,
          "error": msg
        }).then((res: any) => {
          console.log(res);
        }).catch((error: any) => {
          console.log(error);
        });
      }
    );
  }

  cancel() {
    this.navCtrl.pop();
  }

  logoutAlert(message) {
    let vm = this
    if (!vm.alertPresented) {
      vm.alertPresented = true
      vm.alertCtrl.create({
        title: 'Warning!',
        message: message,
        buttons: [{
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
            vm.alertPresented = false
          }
        }],
        cssClass: 'warningAlert',
        enableBackdropDismiss: false
      }).present();
    }
  }
}
