import { Component } from '@angular/core';
import {  NavController, NavParams, Platform, Events } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ProductImgViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-product-img-view',
  templateUrl: 'product-img-view.html',
})
export class ProductImgViewPage {
  textEng:any=["Product Image"];
  textMyan:any=["ကုန်ပစ္စည်း ဓာတ်ပုံ"];
  font: String;
  showFont: any=[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform, private storage: Storage,public changeLanguage: ChangelanguageProvider,public events: Events,public global: GlobalProvider) {
    
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
    
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ProductImgViewPage');
  }

}
