import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, NavController, NavParams } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';

/**
 * Generated class for the DetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
    providers : [ChangelanguageProvider]

})
export class DetailPage {
  From:any;
  To:any;
  Buy:any;
  Sell:any;
  font:string;
  textEng: any=['From Currency','To Currency','Buy Rate','Sell Rate','Details'];
  textMyan : any = ['ဤငွေကြေးပမာဏမှ','ဤငွေကြေးပမာဏသို့','ဝယ်ဈေး','ရောင်းဈေး','အသေးစိတ်အချက်အလက်'];
  showFont:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public changeLanguage:ChangelanguageProvider,public events:Events) {

    this.From = navParams.get('From');
    this.To = navParams.get('To');
    this.Buy = navParams.get('Buy');
    this.Sell = navParams.get('Sell');
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  ionViewDidLoad() {
    
  }

}
