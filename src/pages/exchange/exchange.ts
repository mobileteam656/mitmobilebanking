import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import {
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
  AlertController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { DefaultPopOver } from '../default-pop-over/default-pop-over';
import { DetailPage } from '../detail/detail';
import { Global } from '../global/global';
import { IpchangePage } from '../ipchange/ipchange';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { PopoverPage } from '../popover-page/popover-page';
import {Login} from '../login/login';
declare var window;
/**
 * Generated class for the ExchangePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-exchange',
  templateUrl: 'exchange.html',
  providers : [Global,ChangelanguageProvider]
})
export class ExchangePage {
  public users:any=[];
  public tempuser:any=[];
  ipaddress;string;
  public temp:any=[];
  public w:any=[];
  public loading;
  userdata={ userID:'', sessionID:''};
  status :any;
  errormessagetext:string;
  font:string;
  textEng: any=['Foreign Exchange Rate','No result found!'];
  textMyan : any = ['နိုင်ငံခြားငွေလဲနှုန်း','အချက်အလက်မရှိပါ'];
  showFont:any=[];
  popover : any;
  ststus:any;
  modal: any;
  hardwareBackBtn: any = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http,public toastCtrl:ToastController,public loadingCtrl:LoadingController,
              public  network: Network,private storage : Storage,public all: AllserviceProvider, public global: Global,private slimLoader: SlimLoadingBarService,public changeLanguage:ChangelanguageProvider,public popoverCtrl:PopoverController,
  public platform:Platform,public globalphone:GlobalProvider, public alertCtrl: AlertController) {
    this.ststus=this.navParams.get("ststus");
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
	this.events.subscribe('ipaddress', (ipaddress) => {
     this.ipaddress = ipaddress;
	 
  });
  }

  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/
  showlist(){
   if (this.users != null || this.users != undefined){
     
     for (let j = 0; j < this.users.length; j++) {
      this.temp=this.getcurrencyCode(this.users[j].ccy1);
      this.users[j].currencyCode=this.temp.code;
      this.users[j].flag=this.temp.flag;
    }
   }
}
  getcurrencyCode(code) {
    let allcurencyCode = this.global.currencyCode;
    for (let i = 0; i < allcurencyCode.length; i++) {
      if (allcurencyCode[i].code ==code) {
        this.w.code=  allcurencyCode[i].symbolCode;
       this.w.flag = allcurencyCode[i].flag;
        break;
      }
    }
    return this.w;
  }
  getdata(){
    this.slimLoader.start(() => {
      
    });
        /*this.loading = this.loadingCtrl.create({
          content: "Please wait...",
          dismissOnPageChange :true
          //   duration: 3000
        });
        this.loading.present();*/
   // let param={"userID":this.userdata.userID,"sessionID":this.userdata.sessionID};
    let param={userID:'',sessionID:''};
    
        this.http.post(this.ipaddress+'/service001/getForeignExchangeRates',param).map(res => res.json()).subscribe(response => {
            
            if (response.code == "0000") {
              this.users=response.data;
              this.status = 1;
              this.showlist();
              this.tempuser=this.users;

              this.slimLoader.complete();
            } else {
              this.status = 0;
              let toast = this.toastCtrl.create({
                message:response.desc,
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.slimLoader.complete();
            }
          },
            error => {
              this.status = 0;
              let toast = this.toastCtrl.create({
                message:this.all.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.slimLoader.complete();
          });

        //work process
      }

  ionViewDidLoad() {
    
    this.storage.get('userData').then((result) => {
      this.userdata = result;
      

    });
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;
      this.getdata();
    });
  }
  getItems(ev) {
    var val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.users = this.tempuser.filter((users) => {
        return (users.ccy1.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else{
      this.users = this.tempuser.filter((tempuser) => {
        return (tempuser.ccy1.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  presentProfileModal(obj)
  {
    this.navCtrl.push(DetailPage, {
      From:obj.namCURRFrom,
      To:obj.namCURRTo,
      Buy:obj.numBuyRate,
      Sell: obj.numSellRate

    });
  }

  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }
  ionViewDidLeave(){
    
    this.slimLoader.reset();
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });

  }

  presentPopover(ev) {
    let createPopover=null;
    if(this.ststus=='1'){
      createPopover= PopoverPage;
    }else{
      createPopover=DefaultPopOver;
    }
    this.popover = this.popoverCtrl.create(createPopover, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      
      
      if(data == "0"){
        this.navCtrl.push(IpchangePage);
      }
      else if(data == "1"){
        this.presentPopoverLanguage(ev);
      }
      else if(data == "2"){
        this.globalphone.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      
      
      if(data != null){
        this.storage.set('language',data);
        let temp = {data:'',status:this.ststus};
        temp.data = data;
        temp.status = this.ststus;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  backButtonAction() {
   
      if (this.modal && this.modal.index === 0) {

        this.modal.dismiss();
      } else if (this.ststus == '1') {
        if (this.hardwareBackBtn) {

          let alert = this.alertCtrl.create({
            title: 'Are you sure you want to exit',
            enableBackdropDismiss: false,
            message: '',
            buttons: [{
              text: 'No',
              handler: () => {
                this.hardwareBackBtn = true;
              }
            },
            {
              text: 'Yes',
              handler: () => {
                this.gologout();
              }
            }]
          });
          alert.present();
          this.hardwareBackBtn = false;
        }
      }
    }

}
