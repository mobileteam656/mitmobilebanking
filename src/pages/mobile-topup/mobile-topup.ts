import 'rxjs/add/operator/map';

import {
  AlertController, Events, LoadingController, NavController, NavParams, Platform,
  PopoverController, Select, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Contacts } from '@ionic-native/contacts';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { MptTopupConfirmPage } from '../mpt-topup-confirm/mpt-topup-confirm';
import { PopoverPage } from '../popover-page/popover-page';
import { UtilProvider } from '../../providers/util/util';

declare var window;
@Component({
  selector: 'page-mobile-topup',
  templateUrl: 'mobile-topup.html',
  providers: [ChangelanguageProvider]
})
export class MobileTopupPage {
  @ViewChild('myselect') select: Select;
  passtemp: any;
  textEng: any = ["Account", "Choose Operator Type", "Phone Number", "Amount", "Narrative", "SUBMIT", "Choose Account No.", "Choose Operator Type", "Enter Phone No.", "Choose Amount", "Enter Amount", "Mobile Top Up", "RESET", "Account Balance", "Insufficient Balance", "Cash Back"];
  textMyan: any = ["အကောင့်နံပါတ်", "အော်ပရေတာ အမျိုးအစား", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ထည့်သွင်းမည်", "စာရင်းအမှတ်ရွေးချယ်ပါ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "ငွေပမာဏရွေးချယ်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "ဖုန်းငွေဖြည့်သွင်းခြင်း", "ပြန်စမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ", "ပြန်ရမည့်ငွေ"];
  showFont: any = [];
  font: any = '';
  _obj: any = {};
  userData: any;
  useraccount: any = [];
  ipaddress: any;
  referenceType: any = [];
  referenceTypeBill: any = [];
  amountType: any;
  msgCnpAcc: string;
  msgCnpAmtOther: string;
  msgCnpPhNo: string;
  msgCnpRef: string;
  msgCnpamt: string;
  referencename: any;
  amt: any;
  activepage: any;
  moreContact: any = false;
  moreContactData: any = [];
  contactPh: any = true;
  primColor: any = { 'background-color': '#3B5998' };
  accountBal: any; cashBack: any;
  loading: any;
  popover: any;
  merchant: any;
  alertPresented: any;
  amountBal: any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public barcodeScanner: BarcodeScanner,
    public storage: Storage, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public events: Events, public network: Network, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    private contacts: Contacts, public all: AllserviceProvider, public util: UtilProvider, public changefont: Changefont, public popoverCtrl: PopoverController, public platform: Platform, ) {
    //this._obj.processingCode = "200700";
    this.alertPresented = false;
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.storage.get('ipaddress').then((result) => {
        this.ipaddress = result;
        this.getreferenceData();
        this.getAccountSummary();
      });

    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changelanguage(this.font)
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  getAccountSummary() {
    /*  let loading = this.loadingCtrl.create({
       dismissOnPageChange: true
     });
     loading.present(); */
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        for (let i = 0; i < result.dataList.length; i++) {
          if (result.dataList[i].ccy == 'MMK') {
            this.useraccount.push(result.dataList[i]);
          }
        }
        // this.useraccount = result.dataList;
        //loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert('Warning!', result.desc);
        /*  let confirm = this.alertCtrl.create({
           title: 'Warning!',
           enableBackdropDismiss: false,
           message: result.desc,
           buttons: [
             {
               text: 'OK',
               handler: () => {
                 this.storage.remove('userData');
                 this.events.publish('login_success', false);
                 this.events.publish('lastTimeLoginSuccess', '');
                 this.navCtrl.setRoot(Login, {
                 });
                 this.navCtrl.popToRoot();
               }
             }
           ],
           cssClass: 'warningAlert',
         })
         confirm.present(); */
        //loading.dismiss();
        // this.slimLoader.complete();
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        //loading.dismiss();
        //this.slimLoader.complete();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        //loading.dismiss();
        //this.slimLoader.complete();
      });
  }

  getContact() {
    this.contacts.pickContact().then((data) => {
      if (data.phoneNumbers.length > 1) {
        this.contactPh = false;
        this.moreContact = true;
        this.msgCnpPhNo = '';
        this.moreContactData = data.phoneNumbers;
        for (let i = 0; i < this.moreContactData.length; i++) {
          this.moreContactData[i].value = this.moreContactData[i].value.toString().replace(/ ?-?/g, "");
          if (this.moreContactData[i].value.indexOf("7") == 0 && this.moreContactData[i].value.length == "9") {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("9") == 0 && this.moreContactData[i].value.length == "9") {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("+") != 0 && this.moreContactData[i].value.indexOf("7") != 0 && this.moreContactData[i].value.indexOf("9") != 0 && (this.moreContactData[i].value.length == "8" || this.moreContactData[i].value.length == "9" || this.moreContactData[i].value.length == "7")) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("09") == 0 && (this.moreContactData[i].value.length == 10 || this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 9)) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(2);
          }
          else if (this.moreContactData[i].value.indexOf("959") == 0 && (this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 12 || this.moreContactData[i].value.length == 10)) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(3);
          }
        }
      } else {
        this.contactPh = true;
        this.moreContact = false;
        this._obj.phone = data.phoneNumbers[0].value;
      }
    });
  }

  referenceChange(data) {
    this.referenceTypeBill = data.lovList;
  }

  operatorChange(operatorData) {
    this.msgCnpRef = '';
    this._obj.name = '';
    this.referenceChange(operatorData);
    this.activepage = operatorData;
    this.cashBack = '';
  }

  checkActive(page) {
    return page == this.activepage;
  }
  /* getamountType(data) {
    this.msgCnpAmtOther = '';
    this.msgCnpamt = '';
    if (data == 'other') {
      this.amountType = data;
    } else {
      this.amountType = '';
      this.msgCnpAmtOther = '';
      this._obj.amountother = '';
    }
  } */

  getamount(data) {
    this.msgCnpAmtOther = '';
    this.msgCnpamt = '';
    this._obj.amountType = data.value;
    this._obj.amount = data.name;
    this._obj.bankCharges = data.bankCharges;
    this.cashBack = data.t1;
  }

  getreferenceData() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, paymentType: "2" };
    this.http.post(this.ipaddress + '/service002/getTopUpMerchantData', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
        }
        for (let i = 0; i < data.data.length; i++) {
          if (!Array.isArray(data.data[i].lovList)) {
            tempArray.push(data.data[i].lovList);
            data.data[i].lovList = tempArray;
          }
        }
        this.merchant = data.data;
        this.loading.dismiss();
        //this.slimLoader.complete();
      }
      else if (data.code == "0016") {
        this.logoutAlert('Warning!', data.desc);
        /* let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present(); */
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        //this.slimLoader.complete();
      });
  }

  clicksubmit() {
    let flag1;
    let flag2;
    let flag3;
    let flag4;
    let flag5;
    let num = /^[0-9-\+]*$/;
    if (this._obj.account == '' || this._obj.account == undefined) {
      flag1 = false;
      this.msgCnpAcc = this.showFont[6];
    } else {
      flag1 = true;
      this.msgCnpAcc = '';
    }
    if (this.activepage == '' || this.activepage == undefined) {
      flag2 = false;
      this.msgCnpRef = this.showFont[7];
    } else {
      flag2 = true;
      this.msgCnpRef = '';
    }
    if (this._obj.phone == '' || this._obj.phone == undefined) {
      flag3 = false;
      this.msgCnpPhNo = this.showFont[8];
    } else {
      this._obj.phone = this._obj.phone.toString().replace(/ +/g, "");
      if (num.test(this._obj.phone)) {
        if (this._obj.phone.indexOf("+") == 0 && (this._obj.phone.length == "12" || this._obj.phone.length == "13" || this._obj.phone.length == "11")) {
          flag3 = true;
          this.msgCnpPhNo = '';
        }
        else if (this._obj.phone.indexOf("7") == 0 && this._obj.phone.length == "9") {
          this._obj.phone = '+959' + this._obj.phone;
          flag3 = true;
          this.msgCnpPhNo = '';
        }
        else if (this._obj.phone.indexOf("9") == 0 && this._obj.phone.length == "9") {
          this._obj.phone = '+959' + this._obj.phone;
          flag3 = true;
          this.msgCnpPhNo = '';
        }
        else if (this._obj.phone.indexOf("+") != 0 && this._obj.phone.indexOf("7") != 0 && this._obj.phone.indexOf("9") != 0 && (this._obj.phone.length == "8" || this._obj.phone.length == "9" || this._obj.phone.length == "7")) {
          this._obj.phone = '+959' + this._obj.phone;
          flag3 = true;
          this.msgCnpPhNo = '';
        }
        else if (this._obj.phone.indexOf("09") == 0 && (this._obj.phone.length == 10 || this._obj.phone.length == 11 || this._obj.phone.length == 9)) {
          this.msgCnpPhNo = '';
          this._obj.phone = '+959' + this._obj.phone.substring(2);
          flag3 = true;
        }
        else if (this._obj.phone.indexOf("959") == 0 && (this._obj.phone.length == 11 || this._obj.phone.length == 12 || this._obj.phone.length == 10)) {
          this.msgCnpPhNo = '';
          this._obj.phone = '+959' + this._obj.phone.substring(3);
          flag3 = true;
        }
        else {
          flag3 = false;
          this.msgCnpPhNo = 'Invalid phone number.';
        }
      }
      else {
        flag3 = false;
        this.msgCnpPhNo = this.showFont[8];
      }
    }
    if (this._obj.amount == '' || this._obj.amount == undefined) {
      if (this._obj.name == '' || this._obj.name == undefined) {
        flag4 = false;
        this.msgCnpamt = this.showFont[9];
      }
      else {
        if (this.amountType == 'other') {
          this._obj['amount'] = this._obj.amountother;
        } else {
          this._obj['amount'] = this._obj.name;
        }
        flag4 = true;
        this.msgCnpamt = '';
      }
    } else {
      let amount = parseInt(this._obj.amount);
      if (this._obj.amount.substring(0, 1) == "-" || amount == 0) {
        this.msgCnpamt = this.showFont[9];
        flag4 = false;
      } else if (isNaN(amount)) {
        this.msgCnpamt = this.showFont[9];
        flag4 = false;
      } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this._obj.amount)) {
        this.msgCnpamt = this.showFont[14];
        flag4 = false;
      } else {
        flag4 = true;
        this.msgCnpamt = '';
      }
    }

    if (this.amountType == 'other') {
      if (this._obj.amountother == '' || this._obj.amountother == undefined) {
        flag5 = false;
        this.msgCnpAmtOther = this.showFont[10];
      } else {
        let amount = parseInt(this._obj.amountother);
        if (this._obj.amountother.substring(0, 1) == "-" || amount == 0) {
          this.msgCnpAmtOther = this.showFont[10];
          flag4 = false;
        } else if (isNaN(amount)) {
          this.msgCnpAmtOther = this.showFont[10];
          flag4 = false;
        } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this._obj.amount)) {
          this.msgCnpAmtOther = this.showFont[14];
          flag4 = false;
        } else {
          flag5 = true;
          this.msgCnpAmtOther = '';
        }
      }
    } else {
      flag5 = true;
      this.msgCnpAmtOther = '';
    }
    if (flag1 && flag2 && flag3 && flag4 && flag5) {
      this._obj['operator'] = this.activepage['merchantName'];
      this._obj['processingCode'] = this.activepage['processingCode'];
      this._obj['merchantID'] = this.activepage['merchantID'];
      if (this._obj.narrative == '' || this._obj.narrative == undefined)
        this._obj.narrative = "";
      this.getOtpConfirm();
    }
  }

  getOtpConfirm() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    let param = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      type: '2'
    };
    this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        loading.dismiss();
        this.navCtrl.push(MptTopupConfirmPage, {//TopupconfirmPage
          data: this._obj,
          detail: this._obj,
          otp: data.rKey,
          sKey: data.sKey,
          fromPage: 'mobileTopUp'
        });
      }
      else if (data.code == "0016") {
        this.logoutAlert('Warning!', data.desc);
        loading.dismiss();
        /*  let confirm = this.alertCtrl.create({
           title: 'Warning!',
           enableBackdropDismiss: false,
           message: data.desc,
           buttons: [
             {
               text: 'OK',
               handler: () => {
                 this.storage.remove('userData');
                 this.events.publish('login_success', false);
                 this.events.publish('lastTimeLoginSuccess', '');
                 this.navCtrl.setRoot(Login, {
                 });
                 this.navCtrl.popToRoot();
               }
             }
           ],
           cssClass:"warningAlert"
         })
         confirm.present(); */
      }
      else {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 5000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 5000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        loading.dismiss();
      });
  }

  onChangeSPh(s, i) {
    if (i == "09") {
      this._obj.phone = "+959";
    }
    else if (i == "959") {
      this._obj.phone = "+959";
    }
    this.msgCnpPhNo = "";
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  reset() {
    this._obj.phone = '';
    this._obj.account = "";
    this._obj.activepage = "";
    this._obj.value = "";
    this._obj.amountother = ""
    this._obj.name = "";
    this._obj.narrative = "";
    this._obj.amount = "";
    this._obj.amountType = "";
    this.accountBal = "";
    this.amountBal = "";
    this.amountType = "";
    this.msgCnpAcc = "";
    this.msgCnpAmtOther = "";
    this.msgCnpPhNo = "";
    this.msgCnpRef = "";
    this.msgCnpamt = "";
    this.moreContact = false;
    this.contactPh = true;
    this.operatorChange("");
    this.referenceTypeBill = [];
    this.cashBack = '';
  }

  onClick() {
    if (this.referenceTypeBill.length == 0) {
      this.all.showAlert("Warning!", "Please select operator.");
    }
  }

  changeAcc(s, account) {
    if (s == 1) {
      this.msgCnpAcc = '';
    }
    else if (s == 2) {
      this.msgCnpPhNo = "";
    }
    for (let i = 0; i < this.useraccount.length; i++) {
      if (account == this.useraccount[i].depositAcc) {
        this.amountBal = this.useraccount[i].avlBal;
        this.accountBal = this.useraccount[i].avlBal + " " + this.useraccount[i].ccy;
      }
    }
  }

  inputChange(i) {
    if (i == 3) {
      this.msgCnpAmtOther = '';
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }
  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }

  logoutAlert(title, message) {
    let vm = this
    if (!vm.alertPresented) {
      vm.alertPresented = true
      vm.alertCtrl.create({
        title: title,
        message: message,
        buttons: [{
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
            vm.alertPresented = false
          }
        }],
        cssClass: 'warningAlert',
        enableBackdropDismiss: false
      }).present();
    }
  }
}
