import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Http } from '@angular/http';
import { WalletTopupPage } from '../wallet-topup/wallet-topup';
import { UtilProvider } from '../../providers/util/util';

/**
 * Generated class for the WalletTopupSuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-wallet-topup-success',
  templateUrl: 'wallet-topup-success.html',
  providers: [ChangelanguageProvider]
})
export class WalletTopupSuccessPage {
  passTemp: any;
  passTemp1: any;
  passTemp2: any;
  textEng: any = ["From account", "Wallet ID", "Amount", "Bank Reference No.", "Transaction Date", "Card Expired", "Transaction Approved", "Transfer Type", "Close"];
  textMyan: any = ["အကောင့်နံပါတ် မှ", "ဝေါ(လ်)လက် နံပါတ်", "ငွေပမာဏ", 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ငွေလွှဲ အမျိုးအစား', "ပိတ်မည်"];
  showFont: any = [];
  font: any = '';
  fromPage: any;
  loading: any;
  userdata: any;
  ipaddress: any;
  amount:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
    public http: Http,public util:UtilProvider ) {
    this.passTemp = this.navParams.get("data");
    this.passTemp1 = this.navParams.get("detail");
    this.passTemp2 = this.navParams.get("type");
    this.amount=this.util.formatAmount(this.passTemp1.amount)+"  "+"MMK";
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;

      });
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad WalletTopupSuccessPage');
  }

  getOK() {
    this.navCtrl.setRoot(WalletTopupPage);
  }

}
