import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { CorporateCheckerPage } from '../corporate-checker/corporate-checker';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { CorporateMakerPage } from '../corporate-maker/corporate-maker';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { MainHomePage } from '../main-home/main-home';
@Component({
  selector: 'page-corporate-type',
  templateUrl: 'corporate-type.html',
})
export class CorporateTypePage {
  textMyan: any = ['ကော်ပိုရိတ်','ကော်ပိုရိတ် ငွေလွှဲရန်', 'ကော်ပိုရိတ် ငွေလွှဲစာရင်းအတည်ပြုရန်'];
  textEng: any = ["Corporate","Corporate Transfer", "Transaction Approval"];
  showFont: string[] = [];
  font: string = '';

  constructor(public events: Events, public changefont: Changefont, public navCtrl: NavController, public navParams: NavParams, public changeLanguage: ChangelanguageProvider, public storage: Storage) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  ionViewDidLoad() {

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  getNext(s) {
    if (s == 'D') {
      this.navCtrl.push(CorporateMakerPage)
    } 
    else if (s == 'E') {
      this.navCtrl.push(CorporateCheckerPage)
    }
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage)
  }

}
