import 'rxjs/Rx';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform,
    PopoverController, Select, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { PopoverPage } from '../popover-page/popover-page';
import { WalletTopupConfirmPage } from '../wallet-topup-confirm/wallet-topup-confirm';

declare var window;

@Component({
  selector: 'page-wallet-topup',
  templateUrl: 'wallet-topup.html',
})
export class WalletTopupPage {
  @ViewChild('myselect') select: Select;
  textMyan: any = ["ဝေါ(လ်)လက် ငွေဖြည့်သွင်းခြင်း", "အကောင့်နံပါတ် မှ", "ဝေါ(လ်)လက် အိုင်ဒီ", "ငွေပမာဏ", "အကြောင်းအရာ", "လွှဲပြောင်းမည်", "ရှင်းလင်းသည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "အမည်"];
  textEng: any = ["Wallet Top Up", "From Account No.", "Wallet ID", "Amount", "Narrative", "TRANSFER", "RESET", "Account Balance", "Name"];
  textErrorEng: any = ["Please choose Account number.", "Please fill Wallet ID.", "Please fill amount.", 'Enter correct Wallet ID', 'Please check wallet ID', "Insufficient Balance"]
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ဝေါ(လ်)လက် အိုင်ဒီ ရိုက်ထည့်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", 'မှန်ကန်သော ဝေါ(လ်)လက် အိုင်ဒီ ရိုက်သွင်းပါ', 'ဝေါ(လ်)လက် အိုင်ဒီ စစ်ဆေးပါ', "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
  textError: string[] = [];
  textData: any = [];
  font: any;
  popover: any;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  userData: any;
  loading: any;
  userAccount: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  flag: string;
  fromAccountlist: any;
  walletData: any = {};
  fromaccMsg: any = '';
  walletIDMsg: any = '';
  amountMsg: any = '';
  accountBal: any = '';
  isChecked: boolean = false;
  amountBal: any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, public events: Events,
    public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public platform: Platform,
    public popoverCtrl: PopoverController, private slimLoader: SlimLoadingBarService, public all: AllserviceProvider, public idle: Idle,
    public network: Network, public changefont: Changefont, public util: UtilProvider) {
    this.amountBal = "";
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.storage.get('ipaddress').then((result) => {
        this.ipaddress = result;
        this.getAccountSummary()
      });
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.walletIDMsg != '') {
        this.walletIDMsg = this.textError[1];
      }
      if (this.amountMsg != '') {
        this.amountMsg = this.textError[2];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.walletIDMsg != '') {
        this.walletIDMsg = this.textError[1];
      }
      if (this.amountMsg != '') {
        this.amountMsg = this.textError[2];
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.walletIDMsg != '') {
        this.walletIDMsg = this.textError[1];
      }
      if (this.amountMsg != '') {
        this.amountMsg = this.textError[2];
      }
    }
  }


  ionViewDidLoad() {
  }
  
  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert(result.desc);
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'WalletTopupPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'WalletTopupPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });

    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }


  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true//   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }
          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  }

  inputChange(data) {
    if (data == 1) {
      this.walletIDMsg = "";
      this.isChecked = false;
    } else if (data == 2) {
      this.amountMsg = "";
    }
  }

  reset() {
    this.walletData.account = "";
    this.walletData.walletID = "";
    this.walletData.amount = "";
    this.walletData.narrative = "";
    this.walletData.walletName = "";
    this.fromaccMsg = "";
    this.amountMsg = "";
    this.accountBal = "";
    this.walletIDMsg = "";
    this.isChecked = false;
    this.amountBal = "";
  }

  onChange(i) {
    if (i == "09") {
      this.walletData.walletID = "+959";
    }
    else if (i == "959") {
      this.walletData.walletID = "+959";
    }
  }

  goTopUp() {
    let f1 = false, f3 = false;
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.walletData.walletID == '' || this.walletData.walletID == null || this.walletData.walletID == undefined) {
      this.walletIDMsg = this.textError[1];
    } else {
      if (num.test(this.walletData.walletID)) {
        if (this.walletData.walletID.indexOf("+") == 0 && (this.walletData.walletID.length == 12 || this.walletData.walletID.length == 13 || this.walletData.walletID.length == 11)) {
          this.walletIDMsg = '';
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("7") == 0 && this.walletData.walletID.length == 9) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("9") == 0 && this.walletData.walletID.length == 9) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("+") != 0 && this.walletData.walletID.indexOf("7") != 0 && this.walletData.walletID.indexOf("9") != 0 && (this.walletData.walletID.length == 8 || this.walletData.walletID.length == 9 || this.walletData.walletID.length == 7)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        } else if (this.walletData.walletID.indexOf("09") == 0 && (this.walletData.walletID.length == 10 || this.walletData.walletID.length == 11 || this.walletData.walletID.length == 9)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID.substring(2);
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("959") == 0 && (this.walletData.walletID.length == 11 || this.walletData.walletID.length == 12 || this.walletData.walletID.length == 10)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID.substring(3);
          flag = true;
        }
        else {
          flag = false;
          this.walletIDMsg = this.textError[3];
        }
      }
      else {
        flag = false;
        this.walletIDMsg = this.textError[3];
      }
    }
    if (this.isChecked == false) {
      flag = false;
      this.walletIDMsg = this.textError[4];
    }
    if (this.walletData.account != null && this.walletData.account != '' && this.walletData.account != undefined) {
      this.fromaccMsg = '';
      f1 = true;
    }
    else {
      this.fromaccMsg = this.textError[0];
      f1 = false;
    }
    if (this.walletData.amount != null && this.walletData.amount != '' && this.walletData.amount != undefined) {
      let amount = parseInt(this.walletData.amount);
      if (this.walletData.amount.substring(0, 1) == "-" || amount == 0) {
        this.amountMsg = this.textError[2];
        f3 = false;
      } else if (isNaN(amount)) {
        this.amountMsg = this.textError[2];
        f3 = false;
      }else if (parseInt(this.util.formatToDouble( this.amountBal)) < parseInt(this.walletData.amount)) {
        this.amountMsg = this.textError[5];
        f3 = false;
      } else {
        this.amountMsg = '';
        f3 = true;
      }
    }
    else {
      this.amountMsg = this.textError[2];
      f3 = false;
    }
    if (f1 && flag && f3) {
      this.walletData.fromName=this.userData.userName;
      this.walletData.toName=this.walletData.walletName;
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let type = '16';
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: type, merchantID: '' };
      this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();

          this.navCtrl.push(WalletTopupConfirmPage, {
            data: this.walletData,
            type: type,
            otp: data.rKey,
            sKey: data.sKey,
            fromPage: 'wallet-transfer'
          });
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  checkWallet() {
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.walletData.walletID == '' || this.walletData.walletID == null || this.walletData.walletID == undefined) {
      this.walletIDMsg = this.textError[1];
    } else {
      if (num.test(this.walletData.walletID)) {
        if (this.walletData.walletID.indexOf("+") == 0 && (this.walletData.walletID.length == 12 || this.walletData.walletID.length == 13 || this.walletData.walletID.length == 11)) {
          this.walletIDMsg = '';
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("7") == 0 && this.walletData.walletID.length == 9) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("9") == 0 && this.walletData.walletID.length == 9) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("+") != 0 && this.walletData.walletID.indexOf("7") != 0 && this.walletData.walletID.indexOf("9") != 0 && (this.walletData.walletID.length == 8 || this.walletData.walletID.length == 9 || this.walletData.walletID.length == 7)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID;
          flag = true;
        } else if (this.walletData.walletID.indexOf("09") == 0 && (this.walletData.walletID.length == 10 || this.walletData.walletID.length == 11 || this.walletData.walletID.length == 9)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID.substring(2);
          flag = true;
        }
        else if (this.walletData.walletID.indexOf("959") == 0 && (this.walletData.walletID.length == 11 || this.walletData.walletID.length == 12 || this.walletData.walletID.length == 10)) {
          this.walletIDMsg = '';
          this.walletData.walletID = '+959' + this.walletData.walletID.substring(3);
          flag = true;
        }
        else {
          flag = false;
          this.walletIDMsg = this.textError[3];
        }
      }
      else {
        flag = false;
        this.walletIDMsg = this.textError[3];
      }
    }
    if (flag) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      this.walletIDMsg = '';
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, accountNo: this.walletData.walletID, type: '' };
      this.http.post(this.ipaddress + '/service002/checkWallet', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.isChecked = true;
          this.walletData.walletName = data.accountName;
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}
