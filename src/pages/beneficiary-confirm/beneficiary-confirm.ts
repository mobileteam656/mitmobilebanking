import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { BeneficiaryFailPage } from '../beneficiary-fail/beneficiary-fail';
import { BeneficiaryOtpPage } from '../beneficiary-otp/beneficiary-otp';
import { BeneficiarySuccessPage } from '../beneficiary-success/beneficiary-success';
import { BeneficiaryAddPage } from '../beneficiary-add/beneficiary-add';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the BeneficiaryConfirmPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-beneficiary-confirm',
  templateUrl: 'beneficiary-confirm.html',
})
export class BeneficiaryConfirmPage {
  textMyan: any = ["အတည်ပြုခြင်း", "လက်ခံသူ အိုင်ဒီနံပါတ်", "အကောင့်နံပါတ်", "လက်ခံသူ အမည်", "လက်ခံသူ အီးမေးလ်", "လက်ခံသူ ဖုန်းနံပါတ်", "ပယ်ဖျက်မည်", "စာရင်းသွင်းမည်", "လက်ခံသူ အမျိုးအစား","Branch","ဘဏ်အမည်","ဘဏ်ကုဒ်","ဘဏ်လိပ်စာ","လက်ခံသူ လိပ်စာ"];
  textEng: any = ["Confirmation", "Beneficiary ID", "Account Number", "Beneficiary Name", "Beneficiary Email", "Beneficiary Phone", "CANCEL", "CONFIRM", "Beneficiary Type","Branch", "Bank Name", "Bank Code", "Bank Address","Beneficiary Address"];
  showFont: string[] = [];
  font: string = '';
  errormsg1: any;
  errormsg2: any;
  errormsg3: any;
  errormsg4: any;
  errormsg5: any;
  errormsg6: any;
  obj: any;
  userdata: any;
  ipaddress: any;
  isChecked: boolean = false;
  passOtp: any;
  passSKey: any;
  loading: any;
  bankAddress:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public events: Events,public global:GlobalProvider, public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider,
    public storage: Storage, public alertCtrl: AlertController, public http: Http,
    public toastCtrl: ToastController, private slimLoader: SlimLoadingBarService) {
    this.obj = this.navParams.get("data");
    this.passOtp = this.navParams.get("otp");
    this.passSKey = this.navParams.get('sKey');
    this.bankAddress='';
    if (this.obj.bankAddress1 != '') {
      this.bankAddress += this.obj.bankAddress1
    }
    if (this.obj.bankAddress2 != '') {
      this.bankAddress += ", " + this.obj.bankAddress2
    }
    if (this.obj.bankAddress3 != '') {
      this.bankAddress += ", " + this.obj.bankAddress3
    }
    if (this.obj.bankAddress4 != '') {
      this.bankAddress += ", " + this.obj.bankAddress4
    }
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('userData').then((data) => {
      this.userdata = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      });
    });
  }

  cancel() {
    this.navCtrl.pop();
  }

  confirm() {
    this.slimLoader.start(() => {
    });
    if (this.passOtp == 'true') {
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: 12, merchantID: '', sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.slimLoader.complete();
          this.navCtrl.push(BeneficiaryOtpPage, {
            dataOTP: data,
            data: this.obj,
            sKey: this.passSKey
          })
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {});
                  this.navCtrl.popToRoot();
                }
              }
            ],
cssClass:'confirmAlert'
          })
          confirm.present();
          this.slimLoader.complete();
        }
        else {
          this.all.showAlert("Warning!",data.desc);
          this.slimLoader.complete();
        }
      },
        error => {
          this.all.showAlert("Warning!",this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    }
    else {
      this.slimLoader.complete();
      this.add();
    }
  }
  
  add() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "beneficiaryID": this.obj.beneficiaryID,
      "beneficiaryRef": this.obj.beneficiaryRef,
      "name": this.obj.name,
      "nickName": "",
      "accountNo": this.obj.accountNo,
      "branch": this.obj.branch,
      "email": this.obj.email,
      "phone": this.obj.phone,
      "photo": "",
      "beneTypeValue": this.obj.beneficiaryType.value,
      "beneTypeName": this.obj.beneficiaryType.name,
      "remark": "",
      "sKey": this.passSKey,
      "beneAddress":this.obj.beneAddress,
      "bankName":this.obj.bankName,
      "bankCode":this.obj.bankCode,
      "bankAddress1":this.obj.bankAddress1,
      "bankAddress2":this.obj.bankAddress2,
      "bankAddress3":this.obj.bankAddress3,
      "bankAddress4":this.obj.bankAddress4,
      "bankBranch":this.obj.bankBranch
    };
    this.http.post(this.ipaddress + '/service002/addBeneficiary', param).map(res => res.json()).subscribe(res => {
      if (res.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.setRoot(BeneficiarySuccessPage, {
          data: res,
        })
      }
      else if (res.code == '0014') {
        this.loading.dismiss();
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert("Warning!",res.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  ionViewDidLoad() {
  }
}
