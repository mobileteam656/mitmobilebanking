import 'rxjs/add/operator/map';

import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  MenuController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { MyPopOverPage } from '../my-pop-over-page/my-pop-over-page';
import { TransactionHistoryDetail } from '../transaction-history-detail/transaction-history-detail';

declare var window;
@Component({
  selector: 'page-transaction-history-page',
  templateUrl: 'transaction-history-page.html',
  providers : [ChangelanguageProvider]
})
export class TransactionHistoryPage {
  useraccount:any [] =[];
  currentaccount:any=[];
  savingaccount:any=[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  shwe :string ;
  passTemp1 : any;
  passTemp2 : any;
  passTemp3 : any;
  todayDate : any;
  fromDate:any = '';
  toDate:any = '';
  currentpage:any = 1;
  pageSize:any = 10;
  duration:any = 0;
  durationmsg : any = '';
  resultdata:any = [];
  range:any = false;
  next:number = 1;
  previous:number = 1;
  loading : any;
  status : any = 0;
  ipaddress:string;
  userdata:any;
  showFont:string [] = [];
  textEng: any=['Search Result','Start Date','End Date','Search','No result found','Previous','Next','Filter by Date','Last 10 Transactions','Last 2 Days','Last 5 Days','As at','Ref','Card No.'];
  textMyan : any = ['ရလဒ်','စတင်သည့်ရက်','ပြီးဆုံးသည့်ရက်','ရှာမည်','အချက်အလက်မရှိပါ','နောက်သို့','ရှေ့သို့','ရက်အလိုက်စီစဉ်သည်','နောက်ဆုံး၁၀ကြောင်း','လွန်ခဲ့သော၂ရက်','လွန်ခဲ့သော၅ရက်','ယနေ့','အမှတ်စဉ်','ကတ်နံပါတ်'];
  font:string;
  popover : any;
  datefilter :any;
  constructor(public navCtrl: NavController,public menu: MenuController, public idle:Idle,public navParams: NavParams,
  public datePipe:DatePipe ,public popoverCtrl:PopoverController,public all: AllserviceProvider, public alertCtrl:AlertController,public toastCtrl:ToastController, public loadingCtrl:LoadingController,
  public events:Events,public http:Http,public storage:Storage,private slimLoader: SlimLoadingBarService,public changeLanguage:ChangelanguageProvider,
              public platform:Platform) {
    this.passTemp1 = this.navParams.get('data');
    this.passTemp2 = this.navParams.get('detail');
    this.datefilter = this.navParams.get('date');
    this.storage.get('userData').then((val) => {
      
      this.userdata=val;
    });

    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        if(this.passTemp2.durationType == 0) {
          this.durationmsg = this.showFont[7]+" ("+this.showFont[8]+")";
        }
        else if (this.passTemp2.durationType == 1) {
          this.durationmsg = this.showFont[7] + " (" + this.showFont[9] + ")";
        }
        else if (this.passTemp2.durationType == 2) {
          this.durationmsg = this.showFont[7] + " (" + this.showFont[10] + " )";
        }
        else if (this.passTemp2.durationType == 3){
          this.durationmsg = this.showFont[7]+" ( " + this.datefilter + " )";
          
        }
        
      });
    });
    this.status = 1;
   // if(this.passTemp2.durationType = 0)
    

    let tempArray = [];
    if (!Array.isArray(this.passTemp1.billResults)) {
      tempArray.push(this.passTemp1.billResults);
      this.passTemp1.billResults = tempArray;
    }
    let group_to_values = this.passTemp1.billResults.reduce(function (obj, item) {
      obj[item.transDate] = obj[item.transDate] || [];
      obj[item.transDate].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {
      return {transDate: key, data: group_to_values[key]};
    });


    this.resultdata = groups;
    let date = new Date();
    this.todayDate = this.datePipe.transform(date, 'dd-MM-yyyy');
  }

  ionViewDidLoad() {
    
    //// this.idleWatch();
    this.storage.get('ipaddress').then((result) => {
      
      if(result == null || result ==''){
        
      }
      else{
        this.ipaddress=result;
      }
    });

    this.useraccount = this.navParams.get('accounts');
    if (this.useraccount != undefined) {

      for (var i = 0; i < this.useraccount.length; i++) {
        if (this.useraccount[i].acctype == 'U') {
          this.currentaccount.push(this.useraccount[i]);
        } else {
          this.savingaccount.push(this.useraccount[i]);
        }
      }
    }
    else{
      this.useraccount = [];
    }

  }

  idleWatch(){
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5*60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='TransationHistory')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='TransationHistory'){
        var data=countdown/60;
        this.min=data.toString().split('.')[0];
        this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        this.sec=  (Math.round(this.sec * 100) / 100);
        //   
        this.idleState = 'You\'ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLeave(){
    
    this.slimLoader.reset();
  }

  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(MyPopOverPage, {});

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      //    this.resultdata={};
      
      
      this.duration = data;
      this.currentpage = 1;
      if (data != 3 && data!=null) {
        if (data == 0) {
          this.durationmsg = this.showFont[7]+" ("+this.showFont[8]+")";
        }
        if (data == 1)
          this.durationmsg = this.showFont[7]+" ("+this.showFont[9]+" )";
        else if (data == 2)
          this.durationmsg = this.showFont[7]+" ("+this.showFont[10]+" )";
        this.range = false;
        this.fromDate = '';
        this.toDate = '';
        this.currentpage = 1;
        this.pageSize = 10;
        this.goSearch();
      }
      else if (data == 3) {
        this.range = true;
      }

    });
  }

  nextData() {
    this.next++;
    if (this.next <= this.passTemp1.pageCount) {
      this.currentpage = this.currentpage + 1;
      this.previous = this.next;
      this.goSearch();
    }

  }

  previousData() {
    this.previous--;
    if (this.previous >= 1) {
      this.currentpage = this.previous;
      this.next = this.previous;
      this.goSearch();
    }
  }
  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }

  goSearch(){

    if (this.fromDate != '' && this.toDate != '' && this.duration == 3) {
      this.durationmsg = "Filter by Date ( " + this.datePipe.transform(this.fromDate, 'dd-MM-yyyy') + " to " + this.datePipe.transform(this.toDate, 'dd-MM-yyyy') + " )";
     /* if(this.fromDate.toString().indexOf("-") > -1) {
        this.fromDate = this.datePipe.transform(this.fromDate, 'yyyyMMdd');
        this.toDate = this.datePipe.transform(this.toDate, 'yyyyMMdd');
        
      }*/
    }

    this.slimLoader.start(() => {
      
    });
    let param={
      userID: this.passTemp2.userID,
      acctNo: this.passTemp2.acctNo,
      sessionID: this.passTemp2.sessionID,
      durationType: this.duration,
      fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      totalCount: 0,
      currentPage: this.currentpage,
      pageSize: 10,
      pageCount: 0,
      status: 'all',
      merchantID: this.passTemp2.merchantID
    };

    
    this.http.post(this.ipaddress+'/service001/billPaymentTransactionListing',param).map(res => res.json()).subscribe(data => {
        
        if(data.code == '0000'){
          this.range = false;
          this.status = 1;
          if(data.pageCount>1 && this.currentpage == 1){
            this.next = 1;
            this.previous = 1;
          }
          let tempArray = [];
          if (!Array.isArray(data.billResults)) {
            tempArray.push(data.billResults);
            data.billResults = tempArray;
          }
          this.passTemp1 = data;
          let group_to_values = data.billResults.reduce(function (obj, item) {
            obj[item.transDate] = obj[item.transDate] || [];
            obj[item.transDate].push(item);
            return obj;
          }, {});
          let groups = Object.keys(group_to_values).map(function (key) {
            return {transDate: key, data: group_to_values[key]};
          });


          this.resultdata = groups;
          

         // this.loading.dismiss();
          this.slimLoader.complete();
        }
        else if (data.code == "0016") {
          this.status = 0;
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss:false,

            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                  this.loading.dismiss();
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess','');
                  this.storage.remove('userData');
                  this.navCtrl.setRoot(Login, {});
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass:'warningAlert'
          })
          confirm.present();
         // this.loading.dismiss();
          this.slimLoader.complete();
        }
        else {
          this.status = 0;
          this.range = false;
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.slimLoader.complete();
         // this.loading.dismiss();
        }


      },
        error => {
          this.status = 0;
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.slimLoader.complete();
      });

  }
  gotoDetail(a)
  {
    this.navCtrl.push(TransactionHistoryDetail,
      {
        data:a,
        detail : this.passTemp2

      });
  }
  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = {userID: this.userdata.userID, sessionID: this.userdata.sessionID};

      
      this.http.post(this.ipaddress+'/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.loading.dismiss();
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess','');
            this.storage.remove('userData');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.loading.dismiss();
          }

        },
          error => {
          let toast = this.toastCtrl.create({
            message:this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }
}
