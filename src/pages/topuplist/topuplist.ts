import 'rxjs/Rx';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  App,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ChoosetopupPage } from '../choosetopup/choosetopup';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the TopuplistPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-topuplist',
  templateUrl: 'topuplist.html',
  providers: [ChangelanguageProvider]
})
export class TopuplistPage {
  signinData: any;
  merchantList: any;
  userData: any;
  status: any;
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  flag: string;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  titleData: any = [];
  textMyan: any = ["ငွေဖြည့်သွင်းခြင်း"];
  textEng: any = ["Top Up"];
  font: any = '';
  idleCtrl: any;

  popover: any;
  showFont: any = [];
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public storage: Storage, public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, public idle: Idle, public network: Network, public popoverCtrl: PopoverController, public app: App,
    private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public platform: Platform) {

    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
  }
  presentPopover(ev) {

    this.popover = this.popoverCtrl.create(PopoverPage, {
    });

    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        //  this.navCtrl.push(Language)
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone=>{
          this.callIT(phone);
        })
      }

    });
  }
  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {


      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }


    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }
  clickmerchant(l) {
    this.navCtrl.push(ChoosetopupPage, {
      data: l
    })
  }

  /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.textMyan[j];
      }
    //  this.showFont = this.textMyan;
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j]= this.textEng[j];
      }
    }
    
  }*/

  getTopUpList() {

    this.slimLoader.start(() => {

    });
    /*this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange :true
      //   duration: 3000
    });
    this.loading.present();*/
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID, paymentType: 2 };

    this.http.post(this.ipaddress + '/service001/getMerchant', parameter).map(res => res.json()).subscribe(result => {
      //colorCode,logo

      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.merchantList)) {
          tempArray.push(result.merchantList);
          result.merchantList = tempArray;
        }
        let temp = [];
        let count = 0;
        this.merchantList = result.merchantList;
        for (let i = 0; i < this.merchantList.length; i++) {
          if (this.merchantList[i].processingCode != '000000') {
            temp[count] = this.merchantList[i];
            count++;
          }
        }
        this.merchantList = temp;


        this.status = 1;
        this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        this.status = 0;
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');

                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.all.showAlert('Warning!', result.desc)
        this.slimLoader.complete();

      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
      this.storage.get('userData').then((userData) => {
        this.userData = userData;

        this.getTopUpList();
      });

    }
  }
  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TopuplistPage') {

        if (this.idleCtrl) {
          //  this.idle.ngOnDestroy();
          this.idleCtrl = false;
          this.gologout();
          this.idle.stop();
        }
      }
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'MerchantlistPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        //   
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      /* closes modal */
      this.modal.dismiss();
    } else {
      // let hardwareBackBtn = true;
      /* exits the app, since this is the main/first tab */
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit?',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;

            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
          this.idle.stop();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }
  ionViewCanEnter() {

    // this.idleWatch();
  }
  ionViewDidLoad() {

    this.storage.get('ipaddress').then((result) => {

      if (result == null || result == '') {

      }
      else {
        this.checkNetwork();
        this.ipaddress = result;
      }
    });
  }
  ionViewDidLeave() {

    this.slimLoader.reset();
    this.reload();
    this.idleCtrl = true;
    //  this.idle.stop();
  }

}
