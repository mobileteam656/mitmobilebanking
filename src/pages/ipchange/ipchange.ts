import { Component } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Events, NavController, NavParams, ToastController } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';

/**
 * Generated class for the IpchangePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-ipchange',
  templateUrl: 'ipchange.html',
    providers : [ChangelanguageProvider]
})
export class IpchangePage {
  flag:string;
  data={protocol:'',ipaddress:'',port:'',servicename:''};
  errormessagetext:string;
  ipaddress:string;
  font:string;
  textEng: any=['YCB Bank','Change','Enter protocol','Enter IP address','Enter port number','"Enter service name','Invalid protocol','Invalid IP address','Invalid port number','Update Successfully!','OK'];
  textMyan : any = ['နေပြည်တော်စည်ပင်ဘဏ်','ပြောင်းလဲမည်','protocol ရိုက်သွင်းပါ','IP address ရိုက်သွင်းပါ','port နံပါတ်ရိုက်သွင်းပါ','service အမည်ရိုက်သွင်းပါ','protocol မှားယွင်းနေပါသည်','IP address မှားယွင်းနေပါသည်','port နံပါတ်မှားယွင်းနေပါသည်','ပြောင်းလဲခြင်းအောင်မြင်ပါသည်','သဘောတူပါသည်'];
  showFont:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl:ToastController,public  network: Network,public storage:Storage,public changeLanguage:ChangelanguageProvider,public events:Events) {
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
       for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont.push( this.changefont.UnitoZg(this.textMyan[j]));
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j]= this.textEng[j];
      }
    }
  }*/
  inputChange(data){
    this.errormessagetext='';
  }
  checkNetwork(){
    
    if(this.network.type=="none"){
      this.flag='none';
    }

  }
  validateAddress(){
    if(this.data.protocol=='' || this.data.protocol==null || this.data.protocol==undefined){
      this.errormessagetext = "Enter protocol";
    }else  if(this.data.ipaddress=='' || this.data.ipaddress==null || this.data.ipaddress==undefined){
      this.errormessagetext = "Enter IP address";
    }else if(this.data.port=='' || this.data.port==null || this.data.port==undefined){
      this.errormessagetext = "Enter port number";
    }else if(this.data.servicename=='' || this.data.servicename==null || this.data.servicename==undefined){
      this.errormessagetext = "Enter service name";
    }else{
      this.flag='success';
    }
 }
  changeaddress(){

    this.validateAddress();
   if(this.flag=='success'){
     this.data.protocol = this.data.protocol.toLowerCase();
     this.data.port = this.data.port + '';
     if (this.data.protocol != 'http' && this.data.protocol != 'https') {
       this.errormessagetext = this.showFont[6];
     } else if ((this.data.ipaddress.split(".").length - 1) != 3) {
       this.errormessagetext = this.showFont[7];
     } else if (this.data.port.indexOf('e') > 0 || parseInt(this.data.port)>65535) {
       this.errormessagetext = this.showFont[8];
     } else {
       this.errormessagetext='';
       let tempIP = this.data.protocol + "://" + this.data.ipaddress + ":" + this.data.port + "/" + this.data.servicename + "/module001";
       
	    this.events.publish('ipaddress',tempIP);
       this.storage.get('ipaddress').then((result) => {
         if(result == null || result ==''){
           this.storage.set('ipaddress',tempIP);
         }
         else{
           this.storage.remove('ipaddress');
           this.storage.set('ipaddress',tempIP);
         }
       });

       let toast = this.toastCtrl.create({
         message:this.showFont[9],
          duration: 2000,
          position: 'bottom',
       //  showCloseButton: true,
         dismissOnPageChange: true,
       //  closeButtonText: this.showFont[10]
       });
       toast.present(toast);
     }
   }
  }

  ionViewDidLoad() {
    
    this.checkNetwork();
    /*this.network.onConnect().subscribe(data => {
      

    }, error => console.error(error));*/

/*    this.network.onDisconnect().subscribe(data => {
      this.flag='none';
      let toast = this.toastCtrl.create({
        message:"Check your internet connection!",
        // duration: 2000,
        // position: 'bottom'
        showCloseButton: true,
        closeButtonText: 'OK'
      });
      toast.present(toast);
      

    }, error => console.error(error));*/
    this.storage.get('ipaddress').then((val) => {
      
      this.ipaddress=val;
  /* let   getIP = this.ipaddress.split("/");
      this.data.protocol = getIP[0].replace(":", "");
      this.data.ipaddress = getIP[2].split(":")[0];
      this.data.port = getIP[2].split(":")[1];
      this.data.servicename= getIP[3];*/
    //  $scope.port.servicepath = "module001";
      let res = this.ipaddress.split("/");
      this.data.protocol = res[0].split(':')[0];
      this.data.ipaddress =res[2].split(':')[0];
      this.data.port = res[2].split(':')[1];
      this.data.servicename= res[3];

    });
  }
  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }
}
