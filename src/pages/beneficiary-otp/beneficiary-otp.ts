import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { BeneficiaryConfirmPage } from '../beneficiary-confirm/beneficiary-confirm';
import { BeneficiarySuccessPage } from '../beneficiary-success/beneficiary-success';
import { Login } from '../login/login';

@Component({
  selector: 'page-beneficiary-otp',
  templateUrl: 'beneficiary-otp.html',
})
export class BeneficiaryOtpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  showFont: string[] = [];
  font: string;
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData: any;
  passtemp1: any;
  obj: any;
  passtType: any;
  public loading;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  passSKey: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public all: AllserviceProvider,
    public storage: Storage,public global:GlobalProvider, public alertCtrl: AlertController, public events: Events, public network: Network, public idle: Idle, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.passtemp1 = this.navParams.get('dataOTP');
    this.obj = this.navParams.get('data');
    this.passSKey = this.navParams.get('sKey');
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
      //this.slimLoader.complete();
    })
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    }); */
  }

  getOTP() {
    this.checkNetwork();
    if (this.flag == "success") {
      /* this.slimLoader.start(() => {
      }); */
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 12, merchantID: '', sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.passtemp1.rKey = data.rKey;
          /* window.SMS.startWatch(function () {
          }, function () {
          });
          setTimeout(() => {
          }, 8000);
          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;
            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];
            //this.slimLoader.complete();
          }); */
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.events.publish('login_success', false);
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass:'confirmAlert'
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert("Warning",data.desc);
          //this.slimLoader.complete();
        }
      },
        error => {
          this.all.showAlert("Warning",this.all.getErrorMessage(error));
          //this.slimLoader.complete();
        });
    } else {
      this.all.showAlert("Warning","Check your internet connection!");
    }
    this.network.onDisconnect().subscribe(data => {
      this.flag = 'none';
      this.all.showAlert("Warning","Check your internet connection!");
    }, error => console.error(error));
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  confirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, rKey: this.passtemp1.rKey, otpCode: this.otpcode, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.add();
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass:'confirmAlert'
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert("Warning",data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert("Warning",this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    } else {
      this.all.showAlert("Warning", "Check your internet connection!");
    }
  }

  add() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = {
      "userID": this.userData.userID,
      "sessionID": this.userData.sessionID,
      "beneficiaryID": this.obj.beneficiaryID,
      "name": this.obj.name,
      "nickName": "",
      "accountNo": this.obj.accountNo,
      "branch": "",
      "email": this.obj.email,
      "phone": this.obj.phone,
      "photo": "",
      "beneTypeValue": this.obj.beneficiaryType.value,
      "beneTypeName": this.obj.beneficiaryType.name,
      "remark": "",
      "sKey": this.passSKey
    };
    this.http.post(this.ipaddress + '/service002/addBeneficiary', param).map(res => res.json()).subscribe(res => {
      if (res.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.setRoot(BeneficiarySuccessPage, {
          data: res,
        })
      }
      else if (res.code == '0014') {
        this.loading.dismiss();
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.navCtrl.setRoot(BeneficiaryConfirmPage)
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert("Warning", res.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert("Warning",this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  ionViewDidLoad() {
  }

}