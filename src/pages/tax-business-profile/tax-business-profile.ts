import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { TaxPaymentPage } from '../tax-payment/tax-payment';

@Component({
  selector: 'page-tax-business-profile',
  templateUrl: 'tax-business-profile.html',
})
export class TaxBusinessProfilePage {
  textMyan: any = ["လုပ်ငန်းပရိုဖိုင် ရွေးချယ်ခြင်း", "Industry Code","Company Type","Creditor Branch Code","Creditor Branch Name", "Type of Business","MD Account No.",  "Tax Office Name", "Tax Type","Profile ID","Profile Name","Main Business"];
  textEng: any = ["Choose Business Profile", "Industry Code","Company Type","Creditor Branch Code","Creditor Branch Name", "Type of Business","MD Account No.",  "Tax Office Name", "Tax Type","Profile ID","Profile Name","Main Business"];
  textData: any = [];
  font: any = '';
  businessList: any = [];
  taxData:any=[];
  paymentList: any = [];
  taxList: any = [];
  incomeYear: any = [];
  taxPeriod:any=[];
  //buttonColor: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public changefont: Changefont, public events: Events, public storage: Storage) {
    this.businessList = this.navParams.get('businessList');
    this.taxData=this.navParams.get('data');
    this.paymentList = this.navParams.get("paymentList");
    this.taxList = this.navParams.get("taxList");
    this.incomeYear = this.navParams.get("incomeYear");
    this.taxPeriod=this.navParams.get("taxPeriod");
    let tmpBusi=[];
   /*  for(let i=0;i<this.businessList.size();i++){
      tmpBusi.push({"businessList":{"active":"true",}});
    } */
  }

  ionViewDidLoad() {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goBack(b){
    this.taxData.industryCode=b.industryCode,
    this.taxData.companyType=b.companyType,
    this.taxData.creditorBranchCode=b.creditorBranchCode,
    this.taxData.creditorBranchName=b.creditorBranchName,
    this.taxData.typeOfBusi=b.typeOfBusi,
    this.taxData.mdaccNo=b.mdaccNo,
    this.taxData.taxOfficeName=b.taxOfficeName,
    this.taxData.taxType=b.taxType,
    this.navCtrl.pop();
  }

  goNext(b){
    //this.buttonColor =  {'background':'blue'};
    this.taxData.industryCode=b.industryCode,
    this.taxData.companyType=b.companyType,
    this.taxData.creditorBranchCode=b.creditorBranchCode,
    this.taxData.creditorBranchName=b.creditorBranchName,
    this.taxData.typeOfBusi=b.typeOfBusi,
    this.taxData.mdaccNo=b.mdaccNo,
    this.taxData.taxOfficeName=b.taxOfficeName,
    this.taxData.taxType=b.taxType,
    this.taxData.profileID=b.profileID,
    this.taxData.profileName=b.profileName,
    this.taxData.mainBusiness=b.mainBusiness,
    this.taxData.sessionCode=b.sessionCode,
    this.taxData.fromAcc = "";
    this.taxData.taxAmount = "";
    this.taxData.narrative = "";
    this.taxData.paymentTypeCode = "";
    this.taxData.paymentTypeDesc = "";
    this.taxData.taxTypeCode = "";
    this.taxData.taxTypeDesc = "";
    this.taxData.incomeYearCode = "";
    this.taxData.incomeYearDesc = "";
    this.taxData.taxPeriodCode = "";
    this.taxData.taxPeriodDesc = "";
    this.navCtrl.push(TaxPaymentPage,{//TaxPaymentPage
      data:this.taxData,
      paymentList:this.paymentList,
      taxList:this.taxList,
      businessList:this.businessList,
      incomeYear:this.incomeYear,
      taxPeriod:this.taxPeriod
    });
  }
}
