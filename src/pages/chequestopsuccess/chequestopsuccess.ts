import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChequelistPage } from '../chequelist/chequelist';


/**
 * Generated class for the ChequestopsuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-chequestopsuccess',
  templateUrl: 'chequestopsuccess.html',
    providers : [ChangelanguageProvider]
})
export class ChequestopsuccessPage {
	carryData:any;
	font:string;
	showFont:any=[];
   textEng: any=['Account Number','Cheque Number','Status','Details','Success! Your request to stop cheque has been submitted',"Close"];
    textMyan : any = ['စာရင်းနံပါတ်','ချက်နံပါတ်','ရလဒ်','အသေးစိတ်အချက်အလက်များ','မိတ်ဆွေ၏ချက်နံပါတ် ရပ်ဆိုင်းခြင်း အောင်မြင်ပါသည်။','ပိတ်မည်'];
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public changeLanguage:ChangelanguageProvider) {
	  this.carryData = this.navParams.get('data');
	  	   this.storage.get('language').then((font) => {
      
             this.font = font;
             this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
             {
               
               this.showFont = data;
               
             });
    });
  }
   /*   changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.textMyan[j];
      }
    //  this.showFont = this.textMyan;
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j]= this.textEng[j];
      }
    }
    
  }*/
  goBack(){
	  this.navCtrl.setRoot(ChequelistPage);
  }

  ionViewDidLoad() {
    
  }

}
