import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Events, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { MerchantlistPage } from '../merchantlist/merchantlist';
import { QrUtilityPage } from '../qr-utility/qr-utility';
//import { SkynetPage } from '../skynet/skynet';
/**
 * Generated class for the Merchantsuccess page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-merchantsuccess',
  templateUrl: 'merchantsuccess.html',
  providers: [ChangelanguageProvider]

})
export class Merchantsuccess {
  textMyan: any = ['လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'အမှတ်စဉ်', 'Package', 'ကတ်နံပါတ်', 'စာရင်းနံပါတ်', 'ကျသင့်ငွေ', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', "ပိတ်မည်", "မီတာ နံပါတ်", "Biller အမျိုးအစား", "အသုံးပြုသူအမှတ်စဉ်"];
  textEng: any = ["Transaction Approved", "Reference Number", "Package", "Card Number", "Account Number", "Transaction Amount", "Transaction Date", "Expiry Date", "CLose", "Meter Number", "Bill Type", "Customer Reference No."];
  showFont: string[] = [];
  font: string;
  passtemp1: any;
  passtemp2: any;
  passtemp3: any;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  public loading;
  userData: any;
  cusCss: any = '';
  ipaddress: any = '';
  primColor: any = { 'background-color': '#236ee3' };
  fromPage: any;
  constructor(public navCtrl: NavController, public all: AllserviceProvider, public loadingCtrl: LoadingController, public http: Http, public toastCtrl: ToastController, public navParams: NavParams, public idle: Idle, public events: Events, public storage: Storage, public platform: Platform, public changeLanguage: ChangelanguageProvider) {
    //   platform.registerBackButtonAction(() => this.myHandlerFunction());
    this.passtemp1 = this.navParams.get('data');
    this.passtemp2 = this.navParams.get('detail');
    this.passtemp3 = this.navParams.get('detailmerchant');
    this.fromPage = this.navParams.get('fromPage');
    if (this.passtemp3.processingCode == '080400') {
      this.cusCss = 'resizeskynet';
    }
    else {
      this.cusCss = 'resize';
    }
    if (this.passtemp3.colorCode == '') {
      this.primColor = { 'background-color': '#34a853' };
    } else {
      this.primColor = { 'background-color': this.passtemp3.colorCode };
    }
    this.storage.get('userData').then((userData) => {
      this.userData = userData;

      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
        }
      });
    });



    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });

    });
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'Merchantsuccess')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'Merchantsuccess') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLeave() {

  }
  ionViewDidLoad() {

  }

  /* generateQR() {
    this.navCtrl.setRoot(QrPage, {
      data: this.passtemp1,
      detail: this.passtemp2
    })
  } */

  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");

      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  getOK() {
    if (this.fromPage == 'qrUtility') {
      this.navCtrl.setRoot(QrUtilityPage);
    } else {
      //this.navCtrl.setRoot(SkynetPage);
      this.navCtrl.setRoot(MerchantlistPage);
    }
  }

}
