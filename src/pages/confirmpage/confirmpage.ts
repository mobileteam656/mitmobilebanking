import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ConfirmotpPage } from '../confirmotp/confirmotp';
import { Login } from '../login/login';
import { MerchantErrorPage } from '../merchant-error-page/merchant-error-page';
import { Merchantsuccess } from '../merchantsuccess/merchantsuccess';


@Component({
  selector: 'page-confirmpage',
  templateUrl: 'confirmpage.html',
  providers: [ChangelanguageProvider]
})
export class ConfirmpagePage {
  textMyan: any = ["အကောင့်နံပါတ်", "ကဒ်နံပါတ်", "ရုပ်သံလိုင်းအမည်", "Package အမျိုးအစား", "သက်တမ်း", "အခွန်နှုန်း", "ဝန်ဆောင်ခနှုန်း", "ငွေပမာဏ", "စုစုပေါင်းငွေပမာဏ", "ပယ်ဖျက်မည်", "အတည်ပြုမည်", "အတည်ပြုခြင်း", "ငွေပမာဏ", "ဘဏ်ဝန်ဆောင်ခ", "ရုပ်သံလိုင်းဝန်ဆောင်ခ", "အကြောင်အရာ", "Biller အမျိုးအစား", "Biller အမှတ်စဉ်", "CNP ဝန်ဆောင်ခ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်", "အသုံးပြုသူအမှတ်စဉ်", "ငွေတောင်းခံလွှာနံပါတ်", "အကြောင်အရာ", "အရေအတွက်", "ဈေးနှုန်း", "ပေးဆောင်ရန်ကျန်ရှိသော ဘေလ်", "ဒဏ်ငွေကြေး", "မီတာ နံပါတ်"];
  textEng: any = ["Account", "Card Number", "Merchant Name", "Package", "Month", "Commercial Tax", "Service Charges", "Amount", "Total Amount", "CANCEL", "CONFIRM", "Confirmation",
    "Amount", "Bank Charges", "Merchant Charges", "Narrative", "Biller Type", "Biller Reference No.", "CNP Charges",
    "Operator Type", "Phone Number",
    "Customer No.", "Invoice No.",
    "Items", "Quantity", "Price", "Outstanding Bill", "Penalty", "Meter Number"];
  showFont: string[] = [];
  font: string;
  data: any;
  userdata: any;
  loading: any;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  detail: any;
  passTemp: any;
  passOtp: any;
  passSKey : any;
  constructor(public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.detail = this.navParams.get('data');
    this.passTemp = this.navParams.get('detail');
    this.passOtp = this.navParams.get("otp");
    this.passSKey = this.navParams.get('sKey');
    
    if (this.passTemp.processingCode == '050200') {	//CNP
      this.detail['referenceType'] = this.navParams.get('data').billType;
      this.detail['amountTotal'] = this.navParams.get('data').totalAmount;
      if (this.navParams.get('data').CusrefNo == '') {		//MPT
        this.detail['refNo'] = '';
      } else {
        this.detail['refNo'] = this.navParams.get('data').CusrefNo;
      }
    }
    //pass result={"accounttype":"0020101100004292","billType":"MPT","CusrefNo":"","cnpCharge":"300.00","bankCharges":"200.00","amount":"1256545","totalAmount":1257045,"narrative":""}

    

    this.storage.get('userData').then((data) => {
      this.userdata = data;
      
      this.storage.get('ipaddress').then((result) => {
        
        if (result == null || result == '') {
          
        }
        else {
          this.ipaddress = result;
        }
      });
    });

    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        
        this.showFont = data;
        
      });
    });

  }
  checkNetwork() {
    
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ConfirmpagePage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ConfirmpagePage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  ionViewDidLoad() {
    

    this.checkNetwork();

    // this.idleWatch();
  }
  ionViewDidLeave() {
    
    this.slimLoader.reset();
  }
  confirmbtn() {
    if (this.flag == "success") {
      this.slimLoader.start(() => {
        
      });
      if (this.passOtp == 'true') {
        let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: '1', merchantID: this.passTemp.merchantID, sKey: this.passSKey};
        
        this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
          
          if (data.code == "0000") {
            this.slimLoader.complete();
            this.navCtrl.push(ConfirmotpPage, {
              data: data,
              detail: this.detail,
              detailmerchant: this.passTemp,
              sKey: this.passSKey
            })
          }
          else if (data.code == "0016") {
            let confirm = this.alertCtrl.create({
              title: 'Warning!',
              enableBackdropDismiss: false,
              message: data.desc,
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    
                    this.storage.remove('userData');
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.navCtrl.setRoot(Login, {});
                    this.navCtrl.popToRoot();
                  }
                }
              ],
              cssClass: 'warningAlert',
            })
            confirm.present();
            this.slimLoader.complete();
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.slimLoader.complete();
          });
      }
      else {
        this.slimLoader.complete();
        this.gopayment();
      }
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }

  }
  cancelbtn() {
    this.navCtrl.pop();
  }

  gopayment() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter;
    if (this.passTemp.processingCode == "080400") {
      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.detail.merchantid,
        processingCode: this.detail.processingCode,
        amount: this.detail.amount,
        refNumber: this.detail.cardno,
        field1: this.detail.did,
        field2: this.detail.pid,
        field3: this.detail.peroid,
        field4: this.detail.tid,
        field5: this.detail.accessToken,
        field6: this.detail.packagetype,
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.amountServiceCharges,
        amountTax: this.detail.amountTax,
        amountTotal: this.detail.amountTotal,
        paymentType: '1',
        narrative: '',
        sKey: this.passSKey
      };
    }
    else if (this.passTemp.processingCode == "050200") {
      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.passTemp.merchantID,
        processingCode: this.passTemp.processingCode,
        amount: this.detail.amount,
        refNumber: this.detail.refNo,
        field1: '',
        field2: this.detail.billTypeValue,
        field3: '',
        field4: '',
        field5: '',
        field6: '',
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.bankCharges,
        amountTax: this.detail.cnpCharge,
        amountTotal: this.detail.amountTotal,
        paymentType: '1',
        narrative: this.detail.narrative,
        sKey: this.passSKey
      };
      
    }
    else if (this.passTemp.processingCode == "090500") {

      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.passTemp.merchantID,
        processingCode: this.passTemp.processingCode,
        amount: this.detail.amount,
        refNumber: this.detail.outstandBill,
        field1: '',
        field2: this.detail.meterNo,
        field3: '',
        field4: '',
        field5: '',
        field6: this.detail.referenceType,
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.bankCharges,
        amountTax: this.detail.penalty,
        amountTotal: this.detail.totalAmount,
        paymentType: '1',
        narrative: this.detail.narrative,
        sKey: this.passSKey
      };
    }
    else if (this.passTemp.processingCode == "040300") {
      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.passTemp.merchantID,
        processingCode: this.passTemp.processingCode,
        amount: this.detail.amount,
        refNumber: this.detail.phone,
        field1: '',
        field2: '',
        field3: '',
        field4: this.detail.narrative,
        field5: '',
        field6: this.detail.referenceType,
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.bankCharges,
        amountTax: '',
        amountTotal: this.detail.amountTotal,
        sKey: this.passSKey
      };
    }
    else if (this.passTemp.processingCode == "000100") {
      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.passTemp.merchantID,
        processingCode: this.passTemp.processingCode,
        amount: this.detail.amount,
        refNumber: this.detail.invoiceNo,
        field1: '',
        field2: '',
        field3: '',
        field4: this.detail.narrative,
        field5: '',
        field6: this.detail.customerNo,
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.bankCharges,
        amountTax: '',
        amountTotal: this.detail.amountTotal,
        sKey: this.passSKey
      };
    }
    else if (this.passTemp.processingCode == "030001") {
      parameter = {
        userID: this.userdata.userID,
        sessionID: this.userdata.sessionID,
        bankAcc: this.detail.accounttype,
        merchantID: this.passTemp.merchantID,
        processingCode: this.passTemp.processingCode,
        amount: this.detail.amount,
        refNumber: '',
        field1: '',
        field2: this.detail.quantity,
        field3: this.detail.price,
        field4: this.detail.narrative,
        field5: '',
        field6: this.detail.referenceType,
        field7: '',
        field8: '',
        field9: '',
        field10: '',
        amountServiceCharges: this.detail.bankCharges,
        amountTax: '',
        amountTotal: this.detail.amountTotal,
        sKey: this.passSKey
      };
    }

    
    this.http.post(this.ipaddress + '/service003/goPayment', parameter).map(res => res.json()).subscribe(res => {
      
      if (res.code == "0000") {
        this.loading.dismiss();
        this.navCtrl.setRoot(Merchantsuccess, {
          data: res,
          detail: this.detail,
          detailmerchant: this.passTemp
        })

      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        // this.slimLoader.complete();
        this.loading.dismiss();
      }
      else {

        this.loading.dismiss();
        this.navCtrl.setRoot(MerchantErrorPage, {
          data: res,
          detail: this.detail,
          detailmerchant: this.passTemp
        });

      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }
  gologout() {
    /* this.loading = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange: true
       //   duration: 3000
     });
     this.loading.present();*/
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        
        if (data.code == "0000") {
          // this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    });
  }
}
