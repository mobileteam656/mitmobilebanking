import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { BeneficiaryEditPage } from '../beneficiary-edit/beneficiary-edit';
import { BeneficiaryListPage } from '../beneficiary-list/beneficiary-list';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { BeneficiaryDeleteSuccessPage } from '../beneficiary-delete-success/beneficiary-delete-success';

/**
 * Generated class for the BeneficiaryInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-beneficiary-info',
  templateUrl: 'beneficiary-info.html',
  providers: [ChangelanguageProvider]
})
export class BeneficiaryInfoPage {
  beneficiaryData: any;
  userData: any;
  ipaddress: any;
  font: any;
  textData: any = [];
  bankAddress: any;
  textMyan = ["လက်ခံသူ အချက်အလက်", "လက်ခံသူ အမည်တို", "လက်ခံသူ အမည်", "လက်ခံသူ အကောင့်နံပါတ်", "လက်ခံသူ အီးမေးလ်", "လက်ခံသူ ဖုန်းနံပါတ်", "ဖျက်မည်", "ပြင်မည်", "လက်ခံသူ အမျိုးအစား", "Branch", "ဘဏ်အမည်", "ဘဏ်ကုဒ်", "ဘဏ်လိပ်စာ", "လက်ခံသူ လိပ်စာ"];
  textEng = ["Beneficiary Info", "Beneficiary Short Name", "Beneficiary Name", "Beneficiary Account Number", "Beneficiary Email", "Beneficiary Phone", "DELETE", "EDIT", "Beneficiary Type", "Branch", "Bank Name", "Bank Code", "Bank Address", "Beneficiary Address"];
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle,
    public http: Http, public changefont: Changefont, public all: AllserviceProvider, public global: GlobalProvider, public events: Events, public navParams: NavParams, public changeLanguage: ChangelanguageProvider,
    public storage: Storage, public alertCtrl: AlertController, private slimLoader: SlimLoadingBarService) {
    this.beneficiaryData = this.navParams.get('data');
    this.bankAddress = '';
    if (this.beneficiaryData.bankAddress1 != '') {
      this.bankAddress += this.beneficiaryData.bankAddress1
    }
    if (this.beneficiaryData.bankAddress2 != '') {
      this.bankAddress += ", " + this.beneficiaryData.bankAddress2
    }
    if (this.beneficiaryData.bankAddress3 != '') {
      this.bankAddress += ", " + this.beneficiaryData.bankAddress3
    }
    if (this.beneficiaryData.bankAddress4 != '') {
      this.bankAddress += ", " + this.beneficiaryData.bankAddress4
    }
    //console.log("Beneficiary Data is here : " + JSON.stringify(this.beneficiaryData));
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
      });
    });

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
  }

  delete() {
    this.slimLoader.start(() => {
    });
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID, beneficiaryRef: this.beneficiaryData.beneficiaryRef };
    this.http.post(this.ipaddress + '/service002/deleteBeneficiary', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        this.slimLoader.complete();
        this.navCtrl.setRoot(BeneficiaryDeleteSuccessPage, {
          data: result,
        })
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.slimLoader.complete();
      }
      else {
        this.all.showAlert("Warning!", result.desc);
        this.slimLoader.complete();
      }
    },
      error => {
        this.all.showAlert("Warning!", this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  edit() {
    this.navCtrl.push(BeneficiaryEditPage, {
      data: this.beneficiaryData
    })
  }
  confirmDelete() {
    let alert = this.alertCtrl.create({
      title: 'Confirmation!',
      enableBackdropDismiss: false,
      message: 'Are you sure to delete?',
      buttons: [{
        text: 'No',
        handler: () => {
          alert.dismiss;
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.delete();
        }
      }],
      cssClass:'confirmAlert'
    });
    alert.present();
  }
}
