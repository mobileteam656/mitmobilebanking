import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { TopUpSuccessPage } from '../top-up-success/top-up-success';
import { TopuperrorPage } from '../topuperror/topuperror';

declare var window: any;
declare var cordova: any;

@Component({
  selector: 'page-topupotp',
  templateUrl: 'topupotp.html',
  providers: [ChangelanguageProvider]
})
export class TopupotpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Top Up - Verify"];
  userData: any;
  ipaddress: string;
  smsArived: any;
  otpcode: any = '';
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  topUpData: any;
  merchantIno: any;
  loading: any;
  font: string;
  passSKey: any;
  showFont: string[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public storage: Storage,public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, public network: Network, public idle: Idle, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    this.slimLoader.start(() => {
    });
    this.topUpData = this.navParams.get('data');
    this.merchantIno = this.navParams.get('detail');//rkey
    this.passSKey = this.navParams.get('sKey');
    //  this.passtemp3 = this.navParams.get('detailmerchant');
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      
      this.storage.get('ipaddress').then((result) => {
        
        if (result == null || result == '') {
          
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
      this.slimLoader.complete();
    }) */
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }
  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TopupotpPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'TopupotpPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  getconfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      let param = {
        userID: this.userData.userID,
        sessionID: this.userData.sessionID,
        rKey: this.merchantIno['rKey'],
        otpCode: this.otpcode,
        sKey: this.passSKey
      };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.goPayment();
        } else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
          //   this.slimLoader.complete();
        } else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
          // this.slimLoader.complete();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  doYourStuff() {
    // this.navCtrl.setRoot(MerchantlistPage);
  }

  goPayment() {
    let param;
    let phoneNo;
    if (this.merchantIno.processingCode == "050200") {
      phoneNo = this.topUpData.phoneNo.substring(4);
    }
    else if (this.merchantIno.processingCode == "040300") {
      phoneNo = this.topUpData.phoneNo.substring(4);
      phoneNo = "09" + phoneNo;
    }
    else {
      phoneNo = this.topUpData.phoneNo.replace(/\+/g, '');
    }
    if (this.merchantIno.processingCode == '050200') {
      param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID,
        "bankAcc": this.topUpData.accounttype,
        "merchantID": this.merchantIno.merchantID,
        "processingCode": this.merchantIno.processingCode,
        "amount": this.topUpData.amount,
        "refNumber": phoneNo,
        "field1": "",
        "field2": this.topUpData.value,
        "field3": "",
        "field4": "",
        "field5": "",
        "field6": "",
        "field7": "",
        "field8": "",
        "field9": "",
        "field10": "",
        "amountServiceCharges": "",
        "amountTax": "",
        "amountTotal": this.topUpData.amount,
        "paymentType": "2",
        "narrative": this.topUpData.narrative,
        "sKey": this.passSKey
      };
    } else if (this.merchantIno.processingCode == '040300') {
      param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID,
        "bankAcc": this.topUpData.accounttype,
        "merchantID": this.merchantIno.merchantID,
        "processingCode": this.merchantIno.processingCode,
        "amount": this.topUpData.amount,
        "refNumber": phoneNo,
        "field1": "",
        "field2": this.topUpData.value,
        "field3": this.topUpData.amountType,
        "field4": "",
        "field5": "",
        "field6": "",
        "field7": "",
        "field8": "",
        "field9": "",
        "field10": "",
        "amountServiceCharges": "",
        "amountTax": "",
        "amountTotal": this.topUpData.amount,
        "paymentType": "2",
        "narrative": this.topUpData.narrative,
        "sKey": this.passSKey
      };
    }
    
    this.http.post(this.ipaddress + '/service003/goTopup', param).map(res => res.json()).subscribe(res => {
      
      this.loading.dismiss();
      if (res.code == "0000") {
        this.slimLoader.complete();
        this.topUpData['processingCode'] = this.merchantIno.processingCode;
        this.topUpData['colorCode'] = this.merchantIno.colorCode;
        this.topUpData['bankRefNumber'] = res.bankRefNumber;
        this.navCtrl.setRoot(TopUpSuccessPage, {
          data: res,
          detail: this.topUpData

        })

      }
      else if (res.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: res.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        // this.slimLoader.complete();
        this.loading.dismiss();
      }
      else {
        this.slimLoader.complete();
        this.navCtrl.setRoot(TopuperrorPage, {
          data: res,
          detail: this.topUpData
        });
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
        // this.slimLoader.complete();
      });
  }

  sendOtpCode() {
    this.checkNetwork();
    this.checkNetwork();
    if (this.flag == "success") {
      this.slimLoader.start(() => {
      });
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '2', merchantID: this.merchantIno.merchantID, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.merchantIno['rKey'] = data.rKey;
          //this.goPayment();
          // this.loading.present();
          /* window.SMS.startWatch(function () {
          }, function () {
          });
          setTimeout(() => {
          }, 8000);
          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;
            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];
            this.slimLoader.complete();
          }); */
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                  this.storage.remove('userData');
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.events.publish('login_success', false);
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  ionViewDidLoad() {
    this.checkNetwork();
  }

  ionViewDidLeave() {
    //this.slimLoader.reset();
  }

}
