import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { TaxPaymentFinishPage } from '../tax-payment-finish/tax-payment-finish';

declare var window: any;

@Component({
  selector: 'page-tax-payment-otp',
  templateUrl: 'tax-payment-otp.html',
})
export class TaxPaymentOtpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  userData: any;
  ipaddress: string;
  smsArived: any;
  otpcode: any = '';
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  taxData: any;
  loading: any;
  font: string;
  showFont: string[] = [];
  sKey: any;
  rKey: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public storage: Storage, public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, public network: Network, public idle: Idle, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    /* this.slimLoader.start(() => {
    }); */
    this.taxData = this.navParams.get('data');
    this.sKey = this.navParams.get('sKey');
    this.rKey = this.navParams.get('rKey');
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
     //this.slimLoader.complete();
    }) */
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'MptTopupOtpPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'MptTopupOtpPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  getConfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Processing...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = {
        userID: this.userData.userID,
        sessionID: this.userData.sessionID,
        rKey: this.rKey,
        otpCode: this.otpcode,
        sKey: this.sKey
      };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.goTopUp();
        } else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        } else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  goTopUp() {
    let param = {
      "userID": this.userData.userID,
      "sessionID": this.userData.sessionID,
      "bankAcc": this.taxData.fromAcc,
      "amount": this.taxData.taxAmount,
      "merchantID": this.taxData.merchantID,
      "processingCode": this.taxData.processingCode,
      "refNumber": this.taxData.phoneNumber,
      "field1": "",
      "field2": "2",
      "field3":this.taxData.tinNo,
      "field4":this.taxData.profileName,
      "field5":this.taxData.mainBusiness,
      "field6":this.taxData.companyType,
      "field7":this.taxData.industryCode,
      "field8":this.taxData.phoneNumber,
      "amountServiceCharges": "",
      "paymentType": "1",
      "narrative": this.taxData.narrative,
      "sKey": this.sKey
    }
    this.http.post(this.ipaddress + '/service003/goTaxPayment', param).map(res => res.json()).subscribe(res => {
      this.loading.dismiss();
      if (res.code == "0000") {
        this.navCtrl.setRoot(TaxPaymentFinishPage, {
          data: res,
          detail: this.taxData
        })
      }
      else if (res.code == "0016") {
        this.logoutAlert(res.desc);
        this.loading.dismiss();
      }
      else {
       //this.slimLoader.complete();
        this.navCtrl.setRoot(TaxPaymentFinishPage, {
          data: res,
          detail: this.taxData
        });
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  sendOtpCode() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '1', merchantID: this.taxData.merchantID, sKey: this.sKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.rKey = data.rKey;
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  ionViewDidLoad() {
    this.checkNetwork();
  }

  ionViewDidLeave() {
   // this.slimLoader.reset();
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
