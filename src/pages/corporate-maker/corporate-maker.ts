import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, Platform, PopoverController, Select } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { CorporateMakerComfirmPage } from '../corporate-maker-comfirm/corporate-maker-comfirm';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
declare var window;
@Component({
  selector: 'page-corporate-maker',
  templateUrl: 'corporate-maker.html',
  providers: [ChangelanguageProvider]
})
export class CorporateMakerPage {  
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", "အကြောင်းအရာ", "လွှဲပြောင်းမည်", "ကော်ပိုရိတ် ငွေလွှဲရန်", "ပြန်စမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "အမည်"];
  textEng: any = ["From Account No.", "To  Account No.", "Amount", "Narrative", "TRANSFER", "Corporate Transfer", "RESET", "Account Balance", "Name"];
  textErrorEng: any = ["Please choose account number.", "Please fill account number.", "Please fill amount.", "Shouldn't match from account no. and to account no.", "Please check Account Number", "Insufficient Balance"];
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရိုက်ထည့်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ငွေပေးမည့်အကောင့်နှင့် ငွေလက်ခံမည့်အကောင့် တူ၍မရပါ", "အကောင့်နံပါတ် စစ်ဆေးပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
  textError: string[] = [];
  textData: any = [];
  font: any = '';
  modal: any;
  hardwareBackBtn: any = true;

  userdata: any;
  ipaddress: string;
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  transferData: any = {};
  transferData1: any = {};
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  popover: any;
  txnType: any;
  value: any;
  beneficiaryList: any;
  reqData: any = { "value": [], "type": 0, "title": "", 'ans': '' };
  data: any;
  isChecked: boolean;
  accountBal: any;
  amountBal: any;
  alertPresented: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public global: GlobalProvider, public all: AllserviceProvider, public util: UtilProvider) {
    this.txnType = 'internal';
    this.accountBal = "";
    this.amountBal = "";
    this.alertPresented = false;
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        }
        else {
          this.ipaddress = result;
        }
        this.getAccountSummary();
      })
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      })
    });
  }
  goNext() {
    let f1 = false, f2 = false, f3 = false;
    if (this.transferData.fromAcc != null && this.transferData.fromAcc != '' && this.transferData.fromAcc != undefined) {
      this.fromaccMsg = '';
      f1 = true;
    }
    else {
      this.fromaccMsg = this.textError[0];
      f1 = false;
    }
    if (this.transferData.toAcc != null && this.transferData.toAcc != '' && this.transferData.toAcc != undefined) {
      if (this.transferData.fromAcc == this.transferData.toAcc) {
        this.toaccMsg = this.textError[3];
        f2 = false;
      } else if (this.isChecked == false) {
        this.toaccMsg = this.textError[4];
        f2 = false;
      } else {
        this.toaccMsg = '';
        f2 = true;
      }
    }
    else {
      this.toaccMsg = this.textError[1];
      f2 = false;
    }
    if (this.transferData.amount != null && this.transferData.amount != '' && this.transferData.amount != undefined) {
      let amount = parseInt(this.transferData.amount);
      if (this.transferData.amount.substring(0, 1) == "-" || amount == 0) {
        this.amountcMsg = this.textError[2];
        f3 = false;
      } else if (isNaN(amount)) {
        this.amountcMsg = this.textError[2];
        f3 = false;
      } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.transferData.amount)) {
        this.amountcMsg = this.textError[5];
        f3 = false;
      } else {
        this.amountcMsg = '';
        f3 = true;
      }
    }
    else {
      this.amountcMsg = this.textError[2];
      f3 = false;
    }
    if (f1 && f2 && f3) {
      this.transferData.fromName = this.userdata.userName;
      this.transferData.toName = this.transferData.name;
      for(let i = 0 ; i< this.fromAccountlist.length ; i++){
          if(this.transferData.fromAcc == this.fromAccountlist[i].depositAcc){
            this.transferData.accType = this.fromAccountlist[i].accType;
          }
      }
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      this.navCtrl.push(CorporateMakerComfirmPage, {
        data: this.transferData,
        //type: this.txnType,
        //otp: data.rKey,
        //sKey: data.sKey,
        //fromPage: 'corporate-maker'
      });
    }
  }

  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    else if (s == 2) {
      this.toaccMsg = '';
    }
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
      if (this.fromaccMsg != '') {
        this.fromaccMsg = this.textError[0];
      }
      if (this.toaccMsg != '') {
        this.toaccMsg = this.textError[1];
      }
      if (this.amountcMsg != '') {
        this.amountcMsg = this.textError[2];
      }
    }
  }

  reset() {
    this.transferData.fromAcc = "";
    this.transferData.toAcc = "";
    this.transferData.amount = "";
    this.transferData.refNo = "";
    this.transferData.name = "";
    this.fromaccMsg = '';
    this.toaccMsg = '';
    this.amountcMsg = '';
    this.accountBal = "";
    this.isChecked = false;
    this.amountBal = "";
  }

  ionViewWillEnter() {
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      console.log(JSON.stringify("Account list" + result.dataList));
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  checkAccount() {
    let flag = false;
    if (this.transferData.toAcc != undefined && this.transferData.toAcc != '') {
      flag = true;
    } else {
      flag = false;
      this.toaccMsg = this.textError[1];
    }
    if (flag) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      this.toaccMsg = '';
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, accountNo: this.transferData.toAcc, type: 1 };
      this.http.post(this.ipaddress + '/service002/checkAccount', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          console.log(JSON.stringify("check account"+data));
          this.isChecked = true;
          this.loading.dismiss();
          this.transferData.name = data.accountName;
          this.transferData.branch = data.branch;
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          this.logoutAlert('Warning!', data.desc);
          this.transferData.name = '';
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  }

  inputChange(data) {
    if (data == 1) {
      this.isChecked = false;
      this.toaccMsg = "";
    } else if (data == 2) {
      this.amountcMsg = "";
    }
  }

  logoutAlert(title, message) {
    let vm = this
    if (!vm.alertPresented) {
      vm.alertPresented = true
      vm.alertCtrl.create({
        title: title,
        message: message,
        buttons: [{
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
            vm.alertPresented = false
          }
        }],
        cssClass: 'warningAlert',
        enableBackdropDismiss: false
      }).present();
    }
  }  
}

