import { Component } from '@angular/core';
import { NavController, NavParams, Platform, Events, ToastController, LoadingController, AlertController, PopoverController } from 'ionic-angular';
import { ProductDetailPage } from '../product-detail/product-detail';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { UtilProvider } from '../../providers/util/util';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Idle } from '@ng-idle/core';
import { Http } from '@angular/http';
import { Changefont } from '../changefont/changeFont';
  

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  textEng: any = ["Product"];
  textMyan: any = ["ထုတ်ကုန်များ"];
  showFont: any = [];
  font: string;
  ststus: any;
  userdata: any;
  ipaddress: any;
  loading: any;
  title: any;
  description: any;
  productlist: any = [];
  play: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public changefont: Changefont, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
    public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService,
    public util: UtilProvider, public changeLanguage: ChangelanguageProvider) {
    // this.productdata();
    //this.ststus = this.navParams.get("ststus");
    // if (this.ststus == '1') {
      // this.storage.get('userData').then((val) => {
      //   this.userdata = val;
      //   this.storage.get('ipaddress').then((result) => {
      //     if (result == null || result == '') {
      //       this.ipaddress = this.global.ipaddress;
      //     } else {
      //       this.ipaddress = result;
      //     }

       
      //   })
      // });
    //}

    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = result;
      }

      this.productdata();
    })

    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });

    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }
  doRefresh(refresher) {
    setTimeout(() => {
      this.productdata();
      refresher.complete();
    }, 2000);
  }
  productdata() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: '', sessionID: '' }; //'http://122.248.120.16:8080/AppService/module001'
    this.http.post( this.ipaddress + '/service006/getProduct', param).map(res => res.json()).subscribe(result => {
      this.loading.dismiss();
      console.log(result);
      if (result.code == "0000") {
        if(result.data==null || result.data==undefined){
          this.play=true;
          this.productlist="";
          this.productlist.title="Sorry,There is no data"
          this.all.showAlert('Warning!', result.desc);
          this.loading.dismiss;
        }else{
          let tempArray = [];
          if (!Array.isArray(result.data)) {
            tempArray.push(result.data);
            result.data = tempArray;
          }
          this.play=false;
          this.productlist = result.data;
          this.loading.dismiss();
        }
        
      }
      else if (result.code == "0016") {
        // console.log("No Data")
      }
      else {
        // this.all.showAlert('Warning!', result.desc);
        // this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });


  }

  public productdetail(d) {
    this.navCtrl.push(ProductDetailPage, { data: d });
    //console.log(d);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ProductPage');
  }

  ionViewCanEnter() {
    // console.log('ionViewCanEnter ProductPage');
    // console.log(''+this.showFont[0]);
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        //this.textFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        //this.textFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }
}
