import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangepasswordotpPage } from '../changepasswordotp/changepasswordotp';
import { Login } from '../login/login';
import { MainHomePage } from '../main-home/main-home';
import { GlobalProvider } from '../../providers/global/global';
declare var window;

@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
  providers: [ChangelanguageProvider]
})
export class ChangepasswordPage {
  result = { currentpsw: '', newpsw: '', confirmpsw: '' };
  errormessagetext: string;
  public loading;
  userdata = { userID: '', sessionID: '' };
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  data = { userid: '', sessionid: '', phoneno: '', oldpsw: '', confirmpsw: '', rKey: '' };
  font: string;
  textEng: any = ['Change Password', 'Old Password', 'New Password', 'Confirm Password', 'Change', 'Enter old password', 'Enter new password', 'Enter confirm password', 'Old password and New password should not match', 'Password do not match', 'Please log in with your new password.', 'OK'];
  textMyan: any = ['လျှို့ဝှက်နံပါတ်ပြောင်းမည်', 'လက်ရှိနံပါတ်', 'နံပါတ်အသစ်', 'အတည်ပြုနံပါတ်', 'ပြောင်းလဲမည်', 'လက်ရှိနံပါတ်ရိုက်သွင်းပါ', 'နံပါတ်အသစ်ရိုက်သွင်းပါ', 'အတည်ပြုနံပါတ်ရိုက်သွင်းပါ', 'လက်ရှိနံပါတ်နှင့်ပြောင်းလဲမည့်နံပါတ်အသစ်မတူညီရပါ', 'နံပါတ်အသစ်နှင့်အတည်ပြုနံပါတ်မတူညီပါ', 'Please log in with your new password.', 'OK'];
  showFont: any = [];
  pdata = {
    'userid': '', 'sessionid': '', "pswminlength": "", "pswmaxlength": "", "spchar": "",
    "upchar": "", "lowerchar": "", "pswno": "", "msgCode": "", "msgDesc": ""
  };
  passOtp: any;
  passSKey: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public storage: Storage, public idle: Idle,
    private slimLoader: SlimLoadingBarService, public all: AllserviceProvider, public global: GlobalProvider, public changeLanguage: ChangelanguageProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
    this.storage.get('language').then((font) => {

      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
  }

  /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  inputChange(data) {
    this.errormessagetext = "";
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {

      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };


      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }

  changePassword() {

    if (this.result.currentpsw == '' || this.result.currentpsw == undefined || this.result.currentpsw == null) {
      this.errormessagetext = this.showFont[5];
    } else if (this.result.newpsw == '' || this.result.newpsw == undefined || this.result.newpsw == null) {
      this.errormessagetext = this.showFont[6];
    } else if (this.result.confirmpsw == '' || this.result.confirmpsw == undefined || this.result.confirmpsw == null) {
      this.errormessagetext = this.showFont[7];
    }
    else if (this.result.currentpsw == this.result.newpsw) {
      this.errormessagetext = this.showFont[8];
    }
    else {
      this.errormessagetext = '';
      if (this.result.newpsw != this.result.confirmpsw) {
        this.errormessagetext = this.showFont[9];
      } else {
        this.changePwdGetOTPCode();
        /*
        */

        //work process
      }
    }

  }

  changePwdGetOTPCode() {
    this.errormessagetext = '';
    if (this.result.newpsw != this.result.confirmpsw) {
      this.errormessagetext = this.showFont[9];
    } else {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: '10', merchantID: '' };

      this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.loading.dismiss();
          this.passOtp = data.rKey;
          this.passSKey = data.sKey;
          if (this.passOtp == 'true') {
            this.slimLoader.start(() => {

            });
            this.errormessagetext = "";
            var param = {
              userID: this.userdata.userID,
              sessionID: this.userdata.sessionID,
              type: '',
              sKey: this.passSKey
            };

            this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(response => {

              if (response.code == "0000") {
                this.data.phoneno = this.userdata.userID;
                this.data.oldpsw = this.result.currentpsw;
                this.data.confirmpsw = this.result.newpsw;
                this.data.rKey = response.rKey;
                this.navCtrl.push(ChangepasswordotpPage, {
                  data: this.data,
                  sKey: this.passSKey
                });
                // this.loading.dismiss();
                this.slimLoader.complete();
              }
              else if (response.code == "0016") {
                let confirm = this.alertCtrl.create({
                  title: 'Warning!',
                  enableBackdropDismiss: false,

                  message: response.desc,
                  buttons: [
                    {
                      text: 'OK',
                      handler: () => {

                        this.loading.dismiss();
                        this.events.publish('login_success', false);
                        this.events.publish('lastTimeLoginSuccess', '');
                        this.storage.remove('userData');
                        this.navCtrl.setRoot(Login, {
                        });
                        this.navCtrl.popToRoot();
                      }
                    }
                  ],
                  cssClass: 'confirmAlert'
                })
                confirm.present();
                //  this.loading.dismiss();
                this.slimLoader.complete();
              }
              else {
                let toast = this.toastCtrl.create({
                  message: response.desc,
                  //duration: 3000,
                  position: 'bottom',
                  showCloseButton: true,
                  dismissOnPageChange: true,
                  closeButtonText: 'OK'
                });
                toast.present(toast);
                //   this.loading.dismiss();
                this.slimLoader.complete();
              }

            },
              error => {
                let toast = this.toastCtrl.create({
                  message: this.all.getErrorMessage(error),
                  duration: 3000,
                  position: 'bottom',
                  //  showCloseButton: true,
                  dismissOnPageChange: true,
                  // closeButtonText: 'OK'
                });
                toast.present(toast);
                this.slimLoader.complete();
                //  this.loading.dismiss();
              });

            //work process
          } else {
            this.slimLoader.complete();
            this.changePasswordService();
          }
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });

    }
  }

  changePasswordService() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs();
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "rKey": "",
      "otpCode": "",
      "oldPassword": this.all.getEncryptText(iv, salt, dm, this.result.currentpsw),
      "newPassword": this.all.getEncryptText(iv, salt, dm, this.result.newpsw),
      "sKey": this.passSKey,
      "iv": iv, "dm": dm, "salt": salt
    };
    this.http.post(this.ipaddress + '/service001/changePassword', param).map(res => res.json()).subscribe(response => {
      if (response.code == "0000") {
        let confirm = this.alertCtrl.create({
          title: 'Your password has been changed',
          message: this.showFont[10], // Your password has been reset
          buttons: [
            {
              text: this.showFont[11],
              handler: () => {
                this.gologout();
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else if (response.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          message: response.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.loading.dismiss();
                this.events.publish('login_success', false);
                this.storage.remove('userData');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: response.desc,
          //  duration: 3000,
          position: 'bottom',
          showCloseButton: true,
          dismissOnPageChange: true,
          closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  callIT() {
    let passedNumber = encodeURIComponent(this.global.phone);
    window.location = "tel:" + passedNumber;
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChangepasswordPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ChangepasswordPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        //    
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }

    });
    this.reload();
  }
  readPasswordPolicy() {
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "pswminlength": "",
      "pswmaxlength": "",
      "spchar": "",
      "upchar": "",
      "lowerchar": "",
      "pswno": "",
      "msgCode": "",
      "msgDesc": ""
    };

    this.http.post(this.ipaddress + '/service001/readPswPolicy', param).map(res => res.json()).subscribe(response => {

      if (response.msgCode == "0000") {
        this.pdata = response;

      }
      else if (response.msgCode == "0016") {

      }
      else {

      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }
  ionViewDidLoad() {

    // // this.idleWatch();
    this.errormessagetext = '';
    this.storage.get('userData').then((val) => {


      this.userdata.userID = val.userID;
      this.userdata.sessionID = val.sessionID;
      //  // this.idleWatch();
      this.storage.get('ipaddress').then((ipaddress) => {

        this.ipaddress = ipaddress;
        this.readPasswordPolicy();
      });
    });

  }
  ionViewDidLeave() {
    this.slimLoader.reset();
  }

  backButton() {
    this.navCtrl.setRoot(MainHomePage);
  }
}

