import 'rxjs/add/operator/map';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
  Select,
} from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ChequelistPage } from '../chequelist/chequelist';
import { ChequesuccessPage } from '../chequesuccess/chequesuccess';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { PopoverPage } from '../popover-page/popover-page';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
/**
 * Generated class for the ChequePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-cheque',
  templateUrl: 'cheque.html',
  providers: [ChangelanguageProvider]
})
export class ChequePage {
  @ViewChild('myselect') select: Select;
  userData: any;
  ipaddress: string;
  loading: any;
  useraccount: any;
  chequeNo: any;
  cheacc: any;
  cheqaccountMsg: string;
  accounterrorMsg: string;
  accountlist: any;
  font: string;
  popover: any;
  accountBal: any;
  textEng: any = ['Select Account', 'Cheque Number', 'Choose Account Number', 'Enter Cheque Number', 'Are you sure you want to exit', 'Yes', 'No', 'Cancel', 'Submit', 'Cheque Status Inquiry', "Account Balance"];
  textMyan: any = ['စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ချက်နံပါတ်', 'အကောင့်နံပါတ် ရွေးချယ်ပါ', 'ချက်နံပါတ် ရိုက်ထည့်ပါ', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ', 'ပယ်ဖျက်သည်', 'လုပ်ဆောင်မည်', "ချက်အခြေအနေ စုံစမ်းခြင်း", "ဘဏ်စာရင်းလက်ကျန်ငွေ"];
  showFont: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public storage: Storage, public events: Events, public changeLanguage: ChangelanguageProvider,
    public http: Http, public all: AllserviceProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public platform: Platform, public popoverCtrl: PopoverController, public global: GlobalProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      this.storage.get('userData').then((data) => {
        this.userData = data;
        this.getCustomerAccount();
      });
    });
  }

  getCustomerAccount() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service001/getChequeAccounts', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.dataList)) {
          tempArray.push(data.dataList);
          data.dataList = tempArray;
        }
        this.loading.dismiss();
        this.accountlist = data.dataList;
      }
      else {
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  cancelbtn() {
    this.navCtrl.setRoot(ChequelistPage)
  }

  inputChange(data) {
    this.cheqaccountMsg = '';
  }

  accountChange(account) {
    this.accounterrorMsg = '';
    for (let i = 0; i < this.accountlist.length; i++) {
      if (account == this.accountlist[i].chequeAcc) {
        this.accountBal = this.accountlist[i].avlBal + " " + this.accountlist[i].ccy
      }
    }
  }
  
  okbtn() {
    let flag1 = true, flag2 = true;
    if (this.useraccount == '' || this.useraccount == undefined) {
      this.accounterrorMsg = this.showFont[2];
      flag1 = false;
    }
    if (this.chequeNo == '' || this.chequeNo == undefined) {
      this.cheqaccountMsg = this.showFont[3];
      flag2 = false;
    }

    if (flag1 && flag2) {
      this.loading = this.loadingCtrl.create({
        content: "Pleas Wait...",
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID,
        "accountNo": this.useraccount,
        "chequeNo": this.chequeNo
      };
      this.http.post(this.ipaddress + '/service001/enquiryCheque', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          param["status"] = data.status;
          this.navCtrl.push(ChequesuccessPage, { data: param });
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', error);
          this.loading.dismiss();
        });
    }
  }

  ionViewDidLoad() {
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }
}
