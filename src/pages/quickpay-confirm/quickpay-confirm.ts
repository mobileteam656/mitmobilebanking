import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { QuickpaySuccessPage } from '../quickpay-success/quickpay-success';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-quickpay-confirm',
  templateUrl: 'quickpay-confirm.html',
})
export class QuickpayConfirmPage {

  textMyan: any = ["Bill Aggregator Confirm", "Merchant Name", "Customer(Ref No)", "Company/Customer Name", "ငွေပမာဏ", "ပယ်ဖျက်မည်", "ပေးမည်", "အကောင့်ရွေးချယ်ပါ", "နေရပ်လိပ်စာ", "အမည်", "ငွေပမာဏ", "မှတ်ပုံတင်", "ဖုန်းနံပါတ်", "ကျေးဇူးပြု၍ အချက်အလက်များ ဖြည့်စွက်ပါ။", "အကြောင်းအရာ", "အကောင့်နံပါတ် မှ"];
  textEng: any = ["Bill Aggregator Confirm", "Merchant Name", "Customer(Ref No)", "Company/Customer Name", "Amount", "Cancel", "Pay", "Select Account", "Address", "Name", "Amount", "NRC", "Phone No", "Please fill this field.", "Narrative", "From Account"];
  showFont: string[] = [];
  billerid = "";

  userData: any;
  errormsg: any;
  refNo: any;
  name: any;
  amount: any;
  selectAccount: any;
  userdata: any;
  ipaddress: any;
  passTemp: any;
  passType: any;
  logoutAlert: any;
  transferData: any = {};
  transferData1: any = {};
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  accountBal: any;
  amountBal: any;
  popover: any;
  txnType: any;
  value: any;
  _data = {
    refNo: "P0003", name: "", amount: "", account: "", waveAcc: "", nrc: "", phoneNo: "", description: ""
  }
  font: any = '';
  data: any;
  loading: any;
  flag: string;
  detail: any;
  passOtp: any;
  passSKey: any;
  fromPage: any; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController, public all: AllserviceProvider, public util: UtilProvider, public idle: Idle, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public changefont: Changefont,
    private firebaseAnalytics: FirebaseAnalytics) {



    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
    this.billerid = this.navParams.get("billerid");
    this.storage.get('userData').then((data) => {
      this.userdata = data;

      this.storage.get('ipaddress').then((result) => {

        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;
        }
        this.getAccountSummary();
      });
    });
  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad QuickpayPage');
  }

  goCancel() {
    this.navCtrl.pop();
  }

  goPay() {
    if (this._data.refNo == "" || this._data.name == "" || this._data.amount == "") {
      this.errormsg = this.showFont[13];
    } else {
      this.errormsg = "";
      this.navCtrl.push(QuickpaySuccessPage, {
        billerid: this.billerid,
        data: this._data
      });
    }

    // this.navCtrl.push(QuickpaySuccessPage);
  }

  payment() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = {
      userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: this.transferData.fromAcc,
      toAccount: "0020101100037458", amount: this._data.amount, bankCharges: 0, narrative: "", refNo: '',
      field1: '2', field2: '',
    };
    this.http.post(this.ipaddress + '/service003/goAgentTransfer', parameter).map(res => res.json()).subscribe(data1 => {
      // this.firebase.logEvent('quick_pay', {})
      //     .then((res: any) => { console.log(res); })
      //     .catch((error: any) => console.log(error));

      this.firebaseAnalytics.logEvent('quick_pay', {})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));

      if (data1.code == "0000") {
        this.loading.dismiss();
        /* this.navCtrl.setRoot(AccountTransferDetail, {
          data: data1,
          detail: this.passTemp,
          type: this.passType,
          fromPage: this.fromPage
        }) */
        this.navCtrl.push(QuickpaySuccessPage, {
          billerid: this.billerid,
          data: this._data
        });
      }
      else if (data1.code == "0016") {
        this.logoutAlert(data1.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data1.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert('Warning!', result.desc);        
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  }

}
