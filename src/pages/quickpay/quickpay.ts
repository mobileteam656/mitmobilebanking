import 'rxjs/Rx';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  App,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  PopoverController,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { ChoosetopupPage } from '../choosetopup/choosetopup';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { GlobalProvider } from '../../providers/global/global';
import { QuickpayConfirmPage } from '../quickpay-confirm/quickpay-confirm';

@Component({
  selector: 'page-quickpay',
  templateUrl: 'quickpay.html',
})
export class QuickpayPage {

  textMyan: any = ["Bill Aggregator", "Payment Type", "ငွေပေးချေမှုရွေးရန်", "လုပ်ဆောင်မည်", "ပြန်စမည်","ကျေးဇူးပြု၍ ငွေပေးချေမှု ရွေးချယ်ပါ"];
  textEng: any = ["Bill Aggregator", "Payment Type", "Select Biller", "SUBMIT", "RESET","Please select biller"];
  showFont: string[] = [];
  billerType  = ["ABC Telecomm","One Stop Mart","Wave Money"];
  billerid = "";
  errormsg = "";
  signinData: any;
  merchantList: any;
  userData: any;
  status: any;
  loading: any;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  flag: string;
  ipaddress: string;
  modal: any;
  hardwareBackBtn: any = true;
  titleData: any = [];
  font: any = '';
  idleCtrl: any;
  popover: any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public storage: Storage, public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, public idle: Idle, public network: Network, public popoverCtrl: PopoverController, public app: App,
    private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public platform: Platform) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {

    this.storage.get('ipaddress').then((result) => {

      if (result == null || result == '') {

      }
      else {
        this.checkNetwork();
        this.ipaddress = result;
      }
    });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
      this.storage.get('userData').then((userData) => {
        this.userData = userData;

        this.getMerchant();
      });

    }
  }

  getMerchant() {

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID, paymentType: 3 };
    this.http.post(this.ipaddress + '/service001/getMerchant', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.merchantList)) {
          tempArray.push(result.merchantList);
          result.merchantList = tempArray;
        }
        let temp = [];
        let count = 0;
        this.merchantList = result.merchantList;
        for (let i = 0; i < this.merchantList.length; i++) {
          if (this.merchantList[i].processingCode != '000000') {
            temp[count] = this.merchantList[i];
            count++;
          }
        }
        this.merchantList = temp;


        this.status = 1;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.status = 0;
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,

          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {

                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');

                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.status = 0;
        this.all.showAlert('Warning!', result.desc)
        this.loading.dismiss();

      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }


  goPay() {
    if(this.billerid ==""){
      this.errormsg = this.showFont[5];
    }else{
      this.errormsg = "";
      this.navCtrl.push(QuickpayConfirmPage,{
      billerid : this.billerid    
      });
    }   
  }

  goCancel() {
    this.billerid = "";
  }

}
