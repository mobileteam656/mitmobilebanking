import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform,
    PopoverController, Select, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { AccountTransferConfirmPage } from '../account-transfer-confirm/account-transfer-confirm';
import { BeneficiaryAddPage } from '../beneficiary-add/beneficiary-add';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { LovbyPage } from '../lovbypage/lovbypage';
import { MainHomePage } from '../main-home/main-home';
import { PopoverPage } from '../popover-page/popover-page';

declare var window;
@Component({
    selector: 'page-own-transfer',
    templateUrl: 'own-transfer.html',
})
export class OwnTransferPage {
    @ViewChild('myselect') select: Select;
    textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", "အကြောင်းအရာ", "လွှဲပြောင်းမည်", "ကိုယ်ပိုင်ဘဏ်စာရင်းသို့ ငွေလွှဲရန်", "လက်ခံသူ", "အမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "ပြန်စမည်"];
    textEng: any = ["From Account No.", "To Account No.", "Amount", "Narrative", "TRANSFER", "Own Transfer", "Choose Beneficiary", "Name", "Account Balance", "RESET"];
    textErrorEng: any = ["Please choose account number.", "Please choose account number.", "Please fill amount.", "Shouldn't match from account no. and to account no.", "Please check Account Number", "Insufficient Balance"]
    textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ငွေပေးမည့်အကောင့်နှင့် ငွေလက်ခံမည့်အကောင့် တူ၍မရပါ", "အကောင့်နံပါတ် စစ်ဆေးပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
    textError: string[] = [];
    textData: any = [];
    font: any = '';
    modal: any;
    hardwareBackBtn: any = true;
    userdata: any;
    ipaddress: string;
    loading: any;
    idleState = 'Not started.';
    timedOut = false;
    min: any;
    sec; any;
    transferData: any = {};
    transferData1: any = {};
    fromaccMsg: any = '';
    toaccMsg: any = '';
    amountcMsg: any = '';
    fromaccMsg1: any = '';
    toaccMsg1: any = '';
    amountcMsg1: any = '';
    fromAccountlist: any = [];
    popover: any;
    txnType: any;
    beneficiaryList: any;
    reqData: any = { "value": [], "type": 0, "title": "", 'ans': '' };
    accountBal: any = "";
    amountBal: any;
    checked=false;
    constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
        public changefont: Changefont, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
        public toastCtrl: ToastController, public loadingCtrl: LoadingController, public idle: Idle, public http: Http,
        public platform: Platform, public all: AllserviceProvider, public global: GlobalProvider, private slimLoader: SlimLoadingBarService,
        public util: UtilProvider) {
        this.txnType = 'own';
        this.accountBal = "";
        this.amountBal = "";
        this.storage.get('userData').then((val) => {
            this.userdata = val;
            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                    this.ipaddress = this.global.ipaddress;
                }
                else {
                    this.ipaddress = result;
                }
                this.getAccountSummary();
            })
        });
        this.events.subscribe('changelanguage', lan => {
            this.changelanguage(lan.data)
        });

        this.storage.get('language').then((font) => {
            this.changelanguage(font);
        });
    }

    ionViewDidLoad() {
        this.storage.get('userData').then((val) => {
            this.userdata = val;
            this.storage.get('ipaddress').then((result) => {
                if (result == null || result == '') {
                }
                else {
                    this.ipaddress = result;
                }
            })
        });
    }

    transfer() {
        let num = /^[0-9-\+]*$/;
        let f1 = false, f2 = false, f3 = false;
        if (this.txnType == 'own') {
            if (this.transferData.fromAcc != null && this.transferData.fromAcc != '' && this.transferData.fromAcc != undefined) {
                this.fromaccMsg = '';
                f1 = true;
            }
            else {
                this.fromaccMsg = this.textError[0];
                f1 = false;
            }
            if (this.transferData.toAcc != null && this.transferData.toAcc != '' && this.transferData.toAcc != undefined) {
                if (this.transferData.fromAcc == this.transferData.toAcc) {
                    this.toaccMsg = this.textError[3];
                    f2 = false;
                } else {
                    this.toaccMsg = '';
                    f2 = true;
                }
            }
            else {
                this.toaccMsg = this.textError[1];
                f2 = false;
            }
            if (this.transferData.amount != null && this.transferData.amount != '' && this.transferData.amount != undefined) {
                let amount = parseInt(this.transferData.amount);
                if (this.transferData.amount.substring(0, 1) == "-" || amount == 0) {
                    this.amountcMsg = this.textError[2];
                    f3 = false;
                } else if (isNaN(amount)) {
                    this.amountcMsg = this.textError[2];
                    f3 = false;
                } else if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.transferData.amount)) {
                    this.amountcMsg = this.textError[5];
                    f3 = false;
                } else {
                    this.amountcMsg = '';
                    f3 = true;
                }
            }
            else {
                this.amountcMsg = this.textError[2];
                f3 = false;
            }
        }
        else {
            if (this.transferData1.fromAcc != null && this.transferData1.fromAcc != '' && this.transferData1.fromAcc != undefined) {
                this.fromaccMsg1 = '';
                f1 = true;
            }
            else {
                this.fromaccMsg1 = this.textError[0];
                f1 = false;
            }
            if (this.transferData1.toAcc != null && this.transferData1.toAcc != '' && this.transferData1.toAcc != undefined) {
                if (this.transferData1.fromAcc == this.transferData1.toAcc) {
                    this.toaccMsg1 = this.textError[3];
                    f2 = false;
                } else {
                    this.toaccMsg1 = '';
                    f2 = true;
                }
            }
            else {
                this.toaccMsg1 = this.textError[1];
                f2 = false;
            }
            if (this.transferData1.amount != null && this.transferData1.amount != '' && this.transferData1.amount != undefined) {
                this.amountcMsg1 = '';
                f3 = true;
            }
            else {
                this.amountcMsg1 = this.textError[2];
                f3 = false;
            }
        }
        if (f1 && f2 && f3) {
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: true
            });
            this.loading.present();
            let temp;
            let typ;
            if (this.txnType == 'own') {
                this.transferData.fromName = this.userdata.userName;
                this.transferData.toName = this.userdata.userName;
                temp = this.transferData;
                typ = 3;
            }
            else {
                temp = this.transferData1;
                typ = 4;
            }
            let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: typ, merchantID: '' };
            this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
                if (data.code == "0000") {
                    this.loading.dismiss();
                    this.navCtrl.push(AccountTransferConfirmPage, {
                        data: temp,
                        type: this.txnType,
                        otp: data.rKey,
                        sKey: data.sKey,
                        fromPage: 'own-transfer'
                    });
                }
                else if (data.code == "0016") {
                    let confirm = this.alertCtrl.create({
                        title: 'Warning!',
                        enableBackdropDismiss: false,
                        message: data.desc,
                        buttons: [
                            {
                                text: 'OK',
                                handler: () => {

                                    this.storage.remove('userData');
                                    this.events.publish('login_success', false);
                                    this.events.publish('lastTimeLoginSuccess', '');
                                    this.navCtrl.setRoot(Login, {
                                    });
                                    this.navCtrl.popToRoot();
                                }
                            }
                        ],
                        cssClass: 'warningAlert',
                    })
                    confirm.present();
                    this.loading.dismiss();
                }
                else {
                    this.all.showAlert('Warning!', data.desc);
                    this.loading.dismiss();
                }
            },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        }
    }

    reset() {
        if (this.txnType == 'own') {
            this.transferData.fromAcc = "";
            this.transferData.toAcc = "";
            this.transferData.amount = "";
            this.transferData.refNo = "";
            this.fromaccMsg = '';
            this.toaccMsg = '';
            this.amountcMsg = '';
        }
        else {
            this.transferData1.fromAcc = "";
            this.transferData1.toAcc = "";
            this.transferData1.amount = "";
            this.transferData1.refNo = "";
            this.fromaccMsg1 = '';
            this.toaccMsg1 = '';
            this.amountcMsg1 = '';
        }
        this.accountBal = "";
        this.amountBal = "";
    }

    presentPopover(ev) {
        this.popover = this.popoverCtrl.create(PopoverPage, {
        });
        this.popover.present({
            ev: ev
        });
        let doDismiss = () => this.popover.dismiss();
        let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
        this.popover.onDidDismiss(unregBackButton);
        this.popover.onWillDismiss(data => {
            if (data == "0") {
                this.navCtrl.push(ChangepasswordPage);
            }
            else if (data == "1") {
                this.presentPopoverLanguage(ev);
            }
            else if (data == "2") {
                this.global.getphone().then(phone => {
                    this.callIT(phone);
                })
            }

        });
    }

    presentPopoverpresentPopover(ev) {
        this.popover = this.popoverCtrl.create(PopoverPage, {
        });
        this.popover.present({
            ev: ev
        });
        let doDismiss = () => this.popover.dismiss();
        let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
        this.popover.onDidDismiss(unregBackButton);
        this.popover.onWillDismiss(data => {
            if (data == "0") {
                this.navCtrl.push(ChangepasswordPage);
            }
            else if (data == "1") {
                this.presentPopoverLanguage(ev);
            }
            else if (data == "2") {
                this.global.getphone().then(phone => {
                    this.callIT(phone);
                })
            }
        });
    }

    presentPopoverLanguage(ev) {
        this.popover = this.popoverCtrl.create(LanguagePopOver, {
        });
        this.popover.present({
            ev: ev
        });
        let doDismiss = () => this.popover.dismiss();
        let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
        this.popover.onDidDismiss(unregBackButton);
        this.popover.onWillDismiss(data => {
            if (data != null) {
                this.storage.set('language', data);
                let temp = { data: '', status: 1 };
                temp.data = data;
                temp.status = 1;
                this.events.publish('changelanguage', temp);
            }
        });
    }

    callIT(passedNumber) {
        passedNumber = encodeURIComponent(passedNumber);
        window.location = "tel:" + passedNumber;
    }

    backButtonAction() {
        if (this.modal && this.modal.index === 0) {
            this.modal.dismiss();
        } else {
            if (this.hardwareBackBtn) {
                let alert = this.alertCtrl.create({
                    title: 'Are you sure you want to exit',
                    enableBackdropDismiss: false,
                    message: '',
                    buttons: [{
                        text: 'No',
                        handler: () => {
                            this.hardwareBackBtn = true;
                        }
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            this.gologout();
                        }
                    }]
                });
                alert.present();
                this.hardwareBackBtn = false;
            }
        }
    }

    gologout() {
        this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loading.present();
        this.storage.get('ipaddress').then((ipaddress) => {
            this.ipaddress = ipaddress;
            let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
            this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
                if (data.code == "0000") {
                    this.loading.dismiss();
                    this.events.publish('login_success', false);
                    this.events.publish('lastTimeLoginSuccess', '');
                    this.storage.remove('userData');
                    this.navCtrl.setRoot(Login, {
                    });
                    this.navCtrl.popToRoot();
                }
                else {
                    this.all.showAlert('Warning!', data.desc);
                    this.loading.dismiss();
                }

            },
                error => {
                    this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                    this.loading.dismiss();
                });
        });
    }

    changelanguage(font) {
        if (font == "eng") {
            this.font = "";
            for (let i = 0; i < this.textEng.length; i++) {
                this.textData[i] = this.textEng[i];
            }
            for (let i = 0; i < this.textErrorEng.length; i++) {
                this.textError[i] = this.textErrorEng[i];
            }
            if (this.fromaccMsg != '') {
                this.fromaccMsg = this.textError[0];
            }
            if (this.toaccMsg != '') {
                this.toaccMsg = this.textError[1];
            }
            if (this.amountcMsg != '') {
                this.amountcMsg = this.textError[2];
            }
        }
        else if (font == "zg") {
            this.font = "zg";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
            }
            for (let i = 0; i < this.textErrorMyan.length; i++) {
                this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
            }
            if (this.fromaccMsg != '') {
                this.fromaccMsg = this.textError[0];
            }
            if (this.toaccMsg != '') {
                this.toaccMsg = this.textError[1];
            }
            if (this.amountcMsg != '') {
                this.amountcMsg = this.textError[2];
            }
        }
        else {
            this.font = "uni";
            for (let i = 0; i < this.textMyan.length; i++) {
                this.textData[i] = this.textMyan[i];
            }
            for (let i = 0; i < this.textErrorMyan.length; i++) {
                this.textError[i] = this.textErrorMyan[i];
            }
            if (this.fromaccMsg != '') {
                this.fromaccMsg = this.textError[0];
            }
            if (this.toaccMsg != '') {
                this.toaccMsg = this.textError[1];
            }
            if (this.amountcMsg != '') {
                this.amountcMsg = this.textError[2];
            }
        }
    }

    idleWatch() {
        this.idle.setIdle(5);  //after 5 sec idle
        this.idle.setTimeout(5 * 60);  //5min countdown
        this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
        this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
        this.idle.onTimeout.subscribe(() => {
            this.idleState = 'Timed out!';
            this.timedOut = true;
            let currentpage = this.navCtrl.getActive();
            if (currentpage.component.name == 'OwnTransferPage')
                this.gologout();
        });
        this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
        this.idle.onTimeoutWarning.subscribe((countdown) => {
            let currentpage = this.navCtrl.getActive();
            if (currentpage.component.name == 'OwnTransferPage') {
                var data = countdown / 60;
                this.min = data.toString().split('.')[0];
                this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
                this.sec = (Math.round(this.sec * 100) / 100);
                this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
            }
        });
        this.reload();
    }

    reload() {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    }

    getBeneficiaryList() {
        let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
        this.http.post(this.ipaddress + '/service002/getBeneficiary', parameter).map(res => res.json()).subscribe(result => {
            if (result.code == "0000") {
                let tempArray = [];
                if (!Array.isArray(result.dataList)) {
                    tempArray.push(result.dataList);
                    result.dataList = tempArray;
                }
                let temp = [];
                let count = 0;
                for (let i = 0; i < result.dataList.length; i++) {
                    if (result.dataList[i].beneTypeValue == '2') {
                        temp[count] = result.dataList[i];
                        count++;
                    }
                }
                this.beneficiaryList = temp;
            }
            else if (result.code == "0016") {
                let confirm = this.alertCtrl.create({
                    title: 'Warning!',
                    enableBackdropDismiss: false,
                    message: result.desc,
                    buttons: [
                        {
                            text: 'OK',
                            handler: () => {
                                this.storage.remove('userData');
                                this.events.publish('login_success', false);
                                this.events.publish('lastTimeLoginSuccess', '');
                                this.navCtrl.setRoot(Login, {
                                });
                                this.navCtrl.popToRoot();
                            }
                        }
                    ],
                    cssClass: 'warningAlert',
                })
                confirm.present();
            } else {
                this.beneficiaryList = [];
            }
        },
            error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            });
    }


    addBeneficiary(type) {
        if (this.beneficiaryList.length == 0) {
            let alert = this.alertCtrl.create({
                title: 'Please Add Beneficiary',
                enableBackdropDismiss: false,
                message: '',
                buttons: [{
                    text: 'No',
                    handler: () => {
                        alert.dismiss();

                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.navCtrl.push(BeneficiaryAddPage);
                    }
                }]
            });
            alert.present();
        } else {
            for (let i = 0; i < this.beneficiaryList.length; i++) {
                this.reqData.value[i] = this.beneficiaryList[i].name
            }
            this.reqData.type = type;
            this.reqData.title = 'Beneficiary List';
            this.navCtrl.push(LovbyPage, {
                data: this.reqData
            });
        }
    }

    ionViewWillEnter() {
        if (this.reqData.ans != undefined && this.reqData != '' && this.reqData != null && this.reqData.title == 'Beneficiary List') {
            for (let i = 0; i < this.beneficiaryList.length; i++) {
                if (this.reqData.ans === this.beneficiaryList[i].name) {
                    this.transferData1.name = this.beneficiaryList[i].name;
                    this.transferData1.toAcc = this.beneficiaryList[i].accountNo;
                }
            }
            this.reqData.title = '';
        }
    }

    getAccountSummary() {
        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        this.loading.present();
        let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
        this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
            if (result.code == "0000") {
                let tempArray = [];
                if (!Array.isArray(result.dataList)) {
                    tempArray.push(result.dataList);
                    result.dataList = tempArray;
                }
                this.fromAccountlist = result.dataList;
                this.loading.dismiss();
            }
            else if (result.code == "0016") {
                let confirm = this.alertCtrl.create({
                    title: 'Warning!',
                    enableBackdropDismiss: false,
                    message: result.desc,
                    buttons: [
                        {
                            text: 'OK',
                            handler: () => {

                                this.storage.remove('userData');
                                this.events.publish('login_success', false);
                                this.events.publish('lastTimeLoginSuccess', '');
                                this.navCtrl.setRoot(Login, {
                                });
                                this.navCtrl.popToRoot();
                            }
                        }
                    ],
                    cssClass: 'warningAlert',
                })
                confirm.present();
                this.loading.dismiss();
            }
            else {
                this.all.showAlert("Warning!", result.desc);
                this.loading.dismiss();
            }
        },
            error => {
                this.all.showAlert("Warning!", this.all.getErrorMessage(error));
                this.loading.dismiss();
            });
    }

    changeAcc(s, account) {
        if (s == 1) {
            this.fromaccMsg = '';
        }
        else if (s == 2) {
            this.toaccMsg = '';
        } else if (s == 3) {
            this.fromaccMsg1 = '';
        }
        for (let i = 0; i < this.fromAccountlist.length; i++) {
            if (account == this.fromAccountlist[i].depositAcc) {
                this.amountBal = this.fromAccountlist[i].avlBal;
                this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
            }
        }
        this.checked=true;
    }

    inputChange(data) {
        if (data == 1) {
            this.amountcMsg = "";
        } else if (data == 2) {
            this.amountcMsg1 = "";
        } else if (data == 3) {
            this.toaccMsg1 = "";
        }
    }

    ionViewCanLeave() {
        this.select.close();
        this.platform.registerBackButtonAction(() => {
            this.select.close();
            this.navCtrl.pop();
        });
    }

    backButton() {
        this.navCtrl.setRoot(MainHomePage);
    }
}
