import 'rxjs/Rx';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform,
    PopoverController, Select, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { Login } from '../login/login';
import { PopoverPage } from '../popover-page/popover-page';
import { TopUpListPage } from '../top-up-list/top-up-list';
import { UtilityConfirmPage } from '../utility-confirm/utility-confirm';

//import { Clipboard } from '@ionic-native/clipboard';
declare var window;
@Component({
  selector: 'page-qr-utility',
  templateUrl: 'qr-utility.html',
})
export class QrUtilityPage {
  @ViewChild('myselect') select: Select;
  textMyan: any = ["ငွေပေးချေမှု", "အကောင့်နံပါတ်", "အမှတ်စဉ်", "ဘေလ်နံပါတ်", "အမည်", "အခွန် ငွေပမာဏ", "အခွန်ဌာန", "အခွန် အမျိုးအစား", "ကုန်ဆုံးမည့် ရက်စွဲ", "ဒဏ်ကြေး", "အကြောင်းအရာ", "ပယ်ဖျက်မည်", "လုပ်ဆောင်မည်", "ကော်မရှင် ငွေပမာဏ", "စုစုပေါင်း ငွေပမာဏ", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "နောက်ကျသည့် ရက်ပေါင်း"];
  textEng: any = ["Utility Payment", "Account Number", "Reference Number", "Bill ID", "Customer Name", "Bill Amount", "Department Name", "Tax Description", "Due Date", "Penalty Amount", "Narrative", "CANCEL", "SUBMIT", "Commission Amount", "Total Amount", "Account Balance", "Belated Days"];
  showFont: string[] = [];
  textErrorEng: any = ["Please choose Account number.", "Please scan QR", "Please fill amount.", "Insufficient Balance"]
  textErrorMyan: any = ["ကျေးဇူးပြုပြီး အကောင့်နံပါတ် ရွေးချယ်ပါ", "ကျေးဇူးပြုပြီး QR ဖတ်ပါ", "ကျေးဇူးပြုပြီး ငွေပမာဏ ရိုက်ထည့်ပါ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ"];
  textError: string[] = [];
  merchantType: any = [{ name: 'NPTDC', value: '000000' }];
  popover: any;
  ipaddress: string;
  modal: any;
  checked=false;
  hardwareBackBtn: any = true;
  userData: any;
  loading: any;
  city: any = {};
  useraccount: any;
  qrValue: any = [];
  qrData: any;
  billAmount: any;
  idleState = 'Not started.';
  timedOut = false;
  min: any;
  sec; any;
  flag: string;
  fromAccountlist: any;
  accountBal: any = '';
  fromaccMsg: any = '';
  font: any;
  isScan: boolean;
  requireScan: any;
  totalAmount: any;
  comAmount: any;
  copyText: any;
  validText: boolean = false;
  buttonText: any;
  penaltyAmount: any;
  amountBal: any;
  placeHolderText:any;
  constructor(public global: GlobalProvider, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, public events: Events,
    public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public platform: Platform,
    public popoverCtrl: PopoverController, public changefont: Changefont, public changeLanguage: ChangelanguageProvider, private slimLoader: SlimLoadingBarService, public barcodeScanner: BarcodeScanner, public all: AllserviceProvider, public idle: Idle,
    public network: Network, public util: UtilProvider) {
    this.buttonText = 'QR Scan';
    this.amountBal = "";
    this.city.peocessingCode = '000000';
    this.isScan = false;
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((result) => {
      this.userData = result;
      this.storage.get('ipaddress').then((result) => {
        this.ipaddress = result;
        this.getAccountSummary()
      });
    });

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      this.placeHolderText= "For paste Your QR Text";//""  QR စာသားထည့်ရန်
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
      for (let i = 0; i < this.textErrorEng.length; i++) {
        this.textError[i] = this.textErrorEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.changefont.UnitoZg(this.textErrorMyan[i]);
      }
    }
    else {
      this.font = "uni";
      this.placeHolderText= "QR စာသားထည့်ရန်";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.textErrorMyan.length; i++) {
        this.textError[i] = this.textErrorMyan[i];
      }
    }
  }

  ionViewDidLoad() {
  }

  validation() {
    if (this.isScan == false && this.city.refNo == undefined) {
      this.requireScan = this.textError[1];
    }
    else if (this.city.account != '' && this.city.account != undefined && this.city.account != null && this.isScan == true) {
      this.fromaccMsg = '';
      this.requireScan = '';
      if (parseInt(this.util.formatToDouble(this.amountBal)) < parseInt(this.formatToDouble(this.city.totalAmount))) {
        this.fromaccMsg = this.textError[3];
      } else {
        this.goConfirm();
      }
    } else {
      this.fromaccMsg = this.textError[0];
    }
  }

  goConfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      if (true) {
        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: true
        });
        this.loading.present();
        let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 1, merchantID: this.city.merchantID };
        this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            this.loading.dismiss();
            this.navCtrl.push(UtilityConfirmPage, {
              data: this.city,
              otp: data.rKey,
              sKey: data.sKey
            })
          }
          else if (data.code == "0016") {
            this.logoutAlert(data.desc);
            this.loading.dismiss();
          }
          else {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 5000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          }
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.all.getErrorMessage(error),
              duration: 5000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      }
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    }
  }


  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopoverPage, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(ChangepasswordPage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone => {
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: 1 };
        temp.data = data;
        temp.status = 1;
        this.events.publish('changelanguage', temp);
      }
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit?',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }
  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }
          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }

  scanQR() {
    if (this.validText == false) {
      this.barcodeScanner.scan().then((barcodeData) => {
        this.qrValue = barcodeData.text;
        if (barcodeData.cancelled == false) {
          this.isScan = true;
          this.requireScan = '';
          this.decryptData();
        }
      });
    } else {
      this.qrValue = this.copyText;
      this.decryptData();
    }

  }

  decryptData() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, data: this.qrValue };
      this.http.post(this.ipaddress + '/service002/decryptData', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.isScan = true;
          this.requireScan = '';
          this.loading.dismiss();
          this.qrData = data.data;
          this.city.billId = this.qrData.billId;
          this.city.refNo = this.qrData.refNo;
          this.city.cusName = this.qrData.cusName;
          this.city.amount = this.formatAmount(this.qrData.billAmount);
          this.city.ccy = this.qrData.ccyCode;
          this.city.deptName = this.qrData.deptName;
          this.city.taxDesc = this.qrData.taxDesc;
          this.city.t1 = this.qrData.t1;
          this.city.t2 = this.qrData.t2;
          this.city.dueDate = this.qrData.dueDate;
          this.city.toAcc = this.qrData.deptAcc;
          this.city.penaltyAccount = this.qrData.penaltyAccount;
          this.city.vendorCode = this.qrData.vendorCode;
          this.city.merchantID = this.qrData.merchantID;
          this.city.penaltyAmount = this.formatAmount(this.qrData.penalty);
          this.penaltyAmount = this.city.penaltyAmount + "  " + this.city.ccy;
          this.city.comAmount = this.formatAmount(this.qrData.bankCharges);
          this.city.belatedDays = this.qrData.belatedDays;
          this.billAmount = this.city.amount + "  " + this.city.ccy;
          this.city.totalAmount = this.formatAmount(this.qrData.totalAmount);
          this.comAmount = this.formatAmount(this.qrData.bankCharges) + "  " + this.city.ccy;
          this.totalAmount = this.city.totalAmount + " " + this.city.ccy;
        }
        else {
          let msg = '';

          if (data.statusCode == '200') {
            msg = "It is already paid."
          } else if (data.statusCode == '400') {
            msg = "It is not available at a moment."
          } else if (data.statusCode == '406') {
            msg = "Bill Reference No. is wrong."
          } else {
            msg = data.desc;
          }

          this.all.showAlert('Warning!', msg);
          this.isScan = false;
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
          this.isScan = false;
        });
    });
  }

  presentAlert(title, desc) {
    let alert = this.alertCtrl.create({
      title: title,
      message: desc,
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });
    alert.present();
  }

  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'QrUtilityPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'QrUtilityPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  checkNetwork() {

    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.fromAccountlist = result.dataList;
        this.useraccount = this.fromAccountlist;
        this.city.account = this.useraccount[0].depositAcc;
        this.accountBal = this.useraccount[0].avlBal +" "+ this.useraccount[0].ccy;
        this.amountBal=this.useraccount[0].avlBal;
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        this.logoutAlert(result.desc);
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    for (let i = 0; i < this.fromAccountlist.length; i++) {
      if (account == this.fromAccountlist[i].depositAcc) {
        this.amountBal = this.fromAccountlist[i].avlBal;
        this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
      }
    }
  
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  inputChange() {
    if (this.copyText != undefined && this.copyText != '') {
      this.validText = true;
      this.buttonText = 'Check';
    } else {
      this.buttonText = 'QR Scan';
      this.validText = false;
    }
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }

  backButton() {
    this.navCtrl.setRoot(TopUpListPage, {
      data: '1'
    });
  }
  /* ionViewCanEnter(){
    this.clipboard.paste().then(
      (resolve: string) => {
         alert(resolve);
         this.copyText=resolve;
       },
       (reject: string) => {
        this.copyText=reject;
         alert('Error: ' + reject);
       }
     );
  }
  ionViewWillEnter(){
    this.clipboard.paste().then(
      (resolve: string) => {
         alert(resolve);
         this.copyText=resolve;
       },
       (reject: string) => {
        this.copyText=reject;
         alert('Error: ' + reject);
       }
     );
  } */
}
