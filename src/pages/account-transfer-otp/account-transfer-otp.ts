import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AccountTransferDetail } from '../account-transfer-detail/account-transfer-detail';
import { Login } from '../login/login';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

declare var window: any;
declare var cordova: any;

/**
 * Generated class for the AccountTransferOtpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-account-transfer-otp',
  templateUrl: 'account-transfer-otp.html',
  providers: [ChangelanguageProvider]
})
export class AccountTransferOtpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  showFont: string[] = [];
  font: string;
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData: any;
  passtemp1: any;
  passtemp2: any;
  passtType: any;
  public loading;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  passSKey: any;
  fromPage: any;
  constructor(public navCtrl: NavController, public all: AllserviceProvider, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public storage: Storage, public alertCtrl: AlertController, public events: Events, public network: Network, public idle: Idle, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    private firebaseAnalytics: FirebaseAnalytics) {

    /* this.slimLoader.start(() => {

    }); */
    this.passtemp1 = this.navParams.get('data');
    this.passtemp2 = this.navParams.get('detail');
    this.passtType = this.navParams.get('type');
    this.passSKey = this.navParams.get('sKey');
    this.fromPage = this.navParams.get('fromPage');
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
      //this.slimLoader.complete();
    }) */

    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  sendOtpCode() {
    this.checkNetwork();
    if (this.flag == "success") {
      /* this.loading = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange :true
       //   duration: 3000
       });
       this.loading.present();*/
    /*   this.slimLoader.start(() => {

      }); */
      let transferType
      if (this.passtType == 'own') {
        transferType = 3;
      }
      else if (this.passtType == 'internal') {
        transferType = 4;
      }
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: transferType, merchantID: '', sKey: this.passSKey };

      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {

        if (data.code == "0000") {
          this.passtemp1.rKey = data.rKey;
          // this.loading.present();
          /* window.SMS.startWatch(function () {

          }, function () {

          });

          setTimeout(() => {

          }, 8000);

          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;

            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];

            //this.slimLoader.complete();
          }); */

        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          //this.slimLoader.complete();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          //this.slimLoader.complete();
        });
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
    // // this.idleWatch();
    this.network.onDisconnect().subscribe(data => {
      this.flag = 'none';
      this.all.showAlert('Warning!', "Check your internet connection!");
    }, error => console.error(error));

  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  getconfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Processing...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, rKey: this.passtemp1.rKey, otpCode: this.otpcode, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          let transferType = '';
          if (this.passtType == 'own') {
            transferType = '1';
            let parameter = {
              userID: this.userData.userID, sessionID: this.userData.sessionID, fromAccount: this.passtemp2.fromAcc,
              toAccount: this.passtemp2.toAcc, merchantID: '', amount: this.passtemp2.amount, bankCharges: 0, narrative: this.passtemp2.refNo, 
              refNo: '', field1: transferType, field2: '', sKey: this.passSKey,fromName:this.passtemp2.fromName,toName:this.passtemp2.toName
            };
            this.http.post(this.ipaddress + '/service003/goOwnTransfer', parameter).map(res => res.json()).subscribe(data1 => {
              // this.firebase.logEvent('own_transfer', {})
              //     .then((res: any) => { console.log(res); })
              //     .catch((error: any) => console.log(error));

              this.firebaseAnalytics.logEvent('own_transfer', {})
              .then((res: any) => console.log(res))
              .catch((error: any) => console.error(error));

              if (data1.code == "0000") {
                this.loading.dismiss();
                this.passtemp2.amount = data1.amount;
                this.navCtrl.setRoot(AccountTransferDetail, {
                  data: data1,
                  detail: this.passtemp2,
                  type: this.passtType,
                  fromPage: this.fromPage
                })
              }
              else if (data1.code == "0016") {
                this.logoutAlert(data1.desc);
                this.loading.dismiss();
              }
              else {
                this.all.showAlert('Warning!', data1.desc);
                this.loading.dismiss();
              }

            },
              error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.loading.dismiss();
              });
          }
          else if (this.passtType == 'internal') {
            transferType = '2';
            let parameter = {
              userID: this.userData.userID, sessionID: this.userData.sessionID, fromAccount: this.passtemp2.fromAcc,
              toAccount: this.passtemp2.toAcc, merchantID: '', amount: this.passtemp2.amount, bankCharges: 0, narrative: this.passtemp2.refNo, 
              refNo: '', field1: transferType, field2: '', sKey: this.passSKey,fromName:this.passtemp2.fromName,toName:this.passtemp2.toName
            };
            this.http.post(this.ipaddress + '/service003/goInternalTransfer', parameter).map(res => res.json()).subscribe(data1 => {
              if (data1.code == "0000") {
                this.loading.dismiss();
                this.passtemp2.amount = data1.amount;
                this.navCtrl.setRoot(AccountTransferDetail, {
                  data: data1,
                  detail: this.passtemp2,
                  type: this.passtType,
                  fromPage: this.fromPage
                })
              }
              else if (data1.code == "0016") {
                this.logoutAlert(data1.desc);
                this.loading.dismiss();
              }
              else {
                this.all.showAlert('Warning!', data1.desc);
                this.loading.dismiss();
              }
            },
              error => {
                this.all.showAlert('Warning!', this.all.getErrorMessage(error));
                this.loading.dismiss();
              });
          }
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    } else {
      this.all.showAlert('Warning!', "Check your internet connection!");
    }
  }

  ionViewDidLoad() {
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(Login, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
