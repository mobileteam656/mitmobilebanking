import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController, PopoverController, Events, Platform, AlertController } from 'ionic-angular';
import { LocationDetail } from '../location-detail/location-detail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { DefaultPopOver } from '../default-pop-over/default-pop-over';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { IpchangePage } from '../ipchange/ipchange';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { PopoverPage } from '../popover-page/popover-page';
import { Login } from '../login/login';
declare var window;

@Component({
  selector: 'page-atm-location',
  templateUrl: 'atm-location.html',
  providers: [ChangelanguageProvider]
})

export class AtmLocation {

  location: any;
  alllocation: any = [];
  atmlocation: any = [];
  branchlocation: any = [];
  hasData: boolean = false;
  allocationMessage: string;
  status: any;
  public loading;
  ipaddress: string;
  popover: any;
  font: string = '';
  showFont: any = [];
  textEng: any = ["Location", "Branch", "No Result Found"];
  textMyan: any = ["တည်နေရာ", "ဘဏ်ခွဲများ", "အချက်အလက်မရှိပါ"];
  ststus: any;
  userdata: any;
  modal: any;
  hardwareBackBtn: any = true;
  constructor(public navCtrl: NavController, public menu: MenuController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public storage: Storage,
    private slimLoader: SlimLoadingBarService, public popoverCtrl: PopoverController, public events: Events, public platform: Platform, public changeLanguage: ChangelanguageProvider,
    public global: GlobalProvider, public all: AllserviceProvider, public alertCtrl: AlertController) {
    this.location = 'atm';
    this.ststus = this.navParams.get("ststus");
    this.events.subscribe('ipaddress', (ipaddress) => {
      this.ipaddress = ipaddress;
    });
    if (this.ststus == '1') {
      this.storage.get('userData').then((val) => {
        this.userdata = val;
        this.storage.get('ipaddress').then((result) => {
          if (result == null || result == '') {
            this.ipaddress = this.global.ipaddress;
          }
          else {
            this.ipaddress = result;
          }
        })
      });
    }

  }

  getLocation() {
    this.slimLoader.start(() => {
    });
    let param = { userID: '', sessionID: '' };
    this.http.post(this.ipaddress + '/service001/getLocation', param).map(res => res.json()).subscribe(data => {


      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
          //this.alllocation.data = data.data;
        }
        this.alllocation.data = data.data;
        if (this.alllocation.data.length > 0) {
          this.status = 1;
          for (let i = 0; i < this.alllocation.data.length; i++) {
            let tempListArray = [];
            //if(this.alllocation.data[i].dataList!=undefined){
            if (!Array.isArray(this.alllocation.data[i].dataList)) {
              tempListArray.push(this.alllocation.data[i].dataList);
              this.alllocation.data[i].dataList = tempListArray;
            }
            //}
          }

          this.slimLoader.complete();
          this.hasData = true;
          this.location = this.alllocation.data[0].locationType;

        } else {
          this.slimLoader.complete();
          this.status = 0;
        }
      } else {
        this.status = 0;
        this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  ionViewDidLoad() {
    this.storage.get('ipaddress').then((result) => {
      this.ipaddress = result;
      this.slimLoader.start(() => {
      });
      this.getLocation();
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  getloaction(data) {
    var parm = {
      lat: data.latitude,
      lng: data.longitude,
      name: data.name,
      address: data.address
    }
    this.navCtrl.push(LocationDetail, { data: parm })
  }

  ionViewDidLeave() {
    this.slimLoader.reset();
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });

  }

  presentPopover(ev) {
    let createPopover = null;
    if (this.ststus == '1') {
      createPopover = PopoverPage;
    } else {
      createPopover = DefaultPopOver;
    }
    this.popover = this.popoverCtrl.create(createPopover, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      if (data == "0") {
        this.navCtrl.push(IpchangePage);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone=>{
          this.callIT(phone);
        })
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: this.ststus };
        temp.data = data;
        temp.status = this.ststus;
        this.events.publish('changelanguage', temp);
      }
    });
  }
  setLocation(type) {
    this.location = type;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else if (this.ststus == '1') {
      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }

  }
}
