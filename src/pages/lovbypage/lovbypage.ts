import { Component } from '@angular/core';
import { Platform, NavController, NavParams, ToastController, LoadingController, PopoverController, AlertController, Events, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/map';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';

import { Login } from '../login/login';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the LovPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-lovbypage',
  templateUrl: 'lovbypage.html',
})
export class LovbyPage {
  userdata: any;
  list: any;
  idleState = 'Not started.';
  timedOut = false;
  ipaddress: any;
  loading: any;
  lastPing: Date = null;
  min: any;
  sec; any;
  useraccount: any;
  data: any;
  title:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public events: Events, public idle: Idle, public app: App, public datePipe: DatePipe, public changeLanguage: ChangelanguageProvider,
    public platform: Platform, public global: GlobalProvider) {
     
    this.data = this.navParams.get('data');
    this.title=this.data.title;
    this.list = this.data.value;
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
    });
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'LovbyPage')
        this.gologout();
      // console.log("LovbyPage>>=" + currentpage.component.name);

    });
    this.idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!';
    });
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      // console.log("again touch account transation")
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'LovbyPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  ionViewDidLoad() {
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        // console.log("Ip address>>" + result);
        if (result == null || result == '') {
          // console.log("Ip address>>" + result);
        }
        else {
          this.ipaddress = result;
        }

      });
    });
  }
  ionViewDidLeave() {
    // console.log("ionViewDidLeave");
  }
  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      // console.log("ip address>>" + ipaddress);
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      // console.log("parameter=" + JSON.stringify(param));
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        // console.log("logout  data>>" + JSON.stringify(data));
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }

      },
        error => {
          // console.log("getlocation error=" + error.status);
          /* ionic App error
           ............001) url link worng, not found method (404)
           ........... 002) server not response (500)
           ............003) cross fillter not open (403)
           ............004) server stop (-1)
           ............005) app lost connection (0)
           */
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }
          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });

  }
  selectedData(a) {
    this.data.ans = a;
    this.navCtrl.pop();
  }


}
