import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, PopoverController, Events, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Storage } from '@ionic/storage';
import { DefaultPopOver } from '../default-pop-over/default-pop-over';
import { LanguagePopOver } from '../language-pop-over/language-pop-over';
import { IpchangePage } from '../ipchange/ipchange';
import { CloudPage } from '../cloud/cloud';
import { DomainPage } from '../domain/domain';
import { PopoverPage } from '../popover-page/popover-page';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
/**
 *
 *
 * Generated class for the ContactUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var window;

@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
  providers: [ChangelanguageProvider]
})
export class ContactUs {
  popover: any;
  font: string;
  showFont: any = [];
  loading: any;
  ipaddress: any;
  /*textEng: any=["66-76, Corner of Merchant Road and Pansodan Street, Kyauktada Township,Yangon 11182, Myanmar.","Welcome to Shwe Bank","We would love to hear from you. Please use the address below","Shwe Bank"];
   textMyan : any = ["အမှတ်(၆၆-၇၆), ကုန်သည်လမ်း နှင့် ပန်းဆိုးတန်းလမ်းထောင့် ,  ကြည်မြင်တိုင်မြို့နယ် , ရန်ကုန်။","ရွှေဘဏ်မှ  ကြိုဆိုပါသည်","မိတ်ဆွေထံမှ စုံစမ်းမှုများပြုလုပ်လိုပါလျှင် အောက်ပါလိပ်စာများသို့ ဆက်သွယ်မေးမြန်းနိုင်ပါသည်။","ရွှေဘဏ်"];
 */
  textEng: any = ["mBanking", "Welcome to mBanking", "We would love to hear from you. Please use the address below", "Contact Us"];
  textMyan: any = ["mBanking", "mBanking မှကြိုဆိုပါသည်။", "မိတ်ဆွေထံမှ စုံစမ်းမှုများပြုလုပ်လိုပါလျှင် အောက်ပါလိပ်စာများသို့ ဆက်သွယ်မေးမြန်းနိုင်ပါသည်။", "ဆက်သွယ်ရန်"];

  /* textEng: any=["No (230), Corner of Maha Bandoola Road and Bo Myat Tun Street, Botataung Township, Yangon, Myanmar","Welcome to TCB mBanking","We would love to hear from you. Please use the address below","TCB mBanking"];
   textMyan : any = ["အမှတ်(၂၃၀)၊ မဟာဗန္ဓုလ နှင့် ဗိုလ်မြတ်ထွန်းလမ်းထောင့်၊ ဗိုလ်တစ်ထောင်မြို့နယ်၊ ရန်ကုန်မြို့။","ထွန်းကော်မာရှယ်ဘဏ် မှကြိုဆိုပါသည်။","မိတ်ဆွေထံမှ စုံစမ်းမှုများပြုလုပ်လိုပါလျှင် အောက်ပါလိပ်စာများသို့ ဆက်သွယ်မေးမြန်းနိုင်ပါသည်။","ထွန်းကော်မာရှယ်ဘဏ်"];
 */
  /* textEng: any = ["656 Maha Thukhita Road,Insein Township, Yangon, Myanmar", "Welcome to mBanking 360", "", "mBanking 360"];
  textMyan: any = ["၆၅၆ မဟာသုခိတာလမ်း၊ အင်းစိန်မြို့နယ်၊ ရန်ကုန်၊ မြန်မာ ။", "mBanking 360 မှကြိုဆိုပါသည်။", "", "mBanking 360"];
   */
  ststus: any;
  userdata: any;
  modal: any;
  hardwareBackBtn: any = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, private storage: Storage,
    public changeLanguage: ChangelanguageProvider, public all: AllserviceProvider, public http: Http, public loadingCtrl: LoadingController, public popoverCtrl: PopoverController, public events: Events, public global: GlobalProvider, public alertCtrl: AlertController) {
    this.ststus = this.navParams.get("ststus");
    if (this.ststus == '1') {
      this.storage.get('userData').then((val) => {
        this.userdata = val;
        this.storage.get('ipaddress').then((result) => {
          if (result == null || result == '') {
            this.ipaddress = this.global.ipaddress;
          }
          else {
            this.ipaddress = result;
          }
        })
      });
    }
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        // console.log("Ngar data " + JSON.stringify(data));
        this.showFont = data;
        // console.log("Show pe lay " + JSON.stringify(this.showFont));
      });
    });
  }

  /*changelanguage(lan) {
    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }

    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }

    }
  }*/

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ContactUs');
  }
  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }
  gotoshwewebsite() {
    if (this.platform.is('android')) {
      window.open('http://www.mit.com.mm', '_system');
    }
    else if (this.platform.is('ios')) {
      window.open('http://www.mit.com.mm'); // or itms://
    }
    if (this.platform.is('mobileweb')) {
      // console.log("running in a browser on mobile!");
    }
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });

  }

  presentPopover(ev) {
    let createPopover = null;
    if (this.ststus == '1') {
      createPopover = PopoverPage;
    } else {
      createPopover = DefaultPopOver;
    }
    this.popover = this.popoverCtrl.create(createPopover, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data == "0") {
        this.navCtrl.push(IpchangePage);
        //  this.presentPopoverCloud(ev);
      }
      else if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      else if (data == "2") {
        this.global.getphone().then(phone=>{
          this.callIT(phone);
        })
      }

    });
  }

  presentPopoverCloud(ev) {

    this.popover = this.popoverCtrl.create(CloudPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);


    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data == 1) {
        this.navCtrl.push(DomainPage);
      }
      else if (data == 2) {
        this.navCtrl.push(IpchangePage);
      }


    });
  }

  presentPopoverLanguage(ev) {

    this.popover = this.popoverCtrl.create(LanguagePopOver, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // console.log("popover dismissed");
      // console.log("Selected Item is " + data);
      if (data != null) {
        this.storage.set('language', data);
        let temp = { data: '', status: this.ststus };
        temp.data = data;
        temp.status = this.ststus;
        this.events.publish('changelanguage', temp);
      }


    });
  }
  backButtonAction() {

    if (this.modal && this.modal.index === 0) {

      this.modal.dismiss();
    } else if (this.ststus == '1') {

      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

}
