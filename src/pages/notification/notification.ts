import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, Events, PopoverController, LoadingController, AlertController } from 'ionic-angular';
import { ConditionalExpr } from '@angular/compiler';
import { FCM } from '@ionic-native/fcm';
import { Badge } from '@ionic-native/badge';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Changefont } from '../changefont/changeFont';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { Login } from '../login/login';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  //@ViewChild(Nav) nav: Nav;
  userdata: any;
  ipaddress: any;
  notidata: any;
  flag: boolean = false;
  backend: any;
  loading: any;
  notiData: any = [];
  showNotiList = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public fcm: FCM, public badge: Badge, public platform: Platform,
    public events: Events, public storage: Storage, public changefont: Changefont, public popoverCtrl: PopoverController,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http, public all: AllserviceProvider, public global: GlobalProvider,
    public util: UtilProvider, public changeLanguage: ChangelanguageProvider,
    public alertCtrl: AlertController) {
    // this.notidata = navParams.get("data");

    // console.log(this.notidata);
    // if(this.notidata != '' && this.notidata != undefined)
    // {
    //   this.flag = true;
    // }else {
    //   this.flag =false;
    // }

    // this.fcm.getToken().then(token => {
    //   this.backend.registerToken(token);
    //    console.log(token);
    // });

    // this.fcm.onNotification().subscribe(data => {
    // if(data.wasTapped){
    //   //this.badge.increase(1);
    //   this.notidata = data;
    //   console.log("false",this.notidata);
    // } else {
    //   //this.badge.increase(1);
    //   this.notidata = data;
    //   console.log("true",this.notidata); 

    // }

    // });
    // this.fcm.onTokenRefresh().subscribe(token => {
    //   this.backend.registerToken(token);
    // });
    // this.fcm.unsubscribeFromTopic('marketing');  
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
        } else {
          this.ipaddress = result;
        }
        // this.removeNoti();

      })
    });
  }

  getNotiData() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: '', sessionID: '' };
    this.http.post(this.ipaddress + '/serviceNoti/getNoti', param).map(res => res.json()).subscribe(
      result => {
        // console.log("notdata>>", JSON.stringify(result));

        if (result.code == "0000") {
          if (result.data != undefined && result.data != null && result.data.length > 0) {
            let tempArray = [];
            if (!Array.isArray(result.data)) {
              tempArray.push(result.data);
              result.data = tempArray;
            }
            this.notiData = result.data;
            if (this.notiData != undefined && this.notiData != null && this.notiData.length > 0) {
              this.showNotiList = "show";
            } else {
              this.showNotiList = "notshow";
            }
          } else {
            this.showNotiList = "notshow";
          }

          this.loading.dismiss();
        }
        else if (result.code == "0016") {
          this.showNotiList = "notshow";

          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: result.desc,
            buttons: [{
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {});
                this.navCtrl.popToRoot();
              }
            }],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', result.desc);
          this.loading.dismiss();
          this.showNotiList = "notshow";
        }
      },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
        this.showNotiList = "notshow";
      }
    );
  }

  ionViewDidLoad() {
    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
      } else {
        //this.checkNetwork();
        this.ipaddress = result;
        this.getNotiData();
      }
    });
  }

  // removeNoti(){
  //   let param = { userID: '', sessionID: '' };
  //   this.http.post(this.ipaddress + '/service006/removeNoti', param).map(res => res.json()).subscribe(result => {
  //     this.events.publish('user:created');
  //    console.log("noticount",JSON.stringify(result));

  //   },
  //     error => {

  //     });      
  // }


}
