import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChequestopsuccessPage } from '../chequestopsuccess/chequestopsuccess';
import { Login } from '../login/login';

declare var window: any;
declare var cordova: any;

@Component({
  selector: 'page-cheque-stop-otp',
  templateUrl: 'cheque-stop-otp.html',
})
export class ChequeStopOtpPage {
  textMyan: any = ["????????????????????????? ??????????", "??????????? ????", "??????????", "?????????????????? ????? ?????????????????", "?????????????? ?????????????????", "??????????????? ???????????"];
  textEng: any = ["Stop Cheque-Verify", "Confirmation Code", "CONFIRM", "Confirmation Code has been sent.", "Please Wait.", "Resend"];
  showFont: string[] = [];
  font: string;
  passTemp: any;
  userData: any;
  otpdata: any;
  otpcode: any = '';
  public loading;
  ipaddress: any;
  smsArived: any;
  passSKey : any;
  constructor(public toastCtrl: ToastController,public all: AllserviceProvider, public alertCtrl: AlertController, public http: Http, public loadingCtrl: LoadingController, private slimLoader: SlimLoadingBarService, public events: Events, public changeLanguage: ChangelanguageProvider, public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
   /*  this.slimLoader.start(() => {
    }); */
    this.passTemp = this.navParams.get('data');
    this.userData = this.navParams.get('detail');
    this.otpdata = this.navParams.get('otprKey');
    this.passSKey = this.navParams.get('sKey');
    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
      }
      else {
        this.ipaddress = result;
      }
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
     //this.slimLoader.complete();
    }) */
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  ionViewDidLoad() {
  }

  getconfirm() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, rKey: this.otpdata.rKey, otpCode: this.otpcode, sKey: this.passSKey};
    this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
      if (data.code == "0000") {
        this.http.post(this.ipaddress + '/service001/stopCheque', this.passTemp).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            this.userData.status = data.status;
            this.loading.dismiss();
            this.navCtrl.setRoot(ChequestopsuccessPage, { data: this.userData });
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.loading.dismiss();
          }
        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.loading.dismiss();
          });
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }

  resendOtpCode() {
    /* this.slimLoader.start(() => {
    }); */
    let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '8', merchantID: '', sKey: this.passSKey};
    this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
      this.otpdata = data;
      if (data.code == "0000") {
        this.otpdata.rKey = data.rKey;
       /*  window.SMS.startWatch(function () {
        }, function () {
        });
        setTimeout(() => {
        }, 8000);
        window.document.addEventListener('onSMSArrive', res => {
          this.smsArived = res;
          let arr = this.smsArived.data.body.split(' ');
          this.otpcode = arr[3];
         //this.slimLoader.complete();
        }); */
      }
      else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('lastTimeLoginSuccess', '');
                this.events.publish('login_success', false);
                this.navCtrl.setRoot(Login, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'confirmAlert'
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', data.desc);
       //this.slimLoader.complete();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
       //this.slimLoader.complete();
      });
  }
}
