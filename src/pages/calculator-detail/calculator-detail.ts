import { Component } from '@angular/core';
import { NavController, NavParams, Events, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { AllserviceProvider } from '../../providers/allservice/allservice';

@Component({
  selector: 'page-calculator-detail',
  templateUrl: 'calculator-detail.html',
})
export class CalculatorDetailPage {

  textMyan: any = ["Savings Interest","Fixed Deposit Account"];
  textEng: any = ["Savings Interest","Fixed Deposit Account"];
  showFont: string[] = [];
  billerid = "";
  ipaddress: any;
  errormsg: any;
  errormsg1: any;
  savingamount: number;
  saving: number;
  savingamt: any;
  fixamounts:number;
  fixamount:any;
  totalInterest:any=0.0 +' MMK';
  interest:number;
  fix: number;
  isActive=false;
  day: number;
  fixrate: number;
  fixrate1: any;
  cal: any=[];
  calculatorlist: any=[];
  customList: any=[];
  type: any;
  fixed:any;
  month:number;
  public loading;
  yearRate: any;
  savingrate: any;
  savingrate1: any;
  fixamt: any;
  indexData:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage, public loadingCtrl: LoadingController, public http: Http,public util: UtilProvider,
  public alertCtrl: AlertController, public toastCtrl: ToastController, public appCtrl: App, public global: GlobalProvider,public all: AllserviceProvider) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });   

  }

  ionViewWillEnter(){
    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
        this.ipaddress = this.global.ipaddress;
      } else {
        this.ipaddress = result;
      }

      this.getCalculator();
     
    })

    this.billerid = this.navParams.get("billerid");

    if( this.billerid == "Savings Interest Calculator")
    {
       this.type = "02";
    }
    else if( this.billerid == "Fixed Deposit Account Calculator")
    {
       this.type = "03";
    }
  }
  splitAmount(amount)
  {

    this.fixamounts=amount;
    //this.fixamount=this.util.formatAmount(amount);
    //this.fixamount=
  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  thousand_sperator(num) { 
    if (num!=""&&num!=undefined&&num!=null) {   

    num=num.replace(/,/g, "");
     
    }
  }

  CalculateSaving(){
    if(  this.savingamount == null || this.savingamount == undefined){
      this.errormsg = "Please fill amount."
      this.isActive=false;
    }else{
     var a;
     var b;
     var c;
     this.isActive=true;
     this.saving = this.savingamount * this.savingrate;
     this.saving = this.saving / 1200;
     this.saving = this.util.formatDecimal(this.saving);
     a=this.saving;
     b=this.savingamount;
     c=parseFloat(a) + parseFloat(b);
     this.savingamt=a+'MMK';
     //this.savingamt =c+'MMK';
     this.totalInterest=c+'MMK';
    //  console.log("saving amount==" , JSON.stringify(this.saving))
     this.errormsg="";
    }
  }

  ResetSaving(){
    this.isActive=false;
    this.savingamount = null;
    this.saving = null;
    this.errormsg = '';
    this.savingamt = null;
  }

  CalculateFix(){
  //this.month=parseInt(str.substring(0,str.indexOf("-")));
    if(  this.fixamounts == null || this.fixamounts == undefined){
      this.isActive=false;
      this.errormsg = "Please fill amount."
    }else if(  this.fixrate == null || this.fixrate == undefined){
      this.isActive=false;
      this.errormsg1 = "Please select rate."
    }else{
      this.isActive=true;
      var a;
      var b;
      var c;
      // var str=this.fixed;
      // console.log(this.fixed);
      // this.month=parseInt(str.substring(0,str.indexOf("-")));
      this.fix = this.fixamounts * this.fixrate * this.month;
      this.fix = this.fix / 1200;
      a=this.fix;
      b=this.fixamounts;
      c=parseFloat(a) + parseFloat(b);
      this.interest = c.toFixed(2);
      this.fix = this.util.formatDecimal(this.fix);
      this.fixamt = this.fix+' MMK';
      //this.totalInterest= this.interest +' MMK';
      this.totalInterest=this.util.formatAmount(this.interest)+" "+"MMK";
      
    }
  }

  ResetFix(){
    // this.savingamount=null;
    // this.savingamt=null;
    this.fixrate1=this.fixrate1.replace('%','');
    this.fixed=this.fixed.replace(this.fixrate1,'');
    this.isActive=false;
    this.fixamount = null;
    this.fixamounts = null;
    this.day = null;
    this.fix = null;
    //this.rate = null;
    this.errormsg = '';
    this.errormsg1 = '';
    this.fix = null;
    this.fixamt = null;
    //this.fixrate1 =''; 
  } 
  ionViewDidLoad() {
  
  }

  goCancel(){
    this.navCtrl.pop();
  }


  getCalculator() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let param = { userID: '', sessionID: '' , accType: this.type };
    this.http.post(this.ipaddress + '/service006/getCalculatorRate', param).map(res => res.json()).subscribe(result => {
      this.loading.dismiss();
      //console.log(result);
      if (result.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        this.calculatorlist = result.dataList;
        this.savingrate=this.calculatorlist[0].yearlyRate;
        this.savingrate1 = parseFloat(this.savingrate).toFixed(2)+'%'
        this.mySelect(this.calculatorlist);
        //this.savingrate = this.util.formatDecimal(this.savingrate)+'%';
        console.log("CAl>>>" , JSON.stringify(this.calculatorlist));
        this.loading.dismiss();

      }
      else if (result.code == "0016") {
        // console.log("No Data")
      }
      else {
        this.all.showAlert('Warning!', result.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });


  }

  changeType(fix){
  if(fix!='')
  {
  this.fixrate = fix;
  this.fixrate1 = this.fixrate +'%';
  }
  else{
  this.fixrate1='';
  }
 }
 changeMonth(mm)
 {
   this.indexData=mm;
   this.month=parseInt(this.indexData.substring(0,this.indexData.indexOf("-")));
   console.log(this.month);
 }

  mySelect(calculatorlist)
  {
    //console.log(calculatorlist);
    var j=1;
    for(var i=0;i<=calculatorlist.length;i++)
    {
      if(i!=calculatorlist.length-1)
    {
      if(calculatorlist[i].yearlyRate!=calculatorlist[j].yearlyRate)
      {
      this.customList.push(this.calculatorlist[i]);
      j++;
      }
      else if(calculatorlist[i].yearlyRate==calculatorlist[j].yearlyRate)
      {
        j++;
       }
     
      }
      else 
      {
       if(calculatorlist[i-1].yearlyRate!=calculatorlist[i].yearlyRate)
         {
          this.customList.push(this.calculatorlist[i]);
          }
          else{
            this.customList.push(this.calculatorlist[i]);
          }
       }
    }
  }

}
