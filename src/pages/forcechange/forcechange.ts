import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Idle } from '@ng-idle/core';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';

declare var window;

@Component({
  selector: 'page-forcechange',
  templateUrl: 'forcechange.html',
  providers : [ChangelanguageProvider]
})
export class ForcechangePage {
  result :any;
  userdata :any;
  errormessagetext:string;
  public loading;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  ipaddress:string;
  font:string;
  textEng: any=['Change Password','Old Password','New Password','Confirm Password','Change','Enter old password','Enter new password','Enter confirm password','Old password and New password should not match','Password do not match','You need to log in again.','OK'];
  textMyan : any = ['လျှို့ဝှက်နံပါတ်ပြောင်းမည်','လက်ရှိနံပါတ်','နံပါတ်အသစ်','အတည်ပြုနံပါတ်','ပြောင်းလဲမည်','လက်ရှိနံပါတ်ရိုက်သွင်းပါ','နံပါတ်အသစ်ရိုက်သွင်းပါ','အတည်ပြုနံပါတ်ရိုက်သွင်းပါ','လက်ရှိနံပါတ်နှင့်ပြောင်းလဲမည့်နံပါတ်အသစ်မတူညီရပါ','နံပါတ်အသစ်နှင့်အတည်ပြုနံပါတ်မတူညီပါ','အတွင်းသို့ပြန်လည်ဝင်ရောက်ရပါမည်','သဘောတူပါသည်'];
  showFont:any=[];
  pdata = { 'userid': '', 'sessionid': '', "pswminlength":"",  "pswmaxlength":"",  "spchar":"",
  "upchar":"",  "lowerchar":"",  "pswno":"",  "msgCode" : "",  "msgDesc" : "" };
  constructor(public navCtrl: NavController,public all: AllserviceProvider, public navParams: NavParams, public events: Events, public http: Http,public toastCtrl:ToastController,public loadingCtrl:LoadingController, public alertCtrl:AlertController,public storage:Storage, public idle:Idle,public changeLanguage:ChangelanguageProvider){
    this.result = {};
    this.userdata = this.navParams.get('data');
    
    this.result.userid = this.userdata.userID;
    this.result.sessionid = this.userdata.sessionID;
    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });
    });
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  inputChange(data){
    this.errormessagetext="";
  }
  changePassword(){
    if(this.result.currentpsw=='' || this.result.currentpsw==undefined || this.result.currentpsw==null){
      this.errormessagetext=this.showFont[5];
    }else  if(this.result.newpsw=='' || this.result.newpsw==undefined || this.result.newpsw==null){
      this.errormessagetext=this.showFont[6];
    }else if(this.result.confirmpsw=='' || this.result.confirmpsw==undefined || this.result.confirmpsw==null){
      this.errormessagetext=this.showFont[7];
    }
    else if(this.result.currentpsw==this.result.newpsw){
      this.errormessagetext=this.showFont[8];
    }
    else{
      this.errormessagetext='';
      if(this.result.newpsw!=this.result.confirmpsw){
        this.errormessagetext=this.showFont[9];
      }else{
        this.loading = this.loadingCtrl.create({
          content: "Please wait...",
          dismissOnPageChange :true
          //   duration: 3000
        });
        this.loading.present();
        this.errormessagetext="";
        let iv = this.all.getIvs();
        let dm = this.all.getIvs();
        let salt = this.all.getIvs(); 
        let param = {
          userID : this.result.userid,
          oldPassword : this.all.getEncryptText(iv, salt, dm, this.result.currentpsw),
          newPassword : this.all.getEncryptText(iv, salt, dm, this.result.newpsw),
         // confirmnewpassword : this.result.confirmpsw,
          sessionID : this.result.sessionid,
          "iv": iv, "dm": dm, "salt": salt
        };
        this.http.post(this.ipaddress+'/service001/forceChangePassword',param).map(res => res.json()).subscribe(response => {
              if (response.code == "0000") {
                let confirm = this.alertCtrl.create({
                  title: '',
                  cssClass:this.font,
                  message: this.showFont[10],
                  buttons: [
                    {
                      text:this.showFont[11],
                      handler: () => {
                        
                        this.events.publish('login_success', false);
                        this.events.publish('lastTimeLoginSuccess','');
                        this.storage.remove('userData');
                        this.navCtrl.setRoot(Login, {
                        });
                        this.navCtrl.popToRoot();
                      }
                    }
                  ]
                })
                confirm.present();
                this.loading.dismiss();

             }
              else if(response.code == "0016"){
                let confirm = this.alertCtrl.create({
                  title: 'Warning!',
                  message: response.desc,
                  buttons: [
                    {
                      text: 'OK',
                      handler: () => {
                        
                        this.loading.dismiss();
                        this.events.publish('login_success', false);
                        this.events.publish('lastTimeLoginSuccess','');
                        this.storage.remove('userData');
                        this.navCtrl.setRoot(Login, {
                        });
                        this.navCtrl.popToRoot();
                      }
                    }
                  ],
                  cssClass:'warningAlert'
                })
                confirm.present();
              }
              else {

             let toast = this.toastCtrl.create({
             message:response.desc,
               duration: 3000,
               position: 'bottom',
               //  showCloseButton: true,
               dismissOnPageChange: true,
               // closeButtonText: 'OK'
             });
             toast.present(toast);
             this.loading.dismiss();
             }
          },
            error => {
              let toast = this.toastCtrl.create({
                message:this.all.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.loading.dismiss();
          });

        //work process
      }
    }

  }
  callIT(passedNumber){
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:"+passedNumber;
  }

  readPasswordPolicy(){
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "pswminlength":"",
      "pswmaxlength":"",
      "spchar":"",
      "upchar":"",
      "lowerchar":"",
      "pswno":"",
      "msgCode" : "",
      "msgDesc" : ""
    };
    
    this.http.post(this.ipaddress + '/service001/readPswPolicy', param).map(res => res.json()).subscribe(response => {
      
      if (response.msgCode == "0000") {
        this.pdata=response;
        
      }
      else if (response.msgCode == "0016") {
        
      }
      else {
       
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss(); 
      });
  }

  ionViewDidLoad() {
    
    this.storage.get('ipaddress').then((ipaddress) => {
      
      this.ipaddress = ipaddress;
      this.readPasswordPolicy();
    });
  }
}
