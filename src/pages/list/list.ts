import { Component } from '@angular/core';
import { NavController,  MenuController,NavParams } from 'ionic-angular';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  userdata:any;
  useraccount:any;
  currentaccount:any=[];
  savingaccount:any=[];
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min:any;
  sec;any;
  shwe :string ;
  constructor(public navCtrl: NavController,public menu: MenuController, public idle:Idle,public navParams: NavParams,public storage:Storage) {
    this.menu.swipeEnable(false);
    this.shwe = 'accounts';
  }
  idleWatch(){
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5*60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.navCtrl.pop();  //go to logout page after 5 min idle.
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if(currentpage.component.name=='ListPage'){
        var data=countdown/60;
        this.min=data.toString().split('.')[0];
        this.sec=     parseFloat(0+'.'+data.toString().split('.')[1])*60;
        this.sec=  (Math.round(this.sec * 100) / 100);
         // console.log("ListPage countdown>>"+countdown);

        this.idleState = 'You\'ll logout in ' + this.min+' min ' +this.sec+'  seconds!';
      }
    });
    this.reload();
  }
  ionViewDidLoad() {
    //  // console.log(JSON.stringify(this.navParams.get('accounts')));
    this.storage.get('userData').then((val) => {
      // console.log("outpput="+JSON.stringify(val));
      this.userdata=val.debitAcc;
      // console.log("outpput="+JSON.stringify(this.userdata));
      this.useraccount=val.debitAcc;
      // this.useraccount = this.global.userAccount;
      //this.useraccount = this.navParams.get('accounts');
      if (this.useraccount != undefined) {

        for (var i = 0; i < this.useraccount.length; i++) {
          if (this.useraccount[i].acctype == 'U') {
            this.currentaccount.push(this.useraccount[i]);
          } else {
            this.savingaccount.push(this.useraccount[i]);
          }
        }
      }
      else{
        this.useraccount = [];
      }
      // this.idleWatch();
    });
  }
  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

}
