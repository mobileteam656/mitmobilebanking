import 'rxjs/add/operator/map';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import {
  AlertController,
  Events,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController,
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';
import { MerchantErrorPage } from '../merchant-error-page/merchant-error-page';
import { MerchantlistPage } from '../merchantlist/merchantlist';
import { Merchantsuccess } from '../merchantsuccess/merchantsuccess';

declare var window: any;
declare var cordova: any;

/**
 * Generated class for the ConfirmotpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirmotp',
  templateUrl: 'confirmotp.html',
  providers: [ChangelanguageProvider]
})
export class ConfirmotpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  showFont: string[] = [];
  font: string;
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData: any;
  passtemp1: any;
  passtemp2: any;
  passtemp3: any;
  passSKey : any;
  public loading;
  flag: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public storage: Storage,public all: AllserviceProvider, public alertCtrl: AlertController, public events: Events, public network: Network, public idle: Idle, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider) {
    // platform.registerBackButtonAction(() => this.myHandlerFunction());
    /* this.loading = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange :true
       //   duration: 3000
     });
     this.loading.present()*/
  /*   this.slimLoader.start(() => {
    }); */
    this.passtemp1 = this.navParams.get('data');
    this.passtemp2 = this.navParams.get('detail');
    this.passtemp3 = this.navParams.get('detailmerchant');
    this.passSKey = this.navParams.get('sKey');
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    /* window.SMS.startWatch(function () {
    }, function () {
    });
    setTimeout(() => {
    }, 8000);
    window.document.addEventListener('onSMSArrive', res => {
      this.smsArived = res;
      let arr = this.smsArived.data.body.split(' ');
      this.otpcode = arr[3];
     //this.slimLoader.complete();
    }) */
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  idleWatch() {
    this.idle.setIdle(5);  //after 5 sec idle
    this.idle.setTimeout(5 * 60);  //5min countdown
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ConfirmotpPage')
        this.gologout();
    });
    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      let currentpage = this.navCtrl.getActive();
      if (currentpage.component.name == 'ConfirmotpPage') {
        var data = countdown / 60;
        this.min = data.toString().split('.')[0];
        this.sec = parseFloat(0 + '.' + data.toString().split('.')[1]) * 60;
        this.sec = (Math.round(this.sec * 100) / 100);
        // 
        this.idleState = 'You\'ll logout in ' + this.min + ' min ' + this.sec + '  seconds!';
      }
    });
    this.reload();
  }

  reload() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  ionViewDidLoad() {
    this.checkNetwork();
    //  // this.idleWatch();
    /*  this.network.onDisconnect().subscribe(data => {
        this.flag='none';
        let toast = this.toastCtrl.create({
          message:"Check your internet connection!",
          // duration: 2000,
          // position: 'bottom'
          showCloseButton: true,
          closeButtonText: 'OK'
        });
        toast.present(toast);
        
  
      }, error => console.error(error));*/
  }

  ionViewDidLeave() {
    this.slimLoader.reset();
  }

  getconfirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      /*  this.slimLoader.start(() => {
        });*/
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, rKey: this.passtemp1.rKey, otpCode: this.otpcode, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          let parameter;
          if (this.passtemp3.processingCode == "080400") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp2.merchantid,
              processingCode: this.passtemp2.processingCode,
              amount: this.passtemp2.amount,
              refNumber: this.passtemp2.cardno,
              field1: this.passtemp2.did,
              field2: this.passtemp2.pid,
              field3: this.passtemp2.peroid,
              field4: this.passtemp2.tid,
              field5: this.passtemp2.accessToken,
              field6: this.passtemp2.packagetype,
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.amountServiceCharges,
              amountTax: this.passtemp2.amountTax,
              amountTotal: this.passtemp2.amountTotal,
              paymentType: '1',
              narrative: '',
              sKey: this.passSKey
            };
          }
          else if (this.passtemp3.processingCode == "050200") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp3.merchantID,
              processingCode: this.passtemp3.processingCode,
              amount: this.passtemp2.amount,
              refNumber: this.passtemp2.refNo,
              field1: '',
              field2: this.passtemp2.billTypeValue,
              field3: '',
              field4: '',
              field5: '',
              field6: '',
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.bankCharges,
              amountTax: this.passtemp2.cnpCharge,
              amountTotal: this.passtemp2.amountTotal,
              paymentType: '1',
              narrative: this.passtemp2.narrative,
              sKey: this.passSKey
            };
            
          }
          else if (this.passtemp3.processingCode == "090500") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp3.merchantID,
              processingCode: this.passtemp3.processingCode,
              amount: this.passtemp2.amount,
              refNumber: this.passtemp2.outstandBill,
              field1: '',
              field2: this.passtemp2.meterNo,
              field3: '',
              field4: '',
              field5: '',
              field6: this.passtemp2.referenceType,
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.bankCharges,
              amountTax: this.passtemp2.penalty,
              amountTotal: this.passtemp2.totalAmount,
              paymentType: '1',
              narrative: this.passtemp2.narrative,
              sKey: this.passSKey
            };
          }
          else if (this.passtemp3.processingCode == "040300") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp3.merchantID,
              processingCode: this.passtemp3.processingCode,
              amount: this.passtemp2.amount,
              refNumber: this.passtemp2.phone,
              field1: '',
              field2: '',
              field3: '',
              field4: this.passtemp2.narrative,
              field5: '',
              field6: this.passtemp2.referenceType,
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.bankCharges,
              amountTax: '',
              amountTotal: this.passtemp2.amountTotal,
              sKey: this.passSKey
            };
          }
          else if (this.passtemp3.processingCode == "000100") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp3.merchantID,
              processingCode: this.passtemp3.processingCode,
              amount: this.passtemp2.amount,
              refNumber: this.passtemp2.invoiceNo,
              field1: '',
              field2: '',
              field3: '',
              field4: this.passtemp2.narrative,
              field5: '',
              field6: this.passtemp2.customerNo,
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.bankCharges,
              amountTax: '',
              amountTotal: this.passtemp2.amountTotal,
              sKey: this.passSKey
            };
          }
          else if (this.passtemp3.processingCode == "030001") {
            parameter = {
              userID: this.userData.userID,
              sessionID: this.userData.sessionID,
              bankAcc: this.passtemp2.accounttype,
              merchantID: this.passtemp3.merchantID,
              processingCode: this.passtemp3.processingCode,
              amount: this.passtemp2.amount,
              refNumber: '',
              field1: '',
              field2: this.passtemp2.quantity,
              field3: this.passtemp2.price,
              field4: this.passtemp2.narrative,
              field5: '',
              field6: this.passtemp2.referenceType,
              field7: '',
              field8: '',
              field9: '',
              field10: '',
              amountServiceCharges: this.passtemp2.bankCharges,
              amountTax: '',
              amountTotal: this.passtemp2.amountTotal,
              sKey: this.passSKey
            };
          }
          this.http.post(this.ipaddress + '/service003/goPayment', parameter).map(res => res.json()).subscribe(res => {
            if (res.code == "0000") {
             //this.slimLoader.complete();
              this.navCtrl.setRoot(Merchantsuccess, {
                data: res,
                detail: this.passtemp2,
                detailmerchant: this.passtemp3
              })
            }
            else if (res.code == "0016") {
              let confirm = this.alertCtrl.create({
                title: 'Warning!',
                enableBackdropDismiss: false,
                message: res.desc,
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      
                      this.storage.remove('userData');
                      this.events.publish('login_success', false);
                      this.events.publish('lastTimeLoginSuccess', '');
                      this.navCtrl.setRoot(Login, {
                      });
                      this.navCtrl.popToRoot();
                    }
                  }
                ],
                cssClass: 'warningAlert',
              })
              confirm.present();
              ////this.slimLoader.complete();
              this.loading.dismiss();
            }
            else {
             //this.slimLoader.complete();
              this.navCtrl.setRoot(MerchantErrorPage, {
                data: res,
                detail: this.passtemp2,
                detailmerchant: this.passtemp3
              });
              /*  let toast = this.toastCtrl.create({
                  message:res.desc,
                //  duration: 3000,
                //  position: 'bottom',
                    showCloseButton: true,
                  dismissOnPageChange: true,
                   closeButtonText: 'OK'
                });
                toast.present(toast);
                this.loading.dismiss();*/
            }
          },
            error => {
              let toast = this.toastCtrl.create({
                message: this.all.getErrorMessage(error),
                duration: 5000,
                position: 'bottom',
                dismissOnPageChange: true,
              });
              toast.present(toast);
              this.loading.dismiss();
            });
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  
                  this.storage.remove('userData');
                  this.events.publish('login_success', false);
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
          //  //this.slimLoader.complete();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
          ////this.slimLoader.complete();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  doYourStuff() {
    this.navCtrl.setRoot(MerchantlistPage);
  }

  sendOtpCode() {
    this.checkNetwork();
    if (this.flag == "success") {
      /* this.loading = this.loadingCtrl.create({
         content: "Please wait...",
         dismissOnPageChange :true
         //   duration: 3000
       });
       this.loading.present();*/
     /*  this.slimLoader.start(() => {
      }); */
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: '1', merchantID: this.passtemp3.merchantID, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.passtemp1.rKey = data.rKey;
          // this.loading.present();
          /* window.SMS.startWatch(function () {
          }, function () {
          });
          setTimeout(() => {
          }, 8000);
          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;
            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];
           //this.slimLoader.complete();
          }); */
        }
        else if (data.code == "0016") {
          let confirm = this.alertCtrl.create({
            title: 'Warning!',
            enableBackdropDismiss: false,
            message: data.desc,
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.storage.remove('userData');
                  this.events.publish('lastTimeLoginSuccess', '');
                  this.events.publish('login_success', false);
                  this.navCtrl.setRoot(Login, {
                  });
                  this.navCtrl.popToRoot();
                }
              }
            ],
            cssClass: 'warningAlert',
          })
          confirm.present();
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
         //this.slimLoader.complete();
        }

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
         //this.slimLoader.complete();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
    // // this.idleWatch();
    /*  this.network.onDisconnect().subscribe(data => {
       this.flag='none';
       let toast = this.toastCtrl.create({
         message:"Check your internet connection!",
         duration: 3000,
         position: 'bottom',
         //  showCloseButton: true,
         dismissOnPageChange: true,
         // closeButtonText: 'OK'
       });
       toast.present(toast);
     }, error => console.error(error)); */

  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.events.publish('login_success', false);
          this.events.publish('lastTimeLoginSuccess', '');
          this.storage.remove('userData');
          this.navCtrl.setRoot(Login, {
          });
          this.navCtrl.popToRoot();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }
}
