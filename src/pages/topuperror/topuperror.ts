import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { TopUp } from '../top-up/top-up';

@Component({
  selector: 'page-topuperror',
  templateUrl: 'topuperror.html',
    providers : [ChangelanguageProvider]
})
export class TopuperrorPage { 
textMyan : any = ['လုပ်ဆောင်မှုအတည်ပြုခြင်း',"အကောင့်နံပါတ်","Operator အမျိုးအစား","ဖုန်းနံပါတ်","ငွေပမာဏ",'ပိတ်မည်'];
  textEng: any = ["Top Up - Fail",  "Account Number", "Operator Type", "Phone Number", "Amount","Close"];
  showFont : string [] = [];
  font:string;
  passtemp : any;
  passtemp1  :any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage, public changeLanguage:ChangelanguageProvider) {
     this.passtemp = this.navParams.get('data');
    this.passtemp1 = this.navParams.get('detail');
    
    

    this.storage.get('language').then((font) => {
      
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        
        this.showFont = data;
        
      });

    });
  }

 /* changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      //  this.storage.set('language', "uni");

      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.textMyan[j];
      }
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j] = this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j] = this.textEng[j];
      }
    }
  }*/

  ionViewDidLoad() {
    
  }

  getOK(){
   /*  if(this.passtemp3 == "1"){
      this.navCtrl.setRoot(QrDemo)
    }
    else{ */
      this.navCtrl.setRoot(TopUp)
   /*  } */

  }

}
