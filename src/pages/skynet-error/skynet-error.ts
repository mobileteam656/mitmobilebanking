import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { SkynetTopupPage } from '../skynet-topup/skynet-topup';

@Component({
  selector: 'page-skynet-error',
  templateUrl: 'skynet-error.html',
  providers: [ChangelanguageProvider]
})
export class SkynetErrorPage {

  textMyan: any = [
    "အသေးစိတ်အချက်အလက်", "အကောင့်နံပါတ် မှ",
    "ကဒ်နံပါတ်​", "ပက်​ကေ့နာမည်",
    "​ဘောက်ချာ အမျိုးအစား", "ငွေပမာဏ",
    "ဝန်​ဆောင်ခ", "စုစုပေါင်း ငွေပမာဏ",
    'ပိတ်မည်', "​ဇာတ်ကားနာမည်"
  ];
  textEng: any = [
    "Details", "From Account No.",
    "Card No.", "Package Name",
    "Voucher Type", "Amount",
    "Service Charges", "Total Amount",
    "CLOSE", "Movie Name"
  ];
  showFont: string[] = [];
  font: string;

  merchantErrorObj: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public changeLanguage: ChangelanguageProvider
  ) {
    this.merchantErrorObj = this.navParams.get('data');

    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  closeSkynetError() {
    let paramMerchantSuccessObj = {
      merchantID: this.merchantErrorObj.merchantID,
      processingCode: this.merchantErrorObj.merchantCode
    };

    this.navCtrl.setRoot(SkynetTopupPage, {
      data: paramMerchantSuccessObj
    });
  }

}
