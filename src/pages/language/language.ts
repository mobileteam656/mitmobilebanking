import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';

/**
 * Generated class for the Language page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})
export class Language {
  language: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage, public events: Events) {
    this.storage.get('language').then((font) => {
      // console.log('Your language is', font);
      this.language = font;
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad Language');
  }

  mcqAnswer(s){
    this.storage.set('language',s);
    this.events.publish('changelanguage', s);
  }

}
