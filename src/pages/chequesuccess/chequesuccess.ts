import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChequePage } from '../cheque/cheque';
import { ChequelistPage } from '../chequelist/chequelist';

/**
 * Generated class for the ChequesuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-chequesuccess',
  templateUrl: 'chequesuccess.html',
  providers : [ChangelanguageProvider]
})
export class ChequesuccessPage {
	font:string;
	showFont:any=[];
	carryData: any;
   textEng: any=['Account Number','Cheque Number','Status','Details','Close','Cheque Status Information'];
    textMyan : any = ['စာရင်းနံပါတ်','ချက်နံပါတ်','ရလဒ်','အသေးစိတ်အချက်အလက်များ','ပိတ်မည်','ချက်နှင့်သက်ဆိုင်သောအချက်အလက်'];
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public changeLanguage:ChangelanguageProvider) {
	   this.carryData = this.navParams.get('data');
	   this.storage.get('language').then((font) => {
      
         this.font = font;
         this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
         {
           
           this.showFont = data;
           
         });

    });
  }
    /*changelanguage(lan) {

    if (lan == 'uni') {
      this.font = "uni";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.textMyan[j];
      }
    //  this.showFont = this.textMyan;
    }
    else if (lan == 'zg') {
      this.font = "zg";
      for (let j = 0; j <  this.textMyan.length;j++) {
        this.showFont[j]= this.changefont.UnitoZg(this.textMyan[j]);
      }

    }
    else{
      this.font='';
      for (let j = 0; j <  this.textEng.length;j++) {
        this.showFont[j]= this.textEng[j];
      }
    }
    
  }*/
  goBack(){
	  this.navCtrl.setRoot(ChequelistPage);
  }
  ionViewDidLoad() {
    
  }

}
