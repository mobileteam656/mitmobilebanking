import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GlobalProvider {
  public phone: any;
  public versionEng: any;
  public versionMyan: any;
  version: any;
  appCode: any;
  ipaddress: any;
  imglink: any;
  appName: any;
  flagnoti: boolean;
  constructor(public http: Http) {
    // console.log('Hello GlobalProvider Provider');
    this.phone = "067422573";
    this.version = "1.0.24";
    this.appCode = "MA";
  
    //this.ipaddress = "http://122.248.120.16:8080/AppService/module001"; // 656 
    //this.imglink   = "http://122.248.120.16:8080/DigitalMedia/"; // 656 
    this.ipaddress = "http://192.168.201.210:8080/AppService/module001"; // local 
    //this.ipaddress = "http://192.168.43.117:8082/AppService/module001"; // local
    //this.ipaddress = "http://192.168.215.59:8082/AppService/module001";
    //this.ipaddress = "http://ibanking.mitcloud.com/AppService/module001";

    //ipadress cloud  https://52.187.13.89:8080/ http://52.187.13.89:443/https://mbanking.mitcloud.com/
    //this.ipaddress = "https://mbanking.mitcloud.com/AppService/module001";
    //this.imglink   = "https://mbanking.mitcloud.com/DigitalMedia/";

    // <access origin="*" />
    //<access origin="http://ibanking.mitcloud.com/AppService/" />
    //<allow-intent href="http://ibanking.mitcloud.com/AppService/" />
    // <access origin="http://192.168.201.53:8082/AppService/" />
    // <allow-intent href="http://*/*" />
    // <allow-intent href="https://*/*" />
    // <allow-intent href="http://192.168.201.53:8082/AppService/" />

    this.appName = "mBanking";
    // firebase.google.com

    // username-mobileuser1995

    // password-mbanking123
    // 012307901  shwe
    // 067422573  nsb
    // 019000879   tcb
  }

  getphone() {
    return Promise.resolve(this.phone);
  }
}
