import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
/*
  Generated class for the UtilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
declare var require: any
let num = /^[0-9-\+]*$/;
let flag: boolean;
let phoneData = { phone: "", flag: false }
let normalizePhone;
let mnum = /^[၀-၉-\+]*$/;
let institutionData = { fromInstitutionName: "", toInstitutionName: "", fromInstitutionCode: "", toInstitutionCode: "" }
@Injectable()
export class UtilProvider {

  constructor(public http: Http, public global: GlobalProvider) {
    // console.log('Hello UtilProvider Provider');
  }

  normalizePhone(phone) {
    //if (phone != "" && phone != null && phone != undefined) {
    if (num.test(phone)) {
      if (phone.indexOf("+") == 0 && (phone.length == "12" || phone.length == "13" || phone.length == "11")) {
        flag = true;
        normalizePhone = phone;
      }
      else if (phone.indexOf("7") == 0 && phone.length == "9") {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("9") == 0 && phone.length == "9") {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("09") == 0 && (phone.length == 10 || phone.length == 11 || phone.length == 9)) {
        normalizePhone = '+959' + phone.substring(2);
        flag = true;
      }
      else if (phone.indexOf("7") != 0 && phone.indexOf("9") != 0 && (phone.length == "8" || phone.length == "9" || phone.length == "7")) {
        normalizePhone = '+959' + phone;
        flag = true;
      }
      else if (phone.indexOf("959") == 0 && (phone.length == 11 || phone.length == 12 || phone.length == 10)) {
        normalizePhone = '+959' + phone.substring(3);
        flag = true;
      }
      else {
        normalizePhone = '';
        flag = false;
      }
    } else {
      normalizePhone = '';
      flag = false;
    }
    phoneData.flag = flag;
    phoneData.phone = normalizePhone;
    // console.log("normalizePhone-Provider-phoneData>>" + JSON.stringify(phoneData));
    return phoneData;

  }

  formatDecimal(amount){
    return amount.toFixed(2).replace(/,/g, "");
  }

  formatToDouble(amount) {
    //return amount.replace(/[,]/g, '');
    return amount.replace(/,/g, "");
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  removePlus(n) {
    return n.replace(/\+/g, '');
  }

  checkTwoStringAreEqual(firstString, secondString) {
    if (firstString.trim().equal(secondString.trim())) {
      return true;
    } else {
      return false;
    }
  }

  checkInputIsEmpty(dataFromUser) {
    if (dataFromUser == undefined || dataFromUser == null || dataFromUser.trim().length == 0) {
      return true;
    } else {
      return false;
    }
  }

  checkAmountIsZero(amountFromUser) {
    let amountFloat = parseFloat(amountFromUser);

    if (amountFloat == 0) {
      return true;
    } else {
      return false;
    }
  }

  checkAmountIsLowerThanZero(amountFromUser) {
    let amountFloat = parseFloat(amountFromUser);

    if (amountFloat < 0) {
      return true;
    } else {
      return false;
    }
  }

  checkNumberOrLetter(amountFromUser) {
    if (isNaN(amountFromUser)) {
      return true;
    } else {
      return false;
    }
  }

  /* gettting today date and time without any special character.
  return example is like that -> "1452018143321"
   */
  getImageName() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + ""
      + (currentdate.getMonth() + 1) + ""
      + currentdate.getFullYear() + ""
      + currentdate.getHours() + ""
      + currentdate.getMinutes() + ""
      + currentdate.getSeconds();
    return datetime;
  }

  /* gettting today date and time.
  return example is like that -> "14/5/2018 @ 14:32:40" */
  getTodayDateAndTime() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getFullYear() + " @ "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds();
    return datetime;
  }

  getTodayDate() {
   // var currentdate = new Date();
    var d = new Date();
    var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    return datestring;
  }

  removeComma(param) {
    var amount;
    param = param.replace(/,/g, ''); // Remove ',' 
    amount = param * 1;  
    return amount;   
  }



}
